module.exports = {
  roots: ["<rootDir>/src"],
  modulePaths: ["<rootDir>/src"],
  transform: { "^.+\\.tsx?$": "ts-jest" },
  transformIgnorePatterns: [
    "/node_modules/(?!intl-messageformat|@formatjs/icu-messageformat-parser|siventh-translations|axios).+\\.js$",
  ],
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
  moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json"],
  testPathIgnorePatterns: ["node_modules", "test-config"],
  setupFilesAfterEnv: ["<rootDir>/src/setupTests.ts"],
  moduleNameMapper: {
    ".+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2|svg)$":
      "<rootDir>/src/__mocks__/fileMock.ts",
    "<rootDir>/src/gateway": "<rootDir>/src/__mocks__/firebase.ts",
  },
  preset: "ts-jest/presets/js-with-ts",
  testEnvironment: "jsdom",
};
