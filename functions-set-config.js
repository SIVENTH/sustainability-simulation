/* eslint-disable @typescript-eslint/no-var-requires */
//https://github.com/firebase/firebase-tools/issues/406
const fs = require("fs");
const env = process.argv[2];

const configPath = `./functionvars.${env}.json`;

if (!(configPath && fs.existsSync(configPath))) {
  console.log("JSON config file does not exist");
  return;
}

// eslint-disable-next-line sonarjs/cognitive-complexity
const collectConfigLines = (o, propPath, configLines) => {
  propPath = propPath || "";
  configLines = configLines || [];
  for (const key of Object.keys(o)) {
    const newPropPath = propPath + key;
    if (typeof o[key] === "object") {
      collectConfigLines(o[key], newPropPath + ".", configLines);
    } else if (o[key] != null && o[key] !== "") {
      configLines.push(`${newPropPath}=${JSON.stringify(o[key])}`);
    }
  }
};

const config = require(configPath);
const configLines = [];
collectConfigLines(config, "", configLines);

const cp = require("child_process");
cp.execSync(`firebase -P ${env} functions:config:set ${configLines.join(" ")}`);
console.log("Variables have been set.");
