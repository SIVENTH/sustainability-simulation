import { AuthProvider, useAuth } from "../AuthContext";
import { render, screen } from "@testing-library/react";
import { firebaseUser } from "gateway";

jest.mock("gateway", () => ({
  auth: {
    getAuthListener: (userFn: (user: firebaseUser) => void) => {
      userFn({
        displayName: "testUser",
        getIdTokenResult: () =>
          new Promise((resolve) => {
            resolve({
              expirationTime: new Date().getTime() + 100000,
              token: "testToken",
            });
          }),
        getIdToken: () =>
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          new Promise((resolve) => {
            resolve("testToken");
          }),
      } as unknown as firebaseUser);
      return jest.fn;
    },
  },
}));

describe("AuthContext", () => {
  it("returns authedUser", async () => {
    const TestComponent = () => {
      const { isLoading, authedUser } = useAuth();
      return (
        <>
          {isLoading ? (
            <p>loading </p>
          ) : (
            <> Logged in as {authedUser?.displayName}</>
          )}
        </>
      );
    };
    render(
      <AuthProvider>
        <TestComponent />
      </AuthProvider>
    );
    expect(screen.getByText("Logged in as testUser")).toBeInTheDocument();
  });
});
