/* eslint-disable @typescript-eslint/no-explicit-any */
import { createContext, useContext, useMemo, useEffect } from "react";
import { useAuthedUser } from "hooks";
import { firebaseUser } from "gateway";

interface ProviderState {
  authedUser: firebaseUser | null;
  isLoading: boolean;
  token: string | null;
  organizationId: string | null;
  memberId: string | null;
  organization: any | null;
  teamList: any | null;
  setAuthedUser: (user: firebaseUser | null) => void;
  setOrgId: (orgId: string) => void;
  setMyOrgData: (myOrgData: object) => void;
}

const AuthContext = createContext<ProviderState | null>(null);

interface AuthProviderProps {
  children: React.ReactNode;
}

export const AuthProvider = ({ children }: AuthProviderProps): JSX.Element => {
  const {
    isLoading,
    authedUser,
    token,
    orgId,
    memberId,
    myOrgData,
    teamList,
    setMyOrgData,
    setOrgId,
    setAuthedUser,
  } = useAuthedUser();

  const memoizedState = useMemo(
    () => ({
      isLoading,
      authedUser,
      token,
      organizationId: orgId,
      memberId: memberId,
      organization: myOrgData,
      teamList: teamList,
      setMyOrgData,
      setOrgId,
      setAuthedUser,
    }),

    [
      isLoading,
      token,
      authedUser,
      orgId,
      memberId,
      myOrgData,
      teamList,
      setMyOrgData,
      setOrgId,
      setAuthedUser,
    ]
  );

  const updateLocalStorage = () => {
    if (authedUser) {
      localStorage.setItem("user", JSON.stringify(memoizedState));
      localStorage.setItem("orgId", JSON.stringify(orgId));
      localStorage.setItem("myOrgData", JSON.stringify(myOrgData));
    }
  };

  // useEffect(() => {
  //   const updateLocalStorage = () => {
  //     if (authedUser) {
  //       localStorage.setItem("user", JSON.stringify(memoizedState));
  //       localStorage.setItem("orgId", JSON.stringify(orgId));
  //       localStorage.setItem("myOrgData", JSON.stringify(myOrgData));
  //       // localStorage.setItem("teamList", JSON.stringify(teamList));
  //     }
  //   };

  useEffect(() => {
    updateLocalStorage();
  }, [authedUser, orgId, myOrgData, memoizedState]);

  return (
    <AuthContext.Provider value={memoizedState}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = (): ProviderState => {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error("useAuth must be used within AuthProvider");
  }
  return context;
};
