/* eslint-disable @typescript-eslint/no-explicit-any */
import { createContext, useContext, useEffect, useMemo, useState } from "react";
import { useAuthedUser } from "hooks";
import { firebaseUser, organizationResponseType } from "gateway";
import { getOrganization } from "gateway/services";
// import { getOrganization, getDepartment, getTeam, getUser } from "gateway/services";

// create Type for User Context
interface UserData extends organizationResponseType {
  firebaseUser: firebaseUser;
  displayName: string | null;
  isLoading: boolean;
  organization: any;
  setOrganization: (data: any) => void;
}

interface UserProviderProps {
  children: React.ReactNode;
}

// set default value of user context to null while typing.
const UserContext = createContext<UserData | null>(null);

const getOrg = async (): Promise<any> => {
  try {
    const response: any = await getOrganization(
      window.localStorage.getItem("orgId")
    );

    return response?.data;
  } catch (error) {
    return error;
  }
};

// will need to add these later
// const getDep = async (): Promise<any> => {
//   try {
//     const response: any = await getOrganization(
//       window.localStorage.getItem("orgId")
//     );

//     return response?.data;
//   } catch (error) {
//     return error;
//   }
// };

// const getTeam = async (): Promise<any> => {
//   try {
//     const response: any = await getOrganization(
//       window.localStorage.getItem("orgId")
//     );

//     return response?.data;
//   } catch (error) {
//     return error;
//   }
// };

// const getUser = async (): Promise<any> => {
//   try {
//     const response: any = await getOrganization(
//       window.localStorage.getItem("orgId")
//     );

//     return response?.data;
//   } catch (error) {
//     return error;
//   }
// };

export const UserProvider = ({ children }: UserProviderProps): JSX.Element => {
  const { isLoading, authedUser } = useAuthedUser();
  const [orgData, setOrgData] = useState<any>(null); // may need to populate with response from get request
  const userData = {
    firebaseUser: authedUser,
    displayName: authedUser?.displayName,
    isLoading: isLoading,
    organization: orgData,
    setOrganization: (data: any) => {
      setOrgData(data);
    },
  } as UserData;
  const memoizedState = useMemo(
    () => userData,
    [isLoading, authedUser, orgData]
  );

  useEffect(() => {
    if (orgData == null) {
      setOrgData(getOrg());
    } else {
      // get user from the api using email.
      // set the profile to the user object returned from the api
      setOrgData(orgData);
    }
  }, [orgData]);

  return (
    <UserContext.Provider value={memoizedState}>
      {children}
    </UserContext.Provider>
  );
};

export const useUser = (): UserData => {
  const context = useContext(UserContext);
  if (!context) {
    throw new Error("useUser must be used within UserProvider");
  }
  return context;
};
