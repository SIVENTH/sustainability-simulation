import React, { createContext, useContext, useState, useEffect } from "react";
import { db } from "gateway";
import { useAuth } from "context";

// Firestore Document data
interface CustomerData {
  customer_id: string;
}
interface StripeData {
  customerData: CustomerData | null;
}

interface StripeProviderProps {
  children: React.ReactNode;
}

const StripeContext = createContext<StripeData | null>(null);

export const StripeProvider = ({
  children,
}: StripeProviderProps): JSX.Element => {
  const [customerData, setCustomerData] = useState<CustomerData | null>(null);
  const [error, setError] = useState<Error | null>(null);
  const { authedUser } = useAuth();

  useEffect(() => {
    if (authedUser) {
      return db
        .getDocumentReference(`stripe_customers/${authedUser?.uid}`)
        .onSnapshot((snapshot) => {
          const data = snapshot.data() as CustomerData;
          if (data) {
            setCustomerData(data);
          } else {
            setError(() => {
              throw Error("No Stripe Customer in Firebase");
            });
          }
        });
    }
  }, [authedUser]);

  console.log("[STRIPE PROVIDER]error: ", error);

  return (
    <StripeContext.Provider value={{ customerData }}>
      {children}
    </StripeContext.Provider>
  );
};

export const useStripeData = (): StripeData => {
  const context = useContext(StripeContext);
  if (!context) {
    throw new Error("useStripe must be used within StripeProvider");
  }
  return context;
};
