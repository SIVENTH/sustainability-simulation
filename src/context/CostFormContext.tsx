import { createContext, useContext, useState } from "react";
import { PaymentMethodCreateParams } from "@stripe/stripe-js";

import {
  CostParameters,
  CalculateCostResponse,
  TaxLocation,
  PromoCode,
} from "types";
import { useCalculateCost } from "hooks/stripe";
import { useDefaultFormValues } from "utils";

interface FormContextData {
  cost: CalculateCostResponse;
  costParameters: CostParameters;
  setCostParameters: (costParameters: CostParameters) => void;
  selectedPaymentMethod: string;
  setSelectedPaymentMethod: (paymentMethod: string) => void;
  billingDetails: PaymentMethodCreateParams.BillingDetails;
  setBillingDetails: (
    billingDetails: PaymentMethodCreateParams.BillingDetails
  ) => void;
  taxLocation: TaxLocation;
  setTaxLocation: (taxLocation: TaxLocation) => void;
  promoCode: PromoCode | null;
  setPromoCode: (promoCode: PromoCode) => void;
}

const useCostParameters = () => {
  const defaultValues = useDefaultFormValues();

  return useState<CostParameters>(defaultValues);
};

const CostFormContext = createContext<FormContextData | null>(null);

interface CostFormProviderProps {
  children: React.ReactNode;
}

export const CostFormProvider = ({
  children,
}: CostFormProviderProps): JSX.Element => {
  const [costParameters, setCostParameters] = useCostParameters();

  const [selectedPaymentMethod, setSelectedPaymentMethod] =
    useState<string>("");
  const [billingDetails, setBillingDetails] =
    useState<PaymentMethodCreateParams.BillingDetails>({});
  const [taxLocation, setTaxLocation] = useState<TaxLocation>({
    country: "JP",
  });
  const [promoCode, setPromoCode] = useState<PromoCode | null>(null);

  const cost = useCalculateCost(costParameters);
  return (
    <CostFormContext.Provider
      value={{
        cost,
        costParameters,
        setCostParameters,
        selectedPaymentMethod,
        setSelectedPaymentMethod,
        billingDetails,
        setBillingDetails,
        taxLocation,
        setTaxLocation,
        promoCode,
        setPromoCode,
      }}
    >
      {children}
    </CostFormContext.Provider>
  );
};

export const useCostForm = (): FormContextData => {
  const context = useContext(CostFormContext);
  if (!context) {
    throw new Error("useCostForm must be used within CostFormProvider");
  }
  return context;
};
