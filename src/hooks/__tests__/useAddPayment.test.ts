import { renderHook } from "@testing-library/react";
import { useAddPaymentMethod } from "../stripe/useAddPaymentMethod";

jest.mock("gateway", () => ({
  createSetupIntent: jest.fn(async () => ({
    data: { clientSecret: "xxx" },
  })),
}));

const confirmCardSetup = jest.fn();

jest.mock("@stripe/react-stripe-js", () => ({
  useStripe: () => ({
    confirmCardSetup,
  }),
  useElements: () => ({
    getElement: () => "Dummy Card",
  }),
  CardElement: {},
}));

describe("useAddPayment", () => {
  it("Calls stripe API with secret returned from firebase function", async () => {
    const testInput = { name: "Test Name" };

    const { result } = renderHook(() => useAddPaymentMethod());

    const addPaymentMethod = result.current;

    await addPaymentMethod(testInput);

    expect(confirmCardSetup).toHaveBeenCalledWith("xxx", {
      payment_method: {
        card: "Dummy Card",
        billing_details: testInput,
      },
    });
  });
});
