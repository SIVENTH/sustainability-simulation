import { renderHook } from "@testing-library/react";
import { useUpdatePaymentMethod } from "../stripe/useUpdatePaymentMethod";
import { updatePaymentMethod } from "gateway";

jest.mock("gateway", () => ({
  updatePaymentMethod: jest.fn(async () => ({
    data: { clientSecret: "xxx" },
  })),
}));

const confirmCardPayment = jest.fn();

jest.mock("@stripe/react-stripe-js", () => ({
  useStripe: () => ({
    confirmCardPayment,
  }),
}));

describe("useUpdatePaymentMethod", () => {
  it("Calls firebase function with provided ids, and then calls Stripe API with returned secret.", async () => {
    const { result } = renderHook(() => useUpdatePaymentMethod());

    const updatePayment = result.current;

    const subscriptionId = "test_sub_id";
    const paymentMethod = "test_payment_method";

    await updatePayment(subscriptionId, paymentMethod);

    expect(updatePaymentMethod).toHaveBeenCalledWith({
      subscriptionId,
      paymentMethod,
    });

    expect(confirmCardPayment).toHaveBeenCalledWith("xxx");
  });
});
