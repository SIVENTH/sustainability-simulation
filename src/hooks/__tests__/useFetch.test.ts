import { renderHook, waitFor } from "@testing-library/react";
import { useFetch } from "../useFetch";

global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve({ post }),
    ok: true,
  })
) as jest.Mock;

type Post = {
  id: number;
  title: string;
  body: string;
};

const post: Post = { id: 1, title: "test", body: "test" };

describe("useFetch", () => {
  it("fetches data", async () => {
    const { result } = renderHook(() =>
      useFetch<Post[]>("http://jsonplaceholder.typicode.com/posts")
    );
    expect(result.current.status).toBe("fetching");
    await waitFor(() => {
      expect(result.current).toEqual({
        status: "fetched",
        error: null,
        data: { post },
      });
    });
  });
});
