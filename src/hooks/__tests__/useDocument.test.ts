import { useDocument } from "../useDocument";
import { renderHook, waitFor } from "@testing-library/react";
import { DocumentReference } from "gateway";

describe("useDocument", () => {
  it("returns data from the hook", async () => {
    const doc = {
      id: 1,
      data: jest.fn().mockReturnValue({ name: "test" }),
      exists: true,
    };
    const query = {
      get: jest.fn().mockReturnValue(doc),
    } as unknown as DocumentReference;
    const { result } = renderHook(() => useDocument(query));
    const [, loading] = result.current;
    expect(loading).toBe("loading");

    await waitFor(() => {
      const [data] = result.current;
      expect(data).toEqual({ id: 1, name: "test" });
    });
  });
});
