import { useAuthedUser } from "../useAuthedUser";
import { renderHook, waitFor } from "@testing-library/react";
import { firebaseUser } from "gateway";

jest.mock("gateway", () => ({
  auth: {
    getAuthListener: (userFn: (user: firebaseUser) => void) => {
      userFn({
        displayName: "testUser",

        getIdTokenResult: () =>
          new Promise((resolve) => {
            resolve({
              expirationTime: new Date().getTime() + 100000,
              token: "testToken",
            });
          }),

        getIdToken: (forceRefresh = false) => {
          let tokenExpirationTime = new Date().getTime() + 10 * 60 * 1000; // set initial token expiration time to 10 minutes from now
          const currentTime = new Date().getTime();

          if (
            forceRefresh ||
            currentTime + 5 * 60 * 1000 >= tokenExpirationTime
          ) {
            tokenExpirationTime = currentTime + 10 * 60 * 1000; // set new expiration time to 10 minutes from now
            return Promise.resolve("refreshedToken");
          } else {
            return Promise.resolve("testToken");
          }
        },
      } as unknown as firebaseUser);
      return jest.fn();
    },
  },
}));

describe("useAuthedUser", () => {
  it("returns authed user", async () => {
    const { result } = renderHook(() => useAuthedUser());
    expect(result.current.authedUser?.displayName).toBe("testUser");
  });
});

describe("useAuthedUser token functions", () => {
  it("returns token and expiration time", async () => {
    const { result } = renderHook(() => useAuthedUser());
    const tokenInfo = await result.current.authedUser?.getIdTokenResult();
    expect(tokenInfo?.expirationTime).toBeGreaterThan(new Date().getTime());
  });

  it("returns token and refreshes it 5 minutes before expiration", async () => {
    jest.useFakeTimers();

    const { result } = renderHook(() => useAuthedUser());

    // Check that the initial token is "testToken".
    expect(await result.current.authedUser?.getIdToken()).toBe("testToken");

    // Fast-forward 4 minutes and 59 seconds.
    jest.advanceTimersByTime(4 * 60 * 1000 + 59 * 1000);

    // Check that the token is still "testToken".
    expect(await result.current.authedUser?.getIdToken()).toBe("testToken");

    // Fast-forward 1 second to trigger the token refresh.
    jest.advanceTimersByTime(1000);

    // Wait for the token to be refreshed and check that it is "refreshedToken".
    await waitFor(
      () => {
        expect(result.current.authedUser?.getIdToken(true)).resolves.toBe(
          "refreshedToken"
        );
      },
      { timeout: 1000 }
    );

    jest.useRealTimers();
  });
});
