import { usePaginateQuery } from "../usePaginateQuery";
import { renderHook, waitFor } from "@testing-library/react";
import { FirestoreQuery } from "gateway";

describe("usePaginateQuery", () => {
  it("returns data from the hook async", async () => {
    const doc = { id: 1, data: jest.fn().mockReturnValue({ name: "test" }) };
    const snapshot = {
      forEach: (cb) => cb(doc),
      docs: [],
    };
    const query = {
      limit: jest
        .fn()
        .mockReturnValue({ get: () => Promise.resolve(snapshot) }),
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } as unknown as FirestoreQuery<any>;

    const { result } = renderHook(() => usePaginateQuery(query));
    expect(result.current.loading).toBeTruthy();
    await waitFor(() => {
      expect(result.current.simResults).toEqual([{ id: 1, name: "test" }]);
    });
  });
});
