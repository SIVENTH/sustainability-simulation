import { renderHook, act, waitFor } from "@testing-library/react";
import { usePaymentMethods } from "../stripe/usePaymentMethods";

jest.mock("gateway", () => ({
  getPaymentMethods: jest
    .fn(async () => ({
      data: { paymentMethods: [{ id: "pm_id0" }], has_more: false },
    }))
    .mockImplementationOnce(async () => ({
      data: { paymentMethods: [{ id: "pm_id1" }], has_more: true },
    }))
    .mockImplementationOnce(async () => ({
      data: { paymentMethods: [{ id: "pm_id2" }], has_more: true },
    }))
    .mockImplementationOnce(async () => ({
      data: { paymentMethods: [{ id: "pm_id3" }], has_more: false },
    })),
}));

describe("usePaymentMethods", () => {
  it("returns data", async () => {
    const { result } = renderHook(() => usePaymentMethods());

    expect(result.current.isLoading).toBe(true);

    await waitFor(() => {
      expect(result.current.data).toStrictEqual([
        expect.objectContaining({ id: "pm_id1" }),
      ]);
      expect(result.current.hasMore).toBe(true);
    });
  });

  it("fetches next batch", async () => {
    const { result } = renderHook(() => usePaymentMethods());
    await waitFor(() => {
      act(result.current.getNext);

      expect(result.current.isLoading).toBe(true);
    });

    await waitFor(() => {
      expect(result.current.data).toStrictEqual([
        expect.objectContaining({ id: "pm_id2" }),
      ]);

      expect(result.current.hasMore).toBe(true);
    });
  });
});
