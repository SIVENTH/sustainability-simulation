import { usePagination } from "../usePagination";
import { renderHook, act } from "@testing-library/react";
import { intl } from "utils";
import { tea } from "data/tea";

jest.mock("gateway", () => ({
  createSetupIntent: jest.fn(async () => ({
    data: { clientSecret: "xxx" },
  })),
}));

describe("usePagination", () => {
  it("returns expected data", async () => {
    const { result } = renderHook(() => usePagination(tea, 1));
    const { displayData } = result.current;
    const data = displayData();
    expect(data.section).toEqual(
      intl.formatMessage({ id: "tea.background.title" })
    );
  });

  it("paginates data on next", async () => {
    const { result } = renderHook(() => usePagination(tea, 1));
    const { next } = result.current;
    act(() => next());
    const { displayData } = result.current;
    const data = displayData();
    expect(data.section).toEqual(
      intl.formatMessage({ id: "tea.growing.description" })
    );
  });
});
