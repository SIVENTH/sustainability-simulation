import { renderHook, waitFor } from "@testing-library/react";
import { useSubscriptionInvoice } from "../stripe/useSubscriptionInvoice";

jest.mock("gateway", () => ({
  getSubscription: jest.fn(async ({ subscriptionId }) => {
    if (subscriptionId === "sub_id")
      return {
        data: { invoice_id: "in_id0" },
      };
    else throw Error("error");
  }),
}));

describe("useSubscriptionInvoice", () => {
  it("returns data", async () => {
    const { result } = renderHook(() => useSubscriptionInvoice("sub_id"));

    expect(result.current.isLoading).toBe(true);

    await waitFor(() => {
      expect(result.current.invoice).toStrictEqual(
        expect.objectContaining({ invoice_id: "in_id0" })
      );
    });
  });

  it("returns error", async () => {
    const { result } = renderHook(() => useSubscriptionInvoice("wrong_sub_id"));

    expect(result.current.isLoading).toBe(true);

    await waitFor(() => {
      expect(result.current).toStrictEqual(
        expect.objectContaining({ error: Error("error") })
      );
    });
  });
});
