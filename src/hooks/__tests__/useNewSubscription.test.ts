import { renderHook } from "@testing-library/react";
import { useNewSubscription } from "../stripe/useNewSubscription";
import { createStripeSubscription, updateCustomerAddress } from "gateway";
import { TaxLocation } from "types";

afterEach(() => {
  jest.clearAllMocks();
});

jest.mock("gateway", () => ({
  createStripeSubscription: jest.fn(async ({ promoCode }) =>
    promoCode === "FREE"
      ? { data: {} }
      : {
          data: { clientSecret: "xxx" },
        }
  ),
  updateCustomerAddress: jest.fn(),
}));

const confirmCardPayment = jest.fn();

jest.mock("@stripe/react-stripe-js", () => ({
  useStripe: () => ({
    confirmCardPayment,
  }),
  useElements: () => ({
    getElement: () => "Dummy Card",
  }),
  CardElement: {},
}));

const testBillingDetails = { name: "Test Billing Name" };
// added tax location to fulfill number of parameters requirements
const taxLocation: TaxLocation = {
  country: "Japan",
  postalCode: "214-0001",
};

describe("useNewSubscription", () => {
  it("Calls firebase function with provided data, and then calls Stripe API with returned secret.", async () => {
    const { result } = renderHook(() => useNewSubscription());

    const newSubscription = result.current;

    await newSubscription(
      "test_price_id",
      testBillingDetails,
      taxLocation,
      null
    );

    expect(updateCustomerAddress).toHaveBeenCalledWith({
      address: taxLocation,
    });
    expect(createStripeSubscription).toHaveBeenCalledWith({
      priceId: "test_price_id",
      promoCode: null,
    });
    expect(confirmCardPayment).toHaveBeenCalledWith("xxx", {
      payment_method: {
        card: "Dummy Card",
        billing_details: testBillingDetails,
      },
    });
  });

  it("Uses payment ID if provided", async () => {
    const { result } = renderHook(() => useNewSubscription());

    const newSubscription = result.current;
    await newSubscription(
      "test_price_id",
      testBillingDetails,
      taxLocation,
      null,
      "test_payment_id"
    );

    expect(confirmCardPayment).toHaveBeenCalledWith("xxx", {
      payment_method: "test_payment_id",
    });
  });

  it("Skips confirmCardPayment if no client secret is returned", async () => {
    const { result } = renderHook(() => useNewSubscription());

    const newSubscription = result.current;
    await newSubscription(
      "test_price_id",
      testBillingDetails,
      taxLocation,
      "FREE"
    );

    expect(confirmCardPayment).not.toHaveBeenCalled();
  });
});
