import { renderHook, act, waitFor } from "@testing-library/react";
import { usePaymentHistory } from "../stripe/usePaymentHistory";

jest.mock("gateway", () => ({
  getPaymentHistory: jest
    .fn(async () => ({
      data: { invoices: [{ invoice_id: "in_id0" }], has_more: false },
    }))
    .mockImplementationOnce(async () => ({
      data: { invoices: [{ invoice_id: "in_id1" }], has_more: true },
    }))
    .mockImplementationOnce(async () => ({
      data: { invoices: [{ invoice_id: "in_id2" }], has_more: true },
    }))
    .mockImplementationOnce(async () => ({
      data: { invoices: [{ invoice_id: "in_id3" }], has_more: false },
    })),
}));

describe("usePaymentHistory", () => {
  it("returns data", async () => {
    const { result } = renderHook(() => usePaymentHistory());

    expect(result.current.isLoading).toBe(true);

    await waitFor(() => {
      expect(result.current.data).toStrictEqual([
        expect.objectContaining({ invoice_id: "in_id1" }),
      ]);
      expect(result.current.hasMore).toBe(true);
    });
  });

  it("fetches next batch", async () => {
    const { result } = renderHook(() => usePaymentHistory());
    await waitFor(() => {
      act(result.current.getNext);

      expect(result.current.isLoading).toBe(true);
    });

    await waitFor(() => {
      expect(result.current.data).toStrictEqual([
        expect.objectContaining({ invoice_id: "in_id2" }),
      ]);

      expect(result.current.hasMore).toBe(true);
    });
  });
});
