import { renderHook, act, waitFor } from "@testing-library/react";
import { useSubscriptions } from "../stripe/useSubscriptions";

jest.mock("gateway", () => ({
  getSubscriptions: jest
    .fn(async () => ({
      data: { subscriptions: [{ id: "sub_id0" }], has_more: false },
    }))
    .mockImplementationOnce(async () => ({
      data: { subscriptions: [{ id: "sub_id1" }], has_more: true },
    }))
    .mockImplementationOnce(async () => ({
      data: { subscriptions: [{ id: "sub_id2" }], has_more: true },
    }))
    .mockImplementationOnce(async () => ({
      data: { subscriptions: [{ id: "sub_id3" }], has_more: false },
    })),
}));

describe("useSubscriptions", () => {
  it("returns data", async () => {
    const { result } = renderHook(() => useSubscriptions());

    expect(result.current.isLoading).toBe(true);

    await waitFor(() => {
      expect(result.current.data).toStrictEqual([
        expect.objectContaining({ id: "sub_id1" }),
      ]);
      expect(result.current.hasMore).toBe(true);
    });
  });

  it("fetches next batch", async () => {
    const { result } = renderHook(() => useSubscriptions());
    await waitFor(() => {
      act(result.current.getNext);

      expect(result.current.isLoading).toBe(true);
    });

    await waitFor(() => {
      expect(result.current.data).toStrictEqual([
        expect.objectContaining({ id: "sub_id2" }),
      ]);

      expect(result.current.hasMore).toBe(true);
    });
  });
});
