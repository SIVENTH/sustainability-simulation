import { renderHook, waitFor } from "@testing-library/react";
import { useCalculateCost } from "../stripe";
import { SubscriptionType, Frequency, Quantity } from "types";

jest.mock("gateway", () => ({
  calculateCost: jest.fn(async () => ({
    price: 100,
    priceId: null,
  })),
}));

describe("useCalculateCost", () => {
  it("returns data", async () => {
    const testInput = {
      subscriptionType: SubscriptionType.Education,
      numberOfSubscriptions: Quantity.Medium,
      frequency: Frequency.Monthly,
    };

    const { result } = renderHook(() => useCalculateCost(testInput));

    expect(result.current.isLoading).toBe(true);

    await waitFor(() => {
      expect(result.current.data.price).toBe(100);
    });
  });
});
