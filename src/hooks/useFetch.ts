import { useEffect, useReducer, useRef } from "react";

type FetchRequestConfig = {
  method: "POST" | "GET" | "PUT" | "DELETE";
  mode?: "cors" | "no-cors" | "same-origin";
  credentials?: "same-origin" | "include" | "omit";
  cache?: "no-cache" | "reload" | "force-cache" | "only-if-cached";
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  headers: { [key: string]: any };
};

type State<T> = {
  status: "fetching" | "error" | "fetched";
  data?: T | null;
  error?: string | null;
};

enum ActionType {
  SUCCESS = "SUCCESS",
  FAILURE = "FAILURE",
}

type Action<T> =
  | { type: ActionType.SUCCESS; payload: T }
  | { type: ActionType.FAILURE; payload: string };

function reducer<T>(state: State<T>, action: Action<T>): State<T> {
  switch (action.type) {
    case ActionType.SUCCESS:
      return { ...state, status: "fetched", data: action.payload };
    case ActionType.FAILURE:
      return { ...state, status: "error", error: action.payload };
    default:
      return state;
  }
}

type Cache<T> = {
  [url: string]: T;
};

// eslint-disable-next-line max-lines-per-function
export const useFetch = <T = unknown>(
  url: string,
  options?: FetchRequestConfig
): State<T> => {
  const cache = useRef<Cache<T>>({});

  const initialState: State<T> = {
    status: "fetching",
    error: null,
    data: null,
  };
  const [state, dispatch] = useReducer(reducer, initialState);

  // eslint-disable-next-line sonarjs/cognitive-complexity
  useEffect(() => {
    let cancelRequest = false;
    const fetchData = async () => {
      if (cache.current[url]) {
        dispatch({ type: ActionType.SUCCESS, payload: cache.current[url] });
      } else {
        try {
          const response = await fetch(url, options);
          if (!response.ok) {
            const errorData = await response.text();
            const { error } = JSON.parse(errorData);
            dispatch({ type: ActionType.FAILURE, payload: error.message });
            return;
          }
          const data = await response.json();
          cache.current[url] = data;
          if (cancelRequest) return;
          dispatch({ type: ActionType.SUCCESS, payload: data });
        } catch (error) {
          if (cancelRequest) return;

          const payload =
            error instanceof Error ? error.message : "Unexpected error";

          dispatch({
            type: ActionType.FAILURE,
            payload,
          });
        }
      }
    };
    fetchData();
    return () => {
      cancelRequest = true;
    };
    // eslint-disable-next-line
  }, [url]);

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return state as any;
};
