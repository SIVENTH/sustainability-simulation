/* eslint-disable sonarjs/cognitive-complexity */
/* eslint-disable max-lines-per-function */
import { useReducer, useEffect, useRef } from "react";
import { ResultDocument } from "types";
import {
  DocumentData,
  QuerySnapshot,
  QueryDocumentSnapshot,
  CollectionReference,
  FirestoreQuery,
} from "gateway";

interface ResultDocumentWithId extends ResultDocument {
  id: string;
}

enum ActionTypes {
  INITIAL_LOAD = "INITIAL_LOAD",
  LOAD_MORE = "LOAD_MORE",
  ERROR = "ERROR",
  RESET = "RESET",
  RELOADING = "RELOADING",
}

type State = {
  hasMore: boolean;
  triggerEffect: DocumentData | null;
  limit: number;
  simResults: ResultDocumentWithId[];
  lastLoaded: QueryDocumentSnapshot | null;
  loading: boolean;
  error: null | Error | string;
  hasMounted: boolean;
  reloading: boolean;
};

type Action =
  | {
      type: ActionTypes.INITIAL_LOAD;
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      snapshot: QuerySnapshot<any>;
      limit: number;
    }
  | { type: ActionTypes.LOAD_MORE }
  | { type: ActionTypes.ERROR; error: string }
  | { type: ActionTypes.RESET }
  | { type: ActionTypes.RELOADING };

function reducer(state: State, action: Action): State {
  switch (action.type) {
    case ActionTypes.INITIAL_LOAD: {
      const simResults = [...state.simResults];
      if (action.snapshot) {
        action.snapshot.forEach((doc) => {
          if (doc.data) {
            const result = doc.data();
            simResults.push({
              id: doc.id,
              ...result,
            });
          }
        });
      }

      const nextLimit = simResults.length + action.limit;
      const end = simResults.length < action.limit || nextLimit === state.limit;

      return {
        ...state,
        simResults,
        hasMore: !end,
        limit: nextLimit,
        loading: false,
        lastLoaded:
          action.snapshot.docs[action.snapshot.docs.length - 1] ||
          state.lastLoaded,
      };
    }

    case ActionTypes.LOAD_MORE: {
      if (state.hasMore) {
        return {
          ...state,
          triggerEffect: state.lastLoaded,
        };
      }
      return state;
    }
    case ActionTypes.ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
      };
    case ActionTypes.RELOADING:
      return {
        ...state,
        reloading: true,
      };
    case ActionTypes.RESET:
      return {
        ...state,
        reloading: false,
        lastLoaded: null,
        simResults: [],
        triggerEffect: null,
      };
    default:
      return state;
  }
}

const initialState: State = {
  hasMore: false,
  triggerEffect: null,
  limit: 0,
  simResults: [],
  lastLoaded: null,
  loading: true,
  error: null,
  hasMounted: false,
  reloading: false,
};

export type PaginateHookData = {
  loadMore: () => void;
  resetLoad: () => void;
  error: null | Error | string;
  loading: boolean;
  reloading: boolean;
  hasMore: boolean;
  simResults: ResultDocumentWithId[];
};

export const usePaginateQuery = (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  query: CollectionReference | FirestoreQuery<any>,
  { limit = 5 } = {}
): PaginateHookData => {
  const [
    {
      lastLoaded,
      hasMore,
      triggerEffect,
      error,
      simResults,
      loading,
      reloading,
    },
    dispatch,
  ] = useReducer(reducer, initialState);

  const persisted = useRef(query);
  const lastVisible = useRef<DocumentData | null>(lastLoaded);

  useEffect(() => {
    lastVisible.current = lastLoaded;
  }, [lastLoaded]);

  useEffect(() => {
    let isCancelled = false;
    (async function getData(): Promise<void> {
      let query;
      lastVisible.current
        ? (query = persisted.current
            .startAfter(lastVisible.current)
            .limit(limit))
        : (query = persisted.current.limit(limit));
      try {
        const snapshot = await query.get();
        if (!isCancelled) {
          dispatch({ type: ActionTypes.INITIAL_LOAD, snapshot, limit });
        }
      } catch (err) {
        dispatch({ type: ActionTypes.ERROR, error: err.message });
      }
    })();
    return () => {
      isCancelled = true;
    };
  }, [triggerEffect, limit]);

  function loadMore() {
    dispatch({ type: ActionTypes.LOAD_MORE });
  }

  function resetLoad() {
    dispatch({ type: ActionTypes.RELOADING });
    dispatch({ type: ActionTypes.RESET });
  }

  return {
    loadMore,
    resetLoad,
    error,
    loading,
    hasMore,
    simResults,
    reloading,
  };
};
