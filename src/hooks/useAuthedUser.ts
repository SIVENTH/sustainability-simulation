/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable max-lines-per-function */
import { useState, useEffect } from "react";
import { auth, firebaseUser } from "gateway";
import {
  getOrgIdAndMemberIdOnLogin,
  getOrganization,
  getAllUserTeams,
} from "gateway/services";

type AuthedUserHook = {
  authedUser: firebaseUser | null;
  isLoading: boolean;
  token: string;
  orgId: string | null;
  memberId: string;
  myOrgData: object;
  teamList: any;
  setAuthedUser: (user: firebaseUser | null) => void;
  setMyOrgData: (data: object) => void;
  setOrgId: (orgId: string | null) => void;
};

interface OrganizationResponse {
  data: object;
}
interface UserIdsResponse {
  data: {
    organizationId: string;
    memberId: string;
  };
}

export const useAuthedUser = (): AuthedUserHook => {
  const [authedUser, setAuthedUser] = useState<firebaseUser | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [token, setToken] = useState<string>("");

  const [orgId, setOrgId] = useState<string | null>(null);
  const [memberId, setMemberId] = useState<string>("");
  const [myOrgData, setMyOrgData] = useState<object>({});
  const [teamList, setTeamList] = useState<any>([]);

  useEffect(() => {
    const unsubscribe = auth.getAuthListener(
      (user: firebaseUser) => {
        setAuthedUser(user);
        setIsLoading(false);
        user.getIdTokenResult().then((idTokenResult) => {
          const { expirationTime } = idTokenResult;
          const now = new Date().getTime();
          const expireAt = new Date(expirationTime).getTime();
          const timeRemaining = expireAt - now;

          // If the token is about to expire, refresh it (5 mins before)
          if (timeRemaining <= 300000) {
            user.getIdToken(true).then((refreshedToken) => {
              setToken(refreshedToken);
              sessionStorage.removeItem("token");
              sessionStorage.setItem("token", refreshedToken);
            });
          } else {
            setToken(idTokenResult.token);
            sessionStorage.removeItem("token");
            sessionStorage.setItem("token", idTokenResult.token);
          }
        });
      },
      () => {
        setIsLoading(false);
      }
    );
    return () => {
      unsubscribe();
    };
  }, []);

  const getOrgIdAndMemberId = async (): Promise<void> => {
    try {
      const response = await getOrgIdAndMemberIdOnLogin();
      const userIdResponse = response as UserIdsResponse;
      await setOrgId(userIdResponse?.data?.organizationId);
      await setMemberId(userIdResponse?.data?.memberId);
    } catch (error) {
      console.log("error", error);
    }
  };
  const getMyOrganization = async (): Promise<void> => {
    try {
      const response = await getOrganization(orgId);
      const orgResponse = response as OrganizationResponse;
      await setMyOrgData(orgResponse?.data);
      // localStorage.setItem("myOrgData", JSON.stringify(myOrgData));
    } catch (error) {
      console.log("error", error);
    }
  };
  const getAllTeams = async (memberId): Promise<void> => {
    try {
      if (authedUser) {
        const response: any = await getAllUserTeams(memberId);
        await setTeamList(response.data);
      } else {
        setTeamList([]);
      }
    } catch (error) {
      console.error(error);
    }
  };
  useEffect(() => {
    if (authedUser) {
      getOrgIdAndMemberId();
    }
  }, [authedUser]);

  useEffect(() => {
    if (memberId) {
      getAllTeams(memberId);
    }
  }, [memberId]);

  useEffect(() => {
    if (orgId) {
      getMyOrganization();
    }
  }, [orgId]);

  return {
    authedUser,
    isLoading,
    token,
    orgId,
    memberId,
    myOrgData,
    teamList,
    setAuthedUser,
    setMyOrgData,
    setOrgId,
  };
};
