import { useState, useEffect } from "react";
import { getOrganization } from "gateway/services";

interface Response {
  data: object;
}

type OrganizationHook = {
  data: object;
};

export const useFetchOrganization = (
  orgId: string | null
): OrganizationHook => {
  const [data, setData] = useState<object>({});

  useEffect(() => {
    if (orgId) {
      const fetchData = async () => {
        const response = await getOrganization(orgId);
        const data = response as Response;
        setData(data);
      };
      fetchData();
    }
  }, [orgId]);

  return { data };
};
