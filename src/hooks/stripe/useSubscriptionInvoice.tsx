import { useState, useEffect } from "react";
import { getSubscription } from "gateway";

import { Invoice } from "types";

interface ReturnValue {
  invoice: Invoice | null;
  isLoading: boolean;
  error: Error | null;
}

export const useSubscriptionInvoice = (subscriptionId: string): ReturnValue => {
  const [data, setData] = useState<Invoice | null>(null);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<Error | null>(null);

  useEffect(() => {
    setIsLoading(true);
    getSubscription({ subscriptionId })
      .then((resp) => {
        const invoice = resp.data;
        setData(invoice);
        setIsLoading(false);
      })
      .catch((error) => {
        setError(error);
        setIsLoading(false);
      });
  }, []);

  return { invoice: data, isLoading, error };
};
