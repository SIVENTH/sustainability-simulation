import { useState, useEffect, useCallback } from "react";
import { getPaymentHistory } from "gateway";

import { Invoice } from "types";

interface ReturnValue {
  data: Invoice[];
  hasMore: boolean;
  isLoading: boolean;
  error: Error | null;
  getNext: () => void;
}

export const usePaymentHistory = (): ReturnValue => {
  const [data, setData] = useState<Invoice[]>([]);
  const [hasMore, setHasMore] = useState(false);
  const [nextId, setNextId] = useState(null);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<Error | null>(null);

  useEffect(() => {
    setIsLoading(true);
    getPaymentHistory()
      .then((resp) => {
        const { invoices, has_more, next } = resp.data;
        setData(invoices);
        setHasMore(has_more);
        setNextId(next);
        setIsLoading(false);
      })
      .catch((error) => {
        setError(error);
        setIsLoading(false);
      });
  }, []);

  const getNext = useCallback(() => {
    if (hasMore && !isLoading) {
      setIsLoading(true);
      getPaymentHistory({ starting_after: nextId })
        .then((resp) => {
          const { invoices, has_more, next } = resp.data;
          setData([...data, ...invoices]);
          setHasMore(has_more);
          setNextId(next);
          setIsLoading(false);
        })
        .catch((error) => {
          setError(error);
          setIsLoading(false);
        });
    }
  }, [
    hasMore,
    setHasMore,
    nextId,
    setNextId,
    isLoading,
    setIsLoading,
    data,
    setData,
    setError,
  ]);

  return { data, isLoading, error, hasMore, getNext };
};
