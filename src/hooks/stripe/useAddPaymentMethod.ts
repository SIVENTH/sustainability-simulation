import { useCallback, useRef } from "react";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import { PaymentMethodCreateParams } from "@stripe/stripe-js";
import { createSetupIntent } from "gateway";

interface PaymentResponse {
  error?: Error;
}

// eslint-disable-next-line max-lines-per-function
export const useAddPaymentMethod = (): ((
  billingDetails: PaymentMethodCreateParams.BillingDetails
) => Promise<PaymentResponse>) => {
  const stripe = useStripe();
  const elements = useElements();

  const isLoading = useRef<boolean>(false);

  return useCallback(
    // eslint-disable-next-line max-lines-per-function
    async (
      billingDetails: PaymentMethodCreateParams.BillingDetails
    ): Promise<PaymentResponse> => {
      try {
        const cardElement = elements?.getElement(CardElement);

        // In these cases, the submit button should be disabled.
        if (!stripe || !elements || isLoading.current || !cardElement) {
          throw Error("Not ready to submit.");
        }

        isLoading.current = true;
        const resp = await createSetupIntent();
        const { error, clientSecret } = resp.data;
        if (error) throw Error(error);
        const { error: confirmError } = await stripe.confirmCardSetup(
          clientSecret,
          {
            payment_method: {
              card: cardElement,
              billing_details: billingDetails,
            },
          }
        );
        if (confirmError) throw Error(confirmError.message);
        isLoading.current = false;
        return {};
      } catch (error) {
        isLoading.current = false;
        return { error };
      }
    },
    [stripe, elements]
  );
};
