import { useState, useEffect, useCallback } from "react";
import { PaymentMethod } from "@stripe/stripe-js";

import { getPaymentMethods } from "gateway";

interface ReturnValue {
  data: PaymentMethod[];
  isLoading: boolean;
  error: Error | null;
  hasMore: boolean;
  getNext: () => void;
}

export const usePaymentMethods = (): ReturnValue => {
  const [data, setData] = useState<PaymentMethod[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<Error | null>(null);
  const [hasMore, setHasMore] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    getPaymentMethods()
      .then((resp) => {
        const { paymentMethods, has_more } = resp.data;
        setData(paymentMethods);
        setHasMore(has_more);
        setIsLoading(false);
      })
      .catch((error) => {
        setError(error);
        setIsLoading(false);
      });
  }, []);

  const getNext = useCallback(() => {
    if (hasMore && !isLoading) {
      setIsLoading(true);
      getPaymentMethods({ starting_after: data[data.length - 1].id })
        .then((resp) => {
          const { paymentMethods, has_more } = resp.data;
          setData([...data, ...paymentMethods]);
          setHasMore(has_more);
          setIsLoading(false);
        })
        .catch((error) => {
          setError(error);
          setIsLoading(false);
        });
    }
  }, [hasMore, setHasMore, isLoading, setIsLoading, data, setData, setError]);

  return { data, isLoading, error, hasMore, getNext };
};
