import { useCallback, useRef } from "react";
import { CardElement, useStripe, useElements } from "@stripe/react-stripe-js";
import { PaymentMethodCreateParams } from "@stripe/stripe-js";
import { createStripeSubscription, updateCustomerAddress } from "gateway";

import { TaxLocation } from "types";

interface PaymentResponse {
  error?: Error;
  subscriptionId?: string;
}

export type NewSubscriptionFunction = (
  priceId: string,
  billingDetails: PaymentMethodCreateParams.BillingDetails,
  taxLocation: TaxLocation,
  promoCode: string | null,
  paymentMethod?: string
) => Promise<PaymentResponse>;

// eslint-disable-next-line max-lines-per-function
export const useNewSubscription = (): NewSubscriptionFunction => {
  const stripe = useStripe();
  const elements = useElements();

  const isLoading = useRef<boolean>(false);

  return useCallback(
    // eslint-disable-next-line max-lines-per-function
    async (
      priceId: string,
      billingDetails: PaymentMethodCreateParams.BillingDetails,
      taxLocation: TaxLocation,
      promoCode: string | null,
      paymentMethod?: string
      // eslint-disable-next-line sonarjs/cognitive-complexity
    ): Promise<PaymentResponse> => {
      try {
        const cardElement = elements?.getElement(CardElement);

        // In these cases, the submit button should be disabled.
        if (!stripe || !elements || isLoading.current || !cardElement) {
          throw Error("Not ready to submit.");
        }

        const data = {
          priceId,
          promoCode,
        };

        isLoading.current = true;

        await updateCustomerAddress({ address: taxLocation });
        const resp = await createStripeSubscription(data);
        const { error, clientSecret, id } = resp.data;
        if (error) throw Error(error);

        if (clientSecret) {
          const payment_method = paymentMethod || {
            card: cardElement,
            billing_details: billingDetails,
          };
          const { error: confirmError } = await stripe.confirmCardPayment(
            clientSecret,
            {
              payment_method,
            }
          );
          if (confirmError) throw Error(confirmError.message);
        }

        isLoading.current = false;
        return { subscriptionId: id };
      } catch (error) {
        isLoading.current = false;
        return { error } as { error: Error };
      }
    },
    [stripe, elements]
  );
};
