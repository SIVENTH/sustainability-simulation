import { useState, useEffect } from "react";

import { CalculateCostResponse, CostParameters, CostData } from "types";
import { calculateCost } from "gateway";

export const useCalculateCost = (
  costParameters: CostParameters
): CalculateCostResponse => {
  const { subscriptionType, numberOfSubscriptions, frequency } = costParameters;

  const [data, setData] = useState<CostData>({
    price: 0,
    priceId: null,
  });
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<Error | null>(null);

  useEffect(() => {
    if (!subscriptionType || !numberOfSubscriptions || !frequency) {
      setData({
        price: 0,
        priceId: null,
      });
      setIsLoading(false);
      return;
    }

    setIsLoading(true);

    calculateCost(costParameters)
      .then((resp) => {
        setData(resp);
        setIsLoading(false);
      })
      .catch((error) => setError(error));
  }, [costParameters]);

  return { data, isLoading, error };
};
