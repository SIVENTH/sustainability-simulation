/* eslint-disable sonarjs/cognitive-complexity */
import { useCallback, useRef } from "react";
import { useStripe } from "@stripe/react-stripe-js";
import { updatePaymentMethod } from "gateway";

interface UpdateResponse {
  error?: Error;
}

// eslint-disable-next-line max-lines-per-function
export const useUpdatePaymentMethod = (): ((
  subscriptionId: string,
  paymentMethod: string
) => Promise<UpdateResponse>) => {
  const stripe = useStripe();

  const isLoading = useRef<boolean>(false);

  return useCallback(
    async (
      subscriptionId: string,
      paymentMethod: string
    ): Promise<UpdateResponse> => {
      try {
        // In these cases, the submit button should be disabled.
        if (!stripe || isLoading.current) {
          throw Error("Not ready to submit.");
        }

        isLoading.current = true;
        const resp = await updatePaymentMethod({
          paymentMethod,
          subscriptionId,
        });
        const { error, clientSecret } = resp.data;
        if (error) throw Error(error);

        if (clientSecret) {
          const { error: confirmError } = await stripe.confirmCardPayment(
            clientSecret
          );

          if (confirmError) throw Error(confirmError.message);
        }

        isLoading.current = false;
        return {};
      } catch (error) {
        isLoading.current = false;
        return { error };
      }
    },
    [stripe]
  );
};
