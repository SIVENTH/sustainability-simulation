import { useState, useEffect, useCallback } from "react";
import { getSubscriptions } from "gateway";

import { Subscription } from "types";

interface ReturnValue {
  data: Subscription[];
  hasMore: boolean;
  isLoading: boolean;
  error: Error | null;
  getNext: () => void;
}

export const useSubscriptions = (): ReturnValue => {
  const [data, setData] = useState<Subscription[]>([]);
  const [hasMore, setHasMore] = useState(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [error, setError] = useState<Error | null>(null);

  useEffect(() => {
    setIsLoading(true);
    getSubscriptions()
      .then((resp) => {
        const { subscriptions, has_more } = resp.data;
        setData(subscriptions);
        setHasMore(has_more);
        setIsLoading(false);
      })
      .catch((error) => {
        setError(error);
        setIsLoading(false);
      });
  }, []);

  const getNext = useCallback(() => {
    if (hasMore && !isLoading) {
      setIsLoading(true);
      getSubscriptions({ starting_after: data[data.length - 1].id })
        .then((resp) => {
          const { subscriptions, has_more } = resp.data;
          setData([...data, ...subscriptions]);
          setHasMore(has_more);
          setIsLoading(false);
        })
        .catch((error) => {
          setError(error);
          setIsLoading(false);
        });
    }
  }, [hasMore, setHasMore, isLoading, setIsLoading, data, setData, setError]);

  return { data, isLoading, error, hasMore, getNext };
};
