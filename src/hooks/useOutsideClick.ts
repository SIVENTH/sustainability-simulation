import { useState, useEffect, RefObject } from "react";

type Event = MouseEvent | TouchEvent;

export function useOutsideClick<T extends HTMLElement = HTMLElement>(
  ref: RefObject<T>,
  initialState: boolean
): [isOpen: boolean, setIsOpen: React.Dispatch<React.SetStateAction<boolean>>] {
  const [isOpen, setIsOpen] = useState<boolean>(initialState);

  useEffect(() => {
    const handleClickOutside = (event: Event) => {
      if (ref.current && !ref.current.contains(event.target as Node)) {
        setIsOpen(false);
      }
    };

    document.addEventListener("mousedown", handleClickOutside);
    document.addEventListener("touchstart", handleClickOutside);

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
      document.removeEventListener("touchstart", handleClickOutside);
    };
  }, [ref]);

  return [isOpen, setIsOpen];
}
