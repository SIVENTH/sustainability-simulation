/* eslint-disable @typescript-eslint/ban-types */
import { useState } from "react";

type PaginateHook<T> = {
  displayData: () => T;
  next: Function;
  prev: Function;
  jump: Function;
  currentPage: number;
  dataLength: number;
};

export const usePagination = <T>(
  data: T[],
  itemsPerPage: number
): PaginateHook<T> => {
  const [currentPage, setCurrentPage] = useState<number>(1);

  const maxPerPage: number = Math.ceil(data.length / itemsPerPage);
  const next = (): void => {
    setCurrentPage((currentPage) => Math.min(currentPage + 1, maxPerPage));
  };

  const prev = (): void => {
    setCurrentPage((currentPage) => Math.max(currentPage - 1, 1));
  };

  const jump = (page: number): void => {
    const pageNumber = Math.max(1, page);
    setCurrentPage(() => Math.min(pageNumber, maxPerPage));
  };

  const displayData = (): T => {
    const begin = (currentPage - 1) * itemsPerPage;
    const end = begin + itemsPerPage;
    return data.slice(begin, end)[0];
  };

  return {
    next,
    prev,
    jump,
    displayData,
    currentPage,
    dataLength: data.length,
  };
};
