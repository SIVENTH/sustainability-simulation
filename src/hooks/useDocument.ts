import { useReducer, useEffect, useRef } from "react";
import { DocumentReference, DocumentData, FirebaseError } from "gateway";

enum ActionTypes {
  FETCH = "FETCH",
  ERROR = "ERROR",
  LOAD = "LOAD",
  RELOAD = "RELOAD",
}

type Status = "loading" | "loaded" | "error" | null;

type State = {
  data: DocumentData | null;
  status: Status;
  error: FirebaseError | string | null;
  stateUpdate: number;
};

type Action =
  | {
      type: ActionTypes.FETCH;
      snapshot: DocumentData;
    }
  | { type: ActionTypes.ERROR; error: FirebaseError | string | null }
  | { type: ActionTypes.LOAD }
  | { type: ActionTypes.RELOAD };

type reload = () => void;
type Data<T> = T | null;

type LoadingHook<T, S, E, R> = [T, S, E, R];

type DocumentHook<T> = LoadingHook<
  Data<T>,
  Status,
  FirebaseError | string | null,
  reload
>;

function reducer(state: State, action: Action): State {
  switch (action.type) {
    case ActionTypes.FETCH:
      return {
        ...state,
        status: "loaded",
        data: {
          id: action.snapshot.id,
          ...action.snapshot.data(),
        },
      };
    case ActionTypes.ERROR:
      return {
        ...state,
        status: "error",
        error: action.error,
      };
    case ActionTypes.LOAD:
      return {
        ...state,
        status: "loading",
      };
    case ActionTypes.RELOAD:
      return {
        ...state,
        stateUpdate: state.stateUpdate + 1,
      };
    default:
      return state;
  }
}

const initialState: State = {
  data: null,
  status: null,
  error: null,
  stateUpdate: 0,
};

export const useDocument = <T>(query: DocumentReference): DocumentHook<T> => {
  const [{ data, status, error, stateUpdate }, dispatch] = useReducer(
    reducer,
    initialState
  );
  const ref = useRef(query);

  const reload = (): void => dispatch({ type: ActionTypes.RELOAD });

  useEffect(() => {
    if (!ref.current) return;
    (async function getData() {
      try {
        dispatch({ type: ActionTypes.LOAD });
        const snapshot = await ref.current.get();
        if (!snapshot.exists) {
          dispatch({
            type: ActionTypes.ERROR,
            error: "Document is empty",
          });
          return;
        }
        dispatch({ type: ActionTypes.FETCH, snapshot });
      } catch (error) {
        dispatch({ type: ActionTypes.ERROR, error: error.message });
      }
    })();
  }, [stateUpdate]);

  return [data as T, status, error, reload];
};
