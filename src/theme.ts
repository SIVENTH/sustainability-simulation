import { extendTheme } from "@chakra-ui/react";
import { colors } from "utils";

export const theme = extendTheme({
  styles: {
    global: {
      body: {
        fontFamily: "Inter, Source Sans Pro, sans-serif",
        fontWeight: "400px",
      },
    },
  },
  components: {
    Link: {
      baseStyle: {
        padding: "16px 8px",
        fontSize: "17px",
        fontWeight: 400,
      },
    },
    Button: {
      baseStyle: {
        minWidth: "150px",
        maxWidth: "400px",
        borderRadius: 0,
      },
      variants: {
        solid: () => ({
          bg: colors.appGreen,
          color: "white",
          boxShadow:
            "0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12)",
          _hover: {
            bg: "#3EA468",
            boxShadow:
              "0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.15)",
          },
        }),
        gray: () => ({
          bg: colors.gray5,
          color: "white",
          boxShadow:
            "0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12)",
          _hover: {
            bg: colors.gray5,
            boxShadow:
              "0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.15)",
          },
        }),
        grayOutline: () => ({
          bg: "transparent",
          color: colors.gray5,
          border: `1px solid ${colors.gray5}`,
        }),
      },
    },
  },
});
