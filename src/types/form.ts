// TODO: Move to main
export type RegisterFormValues = {
  email: string;
  username: string;
  password: string;
};

export type LoginFormValues = {
  email: string;
  password: string;
};
