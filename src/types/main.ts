// For types used only with the frontend.

import firebase from "firebase";
import { ValidationRule } from "react-hook-form";
import { PaymentMethod } from "@stripe/stripe-js";

// https://stackoverflow.com/questions/43159887/make-a-single-property-optional-in-typescript
type Optional<T, K extends keyof T> = Pick<Partial<T>, K> & Omit<T, K>;

export enum InputType {
  REGULAR_RADIO = "REGULAR_RADIO",
  IMAGE_RADIO = "IMAGE_RADIO",
  DROPDOWN = "DROPDOWN",
  TEXT = "TEXT",
}
export enum TopicType {
  PREP = "PREP",
  GROWING = "GROWING",
  PICKING = "PICKING",
  WITHERING = "WITHERING",
  ROLLING = "ROLLING",
  FERMENTING = "FERMENTING",
  DRYING = "DRYING",
  PACKAGING = "PACKAGING",
  BLENDING = "BLENDING",
  DISTRIBUTION = "DISTRIBUTION",
  CUSTOMER = "CUSTOMER",
  STORAGE = "STORAGE",
  ENDOFLIFE = "ENDOFLIFE",
}

export type Option = {
  value: string | number;
  label: string;
  disabled: boolean;
};

export type Answer = {
  key: number;
  value: string;
  score: number;
  imageUrl: string | null;
};

export type Question = {
  id: number;
  question: string;
  helperText: string;
  type: InputType;
  answers: Answer[];
  required: string | undefined | ValidationRule<boolean>;
};

export type DataModel = {
  id: number;
  section: string;
  date: string;
  meta: string;
  topic: TopicType;
  questions: Question[];
};

export type Student = {
  id: number;
};

export type GroupDocument = {
  name: string;
  size: number | null;
  users: Student[];
  industry: string;
  // score: number | null;
  sessions: number | null;
};

export type ResultDocument = {
  title: string;
  growingScore: number;
  pickingScore: number;
  witheringScore: number;
  rollingScore: number;
  fermentingScore: number;
  dryingScore: number;
  blendingScore: number;
  packagingScore: number;
  distributionScore: number;
  storageScore: number;
  customerScore: number;
  endOfLifeScore: number;
  productionScore: number;
  manufacturingScore: number;
  logisticsScore: number;
  otherScore: number;
  totalScore: number;
  date: firebase.firestore.Timestamp;
  imageUrl?: string;
};

export type NewsQuery = {
  articles: [
    {
      author: string | null;
      content: string;
      description: string;
      publishedAt: string;
      source: {
        id: string;
        name: string;
      };
      title: string;
      url: string;
      urlToImage: string;
    }
  ];
  status: string;
  totalResults: number;
};

export enum SubscriptionType {
  Education = "education",
  Enterprise = "enterprise",
}

export enum Frequency {
  Monthly = "monthly",
  Annual = "annual",
}

export enum Quantity {
  Small = "small",
  Medium = "medium",
  Large = "large",
}

export interface CostParameters {
  subscriptionType: SubscriptionType;
  numberOfSubscriptions: Quantity;
  frequency: Frequency;
}

export type CostData = {
  price: number;
  priceId: string | null;
};

export interface CalculateCostResponse {
  data: CostData;
  isLoading: boolean;
  error: Error | null;
}

export type Subscription = {
  cancel_at?: number;
  cancel_at_period_end?: boolean;
  canceled_at: number | null;
  created: number;
  current_period_end: number;
  current_period_start: number;
  default_payment_method: string;
  ended_at: number | null;
  id: string;
  start_date: number;
  status: string;
  frequency: Frequency;
  subscriptionType: SubscriptionType;
  quantity: Quantity;
};

export type Invoice = {
  paymentId: string;
  status: string;
  status_transitions: Record<string, number | null>;
  amount: number;
  frequency: Frequency;
  subscriptionType: SubscriptionType;
  quantity: Quantity;
  paymentMethod: PaymentMethod;
  invoice_pdf: string | null | undefined;
  invoice_id: string;
  invoice_number: string | null;
  cancel_at_period_end: boolean;
  current_period_end: number;
  subscriptionStatus: string;
  cancel_at: number | null;
};

export type InvoiceOptionalPayment = Optional<
  Invoice,
  "paymentId" | "status" | "amount"
>;

export type TaxLocation = {
  country: string;
  postalCode?: string;
};

export type PromoCode = {
  promoCode: string;
  amount_off?: number;
  percent_off?: number;
  subscriptionTypes: SubscriptionType[];
};
