// For types used with api gateway.

import firebase from "firebase";

export type firebaseUser = firebase.User;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type Observer = firebase.Observer<any, Error>;
export type QuerySnapshot<T> = firebase.firestore.QuerySnapshot<T>;
export type DocumentSnapshot<T> = firebase.firestore.DocumentSnapshot<T>;
export type DocumentReference = firebase.firestore.DocumentReference;
export type DocumentData = firebase.firestore.DocumentData;
export type CollectionReference = firebase.firestore.CollectionReference;
export type FirebaseError = firebase.FirebaseError;
export type FieldValue = firebase.firestore.FieldValue;
export type QueryDocumentSnapshot = firebase.firestore.QueryDocumentSnapshot;
export type FirestoreQuery<T> = firebase.firestore.Query<T>;
export type Timestamp = firebase.firestore.Timestamp;

// miSsuS types

export type apiLoginResponse = {
  organizationId: number;
  memberId: number;
};

export type organizationType = {
  accountType: number;
  email: string | void;
  industryId: number;
};

export type organizationResponseType = {
  data: [
    {
      id: number;
      questions: [];
      registration: string | null;
      createdAt: string;
      account_type: number;
      user_association: [];
      departments: [];
      sessions: [];
      email: string;
      host: string | null;
      profile: object | null;
      industry_id: number;
      questionnaires: [];
    }
  ];
};

export type organizationMember = {
  createdAt: string;
  departments: number[];
  email: string;
  id: number;
  organization: number;
  profile: {
    addresses: string[];
    contact_name: string;
    first_name: string;
    id: number;
    interested_in: string;
    job_title: number;
    last_name: string;
    phone_numbers: string;
    skills: string;
    url: string;
    user: number;
    user_id: number;
  } | null;
  questionnaires: number[];
  questions: number[];
  role: number;
  sessions: number[];
  teams: number[];
  teams_leader: number[];
};

export type orgMemberResponse = {
  data: organizationMember[];
};

export type departments = {
  code: string;
  createdAt: Date;
  id: number;
  name: string;
  sessions: [];
  teams: number[];
  updatedAt: Date;
  users: number[];
};

export type departmentListResponse = {
  data: departments[] | null;
};

export type NewDepartment = {
  code: string | null;
  emails: string[] | null;
  industryId: number | null;
  name: string | null;
  orgId: number | null;
};

export type NewDepartmentResponse = {
  code: string;
  createdAt: string;
  id: number;
  name: string;
  session: [];
  teams: [];
  updatedAt: string;
  users: [];
};

export type NewDepartmentResponseSchema = {
  schema: {
    name: string | null;
    code: string | null;
    teams: [];
    createdAt: string;
    users: [] | null;
    id: number;
    sessions: [];
    industry: number;
    updatedAt: string;
  };
  notRegistered: [];
};

export type teamInfo = {
  departmentId: number;
  description: string;
  emails: string[];
  name: string;
};

export type NewGroup = {
  departmentId: number;
  emails: string[];
  description: string | null;
  // leader: string | null;
  name: string | null;
};

export type NewClassMembers = {
  departmentId: number;
  emails: string[] | null;
};

export type newMemberObj = {
  teamId: number;
  userId: number;
};

export type newMembersObj = {
  emails: string[];
  organizationId: number;
  teamId: number;
};

export type userObj = {
  email: string;
  orgId: number;
  role: number;
};

export type deleteTeamMemberObj = {
  email: string;
  organizationId: number;
  teamId: number;
};

export type sessionObj = {
  organizationId: number;
  questionnaireId: number;
  score: number;
  status: number;
  teamId: number;
  userId: number;
};

export type classMember = {
  id: number;
  email: string;
};

export type deleteMemberObj = {
  departmentId: number;
  email: string;
};

export type classes = {
  code: string;
  createdAt: Date;
  id: number;
  name: string;
  sessions: [];
  teams: number[];
  updatedAt: Date;
  users: number[];
};

export type session = {
  departmentId: number | null;
  teamId: number | null;
  orgId: number;
  questionnaireId: number | null;
  userId: number | null;
};
