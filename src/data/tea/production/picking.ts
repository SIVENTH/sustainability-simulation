import { intl } from "utils";
import { TopicType, InputType } from "types";

export const picking = {
  // total possible score 3
  id: 3,
  section: intl.formatMessage({ id: "tea.picking.title" }),
  date: new Date().toDateString(),
  meta: intl.formatMessage({ id: "tea.picking.description" }),
  topic: TopicType.PICKING,
  questions: [
    {
      id: 12,
      question: intl.formatMessage({ id: "tea.picking.timing" }),
      helperText: intl.formatMessage({ id: "tea.picking.timing.helperText" }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.picking.earlySpring" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.picking.lateSpring" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "tea.picking.earlySummer" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "tea.picking.lateSummer" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 5,
          value: intl.formatMessage({ id: "tea.picking.earlyFall" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 6,
          value: intl.formatMessage({ id: "tea.picking.lateFall" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 7,
          value: intl.formatMessage({ id: "tea.picking.earlyWinter" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 8,
          value: intl.formatMessage({ id: "tea.picking.lateWinter" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 13,
      question: intl.formatMessage({ id: "tea.picking.frequency" }),
      helperText: intl.formatMessage({
        id: "tea.picking.frequency.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.picking.frequency.lessYear" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.picking.frequency.onceYear" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({
            id: "tea.picking.frequency.twiceYear",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "tea.picking.frequency.moreYear" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 14,
      question: intl.formatMessage({ id: "tea.picking.method" }),
      helperText: intl.formatMessage({ id: "tea.picking.method.helperText" }),
      type: InputType.IMAGE_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.picking.method.hand" }),
          score: 0,
          imageUrl:
            "https://firebasestorage.googleapis.com/v0/b/sustainability-sim.appspot.com/o/tea-sim-image%2Fby-hand.jpg?alt=media&token=96c544a0-865a-4a0f-b364-ad1dcfa4ccc1",
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.picking.method.machine" }),
          score: 1,
          imageUrl:
            "https://firebasestorage.googleapis.com/v0/b/sustainability-sim.appspot.com/o/tea-sim-image%2Fby-machine.jpg?alt=media&token=54f201e1-9e5e-4199-bba7-8e045f2a0985",
        },
      ],
    },
  ],
};
