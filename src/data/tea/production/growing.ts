import { intl } from "utils";
import { TopicType, InputType } from "types";

export const growing = {
  // total possible score 41
  id: 2,
  section: intl.formatMessage({ id: "tea.growing.title" }),
  date: new Date().toDateString(),
  meta: intl.formatMessage({ id: "tea.growing.description" }),
  topic: TopicType.GROWING,
  questions: [
    {
      id: 3,
      question: intl.formatMessage({ id: "tea.growing.terrace" }),
      helperText: intl.formatMessage({
        id: "tea.growing.terrace.helpertext",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 4,
      question: intl.formatMessage({ id: "tea.growing.transfer" }),
      helperText: intl.formatMessage({
        id: "tea.growing.transfer.helpertext",
      }),
      type: InputType.IMAGE_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.growing.transferLocation" }),
          score: 0,
          imageUrl:
            "https://firebasestorage.googleapis.com/v0/b/sustainability-sim.appspot.com/o/tea-sim-image%2Ftransfer.jpg?alt=media&token=a57928bf-7e8a-4295-b324-7f11695e1b8a",
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.growing.keepInLocation" }),
          score: 1,
          imageUrl:
            "https://firebasestorage.googleapis.com/v0/b/sustainability-sim.appspot.com/o/tea-sim-image%2Fkeep-in-nursery.jpg?alt=media&token=f826c8db-ee42-472b-97d7-f8c1568ad527",
        },
      ],
    },
    {
      id: 5,
      question: intl.formatMessage({ id: "tea.growing.nurseryLength" }),
      helperText: intl.formatMessage({
        id: "tea.growing.nurseryLength.helpertext",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        // TODO use map to interate through creating this.
        {
          key: 1,
          value: intl.formatMessage({
            id: "oneMonth",
          }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({
            id: "twoMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({
            id: "threeMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({
            id: "fourMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 5,
          value: intl.formatMessage({
            id: "fiveMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 6,
          value: intl.formatMessage({
            id: "sixMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 7,
          value: intl.formatMessage({
            id: "sevenMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 8,
          value: intl.formatMessage({
            id: "eightMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 9,
          value: intl.formatMessage({
            id: "nineMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 10,
          value: intl.formatMessage({
            id: "tenMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 11,
          value: intl.formatMessage({
            id: "elevenMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 12,
          value: intl.formatMessage({
            id: "twelveMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 13,
          value: intl.formatMessage({
            id: "thirteenMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 14,
          value: intl.formatMessage({
            id: "fourteenMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 15,
          value: intl.formatMessage({
            id: "fifteenMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 16,
          value: intl.formatMessage({
            id: "sixteenMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 17,
          value: intl.formatMessage({
            id: "seventeenMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 18,
          value: intl.formatMessage({
            id: "eighteenMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 19,
          value: intl.formatMessage({
            id: "nineteenMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 20,
          value: intl.formatMessage({
            id: "twentyMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 21,
          value: intl.formatMessage({
            id: "twentyOneMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 22,
          value: intl.formatMessage({
            id: "twentyTwoMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 23,
          value: intl.formatMessage({
            id: "twentyThreeMonths",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 24,
          value: intl.formatMessage({
            id: "twentyFourMonths",
          }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 6,
      question: intl.formatMessage({ id: "tea.growing.soilTests" }),
      helperText: intl.formatMessage({
        id: "tea.growing.soilTests.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 7,
      question: intl.formatMessage({ id: "tea.growing.method" }),
      helperText: intl.formatMessage({ id: "tea.growing.method.helperText" }),
      type: InputType.IMAGE_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.growing.method.fromSeeds" }),
          score: 0,
          imageUrl:
            "https://firebasestorage.googleapis.com/v0/b/sustainability-sim.appspot.com/o/tea-sim-image%2Ffrom-seeds.jpg?alt=media&token=a9f102cf-eb88-4b3c-8023-f434f913d9fe",
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.growing.method.fromCutting" }),
          score: 1,
          imageUrl:
            "https://firebasestorage.googleapis.com/v0/b/sustainability-sim.appspot.com/o/tea-sim-image%2Ffrom-cuttings.jpg?alt=media&token=40e1bafc-5550-4de4-90ef-3fb54d925583",
        },
      ],
    },
    {
      id: 8,
      question: intl.formatMessage({ id: "tea.growing.farmSize" }),
      helperText: intl.formatMessage({
        id: "tea.growing.farmSize.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "oneHectare" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "twoHectare" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "threeHectare" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "fourHectare" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 5,
          value: intl.formatMessage({ id: "fiveHectare" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 6,
          value: intl.formatMessage({ id: "sixHectare" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 7,
          value: intl.formatMessage({ id: "sevenHectare" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 8,
          value: intl.formatMessage({ id: "eightHectare" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 9,
      question: intl.formatMessage({ id: "tea.growing.pruningOrShaping" }),
      helperText: intl.formatMessage({
        id: "tea.growing.pruningOrShaping.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 10,
      question: intl.formatMessage({ id: "tea.growing.applyFertilizer" }),
      helperText: intl.formatMessage({
        id: "tea.growing.applyFertilizer.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 11,
      question: intl.formatMessage({ id: "tea.growing.fertilizerMethod" }),
      helperText: intl.formatMessage({
        id: "tea.growing.fertilizerMethod.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({
            id: "tea.growing.fertilizerMethod.notApplicable",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({
            id: "tea.growing.fertilizerMethod.scattering",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({
            id: "tea.growing.fertilizerMethod.root",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({
            id: "tea.growing.fertilizerMethod.mix",
          }),
          score: 1,
          imageUrl: null,
        },
      ],
    },
  ],
};
