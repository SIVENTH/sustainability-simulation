import { DataModel } from "types";
import { background } from "./other/background";
import { growing } from "./production/growing";
import { picking } from "./production/picking";
import { withering } from "./manufacturing/withering";
import { rolling } from "./manufacturing/rolling";
import { fermenting } from "./manufacturing/fermenting";
import { drying } from "./manufacturing/drying";
import { blending } from "./manufacturing/blending";
import { customer } from "./logistics/customer";
import { distribution } from "./logistics/distribution";
import { packaging } from "./logistics/packaging";
import { storage } from "./logistics/storage";
import { endOfLife } from "./other/endoflife";

export const tea: DataModel[] = [
  background,
  growing,
  picking,
  withering,
  rolling,
  fermenting,
  drying,
  blending,
  packaging,
  distribution,
  storage,
  customer,
  endOfLife,
];
