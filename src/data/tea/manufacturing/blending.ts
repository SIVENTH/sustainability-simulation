import { intl } from "utils";
import { TopicType, InputType } from "types";

export const blending = {
  // total possible score 3
  id: 8,
  section: intl.formatMessage({ id: "tea.blending.title" }),
  date: new Date().toDateString(),
  meta: intl.formatMessage({ id: "tea.blending.description" }),
  topic: TopicType.BLENDING,
  questions: [
    {
      id: 61,
      question: intl.formatMessage({ id: "tea.blending.blend" }),
      helperText: intl.formatMessage({ id: "tea.blending.blend.helperText" }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 62,
      question: intl.formatMessage({ id: "tea.blending.purpose" }),
      helperText: intl.formatMessage({ id: "tea.blending.purpose.helperText" }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.blending.aesthetics" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.blending.healthBenefits" }),
          score: 1,
          imageUrl: null,
        },
      ],
    },
    {
      id: 63,
      question: intl.formatMessage({ id: "tea.blending.additives" }),
      helperText: intl.formatMessage({
        id: "tea.blending.additives.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
  ],
};
