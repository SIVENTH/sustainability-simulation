import { intl } from "utils";
import { TopicType, InputType } from "types";

export const drying = {
  // total score 7
  id: 7,
  section: intl.formatMessage({ id: "tea.drying.title" }),
  date: new Date().toDateString(),
  meta: intl.formatMessage({ id: "tea.drying.description" }),
  topic: TopicType.DRYING,
  questions: [
    {
      id: 52,
      question: intl.formatMessage({ id: "tea.drying.method" }),
      helperText: intl.formatMessage({
        id: "tea.drying.method.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.drying.commercial" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.drying.oven" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "tea.drying.sun" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "tea.drying.charcoal" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 5,
          value: intl.formatMessage({ id: "tea.drying.floor" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 53,
      question: intl.formatMessage({ id: "tea.drying.reason" }),
      helperText: intl.formatMessage({
        id: "tea.drying.reason.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.drying.shelf" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.drying.flavor" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "other" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 54,
      question: intl.formatMessage({ id: "tea.drying.flavorMethod" }),
      helperText: intl.formatMessage({
        id: "tea.drying.flavorMethod.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.drying.finishFiring" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.drying.roasting" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 55,
      question: intl.formatMessage({ id: "tea.drying.temperature" }),
      helperText: intl.formatMessage({
        id: "tea.drying.temperature.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: "20C",
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: "25C",
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: "+30C",
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 56,
      question: intl.formatMessage({ id: "tea.drying.length" }),
      helperText: intl.formatMessage({
        id: "tea.drying.length.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: "0-1 week",
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: "1-2 weeks",
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: "2-3 weeks",
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: "+3 weeks",
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 57,
      question: intl.formatMessage({ id: "tea.drying.samplingInterval" }),
      helperText: intl.formatMessage({
        id: "tea.drying.samplingInterval.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: "20-30 Minutes",
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: "31-40 Minutes",
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: "41-50 Minutes",
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 58,
      question: intl.formatMessage({ id: "tea.drying.samplingFrequency" }),
      helperText: intl.formatMessage({
        id: "tea.drying.samplingFrequency.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: "1",
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: "2",
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: "3",
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: "4+",
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 59,
      question: intl.formatMessage({ id: "tea.drying.moistureLevel" }),
      helperText: intl.formatMessage({
        id: "tea.drying.moistureLevel.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: "2-4%",
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: "4.1-5%",
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: "5.1-+6%",
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 60,
      question: intl.formatMessage({ id: "tea.drying.moistureLevelCheck" }),
      helperText: intl.formatMessage({
        id: "tea.drying.moistureLevelCheck.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.drying.ir3000" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.drying.skz111c3" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "other" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
  ],
};
