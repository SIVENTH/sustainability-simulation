import { intl } from "utils";
import { TopicType, InputType } from "types";

export const rolling = {
  // total possible score 8
  id: 5,
  section: intl.formatMessage({ id: "tea.rolling.title" }),
  date: new Date().toDateString(),
  meta: intl.formatMessage({ id: "tea.rolling.description" }),
  topic: TopicType.ROLLING,
  questions: [
    {
      id: 32,
      question: intl.formatMessage({ id: "tea.rolling.method" }),
      helperText: intl.formatMessage({ id: "tea.rolling.method.helperText" }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.rolling.hand" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.rolling.machine" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 33,
      question: intl.formatMessage({ id: "tea.rolling.type" }),
      helperText: intl.formatMessage({ id: "tea.rolling.type.helperText" }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.rolling.soft" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.rolling.hard" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 34,
      question: intl.formatMessage({ id: "tea.rolling.length" }),
      helperText: intl.formatMessage({ id: "tea.rolling.length.helperText" }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.rolling.lessThanFive" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.rolling.moreThanFive" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 35,
      question: intl.formatMessage({ id: "tea.rolling.numberOfRepeats" }),
      helperText: intl.formatMessage({
        id: "tea.rolling.numberOfRepeats.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "one" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "two" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "three" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "four" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 5,
          value: intl.formatMessage({ id: "five" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 36,
      question: intl.formatMessage({ id: "tea.rolling.frequencyOfSamples" }),
      helperText: intl.formatMessage({
        id: "tea.rolling.frequencyOfSamples.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        { key: 1, value: "Every 30 minutes", score: 0, imageUrl: null },
        { key: 2, value: "Every 1 hour", score: 0, imageUrl: null },
        { key: 3, value: "Every 2 hours", score: 0, imageUrl: null },
        { key: 4, value: "Every 3 hours", score: 0, imageUrl: null },
        { key: 5, value: "Every 4 hours or more", score: 0, imageUrl: null },
      ],
    },
    {
      id: 37,
      question: intl.formatMessage({ id: "tea.rolling.numberOfSamples" }),
      helperText: intl.formatMessage({
        id: "tea.rolling.numberOfSamples.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "one" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "two" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "three" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "four" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 5,
          value: intl.formatMessage({ id: "five" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 38,
      question: intl.formatMessage({ id: "tea.rolling.temperature" }),
      helperText: intl.formatMessage({
        id: "tea.rolling.temperature.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 39,
      question: intl.formatMessage({ id: "tea.rolling.speed" }),
      helperText: intl.formatMessage({ id: "tea.rolling.speed.helperText" }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 40,
      question: intl.formatMessage({ id: "tea.rolling.shape" }),
      helperText: intl.formatMessage({ id: "tea.rolling.shape.helperText" }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.rolling.whole" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.rolling.notWhole" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 41,
      question: intl.formatMessage({ id: "tea.rolling.color" }),
      helperText: intl.formatMessage({ id: "tea.rolling.color.helperText" }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "green" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "beige" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "brown" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
  ],
};
