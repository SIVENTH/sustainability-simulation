import { intl } from "utils";
import { TopicType, InputType } from "types";

export const fermenting = {
  // total score
  id: 6,
  section: intl.formatMessage({ id: "tea.fermenting.title" }),
  date: new Date().toDateString(),
  meta: intl.formatMessage({ id: "tea.fermenting.description" }),
  topic: TopicType.FERMENTING,
  questions: [
    {
      id: 42,
      question: intl.formatMessage({ id: "tea.fermenting.fermentationDegree" }),
      helperText: intl.formatMessage({
        id: "tea.fermenting.fermentationDegree.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.fermenting.none" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.fermenting.semi" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "tea.fermenting.full" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "tea.fermenting.post" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 43,
      question: intl.formatMessage({ id: "tea.fermenting.fermentedLeaves" }),
      helperText: intl.formatMessage({
        id: "tea.fermenting.fermentedLeaves.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.fermenting.conventional" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.fermenting.modern" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 44,
      question: intl.formatMessage({ id: "tea.fermenting.location" }),
      helperText: intl.formatMessage({
        id: "tea.fermenting.location.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.fermenting.floor" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.fermenting.sheet" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "tea.fermenting.metalRack" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "tea.fermenting.glazedTiles" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 45,
      question: intl.formatMessage({ id: "tea.fermenting.machines" }),
      helperText: intl.formatMessage({
        id: "tea.fermenting.machines.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.fermenting.trolley" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.fermenting.drum" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "tea.fermenting.cfm" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 46,
      question: intl.formatMessage({ id: "tea.fermenting.temperature" }),
      helperText: intl.formatMessage({
        id: "tea.fermenting.temperature.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: "10C",
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: "20C",
          score: 1,
          imageUrl: null,
        },
        {
          key: 3,
          value: "30C",
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: "40C",
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 47,
      question: intl.formatMessage({ id: "tea.fermenting.length" }),
      helperText: intl.formatMessage({
        id: "tea.fermenting.length.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: "1 hour",
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: "2 hours",
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: "3 hours",
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: "4+ hours",
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 48,
      question: intl.formatMessage({ id: "tea.fermenting.samplingInterval" }),
      helperText: intl.formatMessage({
        id: "tea.fermenting.samplingInterval.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: "20-30 Minutes",
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: "31-40 Minutes",
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: "41-50 Minutes",
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 49,
      question: intl.formatMessage({ id: "tea.fermenting.samplingFrequency" }),
      helperText: intl.formatMessage({
        id: "tea.fermenting.samplingFrequency.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: "1 hour",
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: "2 hours",
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: "3 hours",
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: "4+ hours",
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 50,
      question: intl.formatMessage({ id: "tea.fermenting.color" }),
      helperText: intl.formatMessage({
        id: "tea.fermenting.color.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "lightGreen" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "darkGreen" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "yellow" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "lightRed" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 5,
          value: intl.formatMessage({ id: "darkRed" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 51,
      question: intl.formatMessage({ id: "tea.fermenting.flavor" }),
      helperText: intl.formatMessage({
        id: "tea.fermenting.flavor.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
  ],
};
