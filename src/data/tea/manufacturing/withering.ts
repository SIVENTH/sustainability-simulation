import { intl } from "utils";
import { TopicType, InputType } from "types";

export const withering = {
  // total possible score 45
  id: 4,
  section: intl.formatMessage({ id: "tea.withering.title" }),
  date: new Date().toDateString(),
  meta: intl.formatMessage({ id: "tea.withering.description" }),
  topic: TopicType.WITHERING,
  questions: [
    {
      id: 14,
      question: intl.formatMessage({ id: "tea.withering.method" }),
      helperText: intl.formatMessage({
        id: "tea.withering.method.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.withering.method.orthodox" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.withering.method.ctc" }),
          score: 1,
          imageUrl: null,
        },
      ],
    },
    {
      id: 15,
      question: intl.formatMessage({ id: "tea.withering.location" }),
      helperText: intl.formatMessage({
        id: "tea.withering.location.helperText",
      }),
      type: InputType.IMAGE_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.withering.location.room" }),
          score: 0,
          imageUrl:
            "https://firebasestorage.googleapis.com/v0/b/sustainability-sim.appspot.com/o/tea-sim-image%2Fdrying_factory.jpg?alt=media&token=b490aa26-f806-4d9a-9476-0c4c571c20a7",
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.withering.location.open" }),
          score: 1,
          imageUrl:
            "https://firebasestorage.googleapis.com/v0/b/sustainability-sim.appspot.com/o/tea-sim-image%2Fdrying_outdoor.jpg?alt=media&token=6b0c48f0-3b28-4c33-8332-066a417da178",
        },
      ],
    },
    {
      id: 16,
      question: intl.formatMessage({ id: "tea.withering.accessories" }),
      helperText: intl.formatMessage({
        id: "tea.withering.accessories.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({
            id: "tea.withering.accessories.tools",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({
            id: "tea.withering.accessories.natural",
          }),
          score: 1,
          imageUrl: null,
        },
      ],
    },
    {
      id: 17,
      question: intl.formatMessage({
        id: "tea.withering.temperatureExists",
      }),
      helperText: intl.formatMessage({
        id: "tea.withering.temperatureExists.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 1,
          imageUrl: null,
        },
      ],
    },
    {
      id: 18,
      question: intl.formatMessage({ id: "tea.withering.temperature" }),
      helperText: intl.formatMessage({
        id: "tea.withering.temperature.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        { key: 1, value: "10°C", score: 1, imageUrl: null },
        { key: 2, value: "15°C", score: 0, imageUrl: null },
        { key: 3, value: "20°C", score: 0, imageUrl: null },
        { key: 4, value: "25°C", score: 0, imageUrl: null },
        { key: 5, value: "30°C", score: 0, imageUrl: null },
        { key: 6, value: "35°C", score: 0, imageUrl: null },
      ],
    },
    {
      id: 19,
      question: intl.formatMessage({ id: "tea.withering.humidityExists" }),
      helperText: intl.formatMessage({
        id: "tea.withering.humidityExists.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 1,
          imageUrl: null,
        },
      ],
    },
    {
      id: 20,
      question: intl.formatMessage({ id: "tea.withering.humidity" }),
      helperText: intl.formatMessage({
        id: "tea.withering.humidity.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      // generate answer list for here
      answers: [
        { key: 1, value: "40%", score: 1, imageUrl: null },
        { key: 2, value: "50%", score: 0, imageUrl: null },
        { key: 3, value: "60%", score: 0, imageUrl: null },
        { key: 4, value: "70%", score: 0, imageUrl: null },
        { key: 5, value: "80%", score: 0, imageUrl: null },
      ],
    },
    {
      id: 21,
      question: intl.formatMessage({ id: "tea.withering.hardOrSoft" }),
      helperText: intl.formatMessage({
        id: "tea.withering.hardOrSoft.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.withering.hard" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.withering.soft" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 22,
      question: intl.formatMessage({ id: "tea.withering.chlorophyllCheck" }),
      helperText: intl.formatMessage({
        id: "tea.withering.chlorophyllCheck.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 23,
      question: intl.formatMessage({ id: "tea.withering.caffineCheck" }),
      helperText: intl.formatMessage({
        id: "tea.withering.caffineCheck.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 24,
      question: intl.formatMessage({ id: "tea.withering.flavorCheck" }),
      helperText: intl.formatMessage({
        id: "tea.withering.flavorCheck.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 25,
      question: intl.formatMessage({ id: "tea.withering.oxidationCheck" }),
      helperText: intl.formatMessage({
        id: "tea.withering.oxidationCheck.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 26,
      question: intl.formatMessage({ id: "tea.withering.moistureCheck" }),
      helperText: intl.formatMessage({
        id: "tea.withering.moistureCheck.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 27,
      question: intl.formatMessage({ id: "tea.withering.colorCheck" }),
      helperText: intl.formatMessage({
        id: "tea.withering.colorCheck.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 28,
      question: intl.formatMessage({
        id: "tea.withering.checkMethod.helperText",
      }),
      helperText: intl.formatMessage({
        id: "tea.withering.checkMethod.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.withering.machine" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.withering.manual" }),
          score: 1,
          imageUrl: null,
        },
      ],
    },
    {
      id: 29,
      question: intl.formatMessage({ id: "tea.withering.completionCheck" }),
      helperText: intl.formatMessage({
        id: "tea.withering.completionCheck.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.withering.weight" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.withering.flaccidity" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "tea.withering.aroma" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "tea.withering.other" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 30,
      question: intl.formatMessage({ id: "tea.withering.optimumWeight" }),
      helperText: intl.formatMessage({
        id: "tea.withering.optimumWeight.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      //  need to create list of grams
      answers: [
        { key: 1, value: "1 g", score: 1, imageUrl: null },
        { key: 2, value: "2 g", score: 0, imageUrl: null },
        { key: 3, value: "3 g", score: 0, imageUrl: null },
        { key: 4, value: "4 g", score: 0, imageUrl: null },
        { key: 5, value: "5 g", score: 0, imageUrl: null },
        { key: 6, value: "6 g", score: 0, imageUrl: null },
        { key: 7, value: "7 g", score: 0, imageUrl: null },
        { key: 8, value: "8 g", score: 0, imageUrl: null },
        { key: 9, value: "9 g", score: 0, imageUrl: null },
        { key: 10, value: "10 g", score: 0, imageUrl: null },
      ],
    },
    {
      id: 31,
      question: intl.formatMessage({ id: "tea.withering.length" }),
      helperText: intl.formatMessage({
        id: "tea.withering.length.helperText",
      }),
      type: InputType.DROPDOWN,
      // need  to create number of days
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "oneDay" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "twoDays" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "threeDays" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "fourDays" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 5,
          value: intl.formatMessage({ id: "fiveDays" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 6,
          value: intl.formatMessage({ id: "sixDays" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 7,
          value: intl.formatMessage({ id: "sevenDays" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
  ],
};
