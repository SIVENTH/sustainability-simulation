import { intl } from "utils";
import { TopicType, InputType } from "types";

export const storage = {
  // total possible score 6
  id: 11,
  section: intl.formatMessage({ id: "tea.storage.title" }),
  date: new Date().toDateString(),
  meta: intl.formatMessage({ id: "tea.storage.description" }),
  topic: TopicType.STORAGE,
  questions: [
    {
      id: 77,
      question: intl.formatMessage({ id: "tea.storage.storageRatio" }),
      helperText: intl.formatMessage({
        id: "tea.storage.storageRatio.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        { key: 1, value: "1:1", score: 1, imageUrl: null },
        { key: 2, value: "1:2", score: 0, imageUrl: null },
        { key: 3, value: "2:1", score: 1, imageUrl: null },
        { key: 4, value: "3:1", score: 1, imageUrl: null },
      ],
    },
    {
      id: 78,
      question: intl.formatMessage({ id: "tea.storage.temperature" }),
      helperText: intl.formatMessage({
        id: "tea.storage.temperature.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        { key: 1, value: "10-19C", score: 0, imageUrl: null },
        { key: 2, value: "20-29C", score: 1, imageUrl: null },
        { key: 3, value: "30-40C", score: 0, imageUrl: null },
      ],
    },
    {
      id: 79,
      question: intl.formatMessage({ id: "tea.storage.humidity" }),
      helperText: intl.formatMessage({
        id: "tea.storage.humidity.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        { key: 1, value: "50-59%", score: 0, imageUrl: null },
        { key: 2, value: "60-69%", score: 1, imageUrl: null },
        { key: 3, value: "70-79%", score: 0, imageUrl: null },
        { key: 4, value: "80-89%+", score: 0, imageUrl: null },
      ],
    },
    {
      id: 80,
      question: intl.formatMessage({ id: "tea.storage.length" }),
      helperText: intl.formatMessage({
        id: "tea.storage.length.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        { key: 1, value: "3 months", score: 0, imageUrl: null },
        { key: 2, value: "6 months", score: 0, imageUrl: null },
        { key: 3, value: "12 months or more", score: 1, imageUrl: null },
      ],
    },
    {
      id: 81,
      question: intl.formatMessage({ id: "tea.storage.finalStage" }),
      helperText: intl.formatMessage({
        id: "tea.storage.finalStage.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 82,
      question: intl.formatMessage({ id: "tea.storage.finalPackaging" }),
      helperText: intl.formatMessage({
        id: "tea.storage.finalPackaging.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.packaging.paper" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.packaging.plastic" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "tea.packaging.aluminum" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "other" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
  ],
};
