import { intl } from "utils";
import { TopicType, InputType } from "types";

export const packaging = {
  // total possible score 5
  id: 9,
  section: intl.formatMessage({ id: "tea.packaging.title" }),
  date: new Date().toDateString(),
  meta: intl.formatMessage({ id: "tea.packaging.description" }),
  topic: TopicType.PACKAGING,
  questions: [
    {
      id: 64,
      question: intl.formatMessage({ id: "tea.packaging.type" }),
      helperText: intl.formatMessage({ id: "tea.packaging.type.helperText" }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.packaging.loose" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.packaging.bags" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "other" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 65,
      question: intl.formatMessage({ id: "tea.packaging.final" }),
      helperText: intl.formatMessage({ id: "tea.packaging.final.helperText" }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.packaging.paper" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.packaging.plastic" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "tea.packaging.aluminum" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "other" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 66,
      question: intl.formatMessage({ id: "tea.packaging.shipping" }),
      helperText: intl.formatMessage({
        id: "tea.packaging.shipping.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 67,
      question: intl.formatMessage({ id: "tea.packaging.repackage" }),
      helperText: intl.formatMessage({
        id: "tea.packaging.repackage.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 1,
          imageUrl: null,
        },
      ],
    },
    {
      id: 68,
      question: intl.formatMessage({ id: "tea.packaging.capacity" }),
      helperText: intl.formatMessage({
        id: "tea.packaging.capacity.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: "Up to 99mg",
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: "100mg to 249mg",
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: "250mg to 499mg",
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: "more than 500mg",
          score: 1,
          imageUrl: null,
        },
      ],
    },
  ],
};
