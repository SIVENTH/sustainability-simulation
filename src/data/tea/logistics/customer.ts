import { intl } from "utils";
import { TopicType, InputType } from "types";

export const customer = {
  // total possible score 3
  id: 12,
  section: intl.formatMessage({ id: "tea.customer.title" }),
  date: new Date().toDateString(),
  meta: intl.formatMessage({ id: "tea.customer.description" }),
  topic: TopicType.CUSTOMER,
  questions: [
    {
      id: 83,
      question: intl.formatMessage({ id: "tea.customer.auction" }),
      helperText: intl.formatMessage({ id: "tea.customer.auction.helperText" }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 84,
      question: intl.formatMessage({ id: "tea.customer.buyers" }),
      helperText: intl.formatMessage({ id: "tea.customer.buyers.helperText" }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.customer.distributor" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.customer.manufacturer" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "tea.customer.retailer" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "other" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 85,
      question: intl.formatMessage({ id: "tea.customer.consumers" }),
      helperText: intl.formatMessage({
        id: "tea.customer.consumers.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 86,
      question: intl.formatMessage({ id: "tea.customer.volume" }),
      helperText: intl.formatMessage({
        id: "tea.customer.volume.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.customer.small" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.customer.bulk" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "tea.distribution.both" }),
          score: 1,
          imageUrl: null,
        },
      ],
    },
  ],
};
