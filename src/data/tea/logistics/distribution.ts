import { intl } from "utils";
import { TopicType, InputType } from "types";

export const distribution = {
  // total possible score 8
  id: 10,
  section: intl.formatMessage({ id: "tea.distribution.title" }),
  date: new Date().toDateString(),
  meta: intl.formatMessage({ id: "tea.distribution.description" }),
  topic: TopicType.DISTRIBUTION,
  questions: [
    {
      id: 69,
      question: intl.formatMessage({ id: "tea.distribution.length" }),
      helperText: intl.formatMessage({
        id: "tea.distribution.length.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        { key: 1, value: "Less Than 3 Months", score: 1, imageUrl: null },
        { key: 2, value: "Between 3 and 6 Months", score: 0, imageUrl: null },
        { key: 3, value: "Between 6 and 9 Months", score: 0, imageUrl: null },
        { key: 4, value: "More Than 9 Months", score: 0, imageUrl: null },
      ],
    },
    {
      id: 70,
      question: intl.formatMessage({ id: "tea.distribution.kind" }),
      helperText: intl.formatMessage({
        id: "tea.distribution.kind.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.distribution.normalContainer" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({
            id: "tea.distribution.airtightContainer",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({
            id: "tea.distribution.watertightContainer",
          }),
          score: 1,
          imageUrl: null,
        },
      ],
    },
    {
      id: 71,
      question: intl.formatMessage({ id: "tea.distribution.temperature" }),
      helperText: intl.formatMessage({
        id: "tea.distribution.temperature.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        { key: 1, value: "Less Than 10C", score: 1, imageUrl: null },
        { key: 2, value: "10-20C", score: 0, imageUrl: null },
        { key: 3, value: "More Than 20C", score: 0, imageUrl: null },
      ],
    },
    {
      id: 72,
      question: intl.formatMessage({ id: "tea.distribution.humidity" }),
      helperText: intl.formatMessage({
        id: "tea.distribution.humidity.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        { key: 1, value: "Less Than 60%", score: 0, imageUrl: null },
        { key: 2, value: "60 - 70%", score: 1, imageUrl: null },
        { key: 3, value: "More Than 70%", score: 0, imageUrl: null },
      ],
    },
    {
      id: 73,
      question: intl.formatMessage({ id: "tea.distribution.channel" }),
      helperText: intl.formatMessage({
        id: "tea.distribution.channel.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.distribution.supermarkets" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({
            id: "tea.distribution.specialityStores",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({
            id: "tea.distribution.convenienceStores",
          }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "tea.distribution.onlineStores" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 5,
          value: intl.formatMessage({
            id: "tea.distribution.wholesaleDistributors",
          }),
          score: 1,
          imageUrl: null,
        },
      ],
    },
    {
      id: 74,
      question: intl.formatMessage({ id: "tea.distribution.clients" }),
      helperText: intl.formatMessage({
        id: "tea.distribution.clients.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.distribution.commercial" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.distribution.residential" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "both" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 75,
      question: intl.formatMessage({ id: "tea.distribution.method" }),
      helperText: intl.formatMessage({
        id: "tea.distribution.method.helperText",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.distribution.aircrafts" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.distribution.trucks" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "tea.distribution.ships" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "tea.distribution.mixed" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 76,
      question: intl.formatMessage({ id: "tea.distribution.market" }),
      helperText: intl.formatMessage({
        id: "tea.distribution.market.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.distribution.domestic" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.distribution.foreign" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "both" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
  ],
};
