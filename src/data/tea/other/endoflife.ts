import { intl } from "utils";
import { TopicType, InputType } from "types";

export const endOfLife = {
  // total possible score1 10
  id: 13,
  section: intl.formatMessage({ id: "tea.endOfLife.title" }),
  date: new Date().toDateString(),
  meta: intl.formatMessage({ id: "tea.endOfLife.description" }),
  topic: TopicType.ENDOFLIFE,
  questions: [
    {
      id: 87,
      question: intl.formatMessage({ id: "tea.endOfLife.role" }),
      helperText: intl.formatMessage({ id: "tea.endOfLife.role.helperText" }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.endOfLife.manufacturer" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.endOfLife.consumer" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 88,
      question: intl.formatMessage({ id: "tea.endOfLife.cup" }),
      helperText: intl.formatMessage({ id: "tea.endOfLife.cup.helperText" }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.endOfLife.reusable" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.endOfLife.reusable" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "both" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "neither" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 89,
      question: intl.formatMessage({ id: "tea.endOfLife.kind" }),
      helperText: intl.formatMessage({ id: "tea.endOfLife.kind.helperText" }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.endOfLife.milk" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.endOfLife.noMilk" }),
          score: 1,
          imageUrl: null,
        },
      ],
    },
    {
      id: 90,
      question: intl.formatMessage({ id: "tea.endOfLife.water" }),
      helperText: intl.formatMessage({ id: "tea.endOfLife.water.helperText" }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.endOfLife.tapWater" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.endOfLife.storeWater" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "tea.endOfLife.brandWater" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 91,
      question: intl.formatMessage({ id: "tea.endOfLife.measure" }),
      helperText: intl.formatMessage({
        id: "tea.endOfLife.measure.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "yes" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "no" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 92,
      question: intl.formatMessage({ id: "tea.endOfLife.boil" }),
      helperText: intl.formatMessage({
        id: "tea.endOfLife.boil.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.endOfLife.electricStove" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.endOfLife.gasStove" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 93,
      question: intl.formatMessage({ id: "tea.endOfLife.dispose" }),
      helperText: intl.formatMessage({
        id: "tea.endOfLife.dispose.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.endOfLife.compost" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.endOfLife.trash" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 94,
      question: intl.formatMessage({ id: "tea.endOfLife.avoid" }),
      helperText: intl.formatMessage({
        id: "tea.endOfLife.avoid.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.endOfLife.inventoryControl" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.endOfLife.noInventoryControl" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 95,
      question: intl.formatMessage({ id: "tea.endOfLife.shelfLife" }),
      helperText: intl.formatMessage({
        id: "tea.endOfLife.shelfLife.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: "Before 1 Year",
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: "After 1 Year",
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 96,
      question: intl.formatMessage({ id: "tea.endOfLife.unbought" }),
      helperText: intl.formatMessage({
        id: "tea.endOfLife.unbought.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.endOfLife.donate" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.endOfLife.lowerPrice" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 97,
      question: intl.formatMessage({ id: "tea.endOfLife.disposeExpired" }),
      helperText: intl.formatMessage({
        id: "tea.endOfLife.disposeExpired.helperText",
      }),
      type: InputType.REGULAR_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.endOfLife.compost" }),
          score: 1,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.endOfLife.trash" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
  ],
};
