import { intl } from "utils";
import { TopicType, InputType } from "types";

export const background = {
  // total possible score 0
  id: 1,
  section: intl.formatMessage({ id: "tea.background.title" }),
  date: new Date().toDateString(),
  meta: intl.formatMessage({ id: "tea.background.description" }),
  topic: TopicType.PREP,
  questions: [
    {
      id: 1,
      question: intl.formatMessage({
        id: "tea.background.location",
      }),
      helperText: intl.formatMessage({
        id: "tea.background.location.helpertext",
      }),
      type: InputType.DROPDOWN,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "location.india" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "location.china" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "location.japan" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "location.singapore" }),
          score: 0,
          imageUrl: null,
        },
        {
          key: 5,
          value: intl.formatMessage({ id: "location.vietnam" }),
          score: 0,
          imageUrl: null,
        },
      ],
    },
    {
      id: 2,
      question: intl.formatMessage({ id: "tea.background.type" }),
      helperText: intl.formatMessage({
        id: "tea.background.type.helpertext",
      }),
      type: InputType.IMAGE_RADIO,
      required: intl.formatMessage({ id: "selectionRequired" }),
      answers: [
        {
          key: 1,
          value: intl.formatMessage({ id: "tea.dark" }),
          score: 0,
          imageUrl:
            "https://firebasestorage.googleapis.com/v0/b/sustainability-sim.appspot.com/o/tea-sim-image%2Fdark-tea.jpg?alt=media&token=6073e8f6-8e41-4cee-b58e-b1821d7e5d08",
        },
        {
          key: 2,
          value: intl.formatMessage({ id: "tea.green" }),
          score: 0,
          imageUrl:
            "https://firebasestorage.googleapis.com/v0/b/sustainability-sim.appspot.com/o/tea-sim-image%2Fgreen-tea.jpg?alt=media&token=2bf5b143-e5fa-4146-a213-352ae48c613c",
        },
        {
          key: 3,
          value: intl.formatMessage({ id: "tea.yellow" }),
          score: 0,
          imageUrl:
            "https://firebasestorage.googleapis.com/v0/b/sustainability-sim.appspot.com/o/tea-sim-image%2Fyellow-tea.jpg?alt=media&token=9b0bf322-d2a9-497c-bd4c-6812c3bbcc98",
        },
        {
          key: 4,
          value: intl.formatMessage({ id: "tea.oolong" }),
          score: 0,
          imageUrl:
            "https://firebasestorage.googleapis.com/v0/b/sustainability-sim.appspot.com/o/tea-sim-image%2Foolong-tea.jpg?alt=media&token=14e173ca-3c0c-4444-bb38-8df630297bd8",
        },
        {
          key: 5,
          value: intl.formatMessage({ id: "tea.black" }),
          score: 0,
          imageUrl:
            "https://firebasestorage.googleapis.com/v0/b/sustainability-sim.appspot.com/o/tea-sim-image%2Fblack-tea.jpg?alt=media&token=a345353c-3495-41f2-8fe4-79a79188562e",
        },
      ],
    },
  ],
};
