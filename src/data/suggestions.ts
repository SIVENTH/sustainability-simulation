export const suggestions = [
  {
    title: "PRODUCTION",
    id: 1,
    areas: [
      {
        id: 1,
        type: "Growing:",
        // eslint-disable-next-line sonarjs/no-duplicate-string
        text: "Under Development",
      },
      {
        id: 2,
        type: "Picking:",
        text: "Under Development",
      },
    ],
  },
  {
    title: "MANUFACTURING",
    id: 2,
    areas: [
      {
        id: 3,
        type: "Blending:",
        text: "Under Development",
      },
      {
        id: 4,
        type: "Drying:",
        text: " Under Development",
      },
      {
        id: 5,
        type: "Fermenting:",
        text: "Under Development",
      },
      {
        id: 6,
        type: "Rolling:",
        text: "Under Development",
      },
      {
        id: 7,
        type: "Withering:",
        text: "Under Development",
      },
    ],
  },
  {
    title: "Marketing & Distribution",
    id: 3,
    areas: [
      {
        id: 8,
        type: "",
        text: "Under Development",
      },
    ],
  },
  {
    title: "Customer / Storage",
    id: 4,
    areas: [
      {
        id: 9,
        type: "",
        text: "Under Development",
      },
    ],
  },
  {
    title: "End of Life",
    id: 5,
    areas: [
      {
        id: 10,
        type: "",
        text: "Under Development",
      },
    ],
  },
];
