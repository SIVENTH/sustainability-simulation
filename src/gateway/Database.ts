/* eslint-disable sonarjs/no-useless-catch */
import { Firebase } from "./Firebase";
import {
  QuerySnapshot,
  DocumentSnapshot,
  CollectionReference,
  DocumentReference,
  DocumentData,
} from "../types/gateway";

class Database extends Firebase {
  public async addOneDocument<T extends DocumentData>(
    path: string,
    data: T
  ): Promise<void> {
    await this.db.collection(path).add(data);
  }

  public async addDocumentData<T extends DocumentData>(
    path: string,
    data: T
  ): Promise<void> {
    await this.db.doc(path).set(data, { merge: true });
  }

  public getCollectionReference(path: string): CollectionReference {
    return this.db.collection(path);
  }

  public getDocumentReference(path: string): DocumentReference {
    return this.db.doc(path);
  }

  public async getDocuments<T>(path: string): Promise<T[]> {
    try {
      const querySnapshot = (await this.db
        .collection(path)
        .get()) as QuerySnapshot<T>;
      if (querySnapshot.empty) throw new Error("Collection does not exists");
      const documents: T[] = [];
      querySnapshot.forEach((doc) =>
        documents.push({ id: doc.id, ...doc.data() })
      );
      return documents;
    } catch (error) {
      throw error;
    }
  }

  public async getOneDocument<T extends { [key: string]: string }>(
    path: string
  ): Promise<T> {
    try {
      const documentSnapshot = (await this.db
        .doc(path)
        .get()) as DocumentSnapshot<T>;
      if (!documentSnapshot.exists) throw new Error("Document does not exist");
      return { ...documentSnapshot.data() } as T;
    } catch (error) {
      throw error;
    }
  }
}

export const db = new Database();
