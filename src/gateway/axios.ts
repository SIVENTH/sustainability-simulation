import axios from "axios";

axios.defaults.baseURL = "https://api.siventh.com/api/v1";

if (sessionStorage.getItem("token")) {
  axios.defaults.headers.common["Authorization"] =
    sessionStorage.getItem("token");
}

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    if (error.response.status === 401) {
      sessionStorage.removeItem("token");
      window.location.href = "/login";
    }
    return Promise.reject(error);
  }
);
