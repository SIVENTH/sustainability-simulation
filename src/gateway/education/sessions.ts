import "../axios";
import axios from "axios";
import { sessionObj } from "types";

export const createSession = async (
  sessionObj: sessionObj
): Promise<void | unknown> => {
  try {
    return axios.post("/session", sessionObj);
  } catch (error) {
    return error;
  }
};
