import axios from "axios";

import { teamInfo, classes } from "types";

// DEPARTMENTS or CLASSES
/**
 * Get all classes/departments in an organization
 * @method getAllOrganizationDepartments
 * @param {number} organization The id for the organization to retrieve departments/classes for
 * @return {object} returns deparment/class list as object or error
 */
export const getAllOrganizationDepartments = async (
  organization: string | null
): Promise<classes[] | unknown> => {
  try {
    return await axios.get(`/departments?organization=${organization}`);
  } catch (error) {
    return error;
  }
};

// TEAMS/GROUP
// Creates a new team
export const createNewTeam = async (
  teamInfo: teamInfo
): Promise<void | unknown> => {
  try {
    await axios.post("/team", teamInfo);
    return;
  } catch (error) {
    return error;
  }
};
