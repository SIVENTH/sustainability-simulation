import app from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
// this should be fixed now
const firebaseConfig = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MSG_SENDER_ID,
  appId: process.env.REACT_APP_ADD_ID,
  measurementId: process.env.REACT_APP_MEASUREMENT_ID,
};

app.initializeApp(firebaseConfig);

export class Firebase {
  protected db: app.firestore.Firestore;
  protected auth: app.auth.Auth;

  constructor() {
    this.db = app.firestore();
    this.auth = app.auth();
  }

  public getServerTimestamp(): app.firestore.Timestamp {
    return app.firestore.FieldValue.serverTimestamp() as app.firestore.Timestamp;
  }
}
