import firebase from "firebase";
import LRU from "lru-cache";

import { CostParameters, CostData } from "types";

const calculateCostFirebase = firebase
  .functions()
  .httpsCallable("calculateCost");
export const getSubscriptions = firebase
  .functions()
  .httpsCallable("getSubscriptions");
export const getSubscription = firebase
  .functions()
  .httpsCallable("getSubscription");
export const getPaymentHistory = firebase
  .functions()
  .httpsCallable("getPaymentHistory");
export const createSetupIntent = firebase
  .functions()
  .httpsCallable("createSetupIntent");
export const createStripeSubscription = firebase
  .functions()
  .httpsCallable("createStripeSubscription");
export const getPaymentMethods = firebase
  .functions()
  .httpsCallable("getPaymentMethods");
export const toggleAutomaticRenewal = firebase
  .functions()
  .httpsCallable("toggleAutomaticRenewal");
export const cancelSubscription = firebase
  .functions()
  .httpsCallable("cancelSubscription");
export const updatePaymentMethod = firebase
  .functions()
  .httpsCallable("updatePaymentMethod");
export const updateCardInfo = firebase
  .functions()
  .httpsCallable("updateCardInfo");
export const updateCustomerAddress = firebase
  .functions()
  .httpsCallable("updateCustomerAddress");
export const checkPromoCode = firebase
  .functions()
  .httpsCallable("checkPromoCode");

const calculateCostCache = new LRU({ max: 100 });

export const calculateCost = async (
  costParameters: CostParameters
): Promise<CostData> => {
  const key = JSON.stringify(costParameters);
  const cachedValue = calculateCostCache.get(key) as CostData;

  if (cachedValue) return cachedValue;

  const result = await calculateCostFirebase(costParameters);
  calculateCostCache.set(key, result.data);
  return result.data;
};
