/* eslint-disable @typescript-eslint/no-non-null-assertion */
/* eslint-disable @typescript-eslint/no-non-null-asserted-optional-chain */
import { Firebase } from "./Firebase"; // this should be fixed now
import { Observer } from "../types/gateway";
import { getOrgIdAndMemberIdOnLogin } from "./services";

type NewUserCreds = {
  email: string;
  username: string;
  password: string;
};

type AuthedUserCreds = Omit<NewUserCreds, "username">;

type UserInfo = {
  email: string;
  username: string;
  uid: string;
};

class Auth extends Firebase {
  public async registerUser(creds: NewUserCreds): Promise<void> {
    const newUser = await this.auth.createUserWithEmailAndPassword(
      creds.email,
      creds.password
    );

    await newUser.user?.updateProfile({ displayName: creds.username });
    const userInfo = {
      email: newUser.user?.email!,
      username: newUser.user?.displayName!,
      uid: newUser.user?.uid!,
    };
    await this.saveUser(userInfo);
  }

  private async saveUser(user: UserInfo): Promise<void> {
    const userColRef = this.db.doc(`users/${user.uid}`);
    await userColRef.set({ ...user });
  }

  public async loginUser(creds: AuthedUserCreds): Promise<void> {
    await this.auth.signInWithEmailAndPassword(creds.email, creds.password);
    await getOrgIdAndMemberIdOnLogin();
  }

  public async logout(): Promise<void> {
    await this.auth.signOut();
  }

  public async resetPassword(email: string): Promise<void> {
    await this.auth.sendPasswordResetEmail(email);
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  public getAuthListener(callback: Observer | any, error: any): () => void {
    return this.auth.onAuthStateChanged(callback, error);
  }
}

export const auth = new Auth();
