/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * Provides the base services class
 * @module Services
 */
import axios from "axios";

import { organizationType, organizationResponseType } from "gateway";
import {
  NewDepartment,
  NewDepartmentResponse,
  NewDepartmentResponseSchema,
  NewGroup,
  NewClassMembers,
  teamInfo,
  userObj,
  newMemberObj,
  deleteTeamMemberObj,
  newMembersObj,
  organizationMember,
  orgMemberResponse,
  classMember,
  deleteMemberObj,
  classes,
  session,
  apiLoginResponse,
} from "types"; // remove gateway/education/types file later

const errorHandler = (source, error) => {
  console.log(`[SERVICES/${source}]error message: `, error.message);
  console.log(`[SERVICES/${source}]response message:`, error.data.message);
};

// TODO: CHECK ALL RETURN TYPES AGAIN
/**
 * Used to create an organization
 * @method postOrganization
 * @param {object} organization The data for the oganization to create
 * @return {object} returns organization object or error
 */
export const postOrganization = async (
  organization: organizationType
): Promise<organizationResponseType | unknown> => {
  try {
    return await axios.post("/org", organization);
  } catch (error) {
    errorHandler("postOrganization", error);
    return error;
  }
};

export const getOrgIdAndMemberIdOnLogin = async (): Promise<
  apiLoginResponse | unknown
> => {
  try {
    if (
      axios.defaults.headers.common.Authorization !==
      sessionStorage.getItem("token")
    ) {
      axios.defaults.headers.common.Authorization =
        sessionStorage.getItem("token");
    }
    return await axios.get("/login");
  } catch (error) {
    return error;
  }
};

export const getAssociation = async (): Promise<
  organizationResponseType | unknown
> => {
  try {
    return await axios.get("/org");
  } catch (error) {
    errorHandler("getAssociation", error);
    return error;
  }
};

// get all user's associations (orgs)
export const getAllUserAssociations = async (
  userId: string | null
): Promise<organizationResponseType | unknown> => {
  try {
    return await axios.get(`/user_associations?user=${userId}`);
  } catch (error) {
    errorHandler("getAllUserAssociations", error);
    return error;
  }
};

export const getOrganization = async (
  organizationId: string | null
): Promise<organizationResponseType | unknown> => {
  try {
    return await axios.get(`/org?organization=${organizationId}`);
  } catch (error: any) {
    errorHandler("getOrganization", error);
    return error;
  }
};

/**
 * Get all classes/departments in an organization
 * @method getAllOrganizationDepartments
 * @param {number} organization The id for the organization to retrieve departments/classes for
 * @return {object} returns deparment/class list as object or error
 */
export const getAllOrganizationDepartments = async (
  organization: string | null
): Promise<classes[] | unknown> => {
  try {
    return await axios.get(`/departments?organization=${organization}`);
  } catch (error) {
    return error;
  }
};

// DEPARTMENTS or CLASSES
/**
 * Creates a new class/department
 * @method addNewDepartment
 * @param {object} body Contains code, emails as array, name, and orgId
 * @return {object} returns created deparment/class list as object or error
 */
export const addNewDepartment = async (
  body: NewDepartment
): Promise<NewDepartmentResponseSchema | unknown> => {
  try {
    return await axios.post("/department", body);
  } catch (error) {
    errorHandler("addNewDepartment", error);
    return error;
  }
};

/**
 * Get the department/class
 * @method getDepartmentById
 * @param {object} id The id for the department/class to retrieve
 * @return {object} returns deparment/class as object or error
 */
export const getDepartmentById = async (
  id: number
): Promise<NewDepartmentResponse | unknown> => {
  try {
    return await axios.get(`/department?department=${id}`);
  } catch (error) {
    errorHandler("getDepartmentById", error);
    return error;
  }
};

/**
 * Get all members of the class
 * @method getAllClassMembers
 * @param {number} departmentId Id of the department/class
 * @return {object} list of all the members of the class
 */

export const getAllClassMembers = async (
  departmentId: number
): Promise<classMember[]> => {
  try {
    const res = await axios.get(`/dep-member?departmentId=${departmentId}`);
    return res.data;
  } catch (error) {
    errorHandler("getAllClassMembers", error);
    return [];
  }
};

export const addDepartmentMembers = async (
  body: NewClassMembers
): Promise<void | unknown> => {
  try {
    await axios.post("/dep-member", body);
    return;
  } catch (error) {
    errorHandler("addClassMembers", error);
    return error;
  }
};

/**
 * Remove member from class/department
 * @method deleteDepartmentMember
 * @param {object} body user's email and id for the department/class
 * @return {object} returns sucess or error
 */
export const deleteDepartmentMember = async (
  body: deleteMemberObj
): Promise<void | unknown> => {
  try {
    await axios.delete("/dep-member", { data: body });
    return;
  } catch (error) {
    errorHandler("deleteDepartmentMember", error);
    return error;
  }
};

/**
 * Retrieve all teams based on department id
 * @method getAllDepartmentTeams
 * @param {string} departmentId The departmentid to get teams from
 * @return {object} returns array of teams or error
 */
export const getAllDepartmentTeams = async (
  departmentId: string
): Promise<teamInfo[] | unknown> => {
  try {
    return await axios.get(`/teams?department=${departmentId}`);
  } catch (error) {
    errorHandler("getAllDepartmentTeams", error);
    return error;
  }
};
export const getAllUserTeams = async (
  memberId: string | null
): Promise<teamInfo[] | unknown> => {
  try {
    return await axios.get(`/all-user-teams?user=${memberId}`);
  } catch (error) {
    return error;
  }
};

// deletes a department/class
export const deleteDepartment = async (deleteDepartment: {
  departmentId: number;
}): Promise<void | unknown> => {
  try {
    await axios.delete("/department", { data: deleteDepartment });
    return;
  } catch (error) {
    errorHandler("deleteDepartment", error);
    return error;
  }
};

// TEAMS OR GROUPS
/**
 * Adds a new team to a department
 * @method addTeam
 * @param {object} body Contains all necessary parameters for creating a group/team
 * @param {number} body.departmentId The id for the department/class to create the group in
 * @param {array} body.emails The list of emails to use for the group. At least 1 email is required.
 * @param {string} body.[description] The description of the team
 * @param {string} body.[leader] The email address for the leader. A random leader will be assigned if not listed.
 * @param {string} body.[name] The name of the team/group
 * @return {object} returns group as object or error
 */
export const addTeam = async (body: NewGroup): Promise<teamInfo | unknown> => {
  try {
    return await axios.post("/team", body);
  } catch (error) {
    errorHandler("addTeam", error);
    return error;
  }
};

// Change leader of a team
export const assignLeader = async (
  memberObj: newMemberObj
): Promise<void | unknown> => {
  try {
    await axios.patch("/teams", memberObj);
    return;
  } catch (error) {
    errorHandler("assignLeader", error);
    return error;
  }
};

// Add new member/s to a team
export const addNewMembersToTeam = async (
  members: newMembersObj
): Promise<void | unknown> => {
  try {
    await axios.post("/team-member", members);
    return;
  } catch (error) {
    errorHandler("addNewMembersToTeam", error);
    return error;
  }
};

// Removes a member from a team
export const removeMemberFromTeam = async (
  member: deleteTeamMemberObj
): Promise<void | unknown> => {
  try {
    await axios.delete("/team-member", { data: member });
    return;
  } catch (error) {
    errorHandler("removeMemberFromTeam", error);
    return error;
  }
};

// USERS OR STUDENTS
// Returns all members of an organization
export const getAllOrganizationMembers = async (
  organizationId: string
): Promise<orgMemberResponse | unknown> => {
  try {
    return await axios.get(`/users?orgId=${organizationId}`);
  } catch (error) {
    errorHandler("getAllOrganizationMembers", error);
    return error;
  }
};

// Adds a new user to an organization
export const addUserToOrganization = async (
  userObj: userObj
): Promise<void | unknown> => {
  try {
    await axios.post("/user", userObj);
    return;
  } catch (error) {
    errorHandler("addUserToOrganization", error);
    return error;
  }
};

// Returns one member of an organization
export const getOneOrganizationMember = async (
  orgId: number,
  userId: number
): Promise<organizationMember | unknown> => {
  try {
    return await axios.get(`/user?orgId=${orgId}&userId=${userId}`);
  } catch (error) {
    errorHandler("getOneOrganizationMember", error);
    return error;
  }
};

// QUESTIONNAIRE
/**
 * Retrieve all questionnaires
 * @method getQuestionnaires
 * @return {object} returns array of questionnaires or error
 */
export const getQuestionnaires = async (): Promise<any | unknown> => {
  try {
    return await axios.get("/questionnaire");
  } catch (error) {
    errorHandler("getQuestionnaire", error);
    return error;
  }
};

// QUESTION
/**
 * Retrieve all questions for a specific questionnaire
 * @method getQuestions
 * @param {number} questionnaireId The questionnaire id to get questions for
 * @return {object} returns array of questions or error
 */
export const getQuestions = async (
  questionnaireId: number
): Promise<any | unknown> => {
  try {
    return await axios.get(`/question?questionnaire=${questionnaireId}`);
  } catch (error) {
    errorHandler("getQuestions", error);
    return error;
  }
};

// SESSION
/**
 * Creates and returns a Session (start of a simulation). If no team or user is provided, the session respondent will be the organization.
 * @method postSession
 * @param {object} body The body of the post request containing the below components.
 * @param {number} body.orgId The organization id
 * @param {number} body.questionnaireId The department id
 * @param {number} body.[departmentId] Tdhe department id
 * @param {number} body.[teamId] The group/team id
 * @param {number} body.[userId] The group/team id
 * @return {object} returns array of questions or error
 */
export const postSession = async (body: session): Promise<any | unknown> => {
  try {
    return await axios.post("/session", body);
  } catch (error) {
    errorHandler("postSession", error);
    return error;
  }
};
