import styled from "styled-components";

import { colors } from "utils";

type Props = {
  authedUser?: boolean;
};

export const StyledGridMainOuter = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 80px 1.8fr;
  gap: 40px 40px 40px;
  grid-template-areas:
    "TopHeader"
    "Content";
`;
export const StyledGridMainInner = styled.div<Props>`
  background-color: #fafafa;
  /* background-color: #f3f5fc; */
  grid-area: Content;
  display: grid;
  grid-template-columns: ${(props) =>
    props.authedUser ? " 240px 1fr 296px" : "30px 1fr 30px"};
  grid-template-columns: 240px 1fr 296px;
  grid-template-columns: 240px 1fr;
  grid-template-rows: 1.8fr;
  gap: 40px 40px 40px;
  grid-template-areas: "Nav MainContent";
`;

export const StyledGridTopNav = styled.div`
  grid-area: TopHeader;
`;

export const StyledGridSideNav = styled.div<Props>`
  position: relative;
  background-color: #ffffff;
  grid-area: Nav;
  border-right: 1px solid ${colors.concrete};
  display: ${(props) => (props.authedUser ? "block" : "none")};
  border-right: 1px solid #efefef;
`;

export const StyledGridContent = styled.div`
  grid-area: MainContent;
  height: 100%;
  position: relative;
`;
