import styled from "styled-components";

import { colors } from "utils";

type Props = {
  value: string;
};

export const ProgressContainer = styled.div`
  display: flex;
  width: 100%;
`;

export const BarContainer = styled.div`
  display: block;
  height: 4px;
  width: 100%;
  background-color: ${colors.lightGreen};
  border-radius: 40px;
  margin: 16px 8px 4px 0;
`;

export const Bar = styled.div<Props>`
  display: block;
  height: 100%;
  width: ${(props) => props.value};
  background-color: ${colors.green2};
  border-radius: 40px;
`;

export const ProgressText = styled.div`
  display: block;
  color: #000;
`;
