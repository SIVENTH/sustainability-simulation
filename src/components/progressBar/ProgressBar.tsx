import {
  ProgressContainer,
  BarContainer,
  Bar,
  ProgressText,
} from "./ProgressBar.styled";

interface Props {
  value: number;
}

export const ProgressBar = ({ value }: Props): JSX.Element => {
  return (
    <ProgressContainer>
      <BarContainer>
        <Bar value={`${value}%`} />
      </BarContainer>
      <ProgressText>{value}</ProgressText>
    </ProgressContainer>
  );
};
