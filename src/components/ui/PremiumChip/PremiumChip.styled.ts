import styled from "styled-components";

export const StyledPremiumBadge = styled.span`
  background-color: #ffc300;
  color: #000;
  font-size: 10px;
  padding: 4px;
  margin-left: 5px;
  border-radius: 4px;
  line-height: 1;
`;
