import { FormattedMessage } from "react-intl";
import { StyledPremiumBadge } from "./PremiumChip.styled";

export const PremiumChip = (): JSX.Element => {
  return (
    <StyledPremiumBadge>
      <FormattedMessage id="premium" />
    </StyledPremiumBadge>
  );
};
