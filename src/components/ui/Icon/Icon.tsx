interface IconProps {
  icon: string;
  className?: string;
}

const Icon: React.FC<IconProps> = ({ icon, className }) => {
  return (
    <svg className={className}>
      <path d={icon} />
    </svg>
  );
};

export default Icon;
