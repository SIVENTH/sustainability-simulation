import styled from "styled-components";

interface ModalBodyProps {
  isOrganizationModal?: boolean;
}

export const ModalBlock = styled.div`
  align-items: center;
  bottom: 0;
  justify-content: center;
  left: 0;
  overflow: hidden;
  padding: 0.4rem;
  position: fixed;
  right: 0;
  top: 0;
  display: flex;
  opacity: 1;
  z-index: 400;
  height: 500;
`;

export const ModalOverlay = styled.div`
  background: rgba(247, 248, 249, 0.75);
  bottom: 0;
  cursor: default;
  display: block;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
`;

export const ModalContainer = styled.div`
  background: #ffffff;
  border-radius: 8px;
  display: flex;
  flex-direction: column;
  max-height: 550px;
  max-width: 481px;
  padding: 32px;
  width: 100%;
  z-index: 1;
  box-shadow: 0 0.2rem 0.5rem rgba(48, 55, 66, 0.3);
`;

export const StyledHeaderText = styled.h3`
  font-size: 24px;
  font-weight: 900;
  color: #000000;
  margin-bottom: 12px;

  & + div > div {
    margin: 0;
    margin-bottom: 24px;
  }
`;

export const ModalTitle = styled.span`
  font-size: 30px;
  font-weight: 500;
`;

export const ModalBody = styled.div<ModalBodyProps>`
  overflow-y: ${({ isOrganizationModal }) =>
    isOrganizationModal ? "visible" : "auto"};
  position: relative;
`;

export const StyledInput = styled.input`
  width: 100%;
  border-bottom: 1px solid #cbcbcb;
  padding-bottom: 4px;
  margin-bottom: 24px;
  background-color: transparent;
  &:focus {
    outline: none;
  }
`;

export const StyledTextArea = styled.textarea`
  margin: 12px 0px 24px 0px;
  border-radius: 4px;
  width: 100%;
  border: 1px solid #cbcbcb;
  padding-bottom: 4px;
  background-color: transparent;
  &:focus {
    outline: gray 2px solid;
  }
`;
export const StyledCaption = styled.p`
  color: #606060;
  font-size: 12px;
`;

export const ModalSaveButton = styled.button`
  width: 100%;
  height: 40px;
  padding: 8px 24px;
  border-radius: 4px;
  background-color: #149860;
  color: #ffffff;
  margin-top: 10px;
`;

export const ModalSaveButtonSecondary = styled.button`
  width: 100%;
  height: 40px;
  padding: 8px 24px;
  border-radius: 4px;
  border-style: solid;
  border-width: 1px;
  border-color: #149860;
  color: #149860;
  background-color: #ffffff;
  margin-top: 10px;
`;

export const StyledSettingsButton = styled.img`
  height: 24px;
  width: 24px;
  padding: 3px;
`;

export const StyledButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 32px;
  width: 64px;
  font-size: 12px;
  border-width: 1px;
  border-radius: 5px;
  color: gray;
  border-color: #b3b3b3;
  cursor: pointer;
  margin: auto;
`;

export const ButtonText = styled.p`
  font-size: 16px;
  font-weight: 500;
`;
