import React, { Fragment } from "react";
import {
  ModalBlock,
  ModalBody,
  ModalContainer,
  ModalOverlay,
  ModalTitle,
} from "./index";

interface Modal {
  hideModal: () => void;
  open: boolean;
  children: React.ReactNode;
  title: string;
  isOrganizationModal?: boolean;
}

export const ModalParent = ({
  title,
  children,
  open,
  hideModal,
  isOrganizationModal,
}: Modal): JSX.Element => {
  return (
    <Fragment>
      {open && (
        <ModalBlock>
          <ModalOverlay onClick={hideModal}></ModalOverlay>
          <ModalContainer>
            <ModalTitle>{title}</ModalTitle>
            <ModalBody isOrganizationModal={isOrganizationModal}>
              {children}
            </ModalBody>
          </ModalContainer>
        </ModalBlock>
      )}
    </Fragment>
  );
};
