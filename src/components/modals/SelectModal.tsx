import { FormattedMessage } from "react-intl";
import { intl } from "utils";
import { Form } from "../form";
import { useNavigate } from "react-router-dom";
import {
  useDisclosure,
  Modal,
  ModalFooter,
  Button,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  ModalProps,
} from "@chakra-ui/react";

type Selection = { value: number; label: string; disabled: boolean };
const simOptions: Selection[] = [
  {
    value: 1,
    label: intl.formatMessage({ id: "supplyChains.tea" }),
    disabled: false,
  },
  {
    value: 2,
    label: intl.formatMessage({ id: "supplyChains.coffee" }),
    disabled: true,
  },
  {
    value: 3,
    label: intl.formatMessage({ id: "supplyChains.apparel" }),
    disabled: true,
  },
];

interface Props {
  pathname: string;
  state?: { from: string };
}

export const SelectModal = ({
  pathname, // change from pathname to path based on value selected for simulations
  state,
  ...modalProps
}: Props & Partial<ModalProps>): JSX.Element => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const navigate = useNavigate();

  const handleSubmit = () => {
    if (state) {
      navigate(pathname, { state });
      return;
    }
    navigate(pathname);
  };
  const placeholder = intl.formatMessage({ id: "analysisChoice.placeholder" });

  return (
    <>
      <Button onClick={onOpen}>
        <FormattedMessage id="free" />
      </Button>
      <Modal
        blockScrollOnMount={false}
        isOpen={isOpen}
        onClose={onClose}
        {...modalProps}
      >
        <ModalOverlay />
        <Form<Selection> onSubmit={handleSubmit}>
          <ModalContent>
            <ModalHeader as="h1">
              <FormattedMessage id="analysisChoice.title" />
            </ModalHeader>
            <ModalCloseButton />
            <ModalBody>
              <Form.Select<Selection>
                name="sim_selection"
                options={simOptions}
                isRequired
                placeholder={placeholder}
                rules={{ required: true }}
              />
            </ModalBody>
            <ModalFooter>
              <Form.Button type="submit" title="Submit" />
            </ModalFooter>
          </ModalContent>
        </Form>
      </Modal>
    </>
  );
};
