import {
  StyledOrderTotal,
  StyledSummaryLabel,
  StyledSummaryValue,
} from "./OrderTotal.styled";
import { FormattedMessage } from "react-intl";

import { useCostForm } from "context";
import { PromoCode, SubscriptionType } from "types";

const TaxRow = ({ amount }: { amount: string }) => (
  <>
    <StyledSummaryLabel>
      <FormattedMessage id="tax" />
      (10%)
    </StyledSummaryLabel>
    <StyledSummaryValue>¥{amount}</StyledSummaryValue>
  </>
);

const calculateDiscount = (
  originalCost: number,
  promoCode: PromoCode | null,
  subscriptionType: SubscriptionType
): number => {
  if (!promoCode || !promoCode.subscriptionTypes.includes(subscriptionType))
    return originalCost;

  if (promoCode.amount_off)
    return Math.max(0, originalCost - promoCode.amount_off);

  if (promoCode.percent_off)
    return Math.round((originalCost * (100 - promoCode.percent_off)) / 100);

  return originalCost;
};

export const OrderTotal = (): JSX.Element => {
  const { cost, taxLocation, promoCode, costParameters } = useCostForm();
  // TODO: implement error
  // const { isLoading } = cost;
  const { country } = taxLocation;
  const isTaxable = country === "JP";
  const taxRate = isTaxable ? 0.1 : 0;

  const discountedPrice = calculateDiscount(
    cost.data.price,
    promoCode,
    costParameters.subscriptionType
  );

  return (
    <StyledOrderTotal>
      <StyledSummaryLabel>
        <FormattedMessage id="subtotal" />
      </StyledSummaryLabel>
      {cost.data.price && (
        <StyledSummaryValue>¥{cost.data.price}</StyledSummaryValue>
      )}
      {promoCode && (
        <>
          <StyledSummaryLabel>Discounted Rate</StyledSummaryLabel>
          <StyledSummaryValue>¥{discountedPrice}</StyledSummaryValue>
        </>
      )}

      {isTaxable && <TaxRow amount={(discountedPrice * taxRate).toFixed(0)} />}

      <StyledSummaryLabel>
        <FormattedMessage id="total" />:
      </StyledSummaryLabel>
      <StyledSummaryValue>
        ¥{(discountedPrice * (1 + taxRate)).toFixed(0)}
      </StyledSummaryValue>
    </StyledOrderTotal>
  );
};
