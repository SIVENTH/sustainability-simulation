import { useRef, memo } from "react";
import { useNavigate } from "react-router-dom";
import { Flex, Button } from "../ChakraReplace.styled";
import { StyledFlexDiv, StyledText } from "./NewOrderCard.styled";
import { FormattedMessage } from "react-intl";

import { SubscriptionType } from "types";
import { labels, colors } from "utils";

import { SubscriptionTypeIcon } from "components/billing";

interface SubscriptionNumberInputProps {
  selected?: boolean;
  subscriptionType: SubscriptionType;
}

const SubscriptionNumberInput = ({
  selected,
  subscriptionType,
}: SubscriptionNumberInputProps) => {
  const navigate = useNavigate();

  return (
    <Flex>
      <Button
        // isDisabled={false}
        width="120px"
        borderRadius="0px 4px 4px 0px"
        backgroundColor={selected ? colors.appGreen : colors.gray5}
        bgHover="colors.appGreen"
        onClick={() =>
          navigate(`/billing/confirmation?type=${subscriptionType}`)
        }
      >
        <FormattedMessage id="purchase" />
      </Button>
    </Flex>
  );
};

interface NewOrderCardWrapperProps {
  subscriptionType: SubscriptionType;
  selected?: boolean;
  setSelected: (subscriptionType: SubscriptionType | null) => void;
  inputRef: React.RefObject<HTMLElement>;
  children: React.ReactNode;
}

const NewOrderCardWrapper = ({
  subscriptionType,
  setSelected,
  inputRef,
  children,
}: NewOrderCardWrapperProps) => (
  <StyledFlexDiv
    onClick={() => {
      setSelected(subscriptionType);
      inputRef.current && inputRef.current.focus();
    }}
    role="group"
  >
    {children}
  </StyledFlexDiv>
);

interface NewOrderCardProps {
  subscriptionType: SubscriptionType;
  selected?: boolean;
  setSelected: (subscriptionType: SubscriptionType | null) => void;
}

export const NewOrderCard = ({
  subscriptionType,
  selected,
  setSelected,
}: NewOrderCardProps): JSX.Element => {
  const inputRef = useRef<HTMLInputElement>(null);

  return (
    <NewOrderCardWrapper
      subscriptionType={subscriptionType}
      selected={selected}
      setSelected={setSelected}
      inputRef={inputRef}
    >
      <StyledText>{labels[subscriptionType]}</StyledText>
      <SubscriptionNumberInput
        selected={selected}
        subscriptionType={subscriptionType}
      />
      <SubscriptionTypeIcon
        subscriptionType={subscriptionType}
        selected={selected}
        selectable
      />
    </NewOrderCardWrapper>
  );
};

export default memo(NewOrderCard);
