// Billing Overview Left Side
import { useNavigate } from "react-router-dom";
import { Link, Box } from "../ChakraReplace.styled";
import { FormattedMessage } from "react-intl";
import { StyledBillingLeftContainer } from "pages/billing-overview-page/BillingCard.styled";
import { colors } from "utils";
import { SubscriptionDetailsForm } from "components/billing";
import { CardDetails } from "components/billing";

export const BillingCard = (): JSX.Element => {
  const navigate = useNavigate();

  return (
    <StyledBillingLeftContainer>
      <h3>
        <FormattedMessage id="orderDetails" />
      </h3>
      <SubscriptionDetailsForm></SubscriptionDetailsForm>
      <CardDetails></CardDetails>
      <Box align="right" p="8px" justifyContent="flex-end">
        <Link
          onClick={() => navigate(-1)}
          fontSize="12px"
          color={colors.appGreen}
        >
          <FormattedMessage id="previousPage" />
        </Link>
      </Box>
    </StyledBillingLeftContainer>
  );
};
