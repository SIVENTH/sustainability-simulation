import styled from "styled-components";

// type Props = {
//   width?: string;
// };

export const StyledPaymentMethod = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #e1f4e8;
  font-size: 24px;
  height: 56px;
  padding: 16px 24px;
  margin-top: 24px;
  border-radius: 8px;
`;
export const StyledSubscriptionPlan = styled.div`
  display: flex;
`;

export const StyledBillingDetails = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  gap: 16px;

  .chakra-select__wrapper {
    margin-left: 0;

    > select {
      border-radius: 8px;
    }
  }
`;

export const StyledBillingCardDetails = styled.div`
  display: flex;

  .chakra-form__label {
    margin-top: 24px;
  }

  input,
  select {
    height: 56px;
    border-radius: 8px;
  }
`;

export const StyledCCDetails = styled.div`
  margin-top: 24px;

  .StripeElement {
    height: 56px;
    border-radius: 8px;
  }
`;
