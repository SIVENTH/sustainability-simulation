import { useRef, useState, useCallback } from "react";

import { useCostForm } from "context";
import { checkPromoCode } from "gateway";
import { PromoCode } from "types";

import {
  StyledPromoContainer,
  StyledSummaryLabel,
} from "./PromoCodeInput.styled";

interface PromoCodeResult extends Omit<PromoCode, "promoCode"> {
  couponExists: boolean;
}

const PromoCodeInput = (): JSX.Element => {
  const promoCodeRef = useRef<HTMLInputElement>(null);
  const { setPromoCode } = useCostForm();

  const [loading, setLoading] = useState(false);

  const onClickApply = useCallback(async () => {
    if (!promoCodeRef.current || !promoCodeRef.current.value) return;

    setLoading(true);

    const promoCode = promoCodeRef.current.value;

    try {
      const { data } = await checkPromoCode({
        promoCode,
      });

      const { couponExists, amount_off, percent_off, subscriptionTypes } =
        data as PromoCodeResult;

      if (!couponExists) throw Error("Coupon does not exist");

      setPromoCode({
        promoCode,
        amount_off,
        percent_off,
        subscriptionTypes,
      });

      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  }, [promoCodeRef]);

  return (
    <StyledPromoContainer>
      <StyledSummaryLabel>Promo code</StyledSummaryLabel>
      <input ref={promoCodeRef} placeholder="Insert promo code" />
      <button disabled={loading} onClick={onClickApply}>
        Apply
      </button>
    </StyledPromoContainer>
  );
};

export default PromoCodeInput;
