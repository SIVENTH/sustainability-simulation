import styled from "styled-components";

// type Props = {
//   width?: string;
// };

export const StyledPromoContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: baseline;
  grid-column: span 2;

  input {
    height: 56px;
    border-radius: 8px;
    border: 1px solid #e2e8f0;
    padding-left: 8px;
  }
`;
export const StyledSummaryLabel = styled.p`
  font-size: 16px;
`;
