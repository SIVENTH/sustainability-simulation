// Billing overview page right side Order Summary down to Promo

import { Box } from "../../components/ChakraReplace.styled";
import { FormattedMessage } from "react-intl";
import {
  StyledOrderSummary,
  StyledSummaryLabel,
  StyledSummaryValue,
  StyledLicensePer,
  StyledLicensePerCost,
} from "./OrderDetails.styled";

import { useCostForm } from "context";
import { labels } from "utils";
import PromoCodeInput from "./PromoCodeInput";

export const OrderDetails = (): JSX.Element => {
  const { cost, costParameters } = useCostForm();
  // TODO: implement error removed "isLoading"
  const { data } = cost;
  const { subscriptionType, frequency, numberOfSubscriptions } = costParameters;

  return (
    <StyledOrderSummary>
      <StyledSummaryLabel>
        <FormattedMessage id="subscriptionPeriod" />
      </StyledSummaryLabel>
      <StyledSummaryValue>{labels[frequency]}</StyledSummaryValue>
      <StyledSummaryLabel>
        <FormattedMessage id="subscriptionPlan" />
      </StyledSummaryLabel>
      <StyledSummaryValue>{labels[subscriptionType]}</StyledSummaryValue>
      <Box>
        <StyledLicensePer>
          <StyledSummaryLabel>
            <FormattedMessage id="licenses" />
          </StyledSummaryLabel>
          <StyledLicensePerCost>
            <FormattedMessage
              id="billing.perItemPrice"
              values={{
                cost: `¥${data.price / Number(labels[numberOfSubscriptions])}`,
              }}
            />
          </StyledLicensePerCost>
        </StyledLicensePer>
      </Box>
      <StyledSummaryValue>{labels[numberOfSubscriptions]}</StyledSummaryValue>
      <PromoCodeInput />
    </StyledOrderSummary>
  );
};
