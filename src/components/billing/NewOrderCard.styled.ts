import styled from "styled-components";

// type Props = {
//   width?: string;
// };

export const StyledFlexDiv = styled.div`
  width: 328px;
  height: 166px;
  border: 1px solid #e3e3e3;
  border-radius: 8px;
  padding: 16px;
  flex-direction: column;
  justify-content: space-between;
  position: relative;
  background-color: #fff;
  &:hover {
    background-color: #e3e3e3;
    cursor: pointer;
  }
`;

export const StyledText = styled.p`
  font-size: 24px;
  font-weight: 600;
`;

export const StyledPurchaseButton = styled.button`
  width: 120px;
  border-radius: 0 4px 4px 0;
  background-color: #b3b3b3;
  padding: 0 16px;
  color: #fff;
  box-shadow: 0 2px 5px 0 rgb(0 0 0 / 16%), 0 2px 10px 0 rgb(0 0 0 / 12%);

  &hover {
    background-color: #3ea468;
    box-shadow: 0 5px 11px 0 rgb(0 0 0 / 18%), 0 4px 15px 0 rgb(0 0 0 / 15%);
  }
`;
