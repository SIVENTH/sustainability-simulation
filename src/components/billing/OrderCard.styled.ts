import styled from "styled-components";

export const StyledOrderSummary = styled.div`
  background-color: #ffffff;
  width: 100%;
  padding: 24px;
  border-radius: 8px;

  h3 {
    font-size: 24px;
  }

  Button {
    background-color: #b3b3b3;
    color: #ffffff;
    height: 40px;
    margin-top: 24px;
    padding: 0 24px;
    float: right;

    &:hover {
      background-color: #149860;
    }
  }
`;
