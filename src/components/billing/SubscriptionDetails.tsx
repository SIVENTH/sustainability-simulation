import { TextProps } from "@chakra-ui/react";
import {
  Box,
  Text,
  SimpleGrid,
  Link,
} from "../../components/ChakraReplace.styled";
import { FormattedMessage } from "react-intl";

import { InvoiceOptionalPayment } from "types";
import { labels, colors } from "utils";

import { getPaymentMethodText, getSubscriptionStatusText, intl } from "utils";

const PdfLink = ({ invoice_pdf }: { invoice_pdf: string }) => (
  <Box>
    <Text fontSize="12px" fontWeight={700}>
      <FormattedMessage id="invoice" />
    </Text>
    <Link
      href={invoice_pdf}
      fontSize="16px"
      fontWeight={600}
      color={colors.blue1}
      p="0"
    >
      <FormattedMessage id="download" />
    </Link>
  </Box>
);

interface SubscriptionDataProps extends TextProps {
  label: string;
  value: string | null;
}

const SubscriptionData = ({
  label,
  value,
  ...textProps
}: SubscriptionDataProps) => (
  <Box>
    <Text fontSize="12px" fontWeight={700}>
      {label}
    </Text>
    <Text fontSize="16px" fontWeight={600} {...textProps}>
      {value}
    </Text>
  </Box>
);

interface SubscriptionDetailsProps {
  invoice: InvoiceOptionalPayment;
}

export const SubscriptionDetails = ({
  invoice,
}: SubscriptionDetailsProps): JSX.Element => (
  <SimpleGrid columns={3} spacingY="16px">
    <SubscriptionData
      label={intl.formatMessage({
        id: "subscriptionPlan",
      })}
      value={labels[invoice.subscriptionType]}
      fontSize="24px"
    />
    <SubscriptionData
      label={intl.formatMessage({
        id: "transactionId",
      })}
      value={invoice.paymentId ? invoice.paymentId.slice(3) : "No Payment"}
    />
    <SubscriptionData
      label={intl.formatMessage({
        id: "paymentMethod",
      })}
      value={getPaymentMethodText(invoice.paymentMethod)}
    />
    <Box width="300px">
      <Text>{getSubscriptionStatusText(invoice)}</Text>
    </Box>
    <SubscriptionData
      label={intl.formatMessage({ id: "idNumber" })}
      value={invoice.invoice_number}
    />
    {invoice.invoice_pdf && <PdfLink invoice_pdf={invoice.invoice_pdf} />}
  </SimpleGrid>
);
