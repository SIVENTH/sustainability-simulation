// Billing overview page right side Order summary card
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { OrderTotal, OrderDetails } from "components/billing";
import { Button } from "../ChakraReplace.styled";
import { StyledOrderSummary } from "./OrderCard.styled";
import { PaymentMethodCreateParams } from "@stripe/stripe-js";
import { FormattedMessage } from "react-intl";

import { useCostForm } from "context";
import { useNewSubscription, NewSubscriptionFunction } from "hooks/stripe";
import { TaxLocation } from "types";

const useOnClickAddSubscription = (
  priceId: string | null,
  isSubmitting: boolean,
  setIsSubmitting: (isSubmitting: boolean) => void,
  addSubscription: NewSubscriptionFunction,
  selectedPaymentMethod: string,
  billingDetails: PaymentMethodCreateParams.BillingDetails,
  taxLocation: TaxLocation,
  promoCode: string | null
) => {
  const navigate = useNavigate();

  return async () => {
    if (!priceId) return;
    if (isSubmitting) return;
    setIsSubmitting(true);
    const { error, subscriptionId } = await addSubscription(
      priceId,
      billingDetails,
      taxLocation,
      promoCode,
      selectedPaymentMethod
    );
    setIsSubmitting(false);
    if (error) alert(error);
    else navigate(`/billing/success?subscriptionId=${subscriptionId}`);
  };
};

interface OrderCardContentsProps {
  onClick: React.MouseEventHandler<HTMLButtonElement>;
  disabled?: boolean;
}

const OrderCardContents = ({ onClick, disabled }: OrderCardContentsProps) => (
  <StyledOrderSummary>
    <h3>
      <FormattedMessage id="orderSummary" />
    </h3>
    <OrderDetails></OrderDetails>
    <OrderTotal></OrderTotal>
    <Button onClick={onClick} disabled={disabled}>
      <FormattedMessage id="submitOrder" />
    </Button>
  </StyledOrderSummary>
);

export const OrderCard = (): JSX.Element => {
  const {
    selectedPaymentMethod,
    cost,
    billingDetails,
    taxLocation,
    promoCode,
  } = useCostForm();
  const { priceId } = cost.data;

  const [isSubmitting, setIsSubmitting] = useState(false);

  const addSubscription = useNewSubscription();

  const onClickAddSubscription = useOnClickAddSubscription(
    priceId,
    isSubmitting,
    setIsSubmitting,
    addSubscription,
    selectedPaymentMethod,
    billingDetails,
    taxLocation,
    promoCode && promoCode.promoCode
  );

  return (
    <OrderCardContents
      onClick={onClickAddSubscription}
      disabled={cost.isLoading || isSubmitting}
    />
  );
};
