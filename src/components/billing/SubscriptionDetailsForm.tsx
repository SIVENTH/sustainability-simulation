import { useState } from "react";
import { Spacer, FormLabel } from "@chakra-ui/react";
import { Flex } from "../../components/ChakraReplace.styled";
import {
  StyledSubscriptionFrequency,
  StyledSubscriptionType,
  StyledLabel,
  StyledLicense,
} from "./SubscriptionDetailsForm.styled";
import { useDebouncedCallback } from "use-debounce";
import { useFormContext } from "react-hook-form";
import { FormattedMessage } from "react-intl";

import { SubscriptionType, Frequency, CostParameters, Quantity } from "types";

import { Form } from "components/form";
import { useCostForm } from "context";
import { useDefaultFormValues, labels, intl } from "utils";

type Option<T> = {
  value: T;
  label: string;
  disabled: boolean;
};

const subscriptionTypeOptions = [
  {
    value: SubscriptionType.Education,
    label: labels[SubscriptionType.Education],
    disabled: false,
  },
  {
    value: SubscriptionType.Enterprise,
    label: labels[SubscriptionType.Enterprise],
    disabled: false,
  },
];

const frequencyOptions = [
  {
    value: Frequency.Monthly,
    label: labels[Frequency.Monthly],
    disabled: false,
  },
  {
    value: Frequency.Annual,
    label: labels[Frequency.Annual],
    disabled: false,
  },
];

const quantityOptions = [
  {
    value: Quantity.Small,
    label: labels[Quantity.Small],
    disabled: false,
  },
  {
    value: Quantity.Medium,
    label: labels[Quantity.Medium],
    disabled: false,
  },
  {
    value: Quantity.Large,
    label: labels[Quantity.Large],
    disabled: false,
  },
];

interface OnSubmitProp {
  onSubmit: (parameters: CostParameters) => void;
}

const SubscriptionTypeSelect = ({ onSubmit }: OnSubmitProp) => {
  const { setValue, handleSubmit } = useFormContext();
  const { costParameters } = useCostForm();

  // Use state in order to have an initial value and be able to update the select value instantly.
  // Possibly try to find a better way to do this later.
  const [selectVal, setSelectVal] = useState<SubscriptionType>(
    costParameters.subscriptionType
  );

  // Subscription Type
  return (
    <StyledSubscriptionType>
      <StyledLabel>
        <FormattedMessage id="subscriptionPlan" />
      </StyledLabel>
      <Form.Select<Option<SubscriptionType>>
        name="subscriptionType"
        options={subscriptionTypeOptions}
        isRequired
        placeholder={intl.formatMessage({
          id: "selectSubscriptionPlan",
        })}
        fontSize="xs"
        fontWeight="light"
        size="sm"
        rules={{ required: true }}
        onChange={(e) => {
          setValue("subscriptionType", e.target.value);
          setSelectVal(e.target.value as SubscriptionType);
          handleSubmit((data: CostParameters) => onSubmit(data))();
        }}
        value={selectVal}
      />
    </StyledSubscriptionType>
  );
};

const NumberOfSubscriptionsSelect = ({ onSubmit }: OnSubmitProp) => {
  const { setValue, handleSubmit } = useFormContext();
  const { costParameters } = useCostForm();

  // Use state in order to have an initial value and be able to update the select value instantly.
  // Possibly try to find a better way to do this later.
  const [selectVal, setSelectVal] = useState<Quantity>(
    costParameters.numberOfSubscriptions
  );

  // License Selection part
  return (
    <StyledLicense>
      <StyledLabel>
        <FormattedMessage id="licenses" />
      </StyledLabel>
      <Form.Select<Option<Quantity>>
        name="numberOfSubscriptions"
        options={quantityOptions}
        isRequired
        placeholder={intl.formatMessage({
          id: "selectLicenses",
        })}
        fontSize="xs"
        fontWeight="light"
        size="sm"
        rules={{ required: true }}
        onChange={(e) => {
          setValue("numberOfSubscriptions", e.target.value);
          setSelectVal(e.target.value as Quantity);
          handleSubmit((data: CostParameters) => onSubmit(data))();
        }}
        value={selectVal}
      />
    </StyledLicense>
  );
};

// Subscription type Monthly or Annual
const FrequencySelect = ({ onSubmit }: OnSubmitProp) => {
  const { setValue, handleSubmit } = useFormContext();

  return (
    <StyledSubscriptionFrequency>
      <FormLabel>
        <FormattedMessage id="subscriptionPeriod" />
      </FormLabel>
      <Form.Radio<Option<Frequency>>
        name="frequency"
        options={frequencyOptions}
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        onChange={(frequency: any) => {
          setValue("frequency", frequency);
          handleSubmit((data: CostParameters) => onSubmit(data))();
        }}
      />
    </StyledSubscriptionFrequency>
  );
};

export const SubscriptionDetailsForm = (): JSX.Element => {
  const { setCostParameters } = useCostForm();

  const onSubmit = (values: CostParameters) =>
    setCostParameters({
      ...values,
    });

  const debouncedSubmit = useDebouncedCallback(onSubmit, 500);

  const defaultValues = useDefaultFormValues();

  return (
    <Form<CostParameters>
      onSubmit={debouncedSubmit}
      defaultValues={defaultValues}
    >
      <Flex>
        <SubscriptionTypeSelect onSubmit={debouncedSubmit} />
        <Spacer />
        <NumberOfSubscriptionsSelect onSubmit={debouncedSubmit} />
      </Flex>
      <FrequencySelect onSubmit={debouncedSubmit} />
    </Form>
  );
};
