import { memo } from "react";
import { Box } from "../../components/ChakraReplace.styled";

import { SubscriptionType } from "types";
import { colors } from "utils";

import { BiBuilding as Building } from "react-icons/bi";
import { FaBookReader as Reader } from "react-icons/fa";

interface IconProps {
  subscriptionType: SubscriptionType;
  selected?: boolean;
  selectable?: boolean;
}

const Icon = ({
  subscriptionType,
  selected,
  selectable,
}: IconProps): JSX.Element => (
  <Box position="absolute" top="16px" right="16px">
    <Box
      width="48px"
      height="48px"
      borderRadius="24px"
      backgroundColor={
        selected || !selectable ? colors.lightGreen : colors.lightGray
      }
      // _groupHover={
      //   selectable ? { backgroundColor: colors.lightGreen } : undefined
      // }
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      {subscriptionType === SubscriptionType.Education && (
        <Reader size={20} color={colors.appGreen} />
      )}
      {subscriptionType === SubscriptionType.Enterprise && (
        <Building size={22} color={colors.appGreen} />
      )}
    </Box>
  </Box>
);

export const SubscriptionTypeIcon = memo(Icon);
