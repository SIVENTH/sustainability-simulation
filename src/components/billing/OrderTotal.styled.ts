import styled from "styled-components";

export const StyledOrderTotal = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 24px;
  margin-top: 24px;
  width: 100%;
`;

export const StyledSummaryLabel = styled.p`
  font-size: 16px;
`;

export const StyledSummaryValue = styled.p`
  font-size: 16px;
  font-weight: 600;
  color: #149860;
  text-align: right;
`;
