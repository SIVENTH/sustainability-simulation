import { FormLabel } from "@chakra-ui/react";
import { useFormContext } from "react-hook-form";

import { Form } from "components/form";

import { Option } from "types";

interface BillingDetailsSelectProps<T> {
  name: string;
  label: string;
  onSubmit: (parameters: T) => void;
  options: Option[];
  isRequired?: boolean;
}

const BillingDetailsSelect = <T,>({
  name,
  label,
  options,
  onSubmit,
  isRequired,
}: BillingDetailsSelectProps<T>): JSX.Element => {
  const { setValue, getValues } = useFormContext();

  return (
    <div role="group">
      <FormLabel>{label}</FormLabel>
      <Form.Select
        name={name}
        isRequired={!!isRequired}
        rules={{ required: true }}
        options={options}
        onChange={(e) => {
          setValue(name, e.target.value);
          const values = getValues() as T;
          onSubmit({ ...values });
        }}
      />
    </div>
  );
};

export default BillingDetailsSelect;
