// Billing page order details left side
import { RadioGroup, Radio } from "@chakra-ui/react";
import { Flex, Text, Stack } from "../../components/ChakraReplace.styled";

import { CardElement } from "@stripe/react-stripe-js";
import { PaymentMethod, PaymentMethodCreateParams } from "@stripe/stripe-js";
import { colors } from "utils";

import { usePaymentMethods } from "hooks/stripe";
import { useCostForm } from "context";
import { GrCreditCard as CreditCard } from "react-icons/gr";

import { CardIcon } from "components/billing";
import { Form } from "components/form";
import BillingDetailsInput from "./BillingDetailsInput";
import BillingDetailsSelect from "./BillingDetailsSelect";

import { getPaymentMethodText, intl } from "utils";
// import { TaxLocation } from "types";

import "./cardElement.css";
import {
  StyledPaymentMethod,
  StyledBillingDetails,
  StyledBillingCardDetails,
  StyledCCDetails,
} from "./CardDetails.styled";

// TODO: add country options
const countryOptions = [
  {
    label: intl.formatMessage({
      id: "location.japan",
    }),
    value: "JP",
    disabled: false,
  },
  {
    label: intl.formatMessage({
      id: "location.usa",
    }),
    value: "US",
    disabled: false,
  },
];

const getOptions = (paymentMethods: PaymentMethod[]) =>
  paymentMethods.map((paymentMethod) => ({
    value: paymentMethod.id,
    label: getPaymentMethodText(paymentMethod),
    brand: paymentMethod.card?.brand,
  }));

// Top Level Radio Option
interface TopRadioOptionProps {
  isChecked: boolean;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  label: string;
}

// Credit card radio payment method
const TopRadioOption = ({
  isChecked,
  onChange,
  label,
}: TopRadioOptionProps) => (
  <StyledPaymentMethod>
    <Radio isChecked={isChecked} onChange={onChange}>
      <Text fontSize="16px" fontWeight={600}>
        {label}
      </Text>
    </Radio>
    <CreditCard />
  </StyledPaymentMethod>
);

// Save this Credit Carde Radio
interface SavedCreditCardProps {
  value: string;
  label: string;
  brand: string | undefined;
  isLast: boolean;
}

const SavedCreditCard = ({ label, value, brand }: SavedCreditCardProps) => (
  <Flex
    alignItems="center"
    justifyContent="space-between"
    height="80px"
    px="24px"
    // borderBottom={!isLast ? `1px solid ${colors.gray4}` : ""}
  >
    <Radio value={value}>{label}</Radio>
    <Flex
      width="48px"
      height="32px"
      backgroundColor="{colors.concrete}"
      alignItems="center"
      justifyContent="center"
    >
      <CardIcon brand={brand} color={colors.gray4} />
    </Flex>
  </Flex>
);

interface SavedPaymentMethodsProps {
  selectedPaymentMethod: string;
  setSelectedPaymentMethod: (paymentMethod: string) => void;
  paymentMethods: PaymentMethod[];
  handleChange: (value: string | number) => void;
}

const SavedPaymentMethods = ({
  selectedPaymentMethod,
  setSelectedPaymentMethod,
  paymentMethods,
  handleChange,
}: SavedPaymentMethodsProps): JSX.Element => {
  const options = getOptions(paymentMethods);

  return (
    <>
      <TopRadioOption
        isChecked={!!selectedPaymentMethod}
        onChange={(e) => {
          if (e.target.checked) setSelectedPaymentMethod(options[0].value);
        }}
        label={intl.formatMessage({
          id: "savedPaymentMethod",
        })}
      />
      <RadioGroup value={selectedPaymentMethod} onChange={handleChange}>
        <Stack
          border={`1px solid ${colors.gray4}`}
          borderRadius="8px"
          mb="12px"
        >
          {options.map(({ label, value, brand }, index) => (
            <SavedCreditCard
              key={value}
              label={label}
              value={value}
              brand={brand}
              isLast={index === options.length - 1}
            />
          ))}
        </Stack>
      </RadioGroup>
    </>
  );
};

// Billing details, Prefecture, Zipcode, Country only
const BillingDetailsLastRow = () => {
  const { setBillingDetails } = useCostForm();

  const onSubmit = setBillingDetails;

  return (
    <StyledBillingDetails>
      <BillingDetailsInput<PaymentMethodCreateParams.BillingDetails>
        name="address.state"
        label={intl.formatMessage({ id: "prefecture" })}
        onSubmit={onSubmit}
        isRequired
      />
      <BillingDetailsInput<PaymentMethodCreateParams.BillingDetails>
        name="address.postal_code"
        label={intl.formatMessage({
          id: "postalCode",
        })}
        onSubmit={onSubmit}
        isRequired
      />
      <BillingDetailsSelect<PaymentMethodCreateParams.BillingDetails>
        name="address.country"
        label={intl.formatMessage({ id: "country" })}
        options={countryOptions}
        onSubmit={onSubmit}
        isRequired
      />
    </StyledBillingDetails>
  );
};

// Billing details, address, card name, building, country
const BillingDetailsForm = () => {
  const { setBillingDetails } = useCostForm();

  const onSubmit = setBillingDetails;

  return (
    <StyledBillingCardDetails>
      <Form<PaymentMethodCreateParams.BillingDetails>
        onSubmit={onSubmit}
        defaultValues={{ address: { country: "JP" } }}
      >
        <BillingDetailsInput<PaymentMethodCreateParams.BillingDetails>
          name="name"
          label={intl.formatMessage({ id: "nameOnCard" })}
          onSubmit={onSubmit}
          isRequired
        />
        <BillingDetailsInput<PaymentMethodCreateParams.BillingDetails>
          name="address.line1"
          label={intl.formatMessage({ id: "address" })}
          onSubmit={onSubmit}
          isRequired
        />
        <BillingDetailsInput<PaymentMethodCreateParams.BillingDetails>
          name="address.line2"
          label={intl.formatMessage({ id: "buildingAptName" })}
          onSubmit={onSubmit}
        />
        <BillingDetailsLastRow />
      </Form>
    </StyledBillingCardDetails>
  );
};

interface NewPaymentMethodProps {
  selectedPaymentMethod: string;
  setSelectedPaymentMethod: (paymentMethod: string) => void;
}

// Credit Card Info, number, ccv
const NewPaymentMethod = ({
  selectedPaymentMethod,
  setSelectedPaymentMethod,
}: NewPaymentMethodProps) => (
  <>
    <TopRadioOption
      isChecked={!selectedPaymentMethod}
      onChange={(e) => {
        if (e.target.checked) setSelectedPaymentMethod("");
      }}
      label={intl.formatMessage({ id: "creditCard" })}
    />
    <StyledCCDetails>
      <CardElement
        options={{
          style: {
            base: {
              fontSize: "16px",
              color: "#424770",
              "::placeholder": {
                color: "#aab7c4",
              },
            },
            invalid: {
              color: colors.invalid,
            },
          },
        }}
      />
    </StyledCCDetails>
    <BillingDetailsForm />

    {/* Currently all cards are saved. Confirm if this checkbox is necessary. If so, backend changes are needed. */}
    {/* <FormControl as="fieldset" p="1" mt="5" id="cardDetails">
        <Checkbox colorScheme="green" defaultChecked mt="5">
          <Text fontSize="xs">Save card on file</Text>
        </Checkbox>
      </FormControl> */}
  </>
);

// Country and Zipcode near top
// const BillingLocation = (): JSX.Element => {
//   const { setTaxLocation } = useCostForm();

//   const onSubmit = setTaxLocation;

//   return (
//     <Form<TaxLocation> onSubmit={onSubmit} defaultValues={{ country: "JP" }}>
//       <Flex>
//         <BillingDetailsSelect<TaxLocation>
//           name="country"
//           label={intl.formatMessage({
//             id: "country",
//           })}
//           options={countryOptions}
//           onSubmit={onSubmit}
//           isRequired
//         />
//         <BillingDetailsInput<TaxLocation>
//           name="postal_code"
//           label={intl.formatMessage({
//             id: "postalCode",
//           })}
//           onSubmit={onSubmit}
//         />
//       </Flex>
//     </Form>
//   );
// };

export const CardDetails = (): JSX.Element => {
  const { data: paymentMethods, isLoading } = usePaymentMethods();

  // TODO: implement pagination (use hasMore and getNext) and error

  const { selectedPaymentMethod, setSelectedPaymentMethod } = useCostForm();

  const handleChange = (value: string | number) => {
    setSelectedPaymentMethod(value as string);
  };
  return (
    <>
      {/* Might need later as is connected to on submit and taxes */}
      {/* <BillingLocation /> */}
      {(paymentMethods.length > 0 || isLoading) && (
        // <Skeleton isLoaded={!isLoading}>
        <SavedPaymentMethods
          selectedPaymentMethod={selectedPaymentMethod}
          setSelectedPaymentMethod={setSelectedPaymentMethod}
          paymentMethods={paymentMethods}
          handleChange={handleChange}
        />
        // </Skeleton>
      )}
      <NewPaymentMethod
        selectedPaymentMethod={selectedPaymentMethod}
        setSelectedPaymentMethod={setSelectedPaymentMethod}
      />
    </>
  );
};
