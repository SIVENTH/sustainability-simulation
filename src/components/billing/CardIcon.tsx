import { IconBaseProps } from "react-icons";

import { FaCcDiscover as Discover } from "react-icons/fa";
import {
  GrVisa as Visa,
  GrAmex as Amex,
  GrCreditCard as CreditCard,
} from "react-icons/gr";
import { SiMastercard as Mastercard, SiJcb as Jcb } from "react-icons/si";

interface CardIconProps extends IconBaseProps {
  brand: string | undefined;
}

export const CardIcon = ({ brand, ...rest }: CardIconProps): JSX.Element => {
  switch (brand) {
    case "visa":
      return <Visa {...rest} />;
    case "amex":
      return <Amex {...rest} />;
    case "discover":
      return <Discover {...rest} />;
    case "mastercard":
      return <Mastercard {...rest} />;
    case "jcb":
      return <Jcb {...rest} />;
    default:
      return <CreditCard {...rest} />;
  }
};
