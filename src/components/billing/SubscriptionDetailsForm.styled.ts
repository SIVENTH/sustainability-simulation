import styled from "styled-components";

export const StyledSubscriptionType = styled.div`
  .chakra-select__wrapper {
    margin-left: 0;
  }
`;

export const StyledLabel = styled.label`
  font-size: 16px;
  font-weight: 600;
`;

export const StyledLicense = styled.div`
  width: 60%;

  .chakra-select__wrapper {
    margin-left: 0;
  }
`;

export const StyledSubscriptionFrequency = styled.div`
  margin-top: 24px;

  > label {
    font-size: 16px;
    font-weight: 600;
  }

  .chakra-radio-group > div {
    display: flex;
    flex-direction: row;
  }

  .chakra-radio:first-of-type {
    margin-right: 24px;
  }
`;
