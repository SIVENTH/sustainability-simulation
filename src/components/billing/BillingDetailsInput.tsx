import { useFormContext } from "react-hook-form";

import { Form } from "components/form";

interface BillingDetailsInputProps<T> {
  name: string;
  label: string;
  onSubmit: (parameters: T) => void;
  isRequired?: boolean;
}

const BillingDetailsInput = <T,>({
  name,
  label,
  onSubmit,
  isRequired,
}: BillingDetailsInputProps<T>): JSX.Element => {
  const { setValue, getValues } = useFormContext();

  return (
    <Form.Input
      name={name}
      label={label}
      isRequired={!!isRequired}
      rules={{ required: true }}
      onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
        setValue(name, e.target.value);
        const values = getValues() as T;
        onSubmit({ ...values });
      }}
    />
  );
};

export default BillingDetailsInput;
