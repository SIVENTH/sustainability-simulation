import styled from "styled-components";

// type Props = {
//   width?: string;
// };

export const StyledOrderSummary = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  gap: 24px;
`;

export const StyledSummaryLabel = styled.p`
  font-size: 16px;
`;

export const StyledSummaryValue = styled.p`
  font-size: 16px;
  font-weight: 600;
  color: #149860;
  text-align: right;
`;

export const StyledLicensePer = styled.p`
  display: flex;
  align-items: baseline;
`;
export const StyledLicensePerCost = styled.p`
  display: inline-block;
  font-size: 14px;
  margin-right: 8px;
`;
