import { FormattedMessage } from "react-intl";
import { StyledSideBanner } from "./SideBanner.styled";

interface Props {
  additionalInfo?: string;
  bigImg?: boolean;
  textLeft?: boolean;
}

export const SideBanner: React.FC<Props> = ({
  additionalInfo,
  bigImg,
  textLeft,
}) => {
  return (
    <StyledSideBanner
      additionalInfo={additionalInfo}
      bigImg={bigImg}
      textLeft={textLeft}
      data-testid="side-banner"
    >
      <div className="text-wrapper">
        <h3>
          <FormattedMessage
            description="Side banner intro text"
            id="signup.sidebanner"
          />
        </h3>
      </div>
    </StyledSideBanner>
  );
};
