import styled from "styled-components";
import hero from "images/tea.jpeg";

type Props = {
  additionalInfo?: string;
  bigImg?: boolean;
  textLeft?: boolean;
};

export const StyledSideBanner = styled.div<Props>`
  background-image: url(${hero});
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;

  background-color: #ebebeb;
  min-height: calc(100vh - 80px);
  height: 100%;
  max-width: 100%;
  margin: 0;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: white;
  grid-area: 1 / 1 / 25 / 13;

  @media screen and (max-width: 920px) {
    display: none;
  }

  .text-wrapper {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: ${(props) =>
      props.additionalInfo ? "flex-start" : "center"};
  }

  h3 {
    font-size: 56px;
    margin: 0;
    width: 100%;
    max-width: 395px;
    line-height: 1.25;
  }
  // p {
  //   line-height: 1.5;
  //   width: 100%;
  // }
  // div span {
  //   border-radius: 8px;
  //   width: 100%;
  //   align-self: end;
  // }

  // img {
  //   border: 12px solid #efefef;
  //   object-fit: cover;
  //   margin-bottom: 24px;
  //   margin-right: 0;
  // }

  @media (min-width: 821px) {
    // margin-bottom: 64px;

    img {
      margin-right: 0;
      ${({ textLeft }) =>
        textLeft &&
        `
      grid-column: 13 / -3;
      `}
      ${({ textLeft, bigImg }) =>
        !textLeft &&
        !bigImg &&
        `
        grid-column: 3 / 13;
      `}
    }
  }

  @media (min-width: 820px) {
    // display: grid;
    grid-template-columns: repeat(24, 1fr);
    grid-template-rows: 1fr;
    // margin-bottom: 56px;
    width: 100%;

    .text-wrapper {
      margin-top: 24px;
      ${({ textLeft }) =>
        textLeft &&
        `
      grid-column: 3 / 10;
    `}
      ${({ textLeft, bigImg }) =>
        !textLeft &&
        !bigImg &&
        `
        grid-column: 16 / -3;
    `}
    ${({ bigImg }) =>
        bigImg &&
        `
      grid-column: 3 / 10;
    `}
    }
    img {
      order: ${(props) => (props.textLeft ? 1 : 0)};
      ${({ textLeft }) =>
        textLeft &&
        `
      grid-column: 13 / -1;
    `}
      ${({ textLeft }) =>
        !textLeft &&
        `
      grid-column: 1 / 13;
    `}
    ${({ bigImg }) =>
        bigImg &&
        `
      grid-column: 11 / -1;
      grid-row: 1 / 2;
    `}
    }
  }
`;
