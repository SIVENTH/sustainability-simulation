import styled from "styled-components";

export const StyledQuestionContainer = styled.div`
  width: 100%;
`;

export const StyledQuestion = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: baseline;

  li {
    font-weight: 500;
  }
  span > svg {
    margin-left: 16px;
    fill: #cbcbcb;
  }
`;

export const StyledAnswer = styled.div`
  margin-top: 16px;

  //Dropdown Select
  .react-select__control {
    height: 56px;
    border: none;
    box-shadow: 0 2px 3px rgb(0 0 0 / 1%), 0 2px 2px rgb(0 0 0 / 1%);

    &:hover {
      height: 56px;
      border: none;
    }

    .react-select__value-container {
      padding-left: 20px;
    }
  }
  .react-select__menu {
    margin-top: 2px;
    max-width: 300px;
    border: none;
    border-radius:0px 0px 8px 8px
    box-shadow: 0 2px 3px rgb(0 0 0 / 1%), 0 2px 2px rgb(0 0 0 / 1%);
    box-shadow: 0 19px 38px rgba(0, 0, 0, 0.3), 0 15px 12px rgba(0, 0, 0, 0.22);

    .react-select__option {
      padding-left: 20px;
    }
  }
  //Radio Select
  .option {
    background-color: #ffffff;
    font-weight: 400;
    height: 100%;
    width: 350px;
    padding: 16px 20px;
    border: none;
    /* box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24); */
    box-shadow: 0 2px 3px rgb(0 0 0 / 1%), 0 2px 2px rgb(0 0 0 / 1%);
    transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);

    &:before {
      background-color: #ffffff;
      border: 2px solid #858585;
    }

    &:hover {
      background-color: #e1f4e8;
      box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);

      &:before {
        background-color: #e1f4e8;
      }
    }

    &.selected:before {
      background-color: #149860;
      border: 2px solid #c4ead3;
    }
  }
`;
