import { Tooltip } from "../tooltip";
import { Question, InputType, Answer } from "types";
import { OrderedList, ListItem } from "@chakra-ui/react";
import { Radio, Select } from "components/form-v2";
import {
  StyledQuestionContainer,
  StyledQuestion,
  StyledAnswer,
} from "./Question.styled";

interface FactoryProps {
  question: string;
  type: InputType;
  answers: Answer[];
}

function createInputFactory({
  type,
  answers,
  question,
}: FactoryProps): JSX.Element {
  switch (type) {
    case InputType.REGULAR_RADIO:
    case InputType.IMAGE_RADIO:
      return (
        <Radio
          name={question}
          options={answers.map(({ value }) => ({ value, label: value }))}
        />
      );

    default:
      return (
        <Select
          name={question}
          options={answers.map(({ value }) => ({ value, label: value }))}
          width="296px"
          height="44px"
        />
      );
  }
}

interface Props<T> {
  questions: T[];
}

export const Questions = <T extends Question>({
  questions,
}: Props<T>): JSX.Element => {
  return (
    <OrderedList>
      {questions.map(({ id, question, helperText, answers, type }) => (
        <StyledQuestionContainer key={id}>
          <StyledQuestion>
            <ListItem mt="20px" fontSize="20px">
              {question}
            </ListItem>
            <Tooltip text={helperText} hasArrow placement="right" p="5px" />
          </StyledQuestion>
          <StyledAnswer>
            {createInputFactory({ type, answers, question })}
          </StyledAnswer>
        </StyledQuestionContainer>
      ))}
    </OrderedList>
  );
};
