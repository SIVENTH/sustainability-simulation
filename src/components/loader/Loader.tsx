import { Spinner } from "@chakra-ui/react";
import { Box, Flex } from "../ChakraReplace.styled";

interface Props {
  color?: string;
}

export const Loader = ({ color = "green.600" }: Props): JSX.Element => {
  return (
    <Flex
      height="500px"
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
    >
      <Box>
        <Spinner size="xl" color={color} />
      </Box>
    </Flex>
  );
};
