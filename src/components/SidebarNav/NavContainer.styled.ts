import styled from "styled-components";

export const StyledSignUpIn = styled.div`
  display: flex;
`;

export const StyledSidebarNavContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding: 32px 16px 0 16px;
  min-height: 95vh;

  .disabled {
    color: #929292;
  }
`;

export const StyledNavItems = styled.div`
  display: flex;
  flex-direction: column;

  .premium {
    position: relative;
  }
`;
