/* eslint-disable max-lines-per-function */
import React from "react";
import { Heading } from "@chakra-ui/react";
import { Flex } from "../../components/ChakraReplace.styled";
import { intl } from "utils";

import { DashIcons } from "images";
import NavItem, { LinkEmail } from "./NavItem";

import "./index.css";
import {
  StyledSidebarNavContainer,
  StyledNavItems,
} from "./NavContainer.styled";
import { colors } from "utils/colors";

export const NavContainer: React.FunctionComponent = () => {
  return (
    <StyledSidebarNavContainer>
      <StyledNavItems>
        <NavItem
          icon={DashIcons.DashboardIcon}
          title={intl.formatMessage({ id: "dashboard" })}
          path="/dashboard"
          premium={false}
          disabled={false}
        />
        <NavItem
          icon={DashIcons.ProjectsIcon}
          title={intl.formatMessage({ id: "nav.simulation" })}
          path="/background/tea" // change to a proper selector for industries
          premium={false}
          disabled={false}
        />
        <NavItem
          icon={DashIcons.CsrReportIcon}
          title={intl.formatMessage({ id: "history" })}
          path="/dashboard"
          // path="/billing/overview"
          // need to add logic for going to either billing or history to route or auth
          premium={true}
          disabled={true}
        />
      </StyledNavItems>

      {/* EDUCATION NAV */}
      <Flex flexDirection="column" mt="40px">
        <Heading
          as="h3"
          fontSize="12px"
          color={colors.gray5}
          fontWeight="400"
          ml="20px"
          mb="16px"
        >
          {intl.formatMessage({ id: "education" })}
        </Heading>
        <NavItem
          icon={DashIcons.CsrReportIcon}
          title={intl.formatMessage({ id: "history" })}
          premium={true}
          // path="/education/class" //
          path="/dashboard"
          disabled={true}
        />
        <NavItem
          icon={DashIcons.UserIcon}
          title={intl.formatMessage({ id: "participants" })}
          path="/education/class"
          premium={false}
          disabled={false}
        />
        <NavItem
          icon={DashIcons.LabIcon}
          title={intl.formatMessage({ id: "session" })}
          path="/background/tea"
          premium={false}
          disabled={false}
        />
      </Flex>

      <Flex flexDirection="column" mt="40px">
        <Heading
          as="h3"
          fontSize="12px"
          color={colors.gray5}
          fontWeight="400"
          ml="20px"
          mb="16px"
        >
          {intl.formatMessage({ id: "account" })}
        </Heading>
        <NavItem
          icon={DashIcons.ManageIcon}
          title={intl.formatMessage({ id: "manage" })}
          path="/dashboard" // replace once manage page is designed
          premium={true}
          // path="/manage" // link is removed for now.
          disabled={true}
        />
        <NavItem
          icon={DashIcons.BillingIcon}
          title={intl.formatMessage({ id: "billing" })}
          path="/billing/overview"
          premium={true}
          disabled={true}
        />
        <LinkEmail
          icon={DashIcons.AnalyticsIcon}
          title={intl.formatMessage({ id: "analytics" })}
          premium={true}
          disabled={true}
        />
      </Flex>

      <Flex flexDirection="column" mt="40px">
        <Heading
          as="h3"
          fontSize="12px"
          color={colors.gray5}
          fontWeight="400"
          ml="20px"
          mb="16px"
        >
          {intl.formatMessage({ id: "support" })}
        </Heading>

        <LinkEmail
          icon={DashIcons.HelpIcon}
          title={intl.formatMessage({ id: "help" })}
          premium={true}
          disabled={true}
        />
        <NavItem
          icon={DashIcons.FaqIcon}
          title={intl.formatMessage({ id: "faq" })}
          path="/dashboard"
          premium={true}
          disabled={true}
        />
      </Flex>
    </StyledSidebarNavContainer>
  );
};
