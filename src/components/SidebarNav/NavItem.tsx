import { Link as RouterLink } from "react-router-dom";
import { Menu, MenuButton } from "@chakra-ui/react";
import { Text } from "../../components/ChakraReplace.styled";
import { PremiumChip } from "components/ui/PremiumChip";
import {
  StyledNavItemContainer,
  StyledNavItemLink,
  StyledMenuButton,
  StyledMenuText,
  StyledNavEmailToLink,
} from "./NavItem.styled";
import { email, subject } from "constants/index";

interface NavItemsProps {
  icon: string;
  title: string;
  active?: boolean;
  path: string;
  premium: boolean | undefined;
  disabled: boolean;
}

interface NavEmailLink {
  icon: string;
  title: string;
  active?: boolean;
  premium: boolean | undefined;
  disabled: boolean;
}

export const LinkEmail = ({
  icon,
  title,
  premium,
  disabled,
}: NavEmailLink): JSX.Element => {
  return (
    <StyledNavItemContainer>
      <Menu placement="right">
        <StyledNavEmailToLink
          href={`mailto:${email}?subject=${subject}`}
          disabled={disabled}
        >
          <MenuButton w="100%">
            <StyledMenuButton>
              <StyledMenuText>
                <img src={icon} />
                <Text>{title}</Text>
              </StyledMenuText>
              {premium ? <PremiumChip /> : <></>}
            </StyledMenuButton>
          </MenuButton>
        </StyledNavEmailToLink>
      </Menu>
    </StyledNavItemContainer>
  );
};

const NavItem = ({
  icon,
  title,
  path,
  premium,
  disabled,
}: NavItemsProps): JSX.Element => {
  return (
    <StyledNavItemContainer>
      <Menu placement="right">
        <StyledNavItemLink as={RouterLink} to={path} disabled={disabled}>
          <MenuButton w="100%">
            <StyledMenuButton>
              <StyledMenuText>
                <img src={icon} />
                <Text>{title}</Text>
              </StyledMenuText>

              {premium ? <PremiumChip /> : <></>}
            </StyledMenuButton>
          </MenuButton>
        </StyledNavItemLink>
      </Menu>
    </StyledNavItemContainer>
  );
};

export default NavItem;
