import { FunctionComponent } from "react";
import { Flex } from "../ChakraReplace.styled";
import { NavContainer } from "./NavContainer";
import "./index.css";

export const SidebarNav: FunctionComponent = () => {
  return (
    <Flex>
      <NavContainer />
    </Flex>
  );
};
