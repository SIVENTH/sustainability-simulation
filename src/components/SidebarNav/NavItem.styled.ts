import styled from "styled-components";

export const StyledNavItemContainer = styled.div`
  display: flex;
  background: #ffffff;
  flex-direction: column;
  align-items: flex-start;
  width: 100%;
`;

export const StyledNavItemLink = styled.div<{ disabled: boolean }>`
  background-color: #ffffff;
  margin-bottom: 8px;
  border-radius: 8px;
  width: 100%;
  pointer-events: ${(props) => (props.disabled ? "none" : "auto")};

  button {
    opacity: ${(props) => (props.disabled ? "0.4" : "1")};
  }

  &:hover {
    background-color: ${(props) => (props.disabled ? "white" : "#E1F4E8")};
    text-decoration: none;
  }
`;

export const StyledNavEmailToLink = styled.a<{ disabled: boolean }>`
  background-color: #ffffff;
  margin-bottom: 8px;
  border-radius: 8px;
  width: 100%;
  pointer-events: ${(props) => (props.disabled ? "none" : "auto")};

  button {
    opacity: ${(props) => (props.disabled ? "0.4" : "1")};
  }

  &:hover {
    background-color: ${(props) => (props.disabled ? "white" : "#E1F4E8")};
    text-decoration: none;
  }
`;
export const StyledMenuButton = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 8px 12px 8px 16px;
  img {
    height: 16px;
    width: 16px;
  }

  p {
    display: inline-block;
    white-space: nowrap;
  }
`;

export const StyledMenuText = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;

  p {
    font-size: 16px;
    color: #282828;
    font-weight: 600;
    margin-left: 12px;
  }
`;
