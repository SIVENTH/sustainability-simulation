import React from "react";
import Select from "react-select";

export interface Option {
  label: string;
  value: string;
}

export interface SelectDropdownProps {
  options: Option[];
  value: string;
  placeholder?: string;
  disabled?: boolean;
  error?: boolean;
  onChange: (value: string) => void;
}

export const SelectDropdown: React.FC<SelectDropdownProps> = (
  props
): JSX.Element => {
  const customStyles = {
    control: (provided) => ({
      ...provided,
      borderRadius: 0,
      borderColor: props.error ? "red" : "#ccc",
      boxShadow: "none",
      // width: "210px",
      overflow: "visible",
    }),
  };

  const handleChange = (selectedOption: Option | null) => {
    if (selectedOption !== null) {
      const value = selectedOption.value;
      props.onChange(value);
    }
  };

  const selectedOption = props.options.find(
    (option) => option.value === props.value
  );

  return (
    <Select
      styles={customStyles}
      options={props.options}
      value={selectedOption}
      onChange={handleChange}
      placeholder={props.placeholder}
      isDisabled={props.disabled}
      menuPlacement="bottom"
    />
  );
};
