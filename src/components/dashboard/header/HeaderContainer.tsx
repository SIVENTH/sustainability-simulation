/* eslint-disable max-lines-per-function */
import { useState } from "react";
import { FormattedMessage } from "react-intl";
import { useNavigate } from "react-router-dom";
import { intl } from "utils";
import { ResultDocument } from "types";
import { Form } from "components/form";
import {
  StyledSimTitle,
  StyledSimResultsHeader,
  StyledChangeIndustry,
  StyledStartNewSim,
  ChangeOrganizationButton,
} from "./HeaderContainer.styled";

import {
  ModalParent,
  StyledHeaderText,
  ModalSaveButton,
} from "components/modals";
import { Option, SelectDropdown } from "components/dropdownSelect";
import { SelectContainer } from "components/dashboard/header/HeaderContainer.styled";

type Selection = { value: number; label: string; disabled: boolean };
// TODO: Populate with available simulation/questionnaire options from API
const simOptions: Selection[] = [
  {
    value: 1,
    label: intl.formatMessage({ id: "supplyChains.tea" }),
    disabled: false,
  },
  {
    value: 2,
    label: intl.formatMessage({ id: "supplyChains.coffee.underDevelopment" }),
    disabled: true,
  },
  {
    value: 3,
    label: intl.formatMessage({ id: "supplyChains.apparel.underDevelopment" }),
    disabled: true,
  },
];

// TODO: Populate with available organizations from API
// const organizationOptions: Selection[] = [
//   {
//     value: 1, // change to userID
//     label: intl.formatMessage({ id: "Personal" }),
//     disabled: false,
//   },
//   {
//     value: 2, // change to orgID of org that user has access to
//     label: "Hitotsubashi University",
//     disabled: false,
//   },
// ];

interface ResultDocumentWithId extends ResultDocument {
  id: string;
}

interface Props {
  simResults: ResultDocumentWithId[];
  options: Option[];
  handleSelectChange: (value: string) => void;
  currOrganizationId: string;
}

/**
 * Contains the top/header section of the dashboard.
 * @component HeaderContainer
 * @param {object} simResults The data for all of the completed sim results.
 * TODO: Review whether retrieving the latest sim results with an api call or passing the latest sim results as props is better and adjust accordingly.
 * @return {object} returns the header component
 */
export const HeaderContainer = ({
  simResults,
  options,
  handleSelectChange,
  currOrganizationId,
}: Props): JSX.Element => {
  const [openChangeIndustryModal, setOpenChangeIndustryModal] = useState(false);
  const [openChangeOrganizationModal, setOpenChangeOrganizationModal] =
    useState(false);
  const navigate = useNavigate();
  const handleSubmit = () => {
    navigate("/background/tea"); // change to user selected industry
  };

  /**
   * Used to change to a different industry, will also close the modal
   * @method changeIndustry
   * @param {string} industryId The id of the industry the user wishes to change to.
   */
  const changeIndustry = () => {
    setOpenChangeIndustryModal(false);
  };

  const title = simResults[0]
    ? simResults[0].title
    : intl.formatMessage({ id: "supplyChains.tea" }); // make default title Tea if no simulations for new user exist

  return (
    <StyledSimResultsHeader>
      <StyledSimTitle>{title}</StyledSimTitle>

      <StyledChangeIndustry onClick={() => setOpenChangeIndustryModal(true)}>
        <FormattedMessage id="changeIndustry" />
      </StyledChangeIndustry>

      <ModalParent
        hideModal={() => setOpenChangeIndustryModal(false)}
        open={openChangeIndustryModal}
        title=""
      >
        <Form<Selection> onSubmit={changeIndustry}>
          <StyledHeaderText>
            <FormattedMessage id="analysisChoice.title" />
          </StyledHeaderText>
          <Form.Select<Selection>
            name="sim_selection"
            options={simOptions}
            isRequired
            placeholder={intl.formatMessage({
              id: "analysisChoice.placeholder",
            })}
            rules={{ required: true }}
          />
          <ModalSaveButton>
            <Form.Button
              type="submit"
              title={intl.formatMessage({ id: "submit" })}
            />
          </ModalSaveButton>
          {/* Need to run create class method first */}
        </Form>
      </ModalParent>

      <ChangeOrganizationButton
        onClick={() => setOpenChangeOrganizationModal(true)}
      >
        <FormattedMessage id="changeOrganization" />
      </ChangeOrganizationButton>

      <ModalParent
        hideModal={() => setOpenChangeOrganizationModal(false)}
        open={openChangeOrganizationModal}
        title=""
        isOrganizationModal
      >
        <SelectContainer>
          <SelectDropdown
            options={options}
            onChange={handleSelectChange}
            value={currOrganizationId}
            placeholder="Select Organization"
          />
          <ModalSaveButton
            type="button"
            onClick={() => setOpenChangeOrganizationModal(false)}
          >
            Save
          </ModalSaveButton>
        </SelectContainer>
      </ModalParent>

      <StyledStartNewSim onClick={() => handleSubmit()}>
        <FormattedMessage id="begin" />
      </StyledStartNewSim>
    </StyledSimResultsHeader>
  );
};
