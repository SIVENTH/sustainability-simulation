import styled from "styled-components";

import { colors } from "utils";

export const StyledSimResultsHeader = styled.div`
  display: flex;
  flex-direction: row;
  align-items: baseline;
`;

export const StyledSimTitle = styled.div`
  color: ${colors.green2};
  font-size: 60px;
  font-weight: 900;
`;

export const StyledChangeIndustry = styled.div`
  display: flex;
  justify-content: center;
  align-items: flex-end;
  margin-left: 30px;
  margin-right: auto;
  color: ${colors.blue1};
  cursor: pointer;
  font-size: 16px;
`;

export const StyledStartNewSim = styled.button`
  margin: auto 0;
  background-color: ${colors.green2};
  color: #fff;
  border: 1px;
  border-radius: 5px;
  padding: 16px;
  text-align: center;
  cursor: pointer;
  height: 50%;
`;

export const SelectContainer = styled.div`
  width: 100%;
  height: 100%;
  overflow: visible;
`;

export const ChangeOrganizationButton = styled.button`
  margin: auto 0;
  margin-right: 20px;
  background-color: #fff;
  color: ${colors.green2};
  border: 1px;
  border-style: solid;
  border-radius: 5px;
  padding: 16px;
  text-align: center;
  cursor: pointer;
  height: 50%;
`;
