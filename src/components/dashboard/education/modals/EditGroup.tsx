import { useState } from "react";
import { FormattedMessage } from "react-intl";
import {
  ModalParent,
  StyledHeaderText,
  StyledInput,
  StyledCaption,
  ModalSaveButton,
  ButtonText,
  StyledSettingsButton,
  StyledButton,
} from "components/modals";
import gear from "images/The_Icon_of-SVG/UI/settings.svg";

// TODO: EDIT CSS, CHANGE TO FORMATTED MESSAGES
export default function EditGroup(): JSX.Element {
  const [open, setOpen] = useState(false);
  const [groupNameValue, setGroupNameValue] = useState("Enter Group Name");

  return (
    <>
      <StyledButton onClick={() => setOpen(true)}>
        <StyledSettingsButton src={gear} />
        <FormattedMessage id="edit" />
      </StyledButton>

      <ModalParent hideModal={() => setOpen(false)} open={open} title="">
        <StyledHeaderText>
          <FormattedMessage id="groupMembers" />
        </StyledHeaderText>
        <StyledCaption>
          <FormattedMessage id="You can paste multiple emails into the box below" />
        </StyledCaption>
        {/* name of members with option to remove them */}
        <StyledHeaderText>
          <FormattedMessage id="groupLeader" />
        </StyledHeaderText>
        {/*Name of leader */}
        <StyledHeaderText>
          <FormattedMessage id="groupName" />
        </StyledHeaderText>
        <StyledInput
          value={groupNameValue}
          onChange={(event) => {
            setGroupNameValue(event.target.value);
          }}
        />
        <ModalSaveButton>
          <ButtonText>
            <FormattedMessage id="saveGroup" />
          </ButtonText>
        </ModalSaveButton>{" "}
      </ModalParent>
    </>
  );
}
