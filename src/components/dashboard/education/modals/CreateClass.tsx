/* eslint-disable max-lines-per-function */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { useEffect, useState } from "react";
import { FormattedMessage } from "react-intl";
import { CreateGroup } from "../modals/CreateGroup";
import { ModalParent, StyledHeaderText, StyledInput } from "components/modals";
import { intl } from "utils";
import "react-toastify/dist/ReactToastify.css";

/**
 * Used to create a class or department in an organization
 * @main CreateClass
 * @return {object} returns Create Class button and associated  object or error
 */
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function CreateClass({ onClassUpdated }): JSX.Element {
  const [open, setOpen] = useState(false);
  const [courseNameValue, setCourseNameValue] = useState("");
  const [courseIDValue, setCourseIDValue] = useState("");
  const [industryIdValue, setIndustryIdValue] = useState("1");

  useEffect(() => {
    setCourseNameValue("");
    setCourseIDValue("");
    setIndustryIdValue("1");
  }, []);

  return (
    <>
      <button onClick={() => setOpen(true)}>
        <FormattedMessage id="createClass" />
      </button>

      <ModalParent hideModal={() => setOpen(false)} open={open} title="">
        {/* Course Name based on the University's catalog */}
        <StyledHeaderText>
          <FormattedMessage id="courseName" />
        </StyledHeaderText>
        <StyledInput
          value={courseNameValue}
          placeholder={intl.formatMessage({
            id: "courseName.helperText",
          })}
          onChange={(event) => {
            setCourseNameValue(event.target.value);
          }}
        />
        {/* Course ID based on the University's catalog */}
        <StyledHeaderText>
          <FormattedMessage id="courseID" />
        </StyledHeaderText>
        <StyledInput
          value={courseIDValue}
          placeholder={intl.formatMessage({
            id: "courseID.helperText",
          })}
          onChange={(event) => {
            setCourseIDValue(event.target.value);
          }}
        />
        {/* Used for the session/supply chain that all class members will ananlyze */}
        <StyledHeaderText>
          <FormattedMessage id="industry" />
        </StyledHeaderText>
        <StyledInput
          value={industryIdValue}
          placeholder={intl.formatMessage({
            id: "industry.helperText",
          })}
          onChange={(event) => {
            setIndustryIdValue(event.target.value);
          }}
        />

        {/* Need to run create class method first */}
        <CreateGroup
          onClassUpdated={onClassUpdated}
          industryIdValue={industryIdValue}
          courseNameValue={courseNameValue}
          courseIdValue={courseIDValue}
          setClassModalOpen={setOpen}
          setCourseNameValue={setCourseNameValue}
          setCourseIDValue={setCourseIDValue}
        />
      </ModalParent>
    </>
  );
}
