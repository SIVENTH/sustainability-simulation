/* eslint-disable sonarjs/cognitive-complexity */
/* eslint-disable max-lines-per-function */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { useState } from "react";
import { toast } from "react-toastify";
import { FormattedMessage } from "react-intl";
import { addTeam, getAllClassMembers } from "gateway/services";
import {
  ModalParent,
  StyledHeaderText,
  StyledTextArea,
  StyledCaption,
  ModalSaveButton,
  ButtonText,
} from "components/modals";
import { NewDepartmentResponseSchema } from "types";
import { addNewDepartment, addDepartmentMembers } from "gateway/services";
import { useAuth } from "context";

interface Props {
  onClassUpdated: (newClass) => void;
  industryIdValue: string;
  courseNameValue: string;
  courseIdValue: string;
  setClassModalOpen: (open: boolean) => void;
  setCourseNameValue: (value: string) => void;
  setCourseIDValue: (value: string) => void;
}

// TODO[PENDING]:send request with departmentId(classId) and at least 1 email (this is done but leader field not working yet)
// TODO: display emails in group emails section of chosen emails.
// TODO: remove chosen emails from new list
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const CreateGroup = ({
  onClassUpdated,
  industryIdValue,
  courseNameValue,
  courseIdValue,
  setClassModalOpen,
  setCourseNameValue,
  setCourseIDValue,
}: Props): JSX.Element => {
  const [open, setOpen] = useState(false);
  const [emailValues, setEmailValues] = useState("");
  const { organization } = useAuth();
  const [groupDepartment, setGroupDepartment] = useState<
    NewDepartmentResponseSchema | any
  >();

  function splitEmailValues(emailValues: string) {
    console.log("emailValues: ", emailValues, typeof emailValues);
    const regex = /[\w\d._%+-]+@[a-zA-Z0-9.-]+\.(com|co\.jp)/g;
    const matches: Array<string> = [];
    let match;
    while ((match = regex.exec(emailValues)) !== null) {
      matches.push(match[0]);
    }
    return matches.length > 0 ? matches : null;
  }

  const createClass = async () => {
    try {
      const newClass = await addNewDepartment({
        code: courseIdValue,
        emails: emailValues ? splitEmailValues(emailValues) : null,
        // emails: emailValues
        //   ? emailValues.split(",").map((email) => email.trim())
        //   : null,
        name: courseNameValue,
        orgId: organization.id,
        industryId:
          Number(industryIdValue) !== null ? Number(industryIdValue) : 2,
      });
      setGroupDepartment(newClass);
      onClassUpdated(newClass); // callback to update classList in parent
      setTimeout(() => {
        setCourseIDValue("");
        setCourseNameValue("");
        toast.success("Class successfully created!");
      }, 1000);
    } catch (error) {
      toast.error("Error creating class. Please try again.");
      console.log("[CREATE CLASS/createClass]error: ", error);
    }
  };

  /**
   * Takes an array and splits it based on number of roles in industry
   * TODO: get industry, count number of roles, divide number of students in class by number of roles
   * TODO: randomly assign members
   * @method splitGroup
   * @param {array} emails The emails of the class members
   * @param {object} industry The industry that the class is participating in
   * @return {array} returns array of team/group emails
   */
  const splitGroup = (emails: string[], industry: unknown | null) => {
    console.log("[CREATE GROUP/splitGroup]industry: ", industry); // need to send/get industry
    const numMembers = emails.length; // number of class/group members
    const numRoles = 5;
    const numTeams = Math.ceil(numMembers / numRoles); // rounds up to the nearest integer
    const teams: any = [];

    for (let i = 0; i < numTeams; i++) {
      const start = i * numRoles; // finds the start of the array to slice
      const finish = (i + 1) * numRoles; // finds the end of the array
      const newTeam = emails.slice(start, finish); // splits the array, finish not included
      teams.push(newTeam);
    }
    return teams;
  };

  /**
   * Uses classValue and organization information to create groups/teams of users
   * @method createGroup
   */
  const createGroup = async () => {
    await addDepartmentMembers({
      departmentId: groupDepartment.data.schema.id,
      emails: emailValues ? splitEmailValues(emailValues) : null,
    });
    setOpen(false);
    const classData = groupDepartment.data.schema;
    const members = await getAllClassMembers(classData.id);
    const classMembers = members?.map((member) => {
      // todo split members by email
      return member.email;
    });
    const emails = emailValues.split(",").map((email) => email.trim()); // replace with split version of members
    const leader = emails[0];
    console.log("[CREATE GROUP/createGroup]leader: ", leader);
    setClassModalOpen(false);
    try {
      const teams = splitGroup(classMembers, null); // testing out split group

      for (let i = 0; i < teams.length; i++) {
        const response: any = await addTeam({
          emails: teams[i],
          departmentId: classData.id,
          description: "",
          // leader: leader,
          name: "",
        });

        console.log("[CREATE GROUP/createGroup]response: ", response);
      }
    } catch (error) {
      console.log("[CREATE GROUP/createGroup]error: ", error);
    }
  };

  const handleCreateClass = async () => {
    setOpen(true);
    createClass();
  };

  return (
    <>
      <ModalSaveButton onClick={handleCreateClass}>
        <ButtonText>
          <FormattedMessage id="saveAndCreateGroups" />
        </ButtonText>
      </ModalSaveButton>

      <ModalParent hideModal={() => setOpen(false)} open={open} title="">
        {/* Used for the session/supply chain that all class members will ananlyze */}
        <StyledHeaderText>
          <FormattedMessage id="groupMembers" />
        </StyledHeaderText>
        <StyledCaption>
          <FormattedMessage id="multipleEmail.helperText" />
        </StyledCaption>
        <StyledTextArea
          placeholder="Enter Student Emails"
          value={emailValues}
          onChange={(event) => {
            setEmailValues(event.target.value);
          }}
        />
        <ModalSaveButton onClick={createGroup}>
          <ButtonText>
            <FormattedMessage id="save" />
          </ButtonText>
        </ModalSaveButton>
      </ModalParent>
    </>
  );
};
