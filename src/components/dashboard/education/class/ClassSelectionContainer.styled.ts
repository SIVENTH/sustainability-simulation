import styled, { keyframes } from "styled-components";

import { colors } from "utils";

const rotateAnimation = keyframes`
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
`;

export const LoadingSpinner = styled.div`
  border: 4px solid rgba(255, 255, 255, 0.3);
  border-top-color: #149860;
  border-radius: 50%;
  width: 20px;
  height: 20px;
  animation: ${rotateAnimation} 1s linear infinite;
  margin: 0 auto;
`;

export const StyledContainer = styled.div`
  margin: 20px 0;
  padding: 40px;
  background-color: #ffffff;
  width: 100%;
  border: 1px;
  border-radius: 5px;
  text-align: left;
`;

export const StyledContent = styled.div`
  padding: 0;
`;

export const StyledHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: baseline;
  margin-top: 8px;
`;

export const StyledClassButton = styled.img`
  height: 48px;
  width: 48px;
  padding: 5px;
  margin: 0px auto;
  // cursor: pointer;
`;

export const StyledSettingsButton = styled.img`
  height: 24px;
  width: 24px;
  padding: 3px;
`;

export const StyledBody = styled.div`
  text-align: center;
  margin-top: 20px;
  padding: 40px;
  border-width: 1px;
  border-color: #efefef;
  border-radius: 5px;

  .createClassSection {
    > div {
      margin: 20px;
    }

    > div:last-child {
      color: #149860;
    }

    > div:first-child {
      font-weight: 900;
    }
  }
`;

export const StyledTable = styled.div`
  margin: 48px 0;
  padding: 0;
  > div {
    padding: 0 !important;
  }

  table {
    tr > td {
      min-width: 24%;
    }
    tr > td:last-of-type {
      width: 180px;
    }

    td > div > img:first-of-type {
      margin-left: 0;
    }
  }
`;
export const StyledViewAll = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${colors.blue1};
  cursor: pointer;

  span {
    margin-left: 12px;
  }
`;

export const StyledButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 32px;
  width: 64px;
  font-size: 12px;
  border-width: 1px;
  border-radius: 5px;
  color: gray;
  border-color: #b3b3b3;
  cursor: pointer;
  margin: auto;
`;
