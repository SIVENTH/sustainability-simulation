/* eslint-disable @typescript-eslint/no-explicit-any */
import { classMember } from "gateway";
import {
  deleteDepartment,
  deleteDepartmentMember,
  getAllClassMembers,
} from "gateway/services";
import React, { useEffect, useState } from "react";
import styled from "styled-components";
interface Props {
  classItem: any;
  redirectToDetails: (id: number) => void;
  onClassDeleted: () => void;
  // toggleMenu: () => void;
  setOpen: (open: boolean) => void;
}
import { ModalParent } from "components/modals";
import { LoadingSpinner } from "./ClassSelectionContainer.styled";
import { toast } from "react-toastify";

const Menuwrapper = styled.div`
  background-color: white;
  border: 1px solid black;
  border-radius: 5px;
  padding: 5px;
  z-index: 2;
  position: absolute;
  top: 32px;

  ul {
    list-style: none;
  }

  li {
    padding: 5px;

    &:hover {
      background-color: #f2f2f2;
    }
  }
`;

const MembersList = styled.div`
  display: flex;
  justify-content: space-evenly;
`;

export const EditClassMenu = ({
  classItem,
  onClassDeleted,
  setOpen,
}: Props): JSX.Element => {
  const [openModal, setOpenModal] = useState(false); // why is this not working?
  const [members, setMembers] = useState<classMember[]>([]);
  const [loading, setLoading] = useState(false);

  const deleteClass = async () => {
    setLoading(true);
    try {
      await deleteDepartment({ departmentId: classItem.id });
      onClassDeleted();
      setTimeout(() => {
        setLoading(false);
        setOpen(false);
        toast.success("Class deleted");
      }, 1000);
    } catch (error) {
      toast.error("Error deleting class");
      setLoading(false);
      console.log("error: ", error);
    }
  };

  const getMembers = async (id: number) => {
    try {
      const classMembers = await getAllClassMembers(id);

      setMembers(classMembers ? classMembers : []);
    } catch (error) {
      console.log("error: ", error);
    }
  };

  const handleDeleteMember = async (email: string) => {
    try {
      await deleteDepartmentMember({ departmentId: classItem.id, email });
      await getMembers(classItem.id);
    } catch (error) {
      toast.error("Error deleting member");
      console.log("error: ", error);
    }
  };
  useEffect(() => {
    getMembers(classItem.id);
  }, [classItem]);

  return (
    <Menuwrapper>
      <ul>
        <li>
          <button onClick={deleteClass}>
            {loading ? <LoadingSpinner /> : "Delete Class"}
          </button>{" "}
        </li>
        <li>
          <button onClick={() => setOpenModal(true)}>Edit Members</button>
        </li>
        <ModalParent
          title="Members"
          open={openModal}
          hideModal={() => setOpenModal(false)}
        >
          {members != null && members.length > 0 ? (
            members.map((user: classMember) => {
              return (
                <MembersList key={user.id}>
                  <p>{user.email} </p>
                  <button
                    onClick={() => {
                      handleDeleteMember(user.email);
                    }}
                  >
                    Delete
                  </button>
                </MembersList>
              );
            })
          ) : (
            <div>No Members</div>
          )}
        </ModalParent>
        {/* <li>
          <button>Advanced</button>
        </li> */}
      </ul>
    </Menuwrapper>
  );
};
