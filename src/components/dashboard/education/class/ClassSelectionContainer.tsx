/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable max-lines-per-function */
import { useEffect, useState } from "react";
import { FormattedMessage } from "react-intl";
import { intl } from "utils";
import { Heading } from "components/heading";
import CreateClass from "../modals/CreateClass";

import {
  StyledContainer,
  StyledContent,
  StyledBody,
  StyledHeader,
  StyledViewAll,
  StyledClassButton,
  StyledTable,
} from "./ClassSelectionContainer.styled";
import { Table } from "components/table";
import add from "images/The_Icon_of-SVG/Actions/add_circle_outlined.svg";
import { EditClassButton } from "./EditClassButton";

interface Props {
  classList: any;
  redirectToDetails: (id: number) => void;
  // loadMore: () => void;
  // hasMore: boolean;
  onClassUpdated: (newClass: any) => void;
}

export const ClassSelectionContainer = ({
  classList,
  redirectToDetails,
  // loadMore,
  // hasMore,
  onClassUpdated,
}: Props): JSX.Element => {
  // const loadMoreResults = (): void => {
  //   if (hasMore) loadMore();
  // };
  const [formattedClassList, setFormattedClassList] = useState<any>([]);

  // ClassSelectionContainer component
  const handleClassDeleted = (deletedClassId: number) => {
    setFormattedClassList((prevState) =>
      prevState.filter((classItem: any) => classItem.id !== deletedClassId)
    );
  };

  const getFormattedList = (list: any) => {
    if (!list) return [];
    return list?.map((classItem: any) => {
      return {
        id: classItem.id,
        courseID: classItem.code,
        className: classItem.name,
        date: classItem.createdAt,
        students: classItem.users,
        runs: classItem.sessions,
        edit: classItem.id ? (
          <EditClassButton
            redirectToDetails={redirectToDetails}
            classItem={classItem}
            onClassDeleted={() => handleClassDeleted(classItem.id)}
          />
        ) : null,
      };
    });
  };

  useEffect(() => {
    if (classList) {
      setFormattedClassList(getFormattedList(classList));
    }
  }, [classList]);

  const classListHeader = [
    {
      Header: intl.formatMessage({ id: "courseID" }),
      accessor: "courseID",
    },
    {
      Header: intl.formatMessage({ id: "className" }),
      accessor: "className",
    },
    {
      Header: intl.formatMessage({ id: "date" }),
      accessor: "date",
      Cell: () => {
        return "-";
      },
    },
    {
      Header: intl.formatMessage({ id: "sims.run" }),
      accessor: "runs",
      Cell: () => {
        return "-";
      },
    },
    {
      Header: intl.formatMessage({ id: "edit.class" }),
      accessor: "edit",
    },
  ];

  return (
    <StyledContainer>
      <StyledContent>
        <StyledHeader>
          <Heading>
            <FormattedMessage id="class.selection" />
          </Heading>
          <StyledViewAll>
            <CreateClass onClassUpdated={onClassUpdated} />
          </StyledViewAll>
        </StyledHeader>
        <StyledBody>
          {!classList ? (
            <div className="createClassSection">
              <div>
                <FormattedMessage id="createClass" />
              </div>
              <div>
                <StyledClassButton src={add} />
              </div>
              <div>
                <FormattedMessage id="inviteStudents" />
              </div>
            </div>
          ) : (
            <StyledTable>
              <Table columns={classListHeader} data={formattedClassList} />
            </StyledTable>
          )}
        </StyledBody>
      </StyledContent>
    </StyledContainer>
  );
};
