/* eslint-disable @typescript-eslint/no-explicit-any */
import { useRef } from "react";
import gear from "images/The_Icon_of-SVG/UI/settings.svg";
import styled from "styled-components";
import {
  StyledButton,
  StyledSettingsButton,
} from "./ClassSelectionContainer.styled";
import { FormattedMessage } from "react-intl";
import { EditClassMenu } from "./EditClassMenu";
import { useOutsideClick } from "hooks/useOutsideClick";

interface Props {
  redirectToDetails: (id: number) => void;
  classItem: any;
  onClassDeleted: () => void;
}

const Wrapper = styled.div`
  position: relative;
`;
export const EditClassButton = ({
  redirectToDetails,
  classItem,
  onClassDeleted,
}: Props): JSX.Element => {
  const dropdownRef = useRef(null);
  const [open, setOpen] = useOutsideClick(dropdownRef, false);
  return (
    <Wrapper ref={dropdownRef}>
      <StyledButton onClick={() => setOpen(!open)}>
        <StyledSettingsButton src={gear} />
        <FormattedMessage id="edit" />
      </StyledButton>
      {open && (
        <EditClassMenu
          classItem={classItem}
          redirectToDetails={redirectToDetails}
          onClassDeleted={onClassDeleted}
          setOpen={setOpen}
        />
      )}
    </Wrapper>
  );
};
