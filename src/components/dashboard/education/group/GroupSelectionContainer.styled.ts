import styled from "styled-components";

import { colors } from "utils";

export const StyledContainer = styled.div`
  margin: 20px 0;
  padding: 24px;
  background-color: #ffffff;
  width: 100%;
  border: 1px;
  border-radius: 5px;
  text-align: left;
`;

export const StyledContent = styled.div`
  padding: 0;
`;

export const StyledLink = styled.div`
  cursor: pointer;
`;

export const StyledHeader = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: baseline;

  h3 {
    font-size: 24px;
    font-weight: 600;
  }
`;

export const StyledClassButton = styled.img`
  height: 48px;
  width: 48px;
  padding: 5px;
  margin: 0px auto;
  cursor: pointer;
`;

export const StyledBody = styled.div`
  text-align: center;
  margin-top: 20px;
  padding: 40px;
  border-width: 1px;
  border-color: #efefef;
  border-radius: 5px;

  .createClassSection {
    cursor: pointer;

    > div {
      margin: 20px;
    }

    > div:last-child {
      color: #149860;
    }

    > div:first-child {
      font-weight: 900;
    }
  }
`;

export const StyledTable = styled.div`
  margin: 48px 0;
  padding: 0;
  > div {
    padding: 0 !important;
  }

  table {
    tr > td {
      min-width: 24%;
    }
    tr > td:last-of-type {
      width: 180px;
    }

    td > div > img:first-of-type {
      margin-left: 0;
    }
  }
`;
export const StyledViewAll = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  color: ${colors.blue1};
  cursor: pointer;

  span {
    margin-left: 12px;
  }
`;
