/* eslint-disable max-lines-per-function */
import React from "react";
import { FormattedMessage } from "react-intl";
import { useNavigate } from "react-router-dom";
import { GroupDocument } from "types";
import { intl } from "utils";
import EditGroup from "../modals/EditGroup";
import {
  StyledContainer,
  StyledContent,
  StyledBody,
  StyledHeader,
  StyledViewAll,
  StyledTable,
  StyledClassButton,
  StyledLink,
} from "./GroupSelectionContainer.styled";
import { Table } from "components/table";
import add from "images/The_Icon_of-SVG/Actions/add_circle_outlined.svg";

interface GroupListWithId extends GroupDocument {
  id: string;
}

interface Props {
  groupList: GroupListWithId[];
  redirectToDetails: (id: string) => void;
  loadMore: () => void;
  hasMore: boolean;
}

export const GroupSelectionContainer = ({
  groupList,
  redirectToDetails,
  loadMore,
  hasMore,
}: Props): JSX.Element => {
  const navigate = useNavigate();

  const loadMoreResults = (): void => {
    if (hasMore) loadMore();
  };

  // TODO: Retrieve from API
  const formattedClassList = groupList?.map((classItem) => {
    return {
      id: classItem.id,
      name: (
        <StyledLink onClick={() => redirectToDetails(classItem.id)}>
          {classItem.name}
        </StyledLink>
      ),
      size: classItem.size,
      students: classItem.users,
      industry: classItem.industry,
      // score: classItem.score ? `${classItem.score.toFixed(2)}%` : "",
      sessions: classItem.sessions,
      edit: classItem.size ? <EditGroup /> : null,
    };
  });

  // TODO: Retrieve from API
  const classListHeader = [
    {
      Header: intl.formatMessage({ id: "group.name" }),
      accessor: "name",
    },
    {
      Header: intl.formatMessage({ id: "group.size" }),
      accessor: "size",
    },
    {
      // users or students
      Header: intl.formatMessage({ id: "students" }),
      accessor: "students",
    },
    {
      Header: intl.formatMessage({ id: "industry" }),
      accessor: "industry",
    },
    // score not currently being sent
    // {
    //   Header: intl.formatMessage({ id: "score" }),
    //   accessor: "score",
    // },
    {
      Header: intl.formatMessage({ id: "sims.run" }),
      accessor: "sessions",
    },
    {
      Header: intl.formatMessage({ id: "edit.group" }),
      accessor: "edit",
    },
  ];

  return (
    <StyledContainer>
      <StyledContent>
        <StyledHeader>
          <h3>
            <FormattedMessage id="groupSelection" />
          </h3>
          <StyledViewAll onClick={() => navigate("/education/class")}>
            <FormattedMessage id="edit.class" />
          </StyledViewAll>
        </StyledHeader>
        <StyledBody>
          {groupList[0]?.sessions == null ? (
            <div
              className="createClassSection"
              onClick={() => navigate("/education/class")}
            >
              <div>
                <FormattedMessage id="createClass" />
              </div>
              <div>
                <StyledClassButton src={add} />
              </div>
              <div>
                <FormattedMessage id="inviteStudents" />
              </div>
            </div>
          ) : (
            <StyledTable onClick={loadMoreResults}>
              <Table columns={classListHeader} data={formattedClassList} />
            </StyledTable>
          )}
        </StyledBody>
      </StyledContent>
    </StyledContainer>
  );
};
