import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin: 20px auto;
`;

export const Chart = styled.div`
  min-width: 600px;
  width: 100%;
  min-height: 500px;
  cursor: pointer;
`;

export const ChartDetails = styled.div``;
