/* eslint-disable max-lines-per-function */
import { useLayoutEffect } from "react";
import { FormattedMessage } from "react-intl";
import * as am5 from "@amcharts/amcharts5";
import * as am5xy from "@amcharts/amcharts5/xy";
import * as am5radar from "@amcharts/amcharts5/radar";
import am5themes_Animated from "@amcharts/amcharts5/themes/Animated";
import am5themes_Responsive from "@amcharts/amcharts5/themes/Responsive";
import { intl } from "utils";
import { ResultDocument } from "types";
import { SCard } from "components/card";
import { Container, Chart, ChartDetails } from "./ChartContainer.styled";

interface ResultDocumentWithId extends ResultDocument {
  id: string;
}

interface Props {
  simResults: ResultDocumentWithId[];
  redirectToDetails: (id: string) => void;
}

export const ChartContainer = ({
  simResults,
  redirectToDetails,
}: Props): JSX.Element => {
  useLayoutEffect(() => {
    // Create root element
    // https://www.amcharts.com/docs/v5/getting-started/#Root_element
    const root = am5.Root.new("chartdiv");
    // Set themes
    // https://www.amcharts.com/docs/v5/concepts/themes/
    const myTheme = am5.Theme.new(root);
    myTheme.rule("Label").setAll({
      fill: am5.color("#000"),
      fontSize: "1.5em",
    });
    root.setThemes([
      am5themes_Animated.new(root),
      am5themes_Responsive.new(root),
      myTheme,
    ]);
    // Create chart
    // https://www.amcharts.com/docs/v5/charts/xy-chart/
    const chart = root.container.children.push(
      am5radar.RadarChart.new(root, {
        panX: false,
        panY: false,
        wheelX: "none",
        wheelY: "none",
        startAngle: -84,
        endAngle: 264,
        innerRadius: am5.percent(30),
      })
    );

    const xRenderer = am5radar.AxisRendererCircular.new(root, {
      minGridDistance: 30,
    });
    xRenderer.grid.template.set("forceHidden", true);

    // Create axes
    // https://www.amcharts.com/docs/v5/charts/xy-chart/axes/
    const xAxis = chart.xAxes.push(
      am5xy.CategoryAxis.new(root, {
        maxDeviation: 0,
        renderer: xRenderer,
        categoryField: "category",
      })
    );

    const yRenderer = am5radar.AxisRendererRadial.new(root, {});
    yRenderer.labels.template.set("centerX", am5.p50);

    // Create Y-axis
    const yAxis = chart.yAxes.push(
      am5xy.ValueAxis.new(root, {
        maxDeviation: 0.3,
        min: 0,
        max: 100,
        renderer: yRenderer,
      })
    );

    // Add series
    // https://www.amcharts.com/docs/v5/charts/xy-chart/series/
    const series1 = chart.series.push(
      am5radar.RadarColumnSeries.new(root, {
        name: "Series 1",
        xAxis: xAxis,
        yAxis: yAxis,
        valueYField: "value1",
        categoryXField: "category",
      })
    );

    // Rounded corners for columns
    series1.columns.template.setAll({
      cornerRadius: 5,
      tooltipText: "{categoryX}: {valueY}",
    });

    chart
      ?.get("colors")
      ?.set("colors", [
        am5.color(0x095256),
        am5.color(0x087f8c),
        am5.color(0x5aaa95),
        am5.color(0x86a873),
        am5.color(0xbb9f06),
      ]);

    // Make each column to be of a different color
    series1.columns.template.adapters.add("fill", function (fill, target) {
      return chart?.get("colors")?.getIndex(series1.columns.indexOf(target));
    });

    series1.columns.template.adapters.add("stroke", function (stroke, target) {
      return chart?.get("colors")?.getIndex(series1.columns.indexOf(target));
    });

    // Define data
    const data = [
      {
        category: intl.formatMessage({ id: "tea.production" }),
        value1: simResults[0]?.productionScore,
      },
      {
        category: intl.formatMessage({ id: "tea.manufacturing" }),
        value1: simResults[0]?.manufacturingScore,
      },
      {
        category: intl.formatMessage({ id: "tea.logistics" }),
        value1: simResults[0]?.logisticsScore,
      },
      {
        category: intl.formatMessage({ id: "tea.other" }),
        value1: simResults[0]?.otherScore,
      },
    ];
    xAxis.data.setAll(data);
    series1.data.setAll(data);

    // Make stuff animate on load
    // https://www.amcharts.com/docs/v5/concepts/animations/
    series1.appear(1000);
    chart.appear(1000, 100);

    return () => {
      root.dispose();
    };
  }, []);

  return (
    <Container>
      {/* Sim Results Empty Section */}
      {simResults.length === 0 ? (
        <div>
          <Chart id="chartdiv" />
          <SCard maxWidth="500px" mt={{ base: "50px", lg: "60px" }} p="20px">
            <FormattedMessage id="simExplanation" />
            <br />
            <FormattedMessage id="simExplanation.note" />
          </SCard>
        </div>
      ) : (
        <Chart
          onClick={() => redirectToDetails(simResults[0].id)}
          id="chartdiv"
        >
          <ChartDetails></ChartDetails>
        </Chart>
      )}
    </Container>
  );
};
