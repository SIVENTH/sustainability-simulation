/* eslint-disable max-lines-per-function */
import React from "react";
import { FormattedMessage } from "react-intl";
import { ResultDocument } from "types";
import { createPdfFromText, intl } from "utils";
import { useAuth } from "context";
import {
  StyledContainer,
  StyledContent,
  StyledHeader,
  StyledTable,
  StyledViewAll,
  StyledIcons,
  StyledOptionIcon,
} from "./RecentContainer.styled";
import { Table } from "components/table";
import { PremiumChip } from "components/ui/PremiumChip";
import document_text_outlined from "images/The_Icon_of-SVG/Files/document_text_outlined.svg";
import download_to from "images/The_Icon_of-SVG/UI/download_to.svg";
import share from "images/The_Icon_of-SVG/UI/share.svg";
import { email, subject } from "constants/index";

interface ResultDocumentWithId extends ResultDocument {
  id: string;
}

interface Props {
  simResults: ResultDocumentWithId[];
  redirectToDetails: (id: string) => void;
  loadMore: () => void;
  hasMore: boolean;
}

export const RecentContainer = ({
  redirectToDetails,
  simResults,
  loadMore,
  hasMore,
}: Props): JSX.Element => {
  const { authedUser } = useAuth();
  const premium =
    authedUser && authedUser.hasOwnProperty("premium") ? true : false;

  const loadMoreResults = (): void => {
    if (hasMore) loadMore();
  };

  const simplifiedResults = simResults.map((result) => {
    const newDate: Date = new Date(1970, 0, 1);
    newDate.setSeconds(result.date.seconds);
    const date = newDate.toLocaleDateString(navigator.language, {
      day: "2-digit",
      month: "long",
      year: "numeric",
    });
    const longBreak = "..................................................";
    const shortBreak = ".........................";
    const pdfContent = {
      text: [
        `${intl.formatMessage({ id: "title" })}: ${result.title}`,
        `${intl.formatMessage({ id: "date" })}: ${date}`,
        `${intl.formatMessage({ id: "total" })}: ${result.totalScore}%`,
        `${longBreak}`,
        `${intl.formatMessage({ id: "tea.production.growingScore" })}: ${
          result.growingScore
        }%`,
        `${intl.formatMessage({ id: "tea.production.pickingScore" })}: ${
          result.pickingScore
        }%`,
        `${shortBreak}`,
        `${intl.formatMessage({ id: "tea.production.score" })}: ${
          result.productionScore
        }%`,
        `${longBreak}`,
        `${intl.formatMessage({ id: "tea.manufacturing.witheringScore" })}: ${
          result.witheringScore
        }%`,
        `${intl.formatMessage({ id: "tea.manufacturing.rollingScore" })}: ${
          result.rollingScore
        }%`,
        `${intl.formatMessage({ id: "tea.manufacturing.fermentingScore" })}: ${
          result.fermentingScore
        }%`,
        `${intl.formatMessage({ id: "tea.manufacturing.dryingScore" })}: ${
          result.dryingScore
        }%`,
        `${intl.formatMessage({ id: "tea.manufacturing.blendingScore" })}: ${
          result.blendingScore
        }%`,
        `${shortBreak}`,
        `${intl.formatMessage({ id: "tea.manufacturing.score" })}: ${
          result.manufacturingScore
        }%`,
        `${longBreak}`,
        `${intl.formatMessage({ id: "tea.logistics.packagingScore" })}: ${
          result.packagingScore
        }%`,
        `${intl.formatMessage({ id: "tea.logistics.distributionScore" })}: ${
          result.distributionScore
        }%`,
        `${intl.formatMessage({ id: "tea.logistics.storageScore" })}: ${
          result.storageScore
        }%`,
        `${intl.formatMessage({ id: "tea.logistics.customerScore" })}: ${
          result.customerScore
        }%`,
        `${shortBreak}`,
        `${intl.formatMessage({ id: "tea.logistics.score" })}: ${
          result.logisticsScore
        }%`,
        `${longBreak}`,
        `${intl.formatMessage({ id: "tea.other.endOfLifeScore" })}: ${
          result.endOfLifeScore
        }%`,
        `${shortBreak}`,
        `${intl.formatMessage({ id: "tea.other.score" })}: ${
          result.otherScore
        }%`,
        `${longBreak}`,
        `${intl.formatMessage({ id: "total" })}: ${result.totalScore}%`,
      ],
    };

    return {
      date: date,
      simulation: result.title,
      score: `${result.totalScore.toFixed(2)}%`,
      id: result.id,
      options: (
        <StyledIcons>
          <StyledOptionIcon
            src={document_text_outlined}
            onClick={() => redirectToDetails(result.id)}
          />
          <StyledOptionIcon
            src={download_to}
            onClick={() => createPdfFromText(pdfContent)}
          />
          <StyledOptionIcon
            src={share}
            onClick={() => createPdfFromText(pdfContent)}
          />
        </StyledIcons>
      ),
    };
  });

  const reports = React.useMemo(() => simplifiedResults, []);
  const columns = React.useMemo(
    // need to figure out how to pass id
    () => [
      {
        Header: intl.formatMessage({ id: "date" }),
        accessor: "date",
      },
      {
        Header: intl.formatMessage({ id: "area" }),
        accessor: "simulation",
      },
      {
        Header: intl.formatMessage({ id: "score" }),
        accessor: "score",
      },
      {
        Header: intl.formatMessage({ id: "options" }),
        accessor: "options",
      },
    ],
    []
  );

  return (
    <StyledContainer>
      {/* Sim Results Existing Section */}
      <StyledContent>
        {simResults.length <= 0 ? (
          <div>
            <StyledHeader>
              <h3>
                <FormattedMessage id="recent.title" />
              </h3>
            </StyledHeader>
          </div>
        ) : (
          <div>
            <StyledHeader>
              <h3>
                <FormattedMessage id="recent.title" />
              </h3>
              {premium ? (
                <StyledViewAll onClick={loadMoreResults}>
                  <FormattedMessage id="viewall" />
                </StyledViewAll>
              ) : (
                <StyledViewAll>
                  <a href={`mailto:${email}?subject=${subject}`}>
                    <FormattedMessage id="viewall" />
                    <PremiumChip />
                  </a>
                </StyledViewAll>
              )}
            </StyledHeader>
            <StyledTable>
              <Table columns={columns} data={reports} />
            </StyledTable>
          </div>
        )}
      </StyledContent>
    </StyledContainer>
  );
};
