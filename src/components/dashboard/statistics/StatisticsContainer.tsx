import { FormattedMessage } from "react-intl";
import { useNavigate } from "react-router-dom";
import { ResultDocument } from "types";
import { ProgressBar } from "components/progressBar";
import {
  StyledContainer,
  StyledHeading,
  StyledStatisticsMetric,
  StyledSubTitle,
  StyledDate,
  StyledLink,
  StyledChevron,
  StyledChevronContainer,
  StyledHeadingContainer,
} from "./StatisticsContainer.styled";

import chevron_down from "images/The_Icon_of-SVG/Arrows/chevron_down.svg";
import chevron_up from "images/The_Icon_of-SVG/Arrows/chevron_up.svg";

interface ResultDocumentWithId extends ResultDocument {
  id: string;
}

interface Props {
  simResults: ResultDocumentWithId[];
  redirectToDetails: (id: string) => void;
}

export const StatisticsContainer = ({
  simResults,
  redirectToDetails,
}: Props): JSX.Element => {
  const navigate = useNavigate();
  return (
    <StyledContainer>
      <StyledHeadingContainer>
        <StyledHeading>
          <h3>
            <FormattedMessage id="detailedAnalysisReports" />
          </h3>
          <h5>
            <FormattedMessage id="industryStandardComparison" />
          </h5>
        </StyledHeading>

        <StyledChevronContainer>
          <StyledChevron
            src={chevron_up}
            onClick={() => {
              null;
            }}
          />
          <StyledChevron
            src={chevron_down}
            onClick={() => {
              null;
            }}
          />
        </StyledChevronContainer>
      </StyledHeadingContainer>

      {/* change from pathname to value for selected for simulations */}
      {/* <SelectModal pathname="/background/tea" isCentered /> move to start new simulation button in header */}
      {simResults.length === 0 ? (
        <></>
      ) : (
        <StyledStatisticsMetric
          onClick={() => redirectToDetails(simResults[0].id)}
        >
          <StyledSubTitle>
            <FormattedMessage id="score" />
          </StyledSubTitle>
          <StyledDate>
            {simResults[0]?.date
              .toDate()
              .toLocaleDateString(navigator.language, {
                day: "2-digit",
                month: "long",
                year: "numeric",
              })}
          </StyledDate>
          <ProgressBar value={simResults[0]?.totalScore} />
          <StyledLink onClick={() => navigate("/billing/overview")}>
            <FormattedMessage id="seeAnalysisHistory" />
          </StyledLink>
        </StyledStatisticsMetric>
      )}
    </StyledContainer>
  );
};
