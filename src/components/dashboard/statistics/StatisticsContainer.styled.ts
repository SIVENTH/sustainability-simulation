import styled from "styled-components";

import { colors } from "utils";

export const StyledContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin: 20px 5px;
`;
export const StyledHeadingContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const StyledHeading = styled.div`
  h3 {
    font-size: 20px;
    font-weight: 700;
  }

  h4 {
    font-size: 14px;
    font-weight: 400;
  }
`;

export const StyledStatisticsMetric = styled.div`
  width: 320px;
  margin: 10px 0;
  padding: 16px;
  border: 1px;
  border-radius: 5px;
  border-color: ${colors.concrete};
  border-style: solid;

  &:hover {
    background-color: ${colors.lightGray};
  }
`;

export const StyledSubTitle = styled.p`
  font-size: 12px;
  font-weight: 700;
  color: ${colors.gray4};
`;

export const StyledDate = styled.div`
  font-size: 24px;
  font-weight: 700;
`;

export const StyledLink = styled.div`
  display: inline-block;
  color: ${colors.blue1};
  cursor: pointer;
  font-size: 12px;
  font-weight: 600;
`;

export const StyledChevronContainer = styled.div``;

export const StyledChevron = styled.img`
  height: 32px;
  width: 32px;
  padding: 6px;
  cursor: pointer;
`;
