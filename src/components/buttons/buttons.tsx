import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import { FormattedMessage } from "react-intl";

import { colors } from "utils";

const StyledButton = styled.button`
  padding: 8px 24px;
  border-radius: 4px;
  border-width: 1px;
  border-style: solid;

  font-weight: 600;
  font-size: 16px;
`;

const StyledBackButton = styled(StyledButton)`
  border-color: ${colors.gray5};
  color: ${colors.gray5};
`;

export const NextButton = styled(StyledButton)`
  background-color: ${colors.green2};
  border-color: ${colors.green2};
  color: white;

  &:disabled {
    opacity: 0.4;
    cursor: not-allowed;
  }
`;

interface BackButtonProps {
  onClick?: () => void;
}

export const BackButton = ({ onClick }: BackButtonProps): JSX.Element => {
  const navigate = useNavigate();

  const onClickBack = onClick || (() => navigate(-1));

  return (
    <StyledBackButton type="button" onClick={onClickBack}>
      <FormattedMessage id="back" />
    </StyledBackButton>
  );
};
