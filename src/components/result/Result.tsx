/* eslint-disable max-lines-per-function */
import { FormattedMessage } from "react-intl";
import { Heading } from "../heading";
import { BarChart } from "../bar";
import { colors } from "utils";
import { Flex, Box, Text, VStack } from "../../components/ChakraReplace.styled";

interface Props {
  growingScore: number;
  pickingScore: number;
  witheringScore: number;
  rollingScore: number;
  fermentingScore: number;
  dryingScore: number;
  blendingScore: number;
  packagingScore: number;
  distributionScore: number;
  storageScore: number;
  customerScore: number;
  endOfLifeScore: number;
  productionScore: number;
  manufacturingScore: number;
  logisticsScore: number;
  otherScore: number;
  totalScore: number;
  showSuggestions: () => void;
}

export const Result = ({
  pickingScore,
  growingScore,
  witheringScore,
  rollingScore,
  fermentingScore,
  dryingScore,
  blendingScore,
  packagingScore,
  distributionScore,
  storageScore,
  customerScore,
  endOfLifeScore,
  productionScore,
  manufacturingScore,
  logisticsScore,
  otherScore,
  totalScore,
}: // showSuggestions,
Props): JSX.Element => {
  return (
    <>
      <Heading> Results </Heading>
      <Text> {new Date().toDateString()}</Text>
      <Text my="20px" color={colors.textGray}>
        This is how your current process rates in regards to sustainability.
        After reviewing the following information, proceed to the next screen to
        see how you can improve your processes and what the associated costs
        are.
      </Text>
      <Flex justifyContent="space-between" flexDirection="column">
        <Box>
          <Box lineHeight="30px" px="5px" py="20px">
            <Heading mt="10px" size="md">
              <FormattedMessage id="tea.production.score" />:{" "}
              {productionScore.toFixed(2)}%
            </Heading>
            <Box mt="5px">
              <Text>
                {" "}
                <FormattedMessage id="tea.production.growingScore" />:{" "}
                {growingScore.toFixed(2)}%
              </Text>
              <Text>
                {" "}
                <FormattedMessage id="tea.production.pickingScore" />:{" "}
                {pickingScore.toFixed(2)}%
              </Text>
            </Box>
            <Heading mt="10px" size="md">
              <FormattedMessage id="tea.manufacturing.score" />:{" "}
              {manufacturingScore.toFixed(2)}%
            </Heading>
            <Box mt="5px">
              <Text>
                {" "}
                <FormattedMessage id="tea.manufacturing.witheringScore" />:{" "}
                {witheringScore.toFixed(2)}%
              </Text>
              <Text>
                {" "}
                <FormattedMessage id="tea.manufacturing.rollingScore" />:{" "}
                {rollingScore.toFixed(2)}%
              </Text>
              <Text>
                {" "}
                <FormattedMessage id="tea.manufacturing.fermentingScore" />:{" "}
                {fermentingScore.toFixed(2)}%
              </Text>
              <Text>
                {" "}
                <FormattedMessage id="tea.manufacturing.dryingScore" />:{" "}
                {dryingScore.toFixed(2)}%
              </Text>
              <Text>
                {" "}
                <FormattedMessage id="tea.manufacturing.blendingScore" />:{" "}
                {blendingScore.toFixed(2)}%
              </Text>
            </Box>
            <Heading mt="10px" size="md">
              <FormattedMessage id="tea.logistics.score" />:{" "}
              {logisticsScore.toFixed(2)}%
            </Heading>
            <Box mt="5px">
              <Text>
                {" "}
                <FormattedMessage id="tea.logistics.packagingScore" />:{" "}
                {packagingScore.toFixed(2)}%{" "}
              </Text>
              <Text>
                <FormattedMessage id="tea.logistics.distributionScore" />:{" "}
                {distributionScore.toFixed(2)}%
              </Text>
              <Text>
                {" "}
                <FormattedMessage id="tea.logistics.storageScore" />:{" "}
                {storageScore.toFixed(2)}%{" "}
              </Text>
              <Text>
                {" "}
                <FormattedMessage id="tea.logistics.customerScore" />:{" "}
                {customerScore.toFixed(2)}%{" "}
              </Text>
            </Box>
            <Heading mt="10px" size="md">
              <FormattedMessage id="tea.other.score" />:
            </Heading>
            <Box mt="5px">
              <Text>
                {" "}
                <FormattedMessage id="tea.other.endOfLifeScore" />:{" "}
                {endOfLifeScore.toFixed(2)}%
              </Text>
            </Box>
          </Box>
        </Box>
        <VStack spacing={0}>
          <Heading>
            <FormattedMessage id="supplyChains.tea" />
          </Heading>
          <Heading mt="10px" size="md">
            <FormattedMessage id="total" />: {totalScore.toFixed(2)}%
          </Heading>
          <BarChart
            productionScore={productionScore}
            manufacturingScore={manufacturingScore}
            logisticsScore={logisticsScore}
            otherScore={otherScore}
          />
          {/* <Button onClick={showSuggestions} h="40px" isFullWidth>
            Suggestions
          </Button> */}
        </VStack>
      </Flex>
    </>
  );
};
