import { Heading as ChackraHeading, HeadingProps } from "@chakra-ui/react";

export const Heading = ({
  children,
  ...rest
}: React.PropsWithChildren<HeadingProps>): JSX.Element => {
  return (
    <ChackraHeading as="h1" fontWeight={600} {...rest}>
      {children}
    </ChackraHeading>
  );
};
