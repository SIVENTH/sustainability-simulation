import { Radio, RadioGroup } from "@chakra-ui/react";
import { Answer } from "types";

interface RadioGroupProps {
  options: Answer[];
  defaultValue: string;
}

export const RadioGroupQuestion = ({
  options,
  defaultValue,
}: RadioGroupProps): JSX.Element => {
  return (
    <RadioGroup
      defaultValue={defaultValue}
      justifyContent="flex-start"
      flexDirection={{ base: "column", lg: "row" }}
      mt="5px"
    >
      {options.map(({ key, value }) => (
        <Radio key={key} value={value} size="lg" mr="10px">
          {value}
        </Radio>
      ))}
    </RadioGroup>
  );
};
