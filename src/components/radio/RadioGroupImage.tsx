import { Answer } from "types";
import {
  useRadio,
  RadioProps,
  // SkeletonCircle,
  useRadioGroup,
} from "@chakra-ui/react";
import { Text, Box, Flex, Image } from "../../components/ChakraReplace.styled";

interface RadioImageProps {
  imageUrl: string | null;
  radioProps?: RadioProps;
  defaultValue?: string;
  children: React.ReactNode;
}

const RadioImage = ({
  children,
  radioProps,
  imageUrl,
}: RadioImageProps): JSX.Element => {
  const { getInputProps, getCheckboxProps } = useRadio(radioProps);
  const input = getInputProps();
  const checkbox = getCheckboxProps();

  return (
    <Box as="label">
      <input {...input} />
      <Image
        {...checkbox}
        m="10px"
        mt="10px"
        cursor="pointer"
        opacity={0.7}
        borderWidth="1px"
        borderRadius="full"
        boxShadow="md"
        boxSize="120px"
        objectFit="cover"
        alt="app image"
        // fallback={<SkeletonCircle size="20" />}
        src={imageUrl || undefined}
      />
      <Text align="center">{children}</Text>
    </Box>
  );
};

interface RadioGroupImageProps {
  options: Answer[];
  defaultValue: string;
}

export const RadioGroupImage = ({
  options,
  defaultValue,
}: RadioGroupImageProps): JSX.Element => {
  const { getRootProps, getRadioProps } = useRadioGroup({ defaultValue });
  const group = getRootProps();

  return (
    <Flex flexDirection="column" alignItems="center" {...group}>
      {options.map(({ key, value, imageUrl }) => {
        const radio = getRadioProps({ value });
        return (
          <RadioImage key={key} radioProps={radio} imageUrl={imageUrl}>
            {value}
          </RadioImage>
        );
      })}
    </Flex>
  );
};
