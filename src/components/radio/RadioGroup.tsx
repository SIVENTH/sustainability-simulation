// Normal radio that does not use the "Answer" type.
// Better to consolidate these radios later if possible.

import { Radio, RadioGroup } from "@chakra-ui/react";
import { Stack } from "../../components/ChakraReplace.styled";
import { StringOrNumber } from "@chakra-ui/utils";
import { Option } from "types";

interface RadioGroupProps {
  options: Option[];
  defaultValue: string;
  onChange?: (nextValue: StringOrNumber) => void;
}

export const RadioGroupRegular = ({
  options,
  defaultValue,
  onChange,
}: RadioGroupProps): JSX.Element => {
  return (
    <RadioGroup defaultValue={defaultValue} onChange={onChange}>
      <Stack flexDirection="row">
        {options.map(({ label, value }) => (
          <Radio key={value} value={value}>
            {label}
          </Radio>
        ))}
      </Stack>
    </RadioGroup>
  );
};
