import { useState, useEffect } from "react";
import { Select, FormControl, Skeleton, SelectProps } from "@chakra-ui/react";
import { db } from "gateway";
import { Answer } from "types";
import { useParams } from "react-router-dom";
import { useAuth } from "context";
import {
  Controller,
  useFormContext,
  Message,
  ValidationRule,
} from "react-hook-form";

export type Rules = Partial<{
  required: Message | ValidationRule<boolean>;
}>;

interface DropdownProps<T> extends SelectProps {
  name: string;
  answers: T[];
  isRequired?: boolean;
  placeholder?: string;
  rules?: Rules;
}

// eslint-disable-next-line max-lines-per-function
export const Dropdown = <T extends Answer>({
  name,
  answers,
  isRequired,
  rules,
  placeholder,
  ...rest
}: DropdownProps<T>): JSX.Element => {
  const { id } = useParams<{ id: string }>();
  const { authedUser } = useAuth();
  const { control, errors, setValue } = useFormContext();
  const [defaultValue, setDefautValue] = useState<string>(answers[0].value);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    db.getOneDocument<{ id: string; [key: string]: string }>(
      `users/${authedUser?.uid}/sim-history/${id}`
    )
      .then((doc) => {
        if (doc[name]) {
          setDefautValue(doc[name]);
          setValue(name, doc[name]);
        }
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  }, [name, setValue, id, authedUser?.uid]);

  if (loading) return <Skeleton height="10px" w="60%" />;

  return (
    <Controller
      as={
        <FormControl
          isInvalid={Boolean(errors[name])}
          id={name}
          isRequired={isRequired}
        >
          <Select
            style={{ borderRadius: 0 }}
            defaultValue={defaultValue}
            placeholder={placeholder}
            ml="10px"
            mt="5px"
            size="lg"
            w={{ lg: "60%" }}
            {...rest}
          >
            {answers.map(({ key, value }) => (
              <option key={key}>{value} </option>
            ))}
          </Select>
        </FormControl>
      }
      name={name}
      rule={rules}
      control={control}
      defaultValue={defaultValue}
    />
  );
};
