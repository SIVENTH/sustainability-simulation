import { Box } from "../../components/ChakraReplace.styled";
import { Input } from "./Input";
import { NumberInput } from "./NumberInput";
import { Radio } from "./Radio";
import { QuestionRadio } from "./QuestionRadio";
import { RadioImage } from "./RadioImage";
import { Dropdown } from "./Dropdown";
import { Select } from "./Select";
import { Button } from "./Button";
import {
  useForm,
  FormProvider,
  SubmitHandler,
  UseFormOptions,
} from "react-hook-form";

interface Props<T> {
  children: React.ReactNode;
  onSubmit: SubmitHandler<T>;
  defaultValues?: UseFormOptions<T>["defaultValues"];
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const Form = <TFormValues extends Record<string, any>>({
  children,
  onSubmit,
  defaultValues,
}: Props<TFormValues>): JSX.Element => {
  const methods = useForm<TFormValues>({ mode: "onChange", defaultValues });

  return (
    <FormProvider {...methods}>
      <Box as="form" onSubmit={methods.handleSubmit(onSubmit)}>
        {children}
      </Box>
    </FormProvider>
  );
};

Form.Input = Input;
Form.NumberInput = NumberInput;
Form.Radio = Radio;
Form.QuestionRadio = QuestionRadio;
Form.RadioImage = RadioImage;
Form.Dropdown = Dropdown;
Form.Button = Button;
Form.Select = Select;
