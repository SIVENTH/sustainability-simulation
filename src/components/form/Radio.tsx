import {
  FormControl,
  SelectFieldProps,
  FormErrorMessage,
} from "@chakra-ui/react";
import { StringOrNumber } from "@chakra-ui/utils";
import {
  Controller,
  useFormContext,
  Message,
  ValidationRule,
} from "react-hook-form";

import { RadioGroupRegular } from "../radio";

export type Rules = Partial<{
  required: Message | ValidationRule<boolean>;
}>;

interface SelectProps<T> {
  name: string;
  options: T[];
  isRequired?: boolean;
  placeholder?: string;
  defaultValue?: string;
  rules?: Rules;
  onChange?: (nextValue: StringOrNumber) => void;
}

export const Radio = <
  T extends {
    value: string | number;
    label: string;
    disabled: boolean;
  }
>({
  name,
  options,
  isRequired,
  rules,
  defaultValue = "",
  onChange,
}: SelectFieldProps & SelectProps<T>): JSX.Element => {
  const { control, errors } = useFormContext();

  return (
    <Controller
      as={
        <FormControl
          isInvalid={Boolean(errors[name])}
          id={name}
          isRequired={isRequired}
        >
          <RadioGroupRegular
            options={options}
            defaultValue={defaultValue}
            onChange={onChange}
          />
          <FormErrorMessage> {errors[name]?.message} </FormErrorMessage>
        </FormControl>
      }
      name={name}
      rule={rules}
      control={control}
      defaultValue={defaultValue}
    />
  );
};
