import { Form } from "../Form";
import { render, act, fireEvent, waitFor } from "@testing-library/react";
jest.mock("gateway", () => ({ auth: jest.fn() }));

describe("Form", () => {
  it("renders required elements", () => {
    const onSubmit = jest.fn();
    const screen = render(
      <Form onSubmit={onSubmit}>
        <Form.Input name="test" label="label" placeholder="placeholder" />
      </Form>
    );
    expect(screen.getByLabelText("label")).toBeInTheDocument();
    expect(screen.getByPlaceholderText("placeholder")).toBeInTheDocument();
    expect(screen.container).toMatchSnapshot();
  });

  it("renders an input where user can type", async () => {
    const onSubmit = jest.fn();
    const screen = render(
      <Form onSubmit={onSubmit}>
        <Form.Input name="test" label="label" placeholder="placeholder" />
      </Form>
    );
    const input = screen.getByPlaceholderText("placeholder");
    act(() => {
      fireEvent.change(input, { target: { value: "123" } });
    });
    await waitFor(() =>
      expect(screen.getByDisplayValue("123")).toBeInTheDocument()
    );
  });
});
