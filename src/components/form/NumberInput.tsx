import {
  Controller,
  useFormContext,
  ValidationRule,
  Message,
} from "react-hook-form";
import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  NumberInput as Input,
  NumberInputProps,
  NumberInputField,
  FormLabelProps,
} from "@chakra-ui/react";

export type Rules = Partial<{
  required: Message | ValidationRule<boolean>;
  min: ValidationRule<number | string>;
  max: ValidationRule<number | string>;
  maxLength: ValidationRule<number | string>;
  minLength: ValidationRule<number | string>;
  pattern: ValidationRule<RegExp>;
}>;

export interface InputProps extends NumberInputProps {
  name: string;
  label: string;
  rules?: Rules;
  isRequired?: boolean;
  defaultValue?: string;
  placeholder?: string;
  labelProps?: FormLabelProps;
}

export const NumberInput = ({
  name,
  label,
  rules,
  isRequired,
  defaultValue = "",
  placeholder,
  labelProps,
  ...rest
}: InputProps): JSX.Element => {
  const { control, errors } = useFormContext();

  return (
    <Controller
      as={
        <FormControl
          isInvalid={Boolean(errors[name])}
          id={name}
          isRequired={isRequired}
        >
          <FormLabel {...labelProps}>{label}</FormLabel>
          <Input placeholder={placeholder} borderLeftRadius={0} {...rest}>
            <NumberInputField />
          </Input>
          <FormErrorMessage> {errors[name]?.message} </FormErrorMessage>
        </FormControl>
      }
      name={name}
      rules={rules}
      control={control}
      defaultValue={defaultValue}
    />
  );
};
