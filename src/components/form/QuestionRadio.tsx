import { useEffect, useState } from "react";
import { RadioGroupQuestion } from "../radio";
import { FormControl, FormErrorMessage, Skeleton } from "@chakra-ui/react";
import { useParams } from "react-router-dom";
import { Answer } from "types";
import { db } from "gateway";
import { useAuth } from "context";
import {
  Controller,
  useFormContext,
  ValidationRule,
  Message,
} from "react-hook-form";

export type Rules = Partial<{
  required: Message | ValidationRule<boolean>;
}>;

export interface RadioProps<T> {
  name: string;
  answers: T[];
  type?: string;
  rules?: Rules;
  isRequired?: boolean;
}

export const QuestionRadio = <T extends Answer>({
  name,
  answers,
  isRequired,
  rules,
}: RadioProps<T>): JSX.Element => {
  const { id } = useParams<{ id: string }>();
  const { authedUser } = useAuth();
  const { control, errors, setValue } = useFormContext();
  const [defaultValue, setDefautValue] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    db.getOneDocument<{ id: string; [key: string]: string }>(
      `users/${authedUser?.uid}/sim-history/${id}`
    )
      .then((doc) => {
        if (doc[name]) {
          setDefautValue(doc[name]);
          setValue(name, doc[name]);
        }
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  }, [name, setValue, id, authedUser?.uid]);

  if (loading) return <Skeleton height="10px" w="60%" />;

  return (
    <Controller
      as={
        <FormControl
          isInvalid={Boolean(errors[name])}
          id={name}
          isRequired={isRequired}
        >
          <RadioGroupQuestion options={answers} defaultValue={defaultValue} />
          <FormErrorMessage> {errors[name]?.message} </FormErrorMessage>
        </FormControl>
      }
      name={name}
      rules={rules}
      mode="onChange"
      control={control}
      defaultValue={defaultValue}
    />
  );
};
