import React from "react";
import { useFormContext } from "react-hook-form";
import { ButtonProps as ChakraButtonProps } from "@chakra-ui/react";
import { Button as ChakraButton } from "../../components/ChakraReplace.styled";

export interface ButtonProps extends ChakraButtonProps {
  title: string;
  type: "button" | "submit" | "reset" | undefined;
}

export const Button: React.FC<ButtonProps> = ({
  title,
  type,
  ...rest
}): JSX.Element => {
  const { formState } = useFormContext();

  return (
    <ChakraButton
      type={type}
      disabled={
        (!formState.isValid || formState.isSubmitting) && type === "submit"
      }
      {...rest}
    >
      {title}
    </ChakraButton>
  );
};
