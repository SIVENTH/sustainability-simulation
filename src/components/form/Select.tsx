import {
  Select as ChakraSelect,
  FormControl,
  SelectFieldProps,
} from "@chakra-ui/react";
import {
  Controller,
  useFormContext,
  Message,
  ValidationRule,
} from "react-hook-form";
import { Option } from "types";

export type Rules = Partial<{
  required: Message | ValidationRule<boolean>;
}>;

interface SelectProps<T> {
  name: string;
  options: T[];
  isRequired?: boolean;
  placeholder?: string;
  defaultValue?: string;
  rules?: Rules;
}

// eslint-disable-next-line max-lines-per-function
export const Select = <T extends Option>({
  name,
  options,
  isRequired,
  rules,
  placeholder,
  defaultValue = "",
  ...selectFieldProps
}: SelectFieldProps & SelectProps<T>): JSX.Element => {
  const { control, errors } = useFormContext();

  return (
    <Controller
      as={
        <FormControl
          isInvalid={Boolean(errors[name])}
          id={name}
          isRequired={isRequired}
        >
          <ChakraSelect
            style={{ borderRadius: 0 }}
            placeholder={placeholder}
            ml="10px"
            mt="5px"
            size="lg"
            {...selectFieldProps}
          >
            {options.map(({ value, label, disabled }) => (
              <option key={value} value={value} disabled={disabled}>
                {label}
              </option>
            ))}
          </ChakraSelect>
        </FormControl>
      }
      name={name}
      rule={rules}
      control={control}
      defaultValue={defaultValue}
    />
  );
};
