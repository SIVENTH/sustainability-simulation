import {
  Controller,
  useFormContext,
  ValidationRule,
  Message,
} from "react-hook-form";
import {
  FormControl,
  FormLabel,
  FormErrorMessage,
  Input as TextInput,
} from "@chakra-ui/react";

export type Rules = Partial<{
  required: Message | ValidationRule<boolean>;
  min: ValidationRule<number | string>;
  max: ValidationRule<number | string>;
  maxLength: ValidationRule<number | string>;
  minLength: ValidationRule<number | string>;
  pattern: ValidationRule<RegExp>;
}>;

export interface InputProps {
  name: string;
  label: string;
  rules?: Rules;
  isRequired?: boolean;
  defaultValue?: string;
  placeholder?: string;
  [key: string]: unknown;
}

export const Input = ({
  name,
  label,
  rules,
  isRequired,
  defaultValue = "",
  placeholder,
  ...rest
}: InputProps): JSX.Element => {
  const { control, errors } = useFormContext();

  return (
    <Controller
      as={
        <FormControl
          isInvalid={Boolean(errors[name])}
          id={name}
          isRequired={isRequired}
        >
          <FormLabel>{label}</FormLabel>
          <TextInput placeholder={placeholder} borderLeftRadius={0} {...rest} />
          <FormErrorMessage> {errors[name]?.message} </FormErrorMessage>
        </FormControl>
      }
      name={name}
      rules={rules}
      mode="onChange"
      control={control}
      defaultValue={defaultValue}
    />
  );
};
