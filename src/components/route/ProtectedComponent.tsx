import { Navigate } from "react-router-dom";
import { useAuth } from "context";

export const ProtectedComponent = ({
  children,
}: {
  children: JSX.Element;
}): JSX.Element => {
  const { authedUser } = useAuth();

  if (!authedUser) {
    return <Navigate to="/" />;
  } else {
    return children;
  }
};
