import { Timestamp } from "gateway";
import { Heading } from "../heading";
import {
  Box,
  Text,
  Flex,
  Image,
  VStack,
} from "../../components/ChakraReplace.styled";

interface Props {
  id: string;
  title: string;
  date: Timestamp;
  totalScore: number;
  handleClick: (id: string) => void;
  imageUrl?: string;
}

export const Card = ({
  id,
  title,
  date,
  imageUrl,
  totalScore,
  handleClick,
}: Props): JSX.Element => {
  return (
    <Box
      maxWidth="sm"
      height="250px"
      borderWidth="1px"
      borderRadius="lg"
      overflow="hidden"
      boxShadow="sm"
      cursor="pointer"
      onClick={() => handleClick(id)}
    >
      <Flex p="10px" justifyContent="space-between">
        <VStack align="flex-start">
          <Heading size="md">{title}</Heading>
          <Text> {date.toDate().toDateString()} </Text>
        </VStack>
        <Heading size="md">{totalScore}%</Heading>
      </Flex>
      <Image
        src={imageUrl}
        alt="sim image"
        objectFit="contain"
        // fallback={<Container centerContent>...Loading</Container>}
      />
    </Box>
  );
};
