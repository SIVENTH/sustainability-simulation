import { BoxProps } from "@chakra-ui/react";
import { Box } from "../../components/ChakraReplace.styled";

export const SCard = ({ children, ...rest }: BoxProps): JSX.Element => (
  <Box
    maxWidth="sm"
    maxHeight="350px"
    borderWidth="1px"
    borderRadius="lg"
    overflow="hidden"
    boxShadow="sm"
    cursor="pointer"
    {...rest}
  >
    {children}
  </Box>
);
