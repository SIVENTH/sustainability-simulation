import { Timestamp } from "gateway";
import { createPdfFromText } from "utils";
import { Heading } from "../heading";
import { useColorMode } from "@chakra-ui/react";
import {
  Text,
  HStack,
  VStack,
  Flex,
  Button,
} from "../../components/ChakraReplace.styled";

interface Props {
  id: string;
  title: string;
  date: Timestamp;
  growingScore: number;
  pickingScore: number;
  totalScore: number;
  handleClick: (id: string) => void;
}

export const VCard = ({
  id,
  title,
  date,
  growingScore,
  pickingScore,
  totalScore,
  handleClick,
}: Props): JSX.Element => {
  const { colorMode } = useColorMode();

  return (
    <Flex
      justifyContent="space-between"
      align="center"
      flexDirection="column"
      p="10px"
      px="20px"
      m="15px"
      minHeight="100px"
      width="80%"
      backgroundColor={colorMode === "light" ? "lightgray" : "gray.800"}
      borderWidth="1px"
      borderRadius="lg"
      overflow="hidden"
      boxShadow="sm"
      cursor="pointer"
    >
      <HStack spacing={57}>
        <VStack spacing={0}>
          <Heading size="md">{title}</Heading>
          <Text> {date.toDate().toDateString()} </Text>
        </VStack>
        <Heading size="md">{totalScore}%</Heading>
      </HStack>
      <Flex flex="0.5" flexDirection="column" justifyContent="flex-end">
        <HStack>
          <Button onClick={() => handleClick(id)}>Details</Button>
          <Button
            onClick={() =>
              createPdfFromText({
                text: [
                  `Growing Score: ${growingScore}%`,
                  `Picking Score: ${pickingScore}%`,
                  `Total Score: ${totalScore}%`,
                ],
              })
            }
          >
            Download
          </Button>
        </HStack>
      </Flex>
    </Flex>
  );
};
