import styled from "styled-components";

export const StyledHeaderContainer = styled.div`
  h1 {
    font-size: 48px;
  }
`;

export const StyledHeaderInfo = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: baseline;
`;

export const StyledSessionInfo = styled.div`
  font-weight: bold;
  font-size: 14px;
  color: #4eb478;
`;

export const StyledDate = styled.div`
  color: #424242;
  font-weight: bold;
  font-size: 14px;
`;

export const StyledAdditionalInfo = styled.div`
  font-size: 18px;
  color: #424242;
  margin: 24px 0 32px 0;
`;
