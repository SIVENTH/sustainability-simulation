import { FormattedMessage } from "react-intl";
import { Heading } from "../heading";
import {
  StyledHeaderContainer,
  StyledSessionInfo,
  StyledDate,
  StyledHeaderInfo,
  StyledAdditionalInfo,
} from "./Section.styled";

interface Props {
  section: string;
  date: string;
  sectionPage: number;
  pageLength: number;
  additionalInfo: string | null;
}

export const Section = ({
  section,
  date,
  sectionPage,
  pageLength,
  additionalInfo,
}: Props): JSX.Element => {
  return (
    <>
      <StyledHeaderContainer>
        <StyledHeaderInfo>
          <StyledSessionInfo>
            <FormattedMessage
              id="section"
              values={{ start: sectionPage, finish: pageLength }}
            />
          </StyledSessionInfo>
          <StyledDate>{date}</StyledDate>
        </StyledHeaderInfo>
        <Heading>{section}</Heading>
        <StyledAdditionalInfo>{additionalInfo}</StyledAdditionalInfo>
      </StyledHeaderContainer>
    </>
  );
};
