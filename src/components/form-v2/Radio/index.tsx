import { memo } from "react";
import { Controller, useFormContext } from "react-hook-form";
import classNames from "classnames";

import { Error } from "components/form-v2";

import { RadioWrapper } from "./Radio.styled";

const Radio = ({ name, options }) => {
  const { control, errors } = useFormContext();

  return (
    <>
      <Controller
        name={name}
        defaultValue=""
        control={control}
        render={({ value, onChange }) => (
          <RadioWrapper>
            {options.map((option) => {
              const { value: currentValue, label } = option;
              return (
                <div
                  key={currentValue}
                  className={classNames("option", {
                    selected: value === currentValue,
                  })}
                  onClick={() => onChange(currentValue)}
                >
                  {label}
                </div>
              );
            })}
          </RadioWrapper>
        )}
      />
      <Error error={errors[name]} />
    </>
  );
};

export default memo(Radio);
