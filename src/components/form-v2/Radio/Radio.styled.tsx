import styled from "styled-components";

import { colors } from "utils";

export const RadioWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 24px 12px;

  .option {
    width: 280px;
    height: 44px;
    border: 1px solid ${colors.gray4};
    border-radius: 8px;
    cursor: pointer;
    padding: 16px;
    color: ${colors.gray4};
    font-weight: 600;

    display: flex;
    align-items: center;
    gap: 8px;

    &.selected {
      background-color: ${colors.lightGreen};
      border-color: ${colors.green2};
      cursor: default;
      color: ${colors.green2};
    }

    &:before {
      content: "";
      width: 12px;
      height: 12px;
      border-radius: 50%;
      background-color: ${colors.lightGreen};
      border: 2px solid ${colors.lightGreen2};
      display: inline-block;
    }

    &.selected:before {
      background-color: ${colors.green2};
    }
  }
`;
