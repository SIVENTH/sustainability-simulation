import styled from "styled-components";

import { colors } from "utils";

interface Props {
  noError: boolean;
}

export const StyledError = styled.p<Props>`
  margin: 0;
  height: 24px;
  margin-top: 4px;
  color: ${colors.textRed};
  font-size: 14px;

  ${({ noError }) => noError && "color: transparent;"}
`;
