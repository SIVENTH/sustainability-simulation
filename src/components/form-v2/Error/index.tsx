import { FieldError } from "react-hook-form";

import { StyledError } from "./Error.styled";

interface Props {
  error: FieldError;
}

const ErrorMessage = ({ error }: Props): JSX.Element => {
  return <StyledError noError={!error}>{error && error.message}</StyledError>;
};

export default ErrorMessage;
