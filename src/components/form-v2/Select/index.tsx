import { memo } from "react";
import ReactSelect from "react-select";
import { Controller, useFormContext } from "react-hook-form";

import { Error } from "components/form-v2";
import { SelectWrapper } from "./Select.styled";

const Select = ({ name, options, width, height }) => {
  const { control, errors } = useFormContext();

  return (
    <>
      <SelectWrapper defaultValue="temp" width={width} height={height}>
        <Controller
          name={name}
          control={control}
          defaultValue=""
          render={({ value, onChange }) => (
            <ReactSelect
              defaultValue="temp"
              value={options.find((option) => option.value === value)}
              onChange={(val) => onChange(val.value)}
              options={options}
              classNamePrefix="react-select"
            />
          )}
        />
      </SelectWrapper>
      <Error error={errors[name]} />
    </>
  );
};

export default memo(Select);
