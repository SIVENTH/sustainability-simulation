import styled from "styled-components";

import { colors } from "utils";

interface Props {
  width: string;
  height: string;
}

export const SelectWrapper = styled.div<Props>`
  .react-select__control,
  .react-select__control:hover {
    border: 1px solid ${colors.green2};
    border-radius: 8px;
    box-shadow: none;
    width: ${({ width }) => width};
    min-height: ${({ height }) => height};
    height: ${({ height }) => height};
  }

  .react-select__input-container {
    margin: 0;
    padding: 0;
  }

  .react-select__value-container {
    height: 100%;
    padding: 0px 8px;
  }

  .react-select__indicator-separator {
    display: none;
  }

  .react-select__menu {
    border: 1px solid ${colors.green2};
    border-radius: 8px;
  }

  .react-select__option--is-focused {
    background-color: ${colors.gray6};
  }

  .react-select__option--is-selected {
    color: ${colors.green2};
    background-color: ${colors.gray6};
  }

  .react-select__option:active {
    background-color: ${colors.gray6};
  }
`;
