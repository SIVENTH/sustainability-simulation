/* eslint-disable react/jsx-key */
/* eslint-disable sonarjs/cognitive-complexity */
import { useTable, useSortBy } from "react-table";
import { StyledTableContainer, Caret } from "./Table.styled";
import caret_down from "images/The_Icon_of-SVG/Arrows/caret_down.svg";
import caret_up from "images/The_Icon_of-SVG/Arrows/caret_up.svg";

// currently being used by "recent analysis reports" and "class selection" to display tables. type is set to uknown because of vareity of data
interface Props {
  columns: { Header: string; accessor: string }[];
  data: unknown[];
}

export const Table = ({ columns, data }: Props): JSX.Element => {
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable(
      {
        columns,
        data,
      },
      useSortBy
    );

  return (
    <StyledTableContainer>
      {/* apply the table props */}
      <table {...getTableProps()}>
        <thead>
          {
            // Loop over the header rows
            headerGroups.map((headerGroup) => (
              // Apply the header row props
              <tr {...headerGroup.getHeaderGroupProps()}>
                {
                  // Loop over the headers in each row
                  headerGroup.headers.map((column) => (
                    // Apply the header cell props
                    <th
                      key={column.id}
                      {...column.getHeaderProps(column.getSortByToggleProps())}
                    >
                      {/* Add a sort direction indicator */}
                      {column.render("Header")}
                      {column.id == "options" ? (
                        " "
                      ) : column.isSorted ? (
                        column.isSortedDesc ? (
                          <Caret src={caret_down} />
                        ) : (
                          <Caret src={caret_up} />
                        )
                      ) : (
                        <Caret src={caret_down} />
                      )}
                    </th>
                  ))
                }
              </tr>
            ))
          }
        </thead>
        {/* Apply the table body props */}
        <tbody {...getTableBodyProps()}>
          {
            // Loop over the table rows
            rows.map((row) => {
              // Prepare the row for display
              prepareRow(row);
              return (
                // Apply the row props
                <tr key={row.id} {...row.getRowProps()}>
                  {
                    // Loop over the rows cells
                    row.cells.map((cell) => {
                      // Apply the cell props
                      return (
                        <td {...cell.getCellProps()}>
                          {
                            // Render the cell contents
                            cell.render("Cell")
                          }
                        </td>
                      );
                    })
                  }
                </tr>
              );
            })
          }
        </tbody>
      </table>
    </StyledTableContainer>
  );
};
