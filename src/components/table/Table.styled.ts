import styled from "styled-components";

export const StyledTableContainer = styled.div`
  padding: 1rem;
  table {
    width: 100%;
    border-spacing: 0;

    tr {
      td {
        padding: 10px;
      }

      :last-child {
        td {
          border-bottom: 0;
        }
      }
    }

    th,
    td {
      margin: 0;
      padding: 0.5rem;
      border-bottom: 1px;
      border-right: 1px;

      :last-child {
        border-right: 0;
      }
    }
  }
`;

export const Caret = styled.img`
  height: 32px;
  width: 32px;
  display: initial;
`;
