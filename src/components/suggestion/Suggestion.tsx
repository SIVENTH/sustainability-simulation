import { Box, Text, Container } from "../../components/ChakraReplace.styled";
import { Heading } from "components/heading";
import { colors } from "utils";
import { suggestions } from "data/suggestions";
import { BarChart } from "components/bar";
import { useScrollToTop } from "hooks";

interface Props {
  totalScore: number | undefined;
  productionScore: number | undefined;
  manufacturingScore: number | undefined;
  logisticsScore: number | undefined;
  otherScore: number | undefined;
}

export const Suggestion = ({
  totalScore,
  productionScore,
  manufacturingScore,
  logisticsScore,
  otherScore,
}: Props): JSX.Element => {
  useScrollToTop();
  return (
    <>
      <Heading>Suggestions</Heading>
      <Heading mt="10px" size="md">
        Total score: {totalScore}%
      </Heading>
      <Text my="20px" color={colors.textGray}>
        This is how your current process rates in regards to sustainability.
        After reviewing the following information, proceed to the next screen to
        see how you can improve your processes and what the associated costs
        are.
      </Text>
      <Container maxWidth="sm">
        <BarChart
          productionScore={productionScore}
          manufacturingScore={manufacturingScore}
          logisticsScore={logisticsScore}
          otherScore={otherScore}
        />
      </Container>
      {suggestions.map(({ id, title, areas }) => (
        <Box key={id} mt="20px">
          <Heading size="lg"> {title} </Heading>
          {areas.map((area) => (
            <Text key={area.id} mt="10px">
              <b> {area.type} </b> {area.text}
            </Text>
          ))}
        </Box>
      ))}
    </>
  );
};
