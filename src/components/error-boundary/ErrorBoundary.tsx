import { Component, ErrorInfo, ReactNode } from "react";
import { Container, Text } from "../../components/ChakraReplace.styled";
import { History } from "history";

interface Props {
  children: ReactNode;
  history: History;
}

interface State {
  hasError: boolean;
}

export class ErrorBoundary extends Component<Props, State> {
  public state: State = {
    hasError: false,
  };

  public static getDerivedStateFromError(): State {
    return { hasError: true };
  }

  public componentDidCatch(error: Error, errorInfo: ErrorInfo): void {
    console.error("Uncaught error:", error, errorInfo);
  }

  public componentDidMount(): void {
    const history = this.props.history;
    history.listen(() => {
      if (this.state.hasError) {
        this.setState({ hasError: false });
      }
    });
    this.setState({ hasError: false });
  }
  public render(): JSX.Element | React.ReactNode {
    if (this.state.hasError) {
      return (
        <Container>
          <Text fontSize="18px" textAlign="center">
            Sorry, we encountered a technical error. Please contact
            administrator at admin@siventh.com. admin.
          </Text>
        </Container>
      );
    }
    return this.props.children;
  }
}
