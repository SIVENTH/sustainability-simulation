import { Tooltip as ChakraTooltip, TooltipProps } from "@chakra-ui/react";
import { FaQuestionCircle } from "react-icons/fa";

interface Props {
  text: string;
}

export const Tooltip = ({
  text,
  ...tooltiProps
}: Props & Partial<TooltipProps>): JSX.Element => (
  <ChakraTooltip label={text} aria-label="A tooltip" {...tooltiProps}>
    <span>
      <FaQuestionCircle />
    </span>
  </ChakraTooltip>
);
