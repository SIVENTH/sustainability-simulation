import { StyledBackButton } from "./BackButton.styled";

export const BackButton = (): JSX.Element => {
  return (
    <StyledBackButton>
      <span>&lt;</span>
    </StyledBackButton>
  );
};
