import styled from "styled-components";

import { colors } from "utils";

export const StyledBackButton = styled.div`
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  top: 55px;
  left: 8px;
  background-color: ${colors.gray3};
  height: 18px;
  width: 18px;
  border-radius: 4px;
`;
