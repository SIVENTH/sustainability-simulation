import styled from "styled-components";

import { colors } from "utils";

export const Container = styled.div`
  justify-content: space-between;
`;

export const StyledNavItems = styled.div`
  display: flex;
  align-items: center;
  ul {
    display: flex;
    align-items: center;
    list-style: none;
    padding: 0;
    margin: 0;
  }
  li {
    margin-right: 16px;
    &:last-child {
      font-size: 14px;
      font-weight: 500;
      /* margin: 0 16px 0 24px; */
    }

    a {
      padding: 0;
    }
    a:hover {
      color: #4a966a;
    }
  }
`;

export const StyledLogOut = styled.div`
  display: flex;
  align-items: center;
  color: ${colors.gray5};
  /* padding: 8px 24px;
  border: 1px solid #b3b3b3; */
  border-radius: 8px;

  &:hover {
    color: #4a966a;
    /* border-color: #4a966a; */
  }
  img {
    height: 16px;
    width: 16px;
    margin-right: 8px;
  }
`;

export const UserProfileContainer = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  cursor: pointer;

  .user-info {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  .display-name {
    font-size: 16px;
    font-weight: 600;
  }
  .email {
    font-size: 14px;
    font-weight: 400;
    color: ${colors.gray5};
  }
`;

export const Avatar = styled.div`
  height: 30px;
  width: 30px;
  border-radius: 50%;
  background-color: gray;
  margin-right: 10px;
`;

export const DropdownContainer = styled.div`
  position: absolute;
  top: 75px;
  right: 43px;
  background-color: white;
  box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.2);
  z-index: 1;
`;

export const DropdownItem = styled.div`
  padding: 10px;
  cursor: pointer;
  &:hover {
    background-color: #f5f5f5;
  }
`;
