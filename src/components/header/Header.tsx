// this can probably be removed
import { StyledHeaderContainer, StyledNavOutter } from "./Header.styled";

import { Logo } from "./Logo";
import { Navigation } from "./Navigation";

export const Header = (): JSX.Element => {
  return (
    <StyledHeaderContainer>
      <StyledNavOutter>
        <Logo />
        <Navigation />
      </StyledNavOutter>
    </StyledHeaderContainer>
  );
};
