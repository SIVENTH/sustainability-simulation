import { useRef } from "react";
import { useNavigate } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import { useAuth } from "context";
import { auth } from "gateway";
import chevron_down from "images/The_Icon_of-SVG/Arrows/chevron_down.svg";
import {
  StyledNavUser,
  StyledDropdownItem,
  StyledDropdownContainer,
  StyledAvatar,
} from "./UserProfile.styled";
import { UserProfileContainer } from "./Navigation.styled";
import { LanguageSelector } from "./LanguageSelector";
import { StyledChevron } from "components/dashboard/statistics/StatisticsContainer.styled";
import { useOutsideClick } from "hooks/useOutsideClick";

export const UserProfile = (): JSX.Element => {
  const { authedUser } = useAuth();
  const displayName = authedUser?.displayName;
  const email = authedUser?.email;
  const navigate = useNavigate();
  const dropdownRef = useRef<HTMLDivElement>(null);

  const [dropdownOpen, setDropdownOpen] = useOutsideClick(dropdownRef, false);

  const toggleDropdown = () => {
    setDropdownOpen(!dropdownOpen);
  };

  const handleLogout = async (): Promise<void> => {
    localStorage.removeItem("user");
    localStorage.removeItem("orgId");
    localStorage.removeItem("myOrgData");
    localStorage.removeItem("teamId");
    localStorage.removeItem("currOrganizationId");
    sessionStorage.removeItem("token");
    await auth.logout();
    navigate("/login");
  };

  return (
    <StyledNavUser ref={dropdownRef}>
      <UserProfileContainer onClick={toggleDropdown} data-testid="menu-button">
        <StyledAvatar />
        <div className="user-info">
          <div>
            <div className="display-name">{displayName} </div>
            <div className="email">{email}</div>
          </div>
          <div>
            <StyledChevron src={chevron_down} />
          </div>{" "}
        </div>
      </UserProfileContainer>
      {dropdownOpen && (
        <StyledDropdownContainer>
          <StyledDropdownItem>
            <LanguageSelector close={() => setDropdownOpen(false)} />
          </StyledDropdownItem>
          <StyledDropdownItem onClick={handleLogout} data-testid="logout">
            <FormattedMessage id="logout" />
          </StyledDropdownItem>
        </StyledDropdownContainer>
      )}
    </StyledNavUser>
  );
};
