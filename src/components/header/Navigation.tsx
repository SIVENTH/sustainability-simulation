import { Link as RouterLink } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import { Link } from "../../components/ChakraReplace.styled";
import { useAuth } from "context";
import { LanguageSelector } from "./LanguageSelector";

import { Container, StyledNavItems } from "./Navigation.styled";
import { UserProfile } from "./UserProfile";

export const Navigation = (): JSX.Element => {
  const { authedUser } = useAuth();

  return (
    <Container>
      {authedUser ? (
        <StyledNavItems>
          <div>
            <UserProfile />
          </div>
        </StyledNavItems>
      ) : (
        <StyledNavItems>
          <ul>
            <li>
              <LanguageSelector />
            </li>
            <li>
              <Link as={RouterLink} to="/login">
                <FormattedMessage id="login" />
              </Link>
            </li>
            <li>
              <Link as={RouterLink} to="/register">
                <FormattedMessage id="signup" />
              </Link>
            </li>
          </ul>
        </StyledNavItems>
      )}
    </Container>
  );
};
