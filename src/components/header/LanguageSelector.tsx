import { useContext } from "react";
import { FormattedMessage } from "react-intl";
import { Context } from "utils/Wrapper";
import { StyledLanguageSelectorContainer } from "./LanguageSelector.styled";

interface Props {
  close?: () => void;
}
export const LanguageSelector = ({ close }: Props): JSX.Element => {
  const { selectLanguage } = useContext(Context);

  return (
    <StyledLanguageSelectorContainer>
      <select
        onChange={(lang) => {
          selectLanguage(lang);
          if (close) close();
        }}
        defaultValue="default"
      >
        <option value="default" disabled hidden>
          <FormattedMessage id="changeLanguage" />
        </option>
        <option value="ar">
          <FormattedMessage id="arabic" />
        </option>
        <option value="de">
          <FormattedMessage id="german" />
        </option>
        <option value="en">
          <FormattedMessage id="english" />
        </option>
        <option value="es">
          <FormattedMessage id="spanish" />
        </option>
        <option value="it">
          <FormattedMessage id="italian" />
        </option>
        <option value="fr">
          <FormattedMessage id="french" />
        </option>
        <option value="hi">
          <FormattedMessage id="hindi" />
        </option>
        <option value="jp">
          <FormattedMessage id="japanese" />
        </option>
        <option value="pt">
          <FormattedMessage id="portuguese" />
        </option>
        <option value="sw">
          <FormattedMessage id="swahili" />
        </option>
        <option value="zh">
          <FormattedMessage id="mandarin" />
        </option>
      </select>
    </StyledLanguageSelectorContainer>
  );
};
