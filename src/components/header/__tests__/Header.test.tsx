import { renderWithRouter } from "utils/test-utils";
import { auth, firebaseUser } from "gateway";
import { UserProfile } from "../UserProfile";
import { fireEvent } from "@testing-library/react";

jest.mock("gateway", () => ({
  auth: {
    getAuthListener: (userFn: (user: firebaseUser) => void) => {
      userFn({
        displayName: null,
        getIdToken: () =>
          new Promise((resolve, reject) => {
            reject(null);
          }),
      } as unknown as firebaseUser);
      return jest.fn;
    },
  },
}));

describe("Header", () => {
  beforeEach(jest.resetAllMocks);

  it("renders logout when user is logged in", () => {
    auth.getAuthListener = (userFn: (user: firebaseUser) => void) => {
      userFn({
        displayName: "testUser",
        getIdTokenResult: () =>
          new Promise((resolve) => {
            resolve({
              expirationTime: new Date().getTime() + 100000,
              token: "testToken",
            });
          }),
        getIdToken: () =>
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          new Promise((resolve) => {
            resolve("testToken");
          }),
      } as unknown as firebaseUser);
      return jest.fn;
    };

    const { getByTestId } = renderWithRouter(<UserProfile />);

    const menuButton = getByTestId("menu-button");
    fireEvent.click(menuButton); // open dropdown menu

    const logoutButton = getByTestId("logout");
    expect(logoutButton).toBeInTheDocument();
  });
});
