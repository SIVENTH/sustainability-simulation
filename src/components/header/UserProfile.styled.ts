import styled from "styled-components";

export const StyledNavUser = styled.div`
  position: relative;
`;

export const StyledDropdownContainer = styled.div`
  position: absolute;
  width: auto;
  top: 60px;
  right: 0;
  background-color: white;
  z-index: 1;
  border: 1px solid #e2e8f0;
  border-top: none;
`;

export const StyledAvatar = styled.div`
  height: 30px;
  width: 30px;
  border-radius: 50%;
  background-color: gray;
  margin-right: 10px;
`;

export const StyledDropdownItem = styled.div`
  padding: 12px 20px;
  cursor: pointer;

  > div > select {
    background-color: inherit;
  }

  &:hover {
    background-color: #e1f4e8;
  }
`;
