import { Image, Link } from "../../components/ChakraReplace.styled";
import logo from "images/logo.svg";

import { StyledLogoContainer } from "./Logo.styled";

export const Logo = (): JSX.Element => {
  return (
    <StyledLogoContainer>
      <Link href="https://siventh.com/">
        <Image alt="SIVENTH Logo" display="inline" width="150px" src={logo} />
      </Link>
    </StyledLogoContainer>
  );
};
