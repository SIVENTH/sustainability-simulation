import styled from "styled-components";

export const StyledHeaderContainer = styled.div`
  display: block;
  height: 80px;
  border-bottom: 1px solid #efefef;
`;

export const StyledNavOutter = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  align-items: center;
  margin: 0 16px;
  padding: 20px;
`;
