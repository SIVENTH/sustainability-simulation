import { useMemo } from "react";
import { VictoryBar, VictoryChart, VictoryAxis } from "victory";

import { colors } from "utils";

interface Props {
  productionScore: number | undefined;
  manufacturingScore: number | undefined;
  logisticsScore: number | undefined;
  otherScore: number | undefined;
  width?: number;
  height?: number;
}

export const BarChart = ({
  width = 600,
  height = 300,
  productionScore,
  manufacturingScore,
  logisticsScore,
  otherScore,
}: Props): JSX.Element => {
  const memoizedData = useMemo(() => {
    return [
      { area: 1, score: productionScore },
      { area: 2, score: manufacturingScore },
      { area: 3, score: logisticsScore },
      { area: 4, score: otherScore },
    ];
  }, [productionScore, manufacturingScore, logisticsScore, otherScore]);

  return (
    <VictoryChart domainPadding={40} width={width} height={height}>
      <VictoryAxis
        style={{
          axis: { stroke: "grey" },
          tickLabels: { fill: "grey" },
        }}
        tickValues={[1, 2, 3, 4]}
        tickFormat={["Production", "Manufacturing", "Logistics", "Others"]}
      />
      <VictoryAxis
        style={{
          axis: { stroke: "grey" },
          grid: { stroke: "grey" },
          tickLabels: { fill: "grey" },
        }}
        dependentAxis
        tickValues={[0, 20, 40, 60, 80, 100]}
        tickFormat={(x) => `${(x / 100) * 100}%`}
      />
      <VictoryBar
        style={{ data: { fill: colors.appGreen, width: 50 } }}
        animate={{
          duration: 2000,
          onLoad: { duration: 1000 },
        }}
        data={memoizedData}
        x="area"
        y="score"
      />
    </VictoryChart>
  );
};
