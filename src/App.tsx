import { useEffect } from "react";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import { hotjar } from "react-hotjar";

// Make sure to call `loadStripe` outside of a component’s render to avoid
// recreating the `Stripe` object on every render.
const stripePromise = loadStripe(
  "pk_test_51IjQQiE5Auf2lZEoyGli7wFX96vyGcgD9YCGTIy4gaxBIzXtRDzC1NKZVysgkJ3wNLm3vycYEAg01Otek3fyg4On00xz5Jfszl"
);

import { Mixpanel } from "utils/AnalyticsWrapper";

import { useAuth } from "context";
import { Loader } from "components/loader";
import { ErrorBoundary } from "components/error-boundary";
import AppLayout from "./AppLayout";
import { createBrowserHistory } from "history";
import "gateway/axios";
import { ToastContainer } from "react-toastify";
export function App(): JSX.Element {
  const { authedUser, isLoading } = useAuth();
  const history = createBrowserHistory();
  useEffect(() => {
    Mixpanel.track("App loaded");
    hotjar.initialize(2398586, 6);
  }, []);

  if (isLoading) return <Loader />;

  return (
    <Elements stripe={stripePromise}>
      <ErrorBoundary history={history}>
        <AppLayout authedUser={authedUser} />
        <ToastContainer position="bottom-right" />
      </ErrorBoundary>
    </Elements>
  );
}
