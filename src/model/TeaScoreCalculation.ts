/* eslint-disable @typescript-eslint/ban-types */
import { ScoreCalculation } from "./ScoreCalculation";
import { TeaModel } from "./TeaModel";
import { tea } from "../data/tea";
import { Model } from "./TeaModel";
import { TopicType, Answer, DataModel } from "types";

class TeaScoreCalculation extends ScoreCalculation {
  constructor(
    private model: Model,
    growingScore: number,
    pickingScore: number,
    witheringScore: number,
    rollingScore: number,
    fermentingScore: number,
    dryingScore: number,
    blendingScore: number,
    packagingScore: number,
    distributionScore: number,
    storageScore: number,
    customerScore: number,
    endOfLifeScore: number
  ) {
    super(
      growingScore,
      pickingScore,
      witheringScore,
      rollingScore,
      fermentingScore,
      dryingScore,
      blendingScore,
      packagingScore,
      distributionScore,
      storageScore,
      customerScore,
      endOfLifeScore
    );
  }

  static buildModel(
    model: DataModel[],
    growingScore: number,
    pickingScore: number,
    witheringScore: number,
    rollingScore: number,
    fermentingScore: number,
    dryingScore: number,
    blendingScore: number,
    packagingScore: number,
    distributionScore: number,
    storageScore: number,
    customerScore: number,
    endOfLifeScore: number
  ): TeaScoreCalculation {
    return new TeaScoreCalculation(
      new TeaModel(model),
      growingScore,
      pickingScore,
      witheringScore,
      rollingScore,
      fermentingScore,
      dryingScore,
      blendingScore,
      packagingScore,
      distributionScore,
      storageScore,
      customerScore,
      endOfLifeScore
    );
  }

  public calculateScore = (
    topic: string,
    answers: Object
  ): number | undefined => {
    if (topic === TopicType.PREP) return;
    const earnedPoints = this.calculatePoints(answers);
    return this.calculateSpecificScore(topic, earnedPoints);
  };

  private calculatePoints = (answers: Object) => {
    const filteredScores: Answer[] = [];
    Object.values(answers).forEach((answer) => {
      const scoreLookup = this.model
        .mapScoreValues()
        .find(({ value }) => value === answer);
      if (scoreLookup) filteredScores.push(scoreLookup);
    });
    return filteredScores.reduce(
      (accumulator: number, { score }) => accumulator + score,
      0
    );
  };

  private calculateSpecificScore = (
    topic: string,
    earnedPoints: number
  ): number => {
    switch (topic) {
      case TopicType.GROWING: {
        return this.calculateGrowingScore(earnedPoints);
      }
      case TopicType.PICKING: {
        return this.calculatePickingScore(earnedPoints);
      }
      case TopicType.WITHERING: {
        return this.calculateWitheringScore(earnedPoints);
      }
      case TopicType.ROLLING: {
        return this.calculateRollingScore(earnedPoints);
      }
      case TopicType.FERMENTING: {
        return this.calculateFermentingScore(earnedPoints);
      }
      case TopicType.DRYING: {
        return this.calculateDryingScore(earnedPoints);
      }
      case TopicType.BLENDING: {
        return this.calculateBlendingScore(earnedPoints);
      }
      case TopicType.PACKAGING: {
        return this.calculatePackagingScore(earnedPoints);
      }
      case TopicType.DISTRIBUTION: {
        return this.calculateDistributionScore(earnedPoints);
      }
      case TopicType.STORAGE: {
        return this.calculateStorageScore(earnedPoints);
      }
      case TopicType.CUSTOMER: {
        return this.calculateCustomerScore(earnedPoints);
      }
      default: {
        return this.calculateEndOfLifeScore(earnedPoints);
      }
    }
  };
}

export const TeaScore = TeaScoreCalculation.buildModel(
  tea,
  9, // growing
  3, // picking
  18, // withering
  8, // rolling
  9, // fermenting
  7, // drying
  3, // blending
  5, // packaging
  8, // distribution
  6, // storage
  3, // customer
  10 // end of life
);
