import { DataModel, Answer } from "types";

export interface Model {
  mapScoreValues: () => Answer[];
}

export class TeaModel implements Model {
  constructor(private data: DataModel[]) {}

  public mapScoreValues(): Answer[] {
    return this.data.flatMap(({ questions }) =>
      questions
        .flatMap(({ answers }) => answers)
        .filter(({ score }) => score > 0)
    );
  }
}
