export abstract class ScoreCalculation {
  private growingScore: number;
  private pickingScore: number;
  private witheringScore: number;
  private rollingScore: number;
  private fermentingScore: number;
  private dryingScore: number;
  private blendingScore: number;
  private packagingScore: number;
  private distributionScore: number;
  private storageScore: number;
  private customerScore: number;
  private endOfLifeScore: number;

  constructor(
    growingSore: number,
    pickingScore: number,
    witheringScore: number,
    rollingScore: number,
    fermentingScore: number,
    dryingScore: number,
    blendingScore: number,
    packagingScore: number,
    distributionScore: number,
    storageScore: number,
    customerScore: number,
    endOfLifeScore: number
  ) {
    this.growingScore = growingSore;
    this.pickingScore = pickingScore;
    this.witheringScore = witheringScore;
    this.rollingScore = rollingScore;
    this.fermentingScore = fermentingScore;
    this.dryingScore = dryingScore;
    this.blendingScore = blendingScore;
    this.packagingScore = packagingScore;
    this.distributionScore = distributionScore;
    this.storageScore = storageScore;
    this.customerScore = customerScore;
    this.endOfLifeScore = endOfLifeScore;
  }

  protected calculateGrowingScore = (earnedPoints: number): number => {
    return parseFloat(((earnedPoints / this.growingScore) * 100).toFixed(2));
  };

  protected calculatePickingScore = (earnedPoints: number): number => {
    return parseFloat(((earnedPoints / this.pickingScore) * 100).toFixed(2));
  };

  protected calculateWitheringScore = (earnedPoints: number): number => {
    return parseFloat(((earnedPoints / this.witheringScore) * 100).toFixed(2));
  };

  protected calculateRollingScore = (earnedPoints: number): number => {
    return parseFloat(((earnedPoints / this.rollingScore) * 100).toFixed(2));
  };

  protected calculateFermentingScore = (earnedPoints: number): number => {
    return parseFloat(((earnedPoints / this.fermentingScore) * 100).toFixed(2));
  };

  protected calculateDryingScore = (earnedPoints: number): number => {
    return parseFloat(((earnedPoints / this.dryingScore) * 100).toFixed(2));
  };

  protected calculateBlendingScore = (earnedPoints: number): number => {
    return parseFloat(((earnedPoints / this.blendingScore) * 100).toFixed(2));
  };

  protected calculatePackagingScore = (earnedPoints: number): number => {
    return parseFloat(((earnedPoints / this.packagingScore) * 100).toFixed(2));
  };

  protected calculateDistributionScore = (earnedPoints: number): number => {
    return parseFloat(
      ((earnedPoints / this.distributionScore) * 100).toFixed(2)
    );
  };

  protected calculateStorageScore = (earnedPoints: number): number => {
    return parseFloat(((earnedPoints / this.storageScore) * 100).toFixed(2));
  };

  protected calculateCustomerScore = (earnedPoints: number): number => {
    return parseFloat(((earnedPoints / this.customerScore) * 100).toFixed(2));
  };

  protected calculateEndOfLifeScore = (earnedPoints: number): number => {
    return parseFloat(((earnedPoints / this.endOfLifeScore) * 100).toFixed(2));
  };
}
