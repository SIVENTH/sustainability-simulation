import { TeaScore } from "../TeaScoreCalculation";
import { TopicType } from "types";

jest.mock("gateway", () => ({
  createSetupIntent: jest.fn(async () => ({
    data: { clientSecret: "xxx" },
  })),
}));

describe("TeaScoreCalculation", () => {
  const Answers = {
    "Do you perform any soil tests?": "Yes",
    "How long will you keep your new plants in nursery?": "1 Month",
    "How will you grow your tea leaves?": "From Cutting",
  };

  it("calculates correctly with one answer selected", () => {
    const points = TeaScore.calculateScore(
      TopicType.GROWING,
      Object.entries(Answers)[0]
    );
    expect(points).toEqual(11.11);
  });

  it("calculates correctly with multiple answers selected", () => {
    const points = TeaScore.calculateScore(TopicType.GROWING, Answers);
    expect(points).toEqual(33.33);
  });
});
