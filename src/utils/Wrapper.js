import React, { useState } from "react";
import { IntlProvider, createIntl, createIntlCache } from "react-intl";
import {
  english,
  japanese,
  spanish,
  hindi,
  chinese,
  italian,
  french,
  portuguese,
  arabic,
  swahili,
  german,
} from "siventh-translations";

import { SubscriptionType, Frequency, Quantity } from "types";

import "@formatjs/intl-numberformat/polyfill";
import "@formatjs/intl-numberformat/locale-data/en";
import "@formatjs/intl-numberformat/locale-data/ja"; // to use japanese need to polyfil with number formats for japan.

export const Context = React.createContext();

const cache = createIntlCache();
const userLang = navigator.language;
let lang = {}; // set language files for use in data components

switch (userLang.toLocaleLowerCase()) {
  case "ar":
    lang = arabic;
    break;
  case "de":
    lang = german;
    break;
  case "es":
    lang = spanish;
    break;
  case "it":
    lang = italian;
    break;
  case "fr":
    lang = french;
    break;
  case "hi":
    lang = hindi;
    break;
  case "jp":
  case "ja":
  case "ja-jp":
    lang = japanese;
    break;
  case "pt":
    lang = portuguese;
    break;
  case "sw":
    lang = swahili;
    break;
  case "zh":
  case "zh-cn":
  case "zh-tw":
    lang = chinese;
    break;
  default:
    lang = english;
}

export const intl = createIntl(
  {
    locale: userLang,
    messages: lang,
  },
  cache
);

export const labels = {
  [SubscriptionType.Education]: intl.formatMessage({ id: "education" }),
  [SubscriptionType.Enterprise]: intl.formatMessage({ id: "enterprise" }),
  [Frequency.Monthly]: intl.formatMessage({ id: "monthly" }),
  [Frequency.Annual]: intl.formatMessage({ id: "annual" }),
  [Quantity.Small]: "500",
  [Quantity.Medium]: "1000",
  [Quantity.Large]: "5000",
};

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const Wrapper = (props) => {
  const [locale, setLocale] = useState(userLang);
  const [messages, setMessages] = useState(lang);

  function selectLanguage(e) {
    const newLocale = e.target.value;
    setLocale(newLocale);
    switch (newLocale) {
      case "ar":
        setMessages(arabic);
        break;
      case "de":
        setMessages(german);
        break;
      case "es":
        setMessages(spanish);
        break;
      case "it":
        setMessages(italian);
        break;
      case "fr":
        setMessages(french);
        break;
      case "hi":
        setMessages(hindi);
        break;
      case "jp":
      case "ja":
      case "ja-jp":
        setMessages(japanese);
        break;
      case "pt":
        setMessages(portuguese);
        break;
      case "sw":
        setMessages(swahili);
        break;
      case "zh":
      case "zh-cn":
      case "zh-tw":
        setMessages(chinese);
        break;
      default:
        setMessages(english);
    }
  }

  return (
    <Context.Provider value={{ locale, selectLanguage }}>
      <IntlProvider messages={messages} locale={locale}>
        {props.children}
      </IntlProvider>
    </Context.Provider>
  );
};

export default Wrapper;
