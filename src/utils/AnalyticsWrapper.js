/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import mixpanel from "mixpanel-browser";
import ReactGA from "react-ga";
import { hotjar } from "react-hotjar";

mixpanel.init("b2d4f049ae3985168442b8c660e1d2a6");
hotjar.initialize(2398586, 6);
// change to production when building for production
let env_check;

switch (process.env.NODE_ENV) {
  case "test":
    env_check = "test";
    ReactGA.initialize("321861464");
    break;
  case "development":
    env_check = "development";
    ReactGA.initialize("321866282");
    break;
  default:
    env_check = "production";
    ReactGA.initialize("255002345"); // not sure if this is correct
}

let actions = {
  identify: (id) => {
    if (env_check) mixpanel.identify(id);
  },
  alias: (id) => {
    if (env_check) mixpanel.alias(id);
  },
  track: (name, props) => {
    if (env_check) mixpanel.track(name, props);
  },
  people: {
    set: (props) => {
      if (env_check) mixpanel.people.set(props);
    },
  },
};

export let Mixpanel = actions;
