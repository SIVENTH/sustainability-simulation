import { useLocation } from "react-router-dom";
import { Text, ChakraSpan } from "../components/ChakraReplace.styled";
import { PaymentMethod } from "@stripe/stripe-js";
import queryString from "query-string";
import { FormattedMessage, FormattedDate } from "react-intl";

import {
  InvoiceOptionalPayment,
  SubscriptionType,
  Frequency,
  Quantity,
} from "types";
import { labels, intl, colors } from "utils";

//https://stripe.com/docs/payments/intents#intent-statuses
// payment intent status to text
export const getStatusLabel = (status: string): JSX.Element => {
  switch (status) {
    case "succeeded":
      return (
        <Text color={colors.green2}>
          <FormattedMessage id="billing.success" />
        </Text>
      );
    case "requires_payment_method":
      return (
        <Text color={colors.textRed}>
          <FormattedMessage id="billing.failed" />
        </Text>
      );
    case "requires_action":
      return (
        <Text color={colors.textYellow}>
          <FormattedMessage id="billing.requiresAction" />
        </Text>
      );
    case "pending":
      return (
        <Text color={colors.textYellow}>
          <FormattedMessage id="billing.pending" />
        </Text>
      );
    case "canceled":
      return (
        <Text color={colors.textRed}>
          <FormattedMessage id="billing.canceled" />
        </Text>
      );
    default:
      return (
        <Text color={colors.textRed}>
          <FormattedMessage id="billing.error" />
        </Text>
      );
  }
};

// convert status transitions to display string
export const getDate = (
  status_transitions: Record<string, number | null>
): Date | undefined => {
  const { paid_at, voided_at, marked_uncollectible_at, finalized_at } =
    status_transitions;

  const timestamp =
    paid_at || voided_at || marked_uncollectible_at || finalized_at;
  return timestamp ? new Date(timestamp * 1000) : undefined;
};

const capitalizeFirst = (str: string) =>
  str.charAt(0).toUpperCase() + str.slice(1);

export const getPaymentMethodText = (paymentMethod: PaymentMethod): string =>
  paymentMethod && paymentMethod.card
    ? `${capitalizeFirst(paymentMethod.card.brand)} ${paymentMethod.card.last4}`
    : "";

// https://stripe.com/docs/api/subscriptions/object#subscription_object-status
// https://mrcoles.com/stripe-api-subscription-status/
export const getSubscriptionStatusText = (
  invoice: InvoiceOptionalPayment
): JSX.Element | string => {
  const {
    cancel_at_period_end,
    current_period_end,
    frequency,
    subscriptionStatus,
    cancel_at,
  } = invoice;

  switch (subscriptionStatus) {
    case "active":
      return (
        <>
          <FormattedMessage
            id="billing.status.subscriptionWill"
            values={{
              frequency: labels[frequency],
              action: cancel_at_period_end ? "cancel" : "renew",
            }}
          />
          <ChakraSpan fontWeight={700}>
            <FormattedDate
              value={current_period_end * 1000}
              year="numeric"
              month="long"
              day="2-digit"
            />
          </ChakraSpan>
        </>
      );
    case "incomplete":
      return intl.formatMessage({ id: "billing.status.incomplete" });
    case "incomplete_expired":
      return intl.formatMessage({ id: "billing.status.incomplete_expired" });
    case "past_due":
      return intl.formatMessage({ id: "billing.status.past_due" });
    case "unpaid":
      return intl.formatMessage({ id: "billing.status.unpaid" });
    case "cancel_at":
      return cancel_at
        ? intl.formatMessage(
            {
              id: "billing.status.canceledOn",
            },
            {
              date: intl.formatDate(cancel_at * 1000, {
                year: "numeric",
                month: "long",
                day: "2-digit",
              }),
            }
          )
        : intl.formatMessage({
            id: "billing.status.canceled",
          });
    default:
      return intl.formatMessage({ id: "billing.status.unknown" });
  }
};

const defaultValues = {
  subscriptionType: SubscriptionType.Education,
  numberOfSubscriptions: Quantity.Small,
  frequency: Frequency.Monthly,
};

const isSubscriptionType = (subscriptionType: string) =>
  (Object.values(SubscriptionType) as string[]).includes(subscriptionType);

export const useDefaultFormValues = (): typeof defaultValues => {
  const location = useLocation();
  const { type } = queryString.parse(location.search);

  return {
    ...defaultValues,
    subscriptionType:
      typeof type === "string" && isSubscriptionType(type)
        ? (type as SubscriptionType)
        : defaultValues.subscriptionType,
  };
};
