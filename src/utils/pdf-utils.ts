import jsPDF from "jspdf";
import html2canvas from "html2canvas";

interface PdfFromTextArgs {
  text: string[];
  header?: string;
  fileName?: string;
  x?: number;
  y?: number;
  width?: number;
  height?: number;
}

export const createPdfFromText = async ({
  text,
  header = "Your score",
  fileName = "results.pdf",
  x = 20,
  y = 20,
}: PdfFromTextArgs): Promise<void> => {
  const doc = new jsPDF("p", "mm");
  doc.text(text, x, y);
  doc.setFont("Helvetica", "bold").text(header, 10, 10);
  doc.save(fileName);
};

interface PdfFromHtmlArgs {
  htmlElement: HTMLDivElement | null;
  fileName?: string;
  width?: number;
  height?: number;
  breakPoint?: string | undefined;
}

type format = "l" | "p" | "portrait" | "landscape" | undefined;

export const createPdfFromHtml = async ({
  htmlElement,
  fileName = "results.pdf",
  width = 0,
  height = 0,
  breakPoint,
}: PdfFromHtmlArgs): Promise<void> => {
  let format: format = "l";
  let x = 5;
  const y = 5;
  const scale = 0.8;
  if (breakPoint === "sm") {
    format = "p";
    x = 25;
  }
  if (breakPoint === "md" || breakPoint === "base") {
    format = "p";
    x = 40;
  }
  const doc = new jsPDF(format, "mm");
  if (!htmlElement) return;
  const canvas = await html2canvas(htmlElement, { logging: false, scale });
  await doc.addImage(canvas, "PNG", x, y, width, height);
  doc.save(fileName);
};
