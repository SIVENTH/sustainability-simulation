import { intl } from "utils";

export const formatError = (err: { message: string; code: string }): string => {
  let errMessage: string = err.message;
  if (err.code === "auth/user-not-found") {
    errMessage = intl.formatMessage({ id: "error.description.userNotFound" });
  }
  if (err.code === "auth/wrong-password") {
    errMessage = intl.formatMessage({ id: "error.description.wrongPassword" });
  }
  if (err.code === "auth/too-many-requests") {
    errMessage = intl.formatMessage({
      id: "error.description.tooManyRequests",
    });
  }
  return errMessage;
};
