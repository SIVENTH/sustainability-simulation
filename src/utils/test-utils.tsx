/* eslint-disable @typescript-eslint/no-explicit-any */
import { unstable_HistoryRouter as HistoryRouter } from "react-router-dom";
import { createMemoryHistory } from "history";
import { render } from "@testing-library/react";
import { AuthProvider } from "context";
import Wrapper from "./Wrapper.js";

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function renderWithRouter(
  ui: JSX.Element,
  {
    route = "/",
    history = createMemoryHistory({ initialEntries: [route] }),
  }: any = {}
) {
  const LandingWrapper = ({ children }: { children: React.ReactNode }) => (
    <AuthProvider>
      <Wrapper>
        <HistoryRouter history={history}>{children}</HistoryRouter>{" "}
      </Wrapper>
    </AuthProvider>
  );
  return {
    ...render(ui, { wrapper: LandingWrapper } as any),
    history,
  };
}

/* export const renderWithContext = (
  ui: JSX.Element,
  Provider: any,
  props?: any
) => {
  render(<Provider {...props}>{ui}</Provider>);
};

export const createTestProps = <T extends {}>(props: T) => ({
  ...props,
}); */
