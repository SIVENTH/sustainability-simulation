import sdg1 from "./SDG Icons 2019_WEB/E-WEB-Goal-01.png";
import sdg2 from "./SDG Icons 2019_WEB/E-WEB-Goal-02.png";
import sdg3 from "./SDG Icons 2019_WEB/E-WEB-Goal-03.png";
import sdg4 from "./SDG Icons 2019_WEB/E-WEB-Goal-04.png";
import sdg5 from "./SDG Icons 2019_WEB/E-WEB-Goal-05.png";
import sdg6 from "./SDG Icons 2019_WEB/E-WEB-Goal-06.png";
import sdg7 from "./SDG Icons 2019_WEB/E-WEB-Goal-07.png";
import sdg8 from "./SDG Icons 2019_WEB/E-WEB-Goal-08.png";
import sdg9 from "./SDG Icons 2019_WEB/E-WEB-Goal-09.png";
import sdg10 from "./SDG Icons 2019_WEB/E-WEB-Goal-10.png";
import sdg11 from "./SDG Icons 2019_WEB/E-WEB-Goal-11.png";
import sdg12 from "./SDG Icons 2019_WEB/E-WEB-Goal-12.png";
import sdg13 from "./SDG Icons 2019_WEB/E-WEB-Goal-13.png";
import sdg14 from "./SDG Icons 2019_WEB/E-WEB-Goal-14.png";
import sdg15 from "./SDG Icons 2019_WEB/E-WEB-Goal-15.png";
import sdg16 from "./SDG Icons 2019_WEB/E-WEB-Goal-16.png";
import sdg17 from "./SDG Icons 2019_WEB/E-WEB-Goal-17.png";

import plus from "./The_Icon_of-SVG/Audiovisual/volume_plus.svg";
// Navigation Icons
// import DashboardIcon from "./The_Icon_of-SVG/UI/dashboard.svg";
// import FileLabeled from "./The_Icon_of-SVG/Files/files_labeled.svg";
import AnalyticsIcon from "./The_Icon_of-SVG/Technology_data/chart_bar_3.svg";
import CsrReportIcon from "./The_Icon_of-SVG/Actions/history.svg";
import LogOutIcon from "./The_Icon_of-SVG/UI/sign_out.svg";

import DashboardIcon from "./The_Icon_of-SVG/CSR Icons/dashboard.svg";
import ManageIcon from "./The_Icon_of-SVG/CSR Icons/account-cog.svg";
import FaqIcon from "./The_Icon_of-SVG/CSR Icons/book-multiple.svg";
import FileLabeled from "./The_Icon_of-SVG/CSR Icons/file_text.svg";
import HelpIcon from "./The_Icon_of-SVG/CSR Icons/help.svg";
import ImpactPortfolioIcon from "./The_Icon_of-SVG/CSR Icons/page-multiple.svg";
import BillingIcon from "./The_Icon_of-SVG/CSR Icons/receipt.svg";
import ProjectsIcon from "./The_Icon_of-SVG/CSR Icons/text-box-search.svg";
import LabIcon from "./The_Icon_of-SVG/Other/lab.svg";
import UserIcon from "./The_Icon_of-SVG/User/user_big.svg";

export const sdgIcons = {
  sdg1,
  sdg2,
  sdg3,
  sdg4,
  sdg5,
  sdg6,
  sdg7,
  sdg8,
  sdg9,
  sdg10,
  sdg11,
  sdg12,
  sdg13,
  sdg14,
  sdg15,
  sdg16,
  sdg17,
  plus,
};

export const DashIcons = {
  DashboardIcon,
  FileLabeled,
  AnalyticsIcon,
  CsrReportIcon,
  LogOutIcon,
  ManageIcon,
  FaqIcon,
  HelpIcon,
  ImpactPortfolioIcon,
  BillingIcon,
  ProjectsIcon,
  UserIcon,
  LabIcon,
};
