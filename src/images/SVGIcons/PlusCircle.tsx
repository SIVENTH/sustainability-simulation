import { FunctionComponent } from "react";
// eslint-disable-next-line max-lines-per-function
export const PlusCircle: FunctionComponent = () => {
  return (
    <svg
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      xmlnsXlink="http://www.w3.org/1999/xlink"
      x="0px"
      y="0px"
      width="40px"
      height="40px"
      viewBox="0 0 24 24"
    >
      <path
        d="M12 2a10 10 0 1 0 10 10A10 10 0 0 0 12 2Zm0 18a8 8 0 1 1 8-8 8 8 0 0 1-8 8Zm3.5-9H13V8.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5V11H8.5a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5H11v2.5a.5.5 0 0 0 .5.5h1a.5.5 0 0 0 .5-.5V13h2.5a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5Z"
        fill="#000"
        stroke="#000"
      />
    </svg>
  );
};
