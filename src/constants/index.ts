export const teaImageUrl =
  "https://firebasestorage.googleapis.com/v0/b/sustainability-sim.appspot.com/o/tea-sim-image%2Ftea_sim.jpg?alt=media&token=690427a4-da51-4d4f-86f9-8182539d9c31";
export const pieChartUrl =
  "https://firebasestorage.googleapis.com/v0/b/sustainability-sim.appspot.com/o/tea-sim-image%2Fpie_chart.png?alt=media&token=0fa7400b-dc59-4f33-8cec-bc76ea8fc8ce";

export const email = "info@siventh.com";
export const subject = "Inquiry about miSsuS premium feature";
