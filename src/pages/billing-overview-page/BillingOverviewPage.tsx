// Initial Billing page where you select the license type only

import { Box, Flex } from "../../components/ChakraReplace.styled";
import { colors } from "utils";
import NewOrderCards from "./NewOrderCards";
import BillingHistory from "./BillingHistory";
import PaymentMethods from "./PaymentMethods";

import { BackButton } from "../../components/BackButton";

export const BillingOverviewPage = (): JSX.Element => {
  return (
    <Box backgroundColor={colors.lightGray} p="40px">
      <BackButton />
      <Flex>
        <Box width="65%">
          <NewOrderCards />
          <BillingHistory />
        </Box>
        <Box width="35%" ml="32px">
          <PaymentMethods />
        </Box>
      </Flex>
    </Box>
  );
};
