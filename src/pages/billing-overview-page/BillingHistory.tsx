import { memo } from "react";
import {
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionIcon,
  AccordionPanel,
  Skeleton,
} from "@chakra-ui/react";
import { Text, Box, Flex, Link } from "../../components/ChakraReplace.styled";
import { FormattedMessage, FormattedDate } from "react-intl";

import { Invoice } from "types";
import { usePaymentHistory } from "hooks/stripe";

import { SubscriptionDetails } from "components/billing";

import { getStatusLabel, getDate, labels, colors } from "utils";

const AccordianButtonContent = ({ invoice }: { invoice: Invoice }) => (
  <AccordionButton
    _hover={{ bg: colors.lightGreen }}
    _expanded={{ bg: colors.lightGreen }}
  >
    <Flex
      fontSize="16px"
      fontWeight={600}
      justifyContent="space-between"
      width="100%"
      textAlign="left"
    >
      <Text width="150px">
        <FormattedDate
          value={getDate(invoice.status_transitions)}
          year="numeric"
          month="long"
          day="2-digit"
        />
      </Text>
      <Text width="100px">{labels[invoice.subscriptionType]}</Text>
      <Text width="240px">{invoice.paymentId.slice(3)}</Text>
      <Text width="120px">{getStatusLabel(invoice.status)}</Text>
      <Box width="100px">¥{invoice.amount}</Box>
      <AccordionIcon />
    </Flex>
  </AccordionButton>
);

const BillingHistoryRow = ({ invoice }: { invoice: Invoice }) => (
  <AccordionItem>
    <AccordianButtonContent invoice={invoice} />
    <AccordionPanel padding="24px">
      <SubscriptionDetails invoice={invoice} />
      <Link
        fontSize="16px"
        fontWeight={600}
        color={colors.blue1}
        p="0"
        mt="16px"
        display="block"
      >
        <FormattedMessage id="viewDetail" />
      </Link>
    </AccordionPanel>
  </AccordionItem>
);

const BillingHistory = () => {
  // TODO: handle hasMore, getNext, error
  const { data, isLoading } = usePaymentHistory();

  return (
    <Box mt="32px" p="24px" backgroundColor="white">
      <Flex gridGap="16px" alignItems="center">
        <Text fontSize="20px" fontWeight={600}>
          <FormattedMessage id="billingHistory" />
        </Text>
        <Text fontSize="16px">
          <FormattedMessage id="viewAll" />
        </Text>
      </Flex>
      <Flex
        fontSize="16px"
        fontWeight={700}
        justifyContent="space-between"
        px="1rem"
        mt="36px"
      >
        <Text width="150px">
          <FormattedMessage id="date" />
        </Text>
        <Text width="100px">
          <FormattedMessage id="subscription" />
        </Text>
        <Text width="240px">
          <FormattedMessage id="transactionId" />
        </Text>
        <Text width="120px">
          <FormattedMessage id="status" />
        </Text>
        <Text width="100px">
          <FormattedMessage id="amount" />
        </Text>
        <Box width="20px">{/* To take the space of the accordion icon */}</Box>
      </Flex>
      <Skeleton isLoaded={!isLoading}>
        <Accordion>
          {data.map((invoice) => (
            <BillingHistoryRow key={invoice.invoice_id} invoice={invoice} />
          ))}
        </Accordion>
      </Skeleton>
    </Box>
  );
};

export default memo(BillingHistory);
