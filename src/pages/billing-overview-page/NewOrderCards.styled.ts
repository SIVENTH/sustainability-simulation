import styled from "styled-components";

export const StyledNewOrderContainer = styled.div`
  height: auto;
  padding: 24px;
  background-color: #ffffff;
`;

export const StyledNewOrderCardOutter = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 24px 0 0 0;
  background-color: #ffffff;
`;
