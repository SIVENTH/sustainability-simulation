import styled from "styled-components";

export const StyledBillingLeftContainer = styled.div`
  background-color: #ffffff;
  height: auto;
  padding: 24px;
  border-radius: 8px;
  /* box-shadow: 0px 10px 23px rgb(0, 0, 0, 0.12); */

  h3 {
    font-size: 23px;
    margin-bottom: 24px;
  }
`;
