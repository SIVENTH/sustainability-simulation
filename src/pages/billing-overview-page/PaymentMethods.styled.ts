import styled from "styled-components";

export const StyledPaymentMethod = styled.div`
  display: flex;
  margin-top: 20px;
  align-items: center;
  &:first-of-type {
    margin-top: 16px;
  }
`;

export const StyledCardSelection = styled.div`
  display: flex;
  width: 48px;
  height: 32px;
  background-color: #e3e3e3;
  align-items: center;
  justify-content: center;
  margin-right: 16px;
`;
