import { useState, memo } from "react";
import { Text } from "../../components/ChakraReplace.styled";
import { FormattedMessage } from "react-intl";

import { SubscriptionType } from "types";

import { NewOrderCard } from "components/billing";

import {
  StyledNewOrderContainer,
  StyledNewOrderCardOutter,
} from "./NewOrderCards.styled";

const NewOrderCards = () => {
  const [selected, setSelected] = useState<SubscriptionType | null>(null);

  return (
    <StyledNewOrderContainer>
      <Text fontSize="20px" fontWeight={600}>
        <FormattedMessage id="billing.addMoreLicenses" />
      </Text>
      <StyledNewOrderCardOutter>
        <NewOrderCard
          subscriptionType={SubscriptionType.Enterprise}
          selected={selected === SubscriptionType.Enterprise}
          setSelected={setSelected}
        />
        <NewOrderCard
          subscriptionType={SubscriptionType.Education}
          selected={selected === SubscriptionType.Education}
          setSelected={setSelected}
        />
      </StyledNewOrderCardOutter>
    </StyledNewOrderContainer>
  );
};

export default memo(NewOrderCards);
