import { memo } from "react";
import { Flex, Text, Box } from "../../components/ChakraReplace.styled";
import {
  StyledPaymentMethod,
  StyledCardSelection,
} from "./PaymentMethods.styled";

import { PaymentMethod } from "@stripe/stripe-js";
import { FormattedMessage } from "react-intl";
import { usePaymentMethods } from "hooks/stripe";
import { getPaymentMethodText } from "utils";
import { CardIcon } from "components/billing";

const PaymentCreditCard = ({
  paymentMethod,
}: {
  paymentMethod: PaymentMethod;
}) => (
  <StyledPaymentMethod>
    <StyledCardSelection>
      <CardIcon brand={paymentMethod.card?.brand} />
    </StyledCardSelection>
    <Text fontSize="16px" fontWeight={600}>
      {getPaymentMethodText(paymentMethod)}
    </Text>
  </StyledPaymentMethod>
);

const PaymentMethods = () => {
  // TODO: handle isLoading, error, hasMore, getNext
  const { data: paymentMethods } = usePaymentMethods();
  return (
    <Box p="24px" backgroundColor="white">
      <Text fontSize="20px" fontWeight={600}>
        <FormattedMessage id="paymentInformation" />
      </Text>
      <Flex gridGap="16px" alignItems="center" mt="24px">
        <Text fontSize="24px" fontWeight={600}>
          <FormattedMessage id="creditCard" />
        </Text>
        <Text fontSize="16px">
          <FormattedMessage id="manage" />
        </Text>
      </Flex>
      <Box>
        {paymentMethods.map((paymentMethod) => (
          <PaymentCreditCard
            key={paymentMethod.id}
            paymentMethod={paymentMethod}
          />
        ))}
      </Box>
    </Box>
  );
};

export default memo(PaymentMethods);
