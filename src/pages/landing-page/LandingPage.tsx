import { CSSProperties } from "react";
import { Box, Text } from "../../components/ChakraReplace.styled";
import { Heading } from "components/heading";
import { FormattedMessage } from "react-intl";
import hero from "images/hero.jpg";
import Subscribe from "./Subscribe";

export const heroStyle: CSSProperties = {
  background: `url(${hero}) no-repeat center center fixed`,
  backgroundSize: "cover",
  height: "calc(100vh - 61px)",
};

// eslint-disable-next-line max-lines-per-function
export const LandingPage = (): JSX.Element => {
  return (
    <Box style={heroStyle}>
      <Box
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
        flex="1"
        height="300px"
        mx="auto"
        mt="20vh"
        p="30px"
        backgroundColor="white"
        width="700px"
        position="absolute"
        left="50%"
        transform="translate(-50%,0)"
      >
        <Heading
          css={{
            color: "#333",
            textAlign: "center",
          }}
        >
          <FormattedMessage
            defaultMessage="Sustainability Simulation"
            description="text for Sustainability Simulation"
            id="miSsuS"
          />
        </Heading>
        <Box mt="10px" maxWidth="800px">
          <Text color="#333" align="center">
            With our simulations you can train employees and optimize your
            existing value chain towards sustainability.
          </Text>
        </Box>
        <Subscribe />
      </Box>
    </Box>
  );
};
