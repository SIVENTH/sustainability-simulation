import "./formStyle.css";

// eslint-disable-next-line max-lines-per-function
function Subscribe(): JSX.Element {
  return (
    <div id="mc_embed_signup">
      <form
        className="validate subscribe"
        id="mc-embedded-subscribe-form"
        action="https://siventh.us3.list-manage.com/subscribe/post?u=60b11845e4ad15693b1503e26&amp;id=92ba15308f"
        method="post"
        name="mc-embedded-subscribe-form"
        target="_blank"
        noValidate
      >
        <div id="mc_embed_signup_scroll">
          <div className="mc-field-group">
            <input
              className="feedback-input required email"
              id="mce-EMAIL"
              name="EMAIL"
              type="email"
              placeholder="Email"
            />
          </div>

          <div className="clear" id="mce-responses">
            <div
              className="response"
              id="mce-error-response"
              style={{ display: "none" }}
            ></div>
            <div
              className="response"
              id="mce-success-response"
              style={{ display: "none" }}
            ></div>
          </div>

          <div
            style={{ position: "absolute", left: "-5000px" }}
            aria-hidden="true"
          >
            <input
              name="b_60b11845e4ad15693b1503e26_92ba15308f"
              tabIndex={-1}
              value=""
            />
          </div>

          <div className="clear">
            <input
              className="button"
              id="mc-embedded-subscribe"
              type="submit"
              value="Subscribe"
              name="subscribe"
            />
          </div>
        </div>
      </form>
    </div>
  );
}

export default Subscribe;
