/* eslint-disable max-lines-per-function */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { useState, useEffect } from "react";
import { FormattedMessage } from "react-intl";
import { useNavigate } from "react-router-dom";
import { usePaginateQuery } from "hooks";
import { db } from "gateway";
import { getAllUserAssociations } from "gateway/services";
import { useAuth } from "context";

import {
  HeaderContainer,
  StatisticsContainer,
  ChartContainer,
  RecentContainer,
  GroupSelectionContainer,
} from "components/dashboard";
import {
  Main,
  Contents,
  StyledSimResults,
  StyledRecentReports,
  Container,
} from "./Dashboard.styled";
import { Option } from "components/dropdownSelect";

export const DashboardPage = (): JSX.Element => {
  const navigate = useNavigate();
  const { authedUser, organizationId, memberId, teamList } = useAuth();
  const [groupList, setGroupList] = useState([
    {
      id: "",
      name: "",
      size: null,
      users: [],
      industry: "",
      organization: "",
      // score: null,
      sessions: null,
    },
  ]);
  const { loading, error } = usePaginateQuery(
    db
      .getCollectionReference(`users/${authedUser?.uid}/sim-results`)
      .orderBy("date", "desc")
  );

  const [options, setOptions] = useState<Option[]>([]);
  const [currOrganizationId, setCurrOrganizationId] =
    useState<any>(organizationId);

  const redirectToDetails = (id: string): void => {
    navigate(`/result/${id}`);
  };
  const { simResults, loadMore, hasMore } = usePaginateQuery(
    db
      .getCollectionReference(`users/${authedUser?.uid}/sim-results`)
      .orderBy("date", "desc")
  );

  // filter teamList by current organizationId (choose from dropdown)
  // it assumes the user can be part of only one team per organization
  const getTeam = (): any => {
    try {
      const orgIdFromLocalStorage = localStorage.getItem("currOrganizationId");
      if (teamList) {
        const team = teamList.filter(
          (item) => `${item.organization}` == orgIdFromLocalStorage
        );
        window.localStorage.setItem(
          "teamId",
          team[0]?.id ? team[0].id : "no team"
        );
        return team[0].id;
      }
    } catch (error) {
      return error;
    }
  };

  const getOptions = async (): Promise<any> => {
    try {
      const response: any = await getAllUserAssociations(memberId);
      const fetchedDataOptions = response.data.map((item: any) => ({
        value: item.organization,
        label: item.organization_email,
      }));
      await setOptions(fetchedDataOptions);
    } catch (error) {
      return error;
    }
  };

  const handleSelectChange = (value: string) => {
    setCurrOrganizationId(value);
    window.localStorage.setItem("currOrganizationId", value);
  };

  useEffect(() => {
    if (teamList) {
      getTeam();
      setGroupList(teamList);
    }
  }, [teamList, currOrganizationId]);

  useEffect(() => {
    if (organizationId) {
      getOptions();
    }
  }, [organizationId]);
  if (error) {
    navigate("/login");
  }
  return (
    <Container>
      {/* Loading */}
      {loading ? (
        <FormattedMessage id="loading" />
      ) : authedUser ? (
        <Contents>
          <StyledSimResults>
            <HeaderContainer
              simResults={simResults}
              options={options}
              currOrganizationId={currOrganizationId}
              handleSelectChange={handleSelectChange}
            />
            <Main>
              <StatisticsContainer
                redirectToDetails={redirectToDetails}
                simResults={simResults}
              />
              <ChartContainer
                redirectToDetails={redirectToDetails}
                simResults={simResults}
              />
            </Main>
          </StyledSimResults>
          <GroupSelectionContainer
            groupList={groupList}
            redirectToDetails={redirectToDetails}
            loadMore={loadMore}
            hasMore={hasMore}
          />
          <StyledRecentReports>
            <RecentContainer
              redirectToDetails={redirectToDetails}
              simResults={simResults}
              loadMore={loadMore}
              hasMore={hasMore}
            />
          </StyledRecentReports>
        </Contents>
      ) : (
        <FormattedMessage id="loading" />
      )}
    </Container>
  );
};
