import styled from "styled-components";

export const Container = styled.div`
  padding: 40px;
  align-items: flex-end;
`;

export const Contents = styled.div``;

export const StyledSimResults = styled.div`
  background-color: #fff;
  border: 1px;
  border-radius: 10px;
  margin-bottom: 40px;
  padding: 24px;
`;

export const Main = styled.div`
  display: flex;
`;

export const StyledRecentReports = styled.div`
  background-color: #fff;
  border: 1px;
  border-radius: 10px;
  margin-bottom: 40px;
  padding: 24px;
`;
