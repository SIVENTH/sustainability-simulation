import { useAuth } from "context";
import { FormattedMessage } from "react-intl";

// eslint-disable-next-line max-lines-per-function
export const ProfilePage = (): JSX.Element => {
  const { authedUser, organizationId, memberId, organization } = useAuth();

  return (
    <>
      <FormattedMessage id="supplyChains.tea" />
      {organization && <pre>{JSON.stringify(organization, null, 2)}</pre>}
      {/* <pre>{JSON.stringify(authedUser, null, 2)}</pre> */}
      <br />
      <br />
      <>This is the User: {authedUser?.email}</>
      <br />
      <br />
      <>This is the member id: {memberId}</>
      <br />
      <br />
      <>This is the organization: {organizationId}</>
    </>
  );
};
