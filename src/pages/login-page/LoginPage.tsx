import { FormattedMessage } from "react-intl";

import {
  StyledContainer,
  StyledLogin,
  StyledLoginInput,
  StyledLoginMsg,
} from "./LoginPage.styled";

import { HStack, Link } from "../../components/ChakraReplace.styled";
import { Navigate } from "react-router-dom";
import { useAuth } from "context";
import { SideBanner } from "components/sideBanner";
import LoginInputs from "./LoginInputs";

export const LoginPage = (): JSX.Element => {
  const { authedUser } = useAuth();

  return authedUser ? (
    <Navigate to="/dashboard" />
  ) : (
    <HStack spacing={100}>
      <StyledContainer>
        <SideBanner />
        <StyledLogin>
          <StyledLoginInput>
            <div className="title">
              <FormattedMessage id="login.title" />
            </div>
            <div className="subtitle">
              <FormattedMessage id="login.subTitle" />
            </div>
            <LoginInputs />
            <div>
              <StyledLoginMsg>
                <FormattedMessage id="login.dontHaveAnAccount" />
                <Link href="/signup">
                  <FormattedMessage id="signup" />
                </Link>
              </StyledLoginMsg>
            </div>
          </StyledLoginInput>
        </StyledLogin>
      </StyledContainer>
    </HStack>
  );
};
