/* eslint-disable max-lines-per-function */
import React from "react";
import { FormattedMessage } from "react-intl";
import { useNavigate } from "react-router-dom";
import { LoginFormValues } from "types";
import {
  FormControl,
  FormLabel,
  Checkbox,
  InputGroup,
  InputRightElement,
} from "@chakra-ui/react";
import {
  Box,
  Stack,
  Button,
  Link,
} from "../../components/ChakraReplace.styled";
import { ReactComponent as Hide } from "../../images/The_Icon_of-SVG/UI/invisible.svg";
import { ReactComponent as Show } from "../../images/The_Icon_of-SVG/UI/hidden_outlined.svg";

import { auth } from "gateway";
import { Form } from "../../components/form";
import { StyledInputs, StyledButton } from "./LoginPage.styled";
import { toast } from "react-toastify";

interface Props {
  additionalInfo?: string;
  bigImg?: boolean;
  textLeft?: boolean;
}

const LoginInputs: React.FC<Props> = ({ additionalInfo, bigImg, textLeft }) => {
  const [show, setShow] = React.useState(false);
  const navigate = useNavigate();
  const storage = window.localStorage;
  const remembered =
    storage.getItem("rememberChecked") === "true" ? true : false;
  const [remember, setRemember] = React.useState(remembered);
  const [rememberedEmail, setRememberedEmail] =
    remember && storage.getItem("email") != ""
      ? React.useState(storage.getItem("email"))
      : React.useState("");

  const handleRemember = (event: {
    target: { checked: boolean | ((prevState: boolean) => boolean) };
  }) => {
    event.target.checked
      ? storage.setItem("rememberChecked", "true")
      : storage.setItem("rememberChecked", "false");
    setRemember(event.target.checked);
  };

  const handleChange = (event: {
    target: {
      value: React.SetStateAction<string> & React.SetStateAction<string | null>;
    };
  }) => {
    setRememberedEmail(event.target.value);
  };

  const handleSubmit = async (formValues: LoginFormValues): Promise<void> => {
    const email = formValues.email
      ? formValues.email
      : rememberedEmail
      ? rememberedEmail
      : "";
    const password = formValues.password;

    try {
      await auth.loginUser({ email, password });
      remember ? storage.setItem("email", email) : storage.setItem("email", "");
      navigate("/dashboard");
    } catch (err) {
      toast.error("Authentication failed. Please try again.");
      console.error(err);
    }
  };

  const handleClick = () => setShow(!show);

  return (
    <StyledInputs
      additionalInfo={additionalInfo}
      bigImg={bigImg}
      textLeft={textLeft}
      data-testid="login-inputs"
    >
      <Box>
        <Form<LoginFormValues> onSubmit={handleSubmit}>
          <FormControl isRequired>
            <FormLabel htmlFor="email" className="startInput">
              <FormattedMessage id="email" />
            </FormLabel>
            <Form.Input
              label=""
              name="email"
              variant="flushed"
              id="email"
              type="email"
              value={rememberedEmail}
              onChange={handleChange}
            />
            <FormLabel htmlFor="password" className="startInput mt30">
              <FormattedMessage id="password" />
            </FormLabel>
            <InputGroup>
              <Form.Input
                label=""
                name="password"
                variant="flushed"
                id="password"
                type={show ? "text" : "password"}
              />
              <InputRightElement width="4.5rem">
                <Button height="1.75rem" fontSize="12px" onClick={handleClick}>
                  {show ? <Hide /> : <Show />}
                </Button>
              </InputRightElement>
            </InputGroup>
            <Stack spacing={2} flexDirection="column" className="mxy16">
              <Box>
                <Checkbox
                  size="lg"
                  isChecked={remembered}
                  onChange={handleRemember}
                  colorScheme="green"
                >
                  <FormattedMessage id="remember" />
                </Checkbox>
              </Box>
              <Box>
                <Link spacing={5} href="/forgot">
                  <FormattedMessage id="forgotPassword" />
                </Link>
              </Box>
            </Stack>
          </FormControl>
          <StyledButton>
            <FormattedMessage id="login" />
          </StyledButton>
        </Form>
      </Box>
    </StyledInputs>
  );
};
export default LoginInputs;
