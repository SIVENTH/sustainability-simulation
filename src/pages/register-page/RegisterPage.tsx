import { FormattedMessage } from "react-intl";

import {
  StyledContainer,
  StyledLogin,
  StyledLoginInput,
  StyledLoginMsg,
} from "./RegisterPage.styled";

import { HStack, Link } from "../../components/ChakraReplace.styled";
import { Navigate } from "react-router-dom";
import { useAuth } from "context";
import { SideBanner } from "components/sideBanner";
import SignupInputs from "./RegisterInputs";

export const RegisterPage = (): JSX.Element => {
  const { authedUser } = useAuth();

  return authedUser ? (
    <Navigate to="/dashboard" />
  ) : (
    <HStack spacing={100}>
      <StyledContainer>
        <SideBanner />
        <StyledLogin>
          <StyledLoginInput>
            <div className="title">
              <FormattedMessage id="signup.title" />
            </div>
            <div className="subtitle">
              <FormattedMessage id="signup.subTitle" />
            </div>
            <SignupInputs />
            <StyledLoginMsg>
              <FormattedMessage id="signup.haveAnAccount" />
              <Link href="/login">
                <FormattedMessage id="login" />
              </Link>
            </StyledLoginMsg>
          </StyledLoginInput>
        </StyledLogin>
      </StyledContainer>
    </HStack>
  );
};
