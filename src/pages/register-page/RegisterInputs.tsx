/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable max-lines-per-function */
import React, { useState } from "react";
import { FormattedMessage } from "react-intl";
import { useNavigate } from "react-router-dom";
import {
  FormControl,
  FormLabel,
  Checkbox,
  CheckboxGroup,
  InputGroup,
  InputRightElement,
} from "@chakra-ui/react";
import {
  Box,
  Stack,
  Button,
  Link,
} from "../../components/ChakraReplace.styled";
import { ReactComponent as Hide } from "../../images/The_Icon_of-SVG/UI/invisible.svg";
import { ReactComponent as Show } from "../../images/The_Icon_of-SVG/UI/hidden_outlined.svg";

import { auth } from "gateway";
import { Form } from "../../components/form";
import { StyledInputs, StyledButton } from "./RegisterPage.styled";
import { RegisterFormValues } from "types";

import { postOrganization } from "gateway/services";
import { organizationType, organizationResponseType } from "gateway";
import { useAuth } from "context";

interface Props {
  additionalInfo?: string;
  bigImg?: boolean;
  textLeft?: boolean;
}

const RegisterInputs: React.FC<Props> = ({
  additionalInfo,
  bigImg,
  textLeft,
}) => {
  const navigate = useNavigate();
  const [show, setShow] = React.useState(false);
  const { setMyOrgData } = useAuth();
  const [org, setOrg] = useState<organizationType[]>([]);

  const createOrganization = async (
    organization: organizationType
  ): Promise<organizationResponseType | unknown> => {
    try {
      // change to GET after Adriana adds.
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      const response: any = await postOrganization(organization); // change to response from api as type
      window.localStorage.setItem("orgId", response?.data.id);
      setOrg(response?.data);
      setMyOrgData(response?.data);
    } catch (error) {
      return error;
    }
  };

  const handleSubmit = async (
    formValues: RegisterFormValues
  ): Promise<void> => {
    const { email, username, password } = formValues;
    try {
      await auth.registerUser({ email, username, password }); // registers user with firebase
      // creates organization with user info
      createOrganization({
        accountType: 0, // Free user
        email: email,
        industryId: 1,
      });
      navigate("/dashboard"); // redirects user to dashboard
    } catch (err) {
      console.log("User Registration Error: ", err);
    }
  };

  return (
    <StyledInputs
      additionalInfo={additionalInfo}
      bigImg={bigImg}
      textLeft={textLeft}
      data-testid="signup-inputs"
    >
      <Box>
        <Form<RegisterFormValues> onSubmit={handleSubmit}>
          <FormControl isRequired>
            <FormLabel htmlFor="username" className="startInput">
              <FormattedMessage id="username" />
            </FormLabel>
            <Form.Input
              label=""
              name="username"
              variant="flushed"
              id="username"
            />

            <FormLabel htmlFor="email" className="startInput mt30">
              <FormattedMessage id="email" />
            </FormLabel>
            <Form.Input
              label=""
              name="email"
              variant="flushed"
              id="email"
              type="email"
            />

            <FormLabel htmlFor="password" className="startInput mt30">
              <FormattedMessage id="password" />
            </FormLabel>
            <InputGroup>
              <Form.Input
                label=""
                name="password"
                variant="flushed"
                id="password"
                type={show ? "text" : "password"}
              />
              <InputRightElement width="4.5rem">
                <Button
                  height="1.75rem"
                  fontSize="12px"
                  onClick={() => setShow(!show)}
                >
                  {show ? <Hide /> : <Show />}
                </Button>
              </InputRightElement>
            </InputGroup>

            <CheckboxGroup colorScheme="green">
              <Stack spacing={2} flexDirection="column">
                <Checkbox isRequired size="lg" value="agree">
                  <FormattedMessage id="agree" />
                </Checkbox>
                <Link href="/terms">
                  <FormattedMessage id="termsAndConditions" />
                </Link>
              </Stack>
            </CheckboxGroup>
          </FormControl>

          <StyledButton>
            <FormattedMessage id="signup" />
          </StyledButton>
        </Form>
      </Box>
    </StyledInputs>
  );
};
export default RegisterInputs;
