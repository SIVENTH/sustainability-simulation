/* eslint-disable max-lines-per-function */
import React from "react";
import { FormattedMessage } from "react-intl";
import { useNavigate } from "react-router-dom";
import { FormControl, FormLabel } from "@chakra-ui/react";
import { Box } from "../../components/ChakraReplace.styled";
import { auth } from "gateway";
import { Form } from "../../components/form";
import { StyledInputs, StyledButton } from "./ForgotPage.styled";
import { LoginFormValues } from "types";

interface Props {
  additionalInfo?: string;
  bigImg?: boolean;
  textLeft?: boolean;
}

const ForgotInputs: React.FC<Props> = ({
  additionalInfo,
  bigImg,
  textLeft,
}) => {
  const navigate = useNavigate();
  const handleSubmit = async (formValues: LoginFormValues): Promise<void> => {
    const { email } = formValues;

    try {
      await auth.resetPassword(email);
      navigate("/dashboard");
    } catch (err) {
      console.log("Authentication Error");
    }
  };

  return (
    <StyledInputs
      additionalInfo={additionalInfo}
      bigImg={bigImg}
      textLeft={textLeft}
      data-testid="forgot-inputs"
    >
      <Box>
        <Form<LoginFormValues> onSubmit={handleSubmit}>
          <FormControl isRequired>
            <FormLabel htmlFor="email" className="startInput">
              <FormattedMessage id="email" />
            </FormLabel>
            <Form.Input
              label=""
              name="email"
              variant="flushed"
              id="email"
              type="email"
            />
          </FormControl>
          <StyledButton>
            <FormattedMessage id="resetPassword" />
          </StyledButton>
        </Form>
      </Box>
    </StyledInputs>
  );
};
export default ForgotInputs;
