import { FormattedMessage } from "react-intl";

import {
  StyledContainer,
  StyledLogin,
  StyledLoginInput,
  StyledLoginMsg,
} from "./ForgotPage.styled";

import { HStack, Link } from "../../components/ChakraReplace.styled";
import { Navigate } from "react-router-dom";
import { useAuth } from "context";
import { SideBanner } from "components/sideBanner";
import ForgotInputs from "./ForgotInputs";

export const ForgotPage = (): JSX.Element => {
  const { authedUser } = useAuth();

  return authedUser ? (
    <Navigate to="/dashboard" />
  ) : (
    <HStack spacing={100}>
      <StyledContainer>
        <SideBanner />
        <StyledLogin>
          <StyledLoginInput>
            <div className="title">
              <FormattedMessage id="forgot.title" />
            </div>
            <div className="subtitle">
              <FormattedMessage id="forgot.subTitle" />
            </div>
            <ForgotInputs />
            <div>
              <StyledLoginMsg>
                <Link href="/login">
                  <FormattedMessage id="backToLogin" />
                </Link>
              </StyledLoginMsg>
            </div>
          </StyledLoginInput>
        </StyledLogin>
      </StyledContainer>
    </HStack>
  );
};
