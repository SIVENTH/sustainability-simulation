import styled from "styled-components";

import { colors } from "utils";

type Props = {
  additionalInfo?: string;
  bigImg?: boolean;
  textLeft?: boolean;
};

export const StyledContainer = styled.div<Props>`
  width: 100%;
  // margin: 40px;
  display: grid;
  grid-template-columns: repeat(24, 1fr);
  grid-template-rows: repeat(24, 1fr);
  grid-column-gap: 0px;
  grid-row-gap: 0px;

  .title {
    font-size: 56px;
    font-weight: 900;
    margin-bottom: 40px;
  }

  .subtitle {
    font-weight: 500;
    margin-bottom: 40px;
  }
`;

export const StyledLogin = styled.div`
  grid-area: 1 / 13 / 25 / 25;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  .title {
    line-height: 1.2;
  }
  @media screen and (max-width: 920px) {
    grid-area: 1 / 1 / 25 / 25;
  }
`;

export const StyledLoginInput = styled.div`
  padding: 30px;
  max-width: 450px;
`;

export const StyledInputs = styled.div<Props>`
  min-width: 100%;
  max-width: 800px;
  margin: 0px auto;
  display: flex;
  flex-direction: column;

  label {
    margin: 0;
    padding: 0;

    &.startInput {
      font-size: 12px;
      color: ${colors.gray5};
    }
    &.mt30 {
      margin-top: 30px;
    }
  }
  .chakra-checkbox__label {
    font-size: 14px;
  }
  .chakra-checkbox__control {
    width: 16px;
    height: 16px;
  }
  a[href^="/terms"] {
    font-size: 14px;
    color: #3366bb;
    margin: 0;
    padding-left: 4px;

    &:hover {
      color: #2c4b81;
    }
  }

  label {
    margin: 0;
    padding: 0;

    &.startInput {
      font-size: 12px;
      color: ${colors.gray5};
    }
    &.mt30 {
      margin-top: 30px;
    }
  }

  input {
    height: 32px;
  }
  input:focus {
    border-color: ${colors.green2};
  }

  // Show hide password button
  .chakra-input__right-element {
    top: unset;
    bottom: 0;
    width: unset;
    padding: 0;

    button {
      background-color: unset;
      font-size: 12px;
      font-weight: 400;
      color: ${colors.gray5};
      padding: 4px 8px;
      box-shadow: unset;
    }
  }
`;

export const StyledButton = styled.button<Props>`
  border-radius: 4px;
  font-weight: 600;
  width: 100%;
  height: 40px;
  margin-top: 24px;
  background-color: ${colors.green2};
  color: white;
`;

export const StyledLoginMsg = styled.div`
  margin-top: 24px;

  a[href^="/login"] {
    color: #3366bb;

    &:hover {
      color: #2c4b81;
    }
  }
`;
