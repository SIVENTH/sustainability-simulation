/* eslint-disable max-lines-per-function */
import { FormattedMessage } from "react-intl";
import { useParams, useNavigate, useLocation } from "react-router-dom";
import cuid from "cuid";
import { AspectRatio, OrderedList, ListItem } from "@chakra-ui/react";
import { Button } from "../../components/ChakraReplace.styled";
import {
  StyledIntroContainer,
  StyledContainer,
  StyledBackgroundInfo,
  StyledBackgroundOverview,
  StyledButtonContainer,
} from "./BackgroundPage.styled";

/**
 * Used to display the background informatoin for the selected industry type of simulation
 * @element BackgroundPage
 * @param {object} industry The industry to show a background page for
 * @contents <description> <overview>
 * @return {object} returns container for the background contents
 */
export const BackgroundPage = (): JSX.Element => {
  const { id } = useParams();
  const navigate = useNavigate();
  const location = useLocation();
  const startSimulation = (): void => {
    navigate(`/simulation/${cuid()}`);
  };
  const goBack = (): void => {
    type LocationState = { from: string };
    const locationState = location.state as LocationState;

    if (location.state) {
      navigate(locationState.from);
      return;
    }
    navigate("/dashboard");
  };
  const descriptionVideo = "https://www.youtube.com/embed/HRjJ46JdXZ4";
  const descriptionContent = {
    header: "supplyChains.tea",
    introText: `${id}.intro.description`,
    steps: [
      "tea.intro.steps.0-background",
      "tea.intro.steps.1-production",
      "tea.intro.steps.2-manufacturing",
      "tea.intro.steps.3-blendingAndPackaging",
      "tea.intro.steps.4-marketingAndDistribution",
      "tea.intro.steps.5-storageAndCustomer",
      "tea.intro.steps.6-endOfLife",
    ],
    closingText: `${id}.intro.closing`,
  };

  return (
    <StyledContainer>
      <div className="description">
        <StyledBackgroundInfo>
          <h2>
            <FormattedMessage id={descriptionContent.header} />
          </h2>
          <StyledIntroContainer>
            <FormattedMessage id={descriptionContent.introText} />
            <OrderedList start={0}>
              {descriptionContent.steps.map((step) => (
                <ListItem key={step}>
                  <FormattedMessage id={step} />
                </ListItem>
              ))}
            </OrderedList>
            <FormattedMessage id={descriptionContent.closingText} />
          </StyledIntroContainer>
        </StyledBackgroundInfo>
      </div>
      <div className="sim-overview">
        <StyledBackgroundOverview>
          <AspectRatio maxH="400px" w="100%">
            <iframe
              title="intro video"
              src={descriptionVideo}
              allowFullScreen
            />
          </AspectRatio>
          <StyledButtonContainer>
            <Button
              // variant="outline"
              onClick={goBack}
              height="40px"
              p="8px 24px"
              color="#222222"
              borderRadius="4px"
              border="1px solid #b3b3b3"
            >
              <FormattedMessage id="back" />
            </Button>
            <Button
              onClick={startSimulation}
              height="40px"
              p="8px 24px"
              backgroundColor="#149860"
              color="#ffffff"
              borderRadius="4px"
              bgHover="#118353"
            >
              {" "}
              <FormattedMessage id="begin" />{" "}
            </Button>
          </StyledButtonContainer>
        </StyledBackgroundOverview>
      </div>
    </StyledContainer>
  );
};
