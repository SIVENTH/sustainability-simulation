import styled from "styled-components";

// type Props = {
//   additionalInfo?: string;
//   bigImg?: boolean;
//   textLeft?: boolean;
// };

export const StyledIntroContainer = styled.div`
  line-height: 30px;
  padding: 5px 20px;
  margin-top: 16px;
`;

export const StyledContainer = styled.div`
  width: 100%;
  display: grid;
  grid-template-columns: repeat(24, 1fr);
  grid-template-rows: repeat(24, 1fr);
  grid-column-gap: 16px;
  grid-row-gap: 0px;
  padding: 32px;

  .description {
    grid-area: 1 / 1 / 25 / 13;
  }
  .sim-overview {
    grid-area: 1 / 13 / 25 / 25;
  }

  @media screen and (max-width: 1079px) {
    .description {
      grid-area: 12 / 1 / 25 / 25;
    }
    .sim-overview {
      grid-area: 1 / 1 / 12 / 25;
    }
  }
`;

export const StyledBackgroundInfo = styled.div`
  background: #ffffff;
  border-radius: 16px;
  padding: 24px;

  h2 {
    font-size: 24px;
    font-weight: 600;
  }

  ol {
    margin-top: 24px;
    margin-left: 40px;
    padding-left: 0;
    li {
      margin-left: 0;
      padding-left: 0;
    }

    & + p {
      margin-top: 24px;
    }
  }
`;

export const StyledButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  margin-top: 16px;

  button:last-of-type {
    /* flex-grow: 1; */
    margin-left: 16px;
  }
`;

export const StyledBackgroundOverview = styled.div`
  background: #ffffff;
  border-radius: 16px;
  padding: 24px;
`;
