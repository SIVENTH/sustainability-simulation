import { useState } from "react";
import { Text } from "../../components/ChakraReplace.styled";
import { SubscriptionType } from "types";
import { FormattedMessage } from "react-intl";

import { NewOrderCard } from "components/billing";

interface AddSubscriptionsProps {
  subscriptionType: SubscriptionType;
}

const AddSubscriptions = ({
  subscriptionType,
}: AddSubscriptionsProps): JSX.Element => {
  const [selected, setSelected] = useState<SubscriptionType | null>(null);

  return (
    <>
      <Text fontSize="20px" fontWeight={600} mb="24px">
        <FormattedMessage id="billing.addMoreLicenses" />
      </Text>
      <NewOrderCard
        subscriptionType={subscriptionType}
        selected={selected === subscriptionType}
        setSelected={setSelected}
      />
    </>
  );
};

export default AddSubscriptions;
