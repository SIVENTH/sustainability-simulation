import { Navigate, useNavigate } from "react-router-dom";
import { Button, Box, Flex } from "../../components/ChakraReplace.styled";
import queryString from "query-string";
import { FormattedMessage } from "react-intl";

import SuccessCard from "./SuccessCard";
import AddSubscriptions from "./AddSubscriptions";
import { colors } from "utils";
import { useSubscriptionInvoice } from "hooks/stripe";

export const BillingSuccessPage = (): JSX.Element | null => {
  const navigate = useNavigate();
  const searchParams = queryString.parse(location.search);
  const { subscriptionId } = searchParams;

  if (!subscriptionId || typeof subscriptionId !== "string")
    return <Navigate to="/" />;

  // TODO: handle isLoading
  const { invoice, error } = useSubscriptionInvoice(subscriptionId);

  if (!invoice || error) return null; // TODO: proper error handling

  return (
    <Box backgroundColor={colors.lightGray} p="40px">
      <Flex mb="32px">
        <Box width="65%">
          <SuccessCard invoice={invoice} />
        </Box>
        <Box width="35%" ml="32px">
          <AddSubscriptions subscriptionType={invoice.subscriptionType} />
        </Box>
      </Flex>
      {/* removed variant="grayOutline" from button */}
      <Button onClick={() => navigate("/billing/overview")}>
        <FormattedMessage id="backToBilling" />
      </Button>
    </Box>
  );
};
