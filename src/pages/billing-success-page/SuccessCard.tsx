import { Button, Box, Text, Flex } from "../../components/ChakraReplace.styled";
import { useNavigate } from "react-router-dom";
import { FormattedMessage } from "react-intl";

import { InvoiceOptionalPayment } from "types";
import { labels, colors } from "utils";

import { SubscriptionDetails, SubscriptionTypeIcon } from "components/billing";

interface SuccessCardProps {
  invoice: InvoiceOptionalPayment | null;
}

const SuccessCard = ({ invoice }: SuccessCardProps): JSX.Element | null => {
  const navigate = useNavigate();

  if (!invoice) return null;

  return (
    <Box backgroundColor="white" p="24px" borderRadius="8px">
      <Text fontSize="24px" fontWeight={600}>
        <FormattedMessage id="billing.orderProcessed" />
      </Text>
      <Text fontSize="16px">
        <FormattedMessage id="billing.orderSuccess" />
      </Text>
      <Box
        borderRadius="8px"
        border={`1px solid #939393`}
        overflow="hidden"
        mt="24px"
      >
        <Flex
          height="80px"
          backgroundColor={colors.lightGray}
          justifyContent="space-between"
          alignItems="center"
          position="relative"
          px="16px"
        >
          <Text fontSize="24px" fontWeight={600}>
            {labels[invoice.subscriptionType]}
          </Text>
          <SubscriptionTypeIcon subscriptionType={invoice.subscriptionType} />
        </Flex>
        <Box p="16px">
          <SubscriptionDetails invoice={invoice} />
          <Button mt="24px" onClick={() => navigate("/dashboard")}>
            <FormattedMessage id="startNow" />
          </Button>
        </Box>
      </Box>
    </Box>
  );
};

export default SuccessCard;
