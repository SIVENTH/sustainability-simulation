import { useCallback, useState } from "react";
import { Navigate } from "react-router-dom";
import {
  Box,
  Flex,
  Text,
  ChakraSpan,
  Button,
} from "../../components/ChakraReplace.styled";
import { FcCheckmark as Checkmark } from "react-icons/fc";
import { FiPauseCircle as PauseCircle } from "react-icons/fi";
import { MdBlock as Block } from "react-icons/md";
import queryString from "query-string";
import { FormattedMessage } from "react-intl";

import { toggleAutomaticRenewal, cancelSubscription } from "gateway";
import { intl, colors } from "utils";

const ChecklistItem = ({ text }: { text: string }) => (
  <Flex alignItems="center" mt="8px">
    <Checkmark />
    <Text fontSize="16px" ml="12px">
      {text}
    </Text>
  </Flex>
);

const useOnClickPause = (subscriptionId: string) => {
  const [loading, setLoading] = useState(false);

  const onClickPause = useCallback(async () => {
    if (loading) return;
    try {
      setLoading(true);
      await toggleAutomaticRenewal({
        subscriptionId,
        isAutomatic: false,
      });
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  }, []);

  return {
    onClickPause,
    loading,
  };
};

const useOnClickStop = (subscriptionId: string) => {
  const [loading, setLoading] = useState(false);

  const onClickStop = useCallback(async () => {
    if (loading) return;
    try {
      setLoading(true);
      await cancelSubscription({
        subscriptionId,
      });
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  }, []);

  return {
    onClickStop,
    loading,
  };
};

const PauseCard = ({
  subscriptionId,
}: {
  subscriptionId: string;
}): JSX.Element => {
  const { onClickPause, loading } = useOnClickPause(subscriptionId);

  return (
    <Box width="448px" backgroundColor="white" p="16px" borderRadius="8px">
      <Text fontSize="24px" fontWeight={600} lineHeight="125%">
        <FormattedMessage id="billing.pauseDescription" />
      </Text>
      <Text fontSize="12px" fontWeight={700} color={colors.green2}>
        <FormattedMessage id="billing.pauseDescription2" />
      </Text>
      <ChecklistItem
        text={intl.formatMessage({
          id: "billing.pauseCheck1",
        })}
      />
      <ChecklistItem
        text={intl.formatMessage({
          id: "billing.pauseCheck2",
        })}
      />
      <ChecklistItem
        text={intl.formatMessage({
          id: "billing.pauseCheck3",
        })}
      />
      <Text fontSize="16px" mt="24px">
        <FormattedMessage id="billing.pauseExplanation" />{" "}
        <ChakraSpan>
          <FormattedMessage id="btn.learn.more" />
        </ChakraSpan>
      </Text>
      <Button
        width="100%"
        fontSize="16px"
        fontWeight={600}
        mt="40px"
        onClick={onClickPause}
        disabled={loading}
      >
        <Text mr="12px">
          <FormattedMessage id="pauseSubscription" />
        </Text>
        <PauseCircle size="20px" />
      </Button>
    </Box>
  );
};

interface StopButtonProps {
  onClick: () => void;
  disabled: boolean;
}

const StopButton = ({ onClick, disabled }: StopButtonProps) => (
  // Removed variant="gray" from button
  <Button
    width="100%"
    fontSize="16px"
    fontWeight={600}
    mt="40px"
    onClick={onClick}
    disabled={disabled}
  >
    <Text mr="12px">
      <FormattedMessage id="cancelSubscription" />
    </Text>
    <Block size="20px" />
  </Button>
);

const StopCard = ({
  subscriptionId,
}: {
  subscriptionId: string;
}): JSX.Element => {
  const { onClickStop, loading } = useOnClickStop(subscriptionId);
  return (
    <Box
      width="448px"
      p="16px"
      border={`1px solid ${colors.concrete}`}
      borderRadius="8px"
      ml="128px"
    >
      <Text fontSize="24px" fontWeight={600} lineHeight="125%">
        <FormattedMessage id="billing.cancelDescription" />
      </Text>
      <Text fontSize="12px" fontWeight={700} color={colors.textRed}>
        <FormattedMessage id="billing.cancelDescription2" />
      </Text>
      <ChecklistItem
        text={intl.formatMessage({
          id: "billing.cancelCheck1",
        })}
      />
      <ChecklistItem
        text={intl.formatMessage({
          id: "billing.cancelCheck2",
        })}
      />
      <ChecklistItem
        text={intl.formatMessage({
          id: "billing.cancelCheck3",
        })}
      />
      <Text fontSize="16px" mt="24px">
        <FormattedMessage id="billing.cancelExplanation" />{" "}
        <ChakraSpan>
          <FormattedMessage id="btn.learn.more" />
        </ChakraSpan>
      </Text>
      <StopButton onClick={onClickStop} disabled={loading} />
    </Box>
  );
};

export const BillingCancellationPage = (): JSX.Element => {
  const searchParams = queryString.parse(location.search);
  const { subscriptionId } = searchParams;

  if (!subscriptionId || typeof subscriptionId !== "string")
    return <Navigate to="/" />;

  return (
    <Box backgroundColor={colors.lightGray} p="40px">
      <Text fontSize="24px" fontWeight={600}>
        <FormattedMessage id="cancelYourSubscription" />
      </Text>
      <Text fontSize="16px" width="736px">
        <FormattedMessage id="billing.cancelDescriptionMain" />
      </Text>
      <Flex mt="24px">
        <PauseCard subscriptionId={subscriptionId} />
        <StopCard subscriptionId={subscriptionId} />
      </Flex>
    </Box>
  );
};
