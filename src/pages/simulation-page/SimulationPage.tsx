/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable max-lines-per-function */
import { useEffect, useReducer, useMemo, useState, useRef } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import { useForm, FormProvider } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";

import { usePagination } from "hooks";
import { Questions } from "components/question";
import { Section } from "components/section";
import { Result } from "components/result";
import { Suggestion } from "components/suggestion";
import { tea } from "data/tea";
import { TeaScore } from "model/TeaScoreCalculation";
import { useAuth } from "context";
import { db } from "gateway";
// import { getQuestionnaires, getQuestions, postSession } from "gateway/services";
import { teaImageUrl } from "constants/index";
import { TopicType, ResultDocument, Question } from "types";

import { BackButton, NextButton } from "components/buttons";

import { Container, Navigation } from "./SimulationPage.styled";

type State = {
  growingScore: number;
  pickingScore: number;
  witheringScore: number;
  rollingScore: number;
  fermentingScore: number;
  dryingScore: number;
  packagingScore: number;
  blendingScore: number;
  distributionScore: number;
  customerScore: number;
  storageScore: number;
  endOfLifeScore: number;
  productionScore: number; // Total score for all production related questions.
  manufacturingScore: number; // Total score for all manufacturing and processing related questions.
  logisticsScore: number; // Total score for all logistics related questions.
  otherScore: number; // Total for all remaining related questions that don't fit in elsewhere.
  totalScore: number;
  isResultShown: boolean;
  isSuggestionsShown: boolean;
  isSimOver: boolean;
};

enum ActionTypes {
  CALCULATE_SCORE = "CALCULATE_SCORE",
  SHOW_RESULTS = "SHOW_RESULTS",
  SHOW_SUGGESTIONS = "SHOW_SUGGESTIONS",
  FINISH_EARLY = "FINISH_EARLY",
}

type Action =
  | {
      type: ActionTypes.CALCULATE_SCORE;
      payload: { topic: TopicType; points: number };
    }
  | { type: ActionTypes.SHOW_RESULTS; payload: boolean }
  | { type: ActionTypes.SHOW_SUGGESTIONS; payload: boolean }
  | { type: ActionTypes.FINISH_EARLY; payload: boolean };

// calculate scores of all variables sent
function calculateScores(scores: Array<number>) {
  let total = 0;

  scores.forEach((score) => {
    total += score;
  });

  return parseFloat((total / scores.length).toFixed(2));
}

const getDefaultValues = (questions: Question[]) => {
  const defaultValues = {};

  questions.forEach((question) => {
    defaultValues[question.question] = "";
  });

  return defaultValues;
};

const getValidationSchema = (questions: Question[]) => {
  const schemaObject = {};

  // Required value appears to always be a string if present.
  questions.forEach((question) => {
    if (typeof question.required === "string")
      schemaObject[question.question] = yup
        .string()
        .required(question.required);
  });

  return yup.object().shape(schemaObject);
};

function reducer(state: State, action: Action): State {
  // specific to tea
  switch (action.type) {
    case ActionTypes.CALCULATE_SCORE: {
      switch (action.payload.topic) {
        case TopicType.PREP: {
          return {
            ...state,
            growingScore: 0, // set all scores to 0
            productionScore: 0,
            manufacturingScore: 0,
            logisticsScore: 0,
            otherScore: 0,
            totalScore: 0,
          };
        }
        case TopicType.GROWING: {
          return {
            ...state,
            growingScore: parseFloat(action.payload.points.toFixed(2)),
            pickingScore: 0, // set next page's points to 0 to fix back issue.
          };
        }
        case TopicType.PICKING: {
          return {
            ...state,
            pickingScore: parseFloat(action.payload.points.toFixed(2)),
            witheringScore: 0,
          };
        }
        case TopicType.WITHERING: {
          return {
            ...state,
            witheringScore: parseFloat(action.payload.points.toFixed(2)),
            rollingScore: 0, // set next page's points to 0 to fix back issue.
          };
        }
        case TopicType.ROLLING: {
          return {
            ...state,
            rollingScore: parseFloat(action.payload.points.toFixed(2)),
            fermentingScore: 0, // set next page's points to 0 to fix back issue.
          };
        }
        case TopicType.FERMENTING: {
          return {
            ...state,
            fermentingScore: parseFloat(action.payload.points.toFixed(2)),
            dryingScore: 0, // set next page's points to 0 to fix back issue.
          };
        }
        case TopicType.DRYING: {
          return {
            ...state,
            dryingScore: parseFloat(action.payload.points.toFixed(2)),
            blendingScore: 0, // set next page's points to 0 to fix back issue.
          };
        }
        case TopicType.BLENDING: {
          return {
            ...state,
            // send and update Blending Score
            blendingScore: parseFloat(action.payload.points.toFixed(2)),
            packagingScore: 0, // set next page's points to 0 to fix back issue.
            // send and update manufacturing score
          };
        }
        case TopicType.PACKAGING: {
          return {
            ...state,
            packagingScore: parseFloat(action.payload.points.toFixed(2)),
            distributionScore: 0, // set next page's points to 0 to fix back issue.
          };
        }
        case TopicType.DISTRIBUTION: {
          return {
            ...state,
            distributionScore: parseFloat(action.payload.points.toFixed(2)),
            storageScore: 0, // set next page's points to 0 to fix back issue.
          };
        }
        case TopicType.STORAGE: {
          return {
            ...state,
            storageScore: parseFloat(action.payload.points.toFixed(2)),
            customerScore: 0, // set next page's points to 0 to fix back issue.
          };
        }
        case TopicType.CUSTOMER: {
          return {
            ...state,
            customerScore: parseFloat(action.payload.points.toFixed(2)),
          };
        }
        case TopicType.ENDOFLIFE: {
          return {
            ...state,
            endOfLifeScore: parseFloat(action.payload.points.toFixed(2)),
            productionScore: calculateScores([
              // update production scores
              state.growingScore,
              state.pickingScore,
            ]),
            manufacturingScore: calculateScores([
              // update manufacturing scores
              state.witheringScore,
              state.rollingScore,
              state.fermentingScore,
              state.dryingScore,
              state.blendingScore,
            ]),
            logisticsScore: calculateScores([
              // update logistic scores
              state.packagingScore,
              state.distributionScore,
              state.storageScore,
              state.customerScore,
            ]),
            // update other scores
            otherScore: calculateScores([state.endOfLifeScore]),
            // send Total Score when doing nothing else
            totalScore: calculateScores([
              state.productionScore,
              state.manufacturingScore,
              state.logisticsScore,
              state.otherScore,
            ]),
          };
        }
        default: {
          return {
            ...state,
            productionScore: calculateScores([
              // update production scores
              state.growingScore,
              state.pickingScore,
            ]),
            manufacturingScore: calculateScores([
              // update manufacturing scores
              state.witheringScore,
              state.rollingScore,
              state.fermentingScore,
              state.dryingScore,
              state.blendingScore,
            ]),
            logisticsScore: calculateScores([
              // update logistic scores
              state.packagingScore,
              state.distributionScore,
              state.storageScore,
              state.customerScore,
            ]),
            // update other scores
            otherScore: calculateScores([state.endOfLifeScore]),
            // send Total Score when doing nothing else
            totalScore: calculateScores([
              state.productionScore,
              state.manufacturingScore,
              state.logisticsScore,
              state.otherScore,
            ]),
          };
        }
      }
    }
    case ActionTypes.SHOW_RESULTS:
      return {
        ...state,
        isResultShown: action.payload,
        isSimOver: action.payload,
        productionScore: calculateScores([
          // update production scores
          state.growingScore,
          state.pickingScore,
        ]),
        manufacturingScore: calculateScores([
          // update manufacturing scores
          state.witheringScore,
          state.rollingScore,
          state.fermentingScore,
          state.dryingScore,
          state.blendingScore,
        ]),
        logisticsScore: calculateScores([
          // update logistic scores
          state.packagingScore,
          state.distributionScore,
          state.storageScore,
          state.customerScore,
        ]),
        // update other scores
        otherScore: calculateScores([state.endOfLifeScore]),
        // send Total Score when doing nothing else
        totalScore: calculateScores([
          state.productionScore,
          state.manufacturingScore,
          state.logisticsScore,
          state.otherScore,
        ]),
      };
    case ActionTypes.FINISH_EARLY:
      return {
        ...state,
        isResultShown: true,
        isSimOver: true,
        productionScore: calculateScores([
          // update production scores
          state.growingScore,
          state.pickingScore,
        ]),
        manufacturingScore: calculateScores([
          // update manufacturing scores
          state.witheringScore,
          state.rollingScore,
          state.fermentingScore,
          state.dryingScore,
          state.blendingScore,
        ]),
        logisticsScore: calculateScores([
          // update logistic scores
          state.packagingScore,
          state.distributionScore,
          state.storageScore,
          state.customerScore,
        ]),
        // update other scores
        otherScore: calculateScores([state.endOfLifeScore]),
        // send Total Score when doing nothing else
        totalScore: calculateScores([
          state.productionScore,
          state.manufacturingScore,
          state.logisticsScore,
          state.otherScore,
        ]),
      };
    case ActionTypes.SHOW_SUGGESTIONS:
      return {
        ...state,
        isSuggestionsShown: action.payload,
        isResultShown: !state.isResultShown,
        productionScore: calculateScores([
          // update production scores
          state.growingScore,
          state.pickingScore,
        ]),
        manufacturingScore: calculateScores([
          // update manufacturing scores
          state.witheringScore,
          state.rollingScore,
          state.fermentingScore,
          state.dryingScore,
          state.blendingScore,
        ]),
        logisticsScore: calculateScores([
          // update logistic scores
          state.packagingScore,
          state.distributionScore,
          state.storageScore,
          state.customerScore,
        ]),
        // update other scores
        otherScore: calculateScores([state.endOfLifeScore]),
        // send Total Score when doing nothing else
        totalScore: calculateScores([
          state.productionScore,
          state.manufacturingScore,
          state.logisticsScore,
          state.otherScore,
        ]),
      };
    default:
      return state;
  }
}

const initialState: State = {
  pickingScore: 0,
  witheringScore: 0,
  growingScore: 0,
  totalScore: 0,
  isResultShown: false,
  isSimOver: false,
  isSuggestionsShown: false,
  rollingScore: 0,
  fermentingScore: 0,
  dryingScore: 0,
  packagingScore: 0,
  blendingScore: 0,
  distributionScore: 0,
  storageScore: 0,
  customerScore: 0,
  endOfLifeScore: 0,
  productionScore: 0,
  manufacturingScore: 0,
  otherScore: 0,
  logisticsScore: 0,
};

/**
 * Retrieve all questions for a specific questionnaire
 * @page SimulationPage
 * @param {number} questionnaireId The questionnaire id to get questions for
 * @return {object} returns the page for the questions
 * @todo need to store selected simulation/questionnaire either in user context, local storage, or somewhere else
 */
export const SimulationPage = (): JSX.Element => {
  // need seperate start url for individual vs team
  const navigate = useNavigate();
  const params = useParams();
  const { authedUser } = useAuth();
  const [minutes, setMinutes] = useState(0);
  const formRef: any = useRef(null);
  // const [apiQuestionnaire, setApiQuestionnaire] = useState<[]>([]);
  // const [apiQuestion, setApiQuestion] = useState<[]>([]);
  // const [questionnaireSession, setQuestionnaireSession] = useState<any>(null);

  // /**
  //  * Retrieve all questionnaires from API
  //  * @method getQuestionnairesFromAPI
  //  * @param {number} questionnaireId The questionnaire id to find
  //  * @return {object} returns the object with all questionnaires
  //  * @todo implement endpoint that allows getting just a specific questionnaire
  //  */
  // const getQuestionnairesFromAPI = async () => {
  //   const response = await getQuestionnaires();
  //   await setApiQuestionnaire(response.data);
  //   // Find/choose questionnaire from response from user input of which questionnnaire they want to talk. For now defaults to 2.
  // };

  // /**
  //  * Retrieve all qeustions for a specific questionnaire
  //  * @method getQuestionsFromAPI
  //  * @param {number} questionnaireId The questionnaire id to find questions for
  //  * @return {object} the list of questions to find.
  //  * @todo currently hardcoded the questionnaireid, needs to be replaced
  //  */
  // const getQuestionsFromAPI = async () => {
  //   const questionnaireId = 2; // questionnaire id, should populate with the id of the chosen questionnnaire
  //   const response = await getQuestions(questionnaireId);
  //   await setApiQuestion(response.data);
  // };

  // /**
  //  * Start simulation/session for user
  //  * @method createSession
  //  * @param {number} sessionType Whether individual (1) or group/education/enterprise (2) based siumulation
  //  * @param {number} orgId The id of the organization running the session
  //  * @param {number} questionnaireId The id of the questionnaire, currently only 2 is available.
  //  * @param {number} [departmentId] The id fo the class/department running the session
  //  * @param {number} [teamId] The id fo the team/group running the session
  //  * @param {number} [userId] The id fo the individual running the session
  //  * @return {object} an object represernting the data for the simulation
  //  * @todo currently hardcoded the simulation for group usage, needs to be allowed fro individuals
  //  */
  // const createSession = async () => {
  //   getQuestionnairesFromAPI();
  //   getQuestionsFromAPI();
  //   const orgId = Number(localStorage.getItem("orgId"));
  //   const sessionInfo = {
  //     departmentId: 0,
  //     teamId: 0,
  //     orgId: orgId,
  //     questionnaireId: 2, // hardcoded for now
  //     userId: 0,
  //   };
  //   try {
  //     const response = await postSession(sessionInfo);
  //     // setQuestionnaireSession(response.data);
  //     console.log("[SIMULATION PAGE/createSession]response: ", response);
  //   } catch (error) {
  //     console.log("[SIMULATION PAGE/createSession]error: ", error);
  //   }
  // };

  // createSession();

  const [
    {
      growingScore,
      pickingScore,
      witheringScore,
      rollingScore,
      fermentingScore,
      dryingScore,
      blendingScore,
      packagingScore,
      distributionScore,
      storageScore,
      customerScore,
      endOfLifeScore,
      productionScore,
      manufacturingScore,
      logisticsScore,
      otherScore,
      totalScore,
      isResultShown,
      isSimOver,
      isSuggestionsShown,
    },
    dispatch,
  ] = useReducer(reducer, initialState);

  const { currentPage, displayData, prev, next, dataLength } = usePagination(
    tea,
    1
  );

  // eslint-disable-next-line @typescript-eslint/ban-types, sonarjs/cognitive-complexity
  const onSubmit = async (formData: Object): Promise<void> => {
    if (dataLength <= currentPage && isSimOver) {
      await db.addOneDocument<ResultDocument>(
        `users/${authedUser?.uid}/sim-results`,
        {
          title: "Tea",
          growingScore,
          witheringScore,
          rollingScore,
          fermentingScore,
          dryingScore,
          blendingScore,
          packagingScore,
          distributionScore,
          storageScore,
          customerScore,
          endOfLifeScore,
          pickingScore,
          productionScore,
          manufacturingScore,
          logisticsScore,
          otherScore,
          totalScore,
          imageUrl: teaImageUrl,
          date: db.getServerTimestamp(),
        }
      );
      navigate("/dashboard");
      return;
    }

    const points = TeaScore.calculateScore(topic, formData);

    if (points)
      dispatch({
        type: ActionTypes.CALCULATE_SCORE,
        payload: { topic, points },
      });

    await db.addDocumentData(
      `users/${authedUser?.uid}/sim-history/${params.id}`,
      formData
    );

    if (dataLength <= currentPage && !isSimOver) {
      dispatch({ type: ActionTypes.SHOW_RESULTS, payload: true });
      return;
    }

    if (minutes == 20) {
      console.log("[SIMULATION PAGE/onSubmit]dataLength: ", dataLength);
      console.log("[SIMULATION PAGE/onSubmit]currentPage: ", currentPage);
      console.log("[SIMULATION PAGE/onSubmit]isSimOver: ", isSimOver);
      console.log("finishing early");
      // dispatch({ type: ActionTypes.FINISH_EARLY, payload: true });
      // console.log("finishing early");
    }

    next();
  };

  const handlePrevAction = (): void => {
    if (currentPage === 1) {
      navigate("/background/tea"); // change to user selected intustry
      return;
    }
    if (isSuggestionsShown) {
      dispatch({ type: ActionTypes.SHOW_SUGGESTIONS, payload: false });
      return;
    }
    if (dataLength <= currentPage && isSimOver) {
      dispatch({ type: ActionTypes.SHOW_RESULTS, payload: false });
      return;
    }
    prev();
  };

  const { topic, section, date, meta, questions } = displayData();
  const defaultValues = useMemo(() => getDefaultValues(questions), [questions]);
  const resolver = useMemo(
    () => yupResolver(getValidationSchema(questions)),
    [questions]
  );

  const methods = useForm({
    defaultValues,
    resolver,
  });
  const { handleSubmit, reset } = methods;

  // Reset default values.
  // Currently, form is updated, and then default values are set.
  // The time before the default values are set causes a warning to appear in the console.
  useEffect(() => {
    reset({ ...defaultValues });
  }, [defaultValues]);

  useEffect(() => {
    const interval = setInterval(() => {
      setMinutes((prevMinute) => prevMinute + 1);

      // Check if 20 minutes have passed and submit the form if so
      if (minutes === 20 && formRef.current) {
        // console.log("should submit now");
        // formRef.current.submit();
      }
    }, 60000);

    return () => clearInterval(interval);
  }, [minutes]);

  // 1200000 = 20 min
  // 10000 = 10 seconds

  return (
    <Container>
      <FormProvider {...methods}>
        <form onSubmit={handleSubmit(onSubmit)} ref={formRef}>
          {isResultShown ? (
            <Result
              showSuggestions={() =>
                dispatch({ type: ActionTypes.SHOW_SUGGESTIONS, payload: true })
              }
              pickingScore={pickingScore}
              growingScore={growingScore}
              witheringScore={witheringScore}
              rollingScore={rollingScore}
              fermentingScore={fermentingScore}
              dryingScore={dryingScore}
              blendingScore={blendingScore}
              packagingScore={packagingScore}
              distributionScore={distributionScore}
              customerScore={customerScore}
              storageScore={storageScore}
              endOfLifeScore={endOfLifeScore}
              productionScore={productionScore}
              manufacturingScore={manufacturingScore}
              logisticsScore={logisticsScore}
              otherScore={otherScore}
              totalScore={totalScore}
            />
          ) : isSuggestionsShown ? (
            <Suggestion
              totalScore={totalScore}
              productionScore={productionScore}
              manufacturingScore={manufacturingScore}
              logisticsScore={logisticsScore}
              otherScore={otherScore}
            />
          ) : (
            <>
              <Section
                section={section}
                sectionPage={currentPage}
                date={date}
                additionalInfo={meta}
                pageLength={dataLength}
              />
              <Questions questions={questions} />
            </>
          )}
          <Navigation>
            <BackButton onClick={handlePrevAction} />
            {!isSimOver ? (
              <NextButton type="submit">
                <FormattedMessage id="next" />
              </NextButton>
            ) : (
              <NextButton onClick={onSubmit}>
                <FormattedMessage id="finish" />
              </NextButton>
            )}
          </Navigation>
        </form>
      </FormProvider>
    </Container>
  );
};
