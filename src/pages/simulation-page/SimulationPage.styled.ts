import styled from "styled-components";

export const Container = styled.div`
  padding: 48px;
`;

export const Navigation = styled.div`
  width: 100px;
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 10px;
  margin-top: 20px;
`;
