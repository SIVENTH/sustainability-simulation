import { useRef } from "react";
import { SCard } from "components/card";
import { useParams } from "react-router-dom";
import { BarChart } from "components/bar";
import { useAuth } from "context";
import { useDocument } from "hooks";
import { db } from "gateway";
import { ResultDocument } from "types";
import { SelectModal } from "components/modals";
import { createPdfFromHtml } from "utils";
import { Heading } from "components/heading";
import teaImage from "images/tea.jpeg";
import { colors } from "utils";
import { Skeleton, useBreakpoint } from "@chakra-ui/react";
import {
  Text,
  Box,
  Flex,
  Image,
  Container,
  Stack,
  VStack,
  Button,
} from "../../components/ChakraReplace.styled";

interface ResultWithTotal extends ResultDocument {
  totalScore: number;
}

// eslint-disable-next-line max-lines-per-function
export const DetailPage = (): JSX.Element => {
  const params = useParams();

  const mainDivRef = useRef<HTMLDivElement>(null);
  const { authedUser } = useAuth();
  const breakPoint = useBreakpoint();
  const [resultData, status, error] = useDocument<ResultWithTotal>(
    db.getDocumentReference(`users/${authedUser?.uid}/sim-results/${params.id}`)
  );

  if (error)
    return (
      <Container
        pt="20px"
        display="flex"
        justifyContent="center"
        alignItems="center"
      >
        <Text fontSize="24px" textAlign="center">
          Sorry we encountered a technical error while retrieving your data.
          Please contact administrator at support@siventh.com.
        </Text>
      </Container>
    );

  return (
    <Box p="20px 70px" ref={mainDivRef}>
      {status === "loading" ? (
        <Stack>
          <Skeleton height="10px" />
          <Skeleton height="10px" />
          <Skeleton height="10px" />
        </Stack>
      ) : (
        <>
          <Flex justifyContent="center" flexDirection="column">
            <Stack spacing={10} flexDirection="column">
              <Box width="100%" height="auto">
                <Heading mb="25px" textAlign={{ base: "center", lg: "left" }}>
                  Tea Industry
                </Heading>
                <Image
                  alt="Tea image"
                  src={teaImage}
                  objectFit="cover"
                  // fallback={<Container centerContent> ...Loading </Container>}
                />
              </Box>
              <VStack>
                <Heading size="md">Simulation Score</Heading>
                <Text> {resultData?.date.toDate().toDateString()} </Text>
                <BarChart
                  productionScore={resultData?.productionScore}
                  manufacturingScore={resultData?.manufacturingScore}
                  logisticsScore={resultData?.logisticsScore}
                  otherScore={resultData?.otherScore}
                  height={350}
                />
              </VStack>
            </Stack>
          </Flex>
          <Flex mt="20px" justifyContent="center" flexDirection="column">
            <Stack flexDirection="column" spacing={50}>
              <SCard p="10px">
                <Heading size="md">Details</Heading>
                <Text color={colors.textGray} mt="5px">
                  Coming soon: detailed analytics about your simulation
                  performance. Click the Details button below to view your
                  simulation results. Click the Download button below to
                  download your simulation results.
                </Text>
                <Stack mt="15px" justifyContent="center" flexDirection="column">
                  <Button
                    onClick={() =>
                      createPdfFromHtml({
                        htmlElement: mainDivRef.current,
                        breakPoint,
                      })
                    }
                  >
                    Download
                  </Button>
                </Stack>
              </SCard>
              <SCard p="10px" mt={{ base: "10px" }}>
                <Heading size="md">Your simulation</Heading>
                <Text color={colors.textGray} mt="5px">
                  To start a new simulation, click one of the buttons below.
                  Note: Subscriptions are currently under development.
                </Text>
                <Stack mt="60px" justifyContent="center" flexDirection="column">
                  {/* change from pathname to value for selected for simulations */}
                  <SelectModal
                    pathname="/background/tea" // change to user selected image
                    state={{ from: location.pathname }}
                    isCentered
                  />
                  <Button disabled={true}> Subscribe </Button>
                </Stack>
              </SCard>
            </Stack>
          </Flex>
        </>
      )}
    </Box>
  );
};
