/* eslint-disable @typescript-eslint/no-explicit-any */
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useAuth } from "context";
import { ClassSelectionContainer } from "components/dashboard";
// import { usePaginateQuery } from "hooks";
// import { db } from "gateway";
import { StyledContainer } from "./ClassOverViewPage.styled";
import { getAllOrganizationDepartments } from "gateway/services";
import { departmentListResponse } from "types";

export const ClassOverviewPage = (): JSX.Element => {
  const navigate = useNavigate();
  const { organization } = useAuth();
  const [classList, setClassList] = useState<departmentListResponse[]>([]);
  const [fetchClasses, setFetchClasses] = useState(false);

  const redirectToDetails = (id: number): void => {
    navigate(`/result/${id}`);
  };
  // const { loadMore, hasMore } = usePaginateQuery(
  //   db
  //     .getCollectionReference(`users/${authedUser?.uid}/sim-results`)
  //     .orderBy("date", "desc")
  // );

  const getClassList = async (id: string): Promise<any> => {
    try {
      const response: any = await getAllOrganizationDepartments(id);
      setClassList(response?.data);
      return response;
    } catch (error) {
      return error;
    }
  };

  const handleClassUpdated = (newClass: any) => {
    setClassList((prevState) => [...(prevState ?? []), newClass]);
    setFetchClasses(true);
  };

  const orgId = organization ? organization.id : localStorage.getItem("orgId");
  useEffect(() => {
    if (orgId && (fetchClasses || classList.length === 0)) {
      getClassList(orgId).then(() => {
        setFetchClasses(false);
      });
    }
  }, [orgId, fetchClasses, classList.length]);

  return (
    <StyledContainer>
      <ClassSelectionContainer
        classList={classList}
        redirectToDetails={redirectToDetails}
        // loadMore={loadMore}
        // hasMore={hasMore}
        onClassUpdated={handleClassUpdated}
      />
    </StyledContainer>
  );
};
