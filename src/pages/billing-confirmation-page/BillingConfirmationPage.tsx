import { StyledBillingPageContainer } from "./BillingConfirmationPage.styled";

import { BillingCard } from "components/billing";
import { OrderCard } from "components/billing";

import { CostFormProvider } from "context";

export const BillingConfirmationPage = (): JSX.Element => {
  return (
    <CostFormProvider>
      <StyledBillingPageContainer>
        {/* <Grid height="400px" templateColumns="repeat(2, 1fr)" gap="25px"> */}
        <BillingCard></BillingCard>
        <OrderCard></OrderCard>
      </StyledBillingPageContainer>
    </CostFormProvider>
  );
};
