import styled from "styled-components";

export const StyledBillingPageContainer = styled.div`
  display: grid;
  height: auto;
  padding: 40px;

  @media screen and (min-width: 1200px) {
    grid-template-columns: repeat(2, 1fr);
    gap: 24px;
  }

  h3 {
    font-size: 23px;
    margin-bottom: 24px;
  }
`;
