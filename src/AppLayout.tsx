/* eslint-disable max-lines-per-function */
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { firebaseUser } from "gateway";
import { memo } from "react";

import { Header } from "components/header";
import { ProtectedComponent } from "components/route";
import { BackgroundPage } from "pages/background-page";
import { BillingConfirmationPage } from "pages/billing-confirmation-page";
import { BillingOverviewPage } from "pages/billing-overview-page";
import { DashboardPage } from "pages/dashboard-page";
import { SimulationPage } from "pages/simulation-page";
import { DetailPage } from "pages/detail-page";
import { LoginPage } from "pages/login-page";
import { RegisterPage } from "pages/register-page";
import { ForgotPage } from "pages/forgot-page";
import { BillingCancellationPage } from "pages/billing-cancellation-page";
import { BillingSuccessPage } from "pages/billing-success-page";
import { SidebarNav } from "components/SidebarNav";
import { ProfilePage } from "pages/profile-page";

import {
  StyledGridMainOuter,
  StyledGridMainInner,
  StyledGridTopNav,
  StyledGridSideNav,
  StyledGridContent,
} from "App.styled";
import { ClassOverviewPage } from "pages/education";

const AppLayout = ({
  authedUser,
}: {
  authedUser: firebaseUser | null;
}): JSX.Element => (
  <Router>
    <StyledGridMainOuter>
      <StyledGridTopNav>
        <Header />
      </StyledGridTopNav>
      <Routes>
        <Route path="/login" element={<LoginPage />} />
      </Routes>
      <StyledGridMainInner authedUser>
        {authedUser && (
          <StyledGridSideNav authedUser>
            <SidebarNav />
          </StyledGridSideNav>
        )}
        <StyledGridContent>
          <Routes>
            <Route path="/" element={<DashboardPage />} />
            <Route path="/signup" element={<RegisterPage />} />
            <Route path="/signin" element={<LoginPage />} />
            <Route path="/register" element={<RegisterPage />} />
            <Route path="/forgot" element={<ForgotPage />} />
            <Route
              path="/dashboard"
              element={
                <ProtectedComponent>
                  <DashboardPage />
                </ProtectedComponent>
              }
            />
            <Route
              path="/profile"
              element={
                <ProtectedComponent>
                  <ProfilePage />
                </ProtectedComponent>
              }
            />
            <Route
              path="/background/:id"
              element={
                <ProtectedComponent>
                  <BackgroundPage />
                </ProtectedComponent>
              }
            />
            <Route
              path="/simulation/:id"
              element={
                <ProtectedComponent>
                  <SimulationPage />
                </ProtectedComponent>
              }
            />
            <Route
              path="/result/:id"
              element={
                <ProtectedComponent>
                  <DetailPage />
                </ProtectedComponent>
              }
            />
            <Route
              path="/billing/confirmation"
              element={
                <ProtectedComponent>
                  <BillingConfirmationPage />
                </ProtectedComponent>
              }
            />
            <Route
              path="/billing/overview"
              element={
                <ProtectedComponent>
                  <BillingOverviewPage />
                </ProtectedComponent>
              }
            />
            <Route
              path="/billing/success"
              element={
                <ProtectedComponent>
                  <BillingSuccessPage />
                </ProtectedComponent>
              }
            />
            <Route
              path="/billing/cancellation"
              element={
                <ProtectedComponent>
                  <BillingCancellationPage />
                </ProtectedComponent>
              }
            />
            {/* Education Routes */}
            {/* TO DO: Change elements when pages are ready */}
            <Route
              path="/education/history"
              element={
                <ProtectedComponent>
                  <BillingCancellationPage />
                </ProtectedComponent>
              }
            />

            <Route
              path="/education/class"
              element={
                <ProtectedComponent>
                  <ClassOverviewPage />
                </ProtectedComponent>
              }
            />

            <Route
              path="/education/participants"
              element={
                <ProtectedComponent>
                  <BillingCancellationPage />
                </ProtectedComponent>
              }
            />

            <Route
              path="/education/session"
              element={
                <ProtectedComponent>
                  <BillingCancellationPage />
                </ProtectedComponent>
              }
            />
          </Routes>
        </StyledGridContent>
      </StyledGridMainInner>
    </StyledGridMainOuter>
  </Router>
);

export default memo(AppLayout);
