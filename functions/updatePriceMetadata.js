const Stripe = require("stripe").Stripe;
const fs = require("fs");

const env = process.argv[2];

const configPath = `../functionvars.${env}.json`;

if (!(configPath && fs.existsSync(configPath))) {
  console.log("JSON config file does not exist");
  return;
}


const config = require(configPath);
const prices = config.stripe.price;

const stripe = new Stripe(config.stripe.secret, {
  apiVersion: "2020-08-27",
});

const updateMetadata = async () => {
    for(const [key, value] of Object.entries(prices)) {
        const [typeKey, frequencyKey, quantityKey] = key.split('-');
        const metaData = {};
    
        if (typeKey === 'edu') metaData.subscriptionType = 'education';
        else metaData.subscriptionType = 'enterprise';
    
        metaData.frequency = frequencyKey;
        metaData.quantity = quantityKey;
    
        await stripe.prices.update(
            value,
            {  metadata: metaData  }
        );
    }
}

updateMetadata().then(() => console.log('finished'));
