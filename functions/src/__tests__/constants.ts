export const mock_uid = "mock_uid";
export const mock_email = "mock_email";
export const context = { auth: { uid: mock_uid } };
export const customer_id = "customer_id";
export const subscription_id = "subscription_id";
export const client_secret = "client_secret";
export const price_id = "price_id";
export const payment_intent = "payment_intent";
export const payment_method = "payment_method";
export const product_id = "product_id";

export const error_test_label = "Throws an error if unauthorized.";
