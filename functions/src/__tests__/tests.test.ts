// TODO: Organize tests / separate into different files

import functions from "firebase-functions-test";
import {
  error_test_label,
  context,
  customer_id,
  subscription_id,
  client_secret,
  mock_uid,
  mock_email,
  price_id,
  payment_intent,
  payment_method,
  product_id,
} from "./constants";

const testEnv = functions();
testEnv.mockConfig({
  stripe: {
    secret: "mockSecret",
    signing: "mockSigning",
    price: {
      "edu-monthly-small": "price_edms",
      "edu-monthly-medium": "price_edmm",
      "edu-monthly-large": "price_edml",
      "edu-monthly-unlimited": "price_edmu",
      "edu-annual-small": "price_edas",
      "edu-annual-medium": "price_edam",
      "edu-annual-large": "price_edal",
      "edu-annual-unlimited": "price_edau",
      "ent-monthly-small": "price_enms",
      "ent-monthly-medium": "price_enmm",
      "ent-monthly-large": "price_enml",
      "ent-monthly-unlimited": "price_enmu",
      "ent-annual-small": "price_enas",
      "ent-annual-medium": "price_enam",
      "ent-annual-large": "price_enal",
      "ent-annual-unlimited": "price_enau",
    },
  },
});

/**
 * mock setup
 */
const mockGet = jest.fn();
const mockSet = jest.fn();
const mockDelete = jest.fn();

mockGet.mockReturnValue({ customer_id: customer_id });

jest.mock("firebase-admin", () => ({
  initializeApp: jest.fn(),
  firestore: () => ({
    collection: () => ({
      doc: () => ({
        get: () => ({
          data: mockGet,
        }),
        set: mockSet,
        delete: mockDelete,
      }),
    }),
  }),
}));

jest.mock("../error", () => ({
  handleError: () => {
    throw Error("test_error");
  },
}));

const mockCreateStripeCustomer = jest.fn(() => ({
  id: customer_id,
}));

const mockDeleteStripeCustomer = jest.fn();

const mockCreateStripeSubscription = jest.fn(() => ({
  id: subscription_id,
  latest_invoice: {
    payment_intent: {
      client_secret,
    },
  },
}));

const mockRetrieveStripeSubscription = jest.fn((id) => {
  return id === subscription_id
    ? {
        customer: customer_id,
        latest_invoice: { payment_intent: payment_intent },
      }
    : { customer: "other_customer_id" };
});

const mockListStripeSubscriptions = jest.fn(() => ({
  data: [
    {
      items: {
        data: [
          {
            price: {
              id: "price_enal",
              metadata: {
                subscriptionType: "enterprise",
                frequency: "annual",
                quantity: "large",
              },
            },
          },
        ],
      },
    },
  ],
  has_more: true,
}));

const mockRetrievePaymentMethod = jest.fn((id) => {
  return id === payment_method
    ? {
        customer: customer_id,
      }
    : { customer: "other_customer_id" };
});

const mockListPaymentMethods = jest.fn(() => ({
  data: [],
  has_more: true,
}));

const mockUpdatePaymentMethod = jest.fn();

const mockDeleteStripeSubscription = jest.fn();

const mockUpdateStripeSubscription = jest.fn(() => ({
  status: "past_due",
  latest_invoice: { payment_intent: { client_secret } },
}));

const mockCreateStripeRefund = jest.fn();

const mockCreateSetupIntent = jest.fn(() => ({ client_secret }));

const mockConstructEvent = jest.fn(() => ({
  type: "invoice.payment_succeeded",
  data: {
    object: {
      billing_reason: "subscription_create",
      subscription: subscription_id,
      payment_intent,
    },
  },
}));

const mockRetrievePaymentIntent = jest.fn(() => ({
  payment_method,
}));

const mockListPromotionCodes = jest.fn(({ code }) => {
  switch (code) {
    case "customer_restricted": {
      return {
        data: [
          {
            coupon: {
              percent_off: "100",
              applies_to: {
                products: [product_id],
              },
            },
            customer: "other_customer_id",
          },
        ],
      };
    }
    case "123456": {
      return {
        data: [
          {
            coupon: {
              percent_off: "100",
              applies_to: {
                products: [product_id],
              },
            },
          },
        ],
      };
    }
    default: {
      return { data: [] };
    }
  }
});

const mockRetrieveProduct = jest.fn(() => ({
  metadata: {
    subscriptionType: "education",
  },
}));

jest.mock("stripe", () => ({
  Stripe: class Stripe {
    customers: { create: jest.Mock; del: jest.Mock };
    subscriptions: {
      create: jest.Mock;
      retrieve: jest.Mock;
      update: jest.Mock;
      del: jest.Mock;
      list: jest.Mock;
    };
    refunds: {
      create: jest.Mock;
    };
    paymentMethods: {
      retrieve: jest.Mock;
      update: jest.Mock;
      list: jest.Mock;
    };
    setupIntents: {
      create: jest.Mock;
    };
    webhooks: {
      constructEvent: jest.Mock;
    };
    paymentIntents: {
      retrieve: jest.Mock;
    };
    promotionCodes: {
      list: jest.Mock;
    };
    products: {
      retrieve: jest.Mock;
    };

    constructor() {
      this.customers = {
        create: mockCreateStripeCustomer,
        del: mockDeleteStripeCustomer,
      };
      this.subscriptions = {
        create: mockCreateStripeSubscription,
        retrieve: mockRetrieveStripeSubscription,
        update: mockUpdateStripeSubscription,
        del: mockDeleteStripeSubscription,
        list: mockListStripeSubscriptions,
      };
      this.refunds = {
        create: mockCreateStripeRefund,
      };
      this.paymentMethods = {
        retrieve: mockRetrievePaymentMethod,
        update: mockUpdatePaymentMethod,
        list: mockListPaymentMethods,
      };
      this.setupIntents = {
        create: mockCreateSetupIntent,
      };
      this.webhooks = {
        constructEvent: mockConstructEvent,
      };
      this.paymentIntents = {
        retrieve: mockRetrievePaymentIntent,
      };
      this.promotionCodes = {
        list: mockListPromotionCodes,
      };
      this.products = {
        retrieve: mockRetrieveProduct,
      };
    }
  },
}));

// This must be loaded after functions have been mocked.
// eslint-disable-next-line @typescript-eslint/no-var-requires
const api = require("../index");

afterEach(() => {
  jest.clearAllMocks();
});

describe("createStripeCustomer", () => {
  it("creates a new stripe user and saves the id in firestore", async () => {
    const wrapped = testEnv.wrap(api.createStripeCustomer);

    await wrapped({ uid: mock_uid, email: mock_email });

    expect(mockCreateStripeCustomer).toHaveBeenCalledWith(
      expect.objectContaining({ email: mock_email })
    );

    expect(mockSet).toHaveBeenCalledWith({
      customer_id: customer_id,
    });
  });
});

describe("createStripeSubscription", () => {
  it("creates a new stripe subscription and returns the client secret", async () => {
    const wrapped = testEnv.wrap(api.createStripeSubscription);

    const result = await wrapped({ priceId: price_id }, context);

    expect(mockCreateStripeSubscription).toHaveBeenCalledWith(
      expect.objectContaining({
        customer: customer_id,
        items: [{ price: price_id }],
      })
    );

    expect(result).toEqual(
      expect.objectContaining({
        id: subscription_id,
        clientSecret: client_secret,
      })
    );
  });
});

describe("toggleAutomaticRenewal", () => {
  it("sets new value for cancel_at_period_end", async () => {
    const wrapped = testEnv.wrap(api.toggleAutomaticRenewal);

    await wrapped(
      { subscriptionId: subscription_id, isAutomatic: true },
      context
    );

    expect(mockUpdateStripeSubscription).toHaveBeenCalledWith(
      subscription_id,
      expect.objectContaining({ cancel_at_period_end: false })
    );
  });

  it(error_test_label, async () => {
    const wrapped = testEnv.wrap(api.toggleAutomaticRenewal);

    await expect(
      wrapped(
        { subscriptionId: "unauthorized_sub_id", isAutomatic: true },
        context
      )
    ).rejects.toThrowError();
  });
});

describe("cancelSubscription", () => {
  it("creates refund and cancels subscription", async () => {
    const wrapped = testEnv.wrap(api.cancelSubscription);

    await wrapped({ subscriptionId: subscription_id }, context);

    expect(mockCreateStripeRefund).toHaveBeenCalledWith({
      payment_intent,
    });

    expect(mockDeleteStripeSubscription).toHaveBeenCalledWith(subscription_id);
  });

  it(error_test_label, async () => {
    const wrapped = testEnv.wrap(api.cancelSubscription);

    await expect(
      wrapped({ subscriptionId: "unauthorized_sub_id" }, context)
    ).rejects.toThrowError();
  });
});

describe("updatePaymentMethod", () => {
  it("updates the payment method", async () => {
    const wrapped = testEnv.wrap(api.updatePaymentMethod);

    await wrapped(
      {
        subscriptionId: subscription_id,
        paymentMethod: payment_method,
      },
      context
    );

    expect(mockUpdateStripeSubscription).toHaveBeenCalledWith(
      subscription_id,
      expect.objectContaining({ default_payment_method: payment_method })
    );
  });

  it("returns the client secret if the subscription is past due", async () => {
    const wrapped = testEnv.wrap(api.updatePaymentMethod);

    const result = await wrapped(
      {
        subscriptionId: subscription_id,
        paymentMethod: payment_method,
      },
      context
    );

    expect(result.clientSecret).toBe(client_secret);
  });

  it(error_test_label, async () => {
    const wrapped = testEnv.wrap(api.updatePaymentMethod);

    await expect(
      wrapped({ subscriptionId: "unauthorized_sub_id" }, context)
    ).rejects.toThrowError();
  });
});

describe("updateCardInfo", () => {
  it("updated expiration date", async () => {
    const wrapped = testEnv.wrap(api.updateCardInfo);

    await wrapped(
      {
        paymentId: payment_method,
        expiration: { exp_month: 4, exp_year: 2022 },
      },
      context
    );

    expect(mockUpdatePaymentMethod).toHaveBeenCalledWith(
      payment_method,
      expect.objectContaining({ card: { exp_month: 4, exp_year: 2022 } })
    );
  });

  it(error_test_label, async () => {
    const wrapped = testEnv.wrap(api.updateCardInfo);

    await expect(
      wrapped(
        {
          paymentId: "unauthorized_pm_id",
          expiration: { exp_month: 4, exp_year: 2022 },
        },
        context
      )
    ).rejects.toThrowError();
  });
});

describe("cleanupUser", () => {
  it("deletes user from firestore and stripe", async () => {
    const wrapped = testEnv.wrap(api.cleanupUser);

    await wrapped({
      uid: mock_uid,
    });

    expect(mockDeleteStripeCustomer).toHaveBeenCalledWith(customer_id);
    expect(mockDelete).toHaveBeenCalled();
  });
});

describe("getPaymentMethods", () => {
  it("retrieves payment methods", async () => {
    const wrapped = testEnv.wrap(api.getPaymentMethods);

    const result = await wrapped(null, context);

    expect(mockListPaymentMethods).toHaveBeenCalledWith({
      customer: customer_id,
      type: "card",
    });

    expect(result).toEqual({ paymentMethods: [], has_more: true });
  });

  it("calls list payment methods using starting_after", async () => {
    const wrapped = testEnv.wrap(api.getPaymentMethods);

    await wrapped({ starting_after: payment_method }, context);

    expect(mockListPaymentMethods).toHaveBeenCalledWith({
      customer: customer_id,
      type: "card",
      starting_after: payment_method,
    });
  });
});

describe("createSetupIntent", () => {
  it("creates a setup intent and returns the secret", async () => {
    const wrapped = testEnv.wrap(api.createSetupIntent);

    const result = await wrapped(null, context);

    expect(result).toEqual({ clientSecret: client_secret });
  });
});

describe("getSubscriptions", () => {
  it("returns correct subscription info based on return value of subcriptions.list", async () => {
    const wrapped = testEnv.wrap(api.getSubscriptions);

    const result = await wrapped(null, context);

    expect(mockListStripeSubscriptions).toHaveBeenCalledWith({
      customer: customer_id,
    });

    const subscription = result.subscriptions[0];

    expect(subscription.frequency).toBe("annual");
    expect(subscription.subscriptionType).toBe("enterprise");
    expect(subscription.quantity).toBe("large");
  });

  it("passes starting_after to subscriptions.list", async () => {
    const wrapped = testEnv.wrap(api.getSubscriptions);

    await wrapped({ starting_after: subscription_id }, context);

    expect(mockListStripeSubscriptions).toHaveBeenCalledWith({
      customer: customer_id,
      starting_after: subscription_id,
    });
  });
});

describe("stripeWebhooks", () => {
  it("updates the default payment method", async () => {
    const mockRequest = {
      headers: {
        ["stripe-signature"]: "not_undefined",
      },
    };

    const mockResponse = {
      json: jest.fn(),
    };

    await api.stripeWebhooks(mockRequest, mockResponse);

    expect(mockUpdateStripeSubscription).toHaveBeenCalledWith(
      subscription_id,
      expect.objectContaining({ default_payment_method: payment_method })
    );
  });

  it("deletes the subscription", async () => {
    mockConstructEvent.mockImplementation(() => ({
      type: "invoice.payment_failed",
      data: {
        object: {
          billing_reason: "subscription_create",
          subscription: subscription_id,
          payment_intent,
        },
      },
    }));

    const mockRequest = {
      headers: {
        ["stripe-signature"]: "not_undefined",
      },
    };

    const mockResponse = {
      json: jest.fn(),
    };

    await api.stripeWebhooks(mockRequest, mockResponse);

    expect(mockDeleteStripeSubscription).toHaveBeenCalledWith(subscription_id);
  });
});

describe("calculateCost", () => {
  it("education, monthly, small", () => {
    const mockRequest = {
      subscriptionType: "education",
      frequency: "monthly",
      numberOfSubscriptions: "small",
    };

    const wrapped = testEnv.wrap(api.calculateCost);

    const result = wrapped(mockRequest);

    expect(result).toEqual({
      price: 10000,
      priceId: "price_edms",
    });
  });

  it("enterprise, annual, medium", () => {
    const mockRequest = {
      subscriptionType: "enterprise",
      frequency: "annual",
      numberOfSubscriptions: "medium",
    };

    const wrapped = testEnv.wrap(api.calculateCost);

    const result = wrapped(mockRequest);

    expect(result).toEqual({
      price: 400000,
      priceId: "price_enam",
    });
  });

  it("education, annual, large", () => {
    const mockRequest = {
      subscriptionType: "education",
      frequency: "annual",
      numberOfSubscriptions: "large",
    };

    const wrapped = testEnv.wrap(api.calculateCost);

    const result = wrapped(mockRequest);

    expect(result).toEqual({
      price: 900000,
      priceId: "price_edal",
    });
  });

  it("enterprise, monthly, unlimited", () => {
    const mockRequest = {
      subscriptionType: "enterprise",
      frequency: "monthly",
      numberOfSubscriptions: "unlimited",
    };

    const wrapped = testEnv.wrap(api.calculateCost);

    const result = wrapped(mockRequest);

    expect(result).toEqual({
      price: 2300000,
      priceId: "price_enmu",
    });
  });
});

describe("checkPromoCode", () => {
  it("returns promo code info", async () => {
    const mockRequest = {
      promoCode: "123456",
    };

    const wrapped = testEnv.wrap(api.checkPromoCode);

    const result = await wrapped(mockRequest, context);

    expect(mockListPromotionCodes).toHaveBeenCalledWith({
      active: true,
      code: "123456",
      expand: ["data.coupon.applies_to"],
    });

    expect(result).toEqual({
      couponExists: true,
      percent_off: "100",
      subscriptionTypes: ["education"],
    });
  });

  it("returns proper response when no code matches", async () => {
    const mockRequest = {
      promoCode: "badCode",
    };

    const wrapped = testEnv.wrap(api.checkPromoCode);

    const result = await wrapped(mockRequest, context);

    expect(result).toEqual({
      couponExists: false,
    });
  });

  it("returns an error for unauthorized customer", async () => {
    const mockRequest = {
      promoCode: "customer_restricted",
    };

    const wrapped = testEnv.wrap(api.checkPromoCode);

    await expect(wrapped(mockRequest, context)).rejects.toThrowError();
  });
});
