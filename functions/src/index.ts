/**
 * The following examples were referenced when creating this API.
 * https://github.com/firebase/functions-samples/tree/main/stripe
 * https://github.com/stripe-samples/subscription-use-cases
 */
"use strict";
import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import { Stripe } from "stripe";

import {
  calculateCost,
  getSubscriptions,
  getSubscriptionInvoice,
  getPaymentHistory,
  checkPromoCode,
} from "./cost";
import { handleError } from "./error";
import { NO_CUSTOMER_ERROR } from "./constants";

admin.initializeApp();
const stripe = new Stripe(functions.config().stripe.secret, {
  apiVersion: "2020-08-27",
});

exports.calculateCost = calculateCost;
exports.getSubscriptions = getSubscriptions;
exports.getSubscription = getSubscriptionInvoice;
exports.getPaymentHistory = getPaymentHistory;
exports.checkPromoCode = checkPromoCode;

/**
 * When a user is created, create a Stripe customer object for them.
 */
exports.createStripeCustomer = functions.auth.user().onCreate(async (user) => {
  try {
    const customer = await stripe.customers.create({ email: user.email });
    return await admin
      .firestore()
      .collection("stripe_customers")
      .doc(user.uid)
      .set({
        customer_id: customer.id,
      });
  } catch (error) {
    await handleError(error as Error, user.uid);
    return;
  }
});

exports.createStripeSubscription = functions.https.onCall(
  async (data, context) => {
    if (!context.auth) return;

    const { uid } = context.auth;

    const snap = await admin
      .firestore()
      .collection("stripe_customers")
      .doc(uid)
      .get();

    const customerData = snap.data();

    try {
      if (!customerData) throw Error(NO_CUSTOMER_ERROR);

      const customer = customerData.customer_id;

      const { priceId, promoCode } = data;

      const item = {
        price: priceId,
      };

      const createParams = {
        customer,
        items: [item],
        payment_behavior: "default_incomplete",
        cancel_at_period_end: true,
        automatic_tax: { enabled: true },
        expand: ["latest_invoice.payment_intent"],
      } as Stripe.SubscriptionCreateParams;

      if (promoCode) {
        const response = await stripe.promotionCodes.list({
          active: true,
          code: promoCode,
        });

        const promotionCodes = response.data;
        if (promotionCodes.length === 0) throw Error("Coupon does not exist");

        const promotionCode = promotionCodes[0];

        if (promotionCode.customer && promotionCode.customer !== customer)
          throw Error("Unauthorized promotion code.");

        createParams.coupon = promotionCode.coupon.id;
      }

      // TODO: add idempotencyKey to prevent duplicate requests. For now, rely on front end to properly handle this.
      const subscription = await stripe.subscriptions.create(createParams);

      if (
        typeof subscription.latest_invoice === "string" ||
        !subscription.latest_invoice ||
        typeof subscription.latest_invoice.payment_intent === "string" ||
        !subscription.latest_invoice.payment_intent
      ) {
        // no payment necessary
        return {
          id: subscription.id,
        };
      }

      return {
        id: subscription.id,
        clientSecret: subscription.latest_invoice.payment_intent.client_secret,
      };
    } catch (error) {
      await handleError(error as Error, uid);
      return;
    }
  }
);

exports.toggleAutomaticRenewal = functions.https.onCall(
  async (data, context) => {
    if (!context.auth) return;
    const { uid } = context.auth;

    const snap = await admin
      .firestore()
      .collection("stripe_customers")
      .doc(uid)
      .get();

    const customerData = snap.data();

    const { subscriptionId, isAutomatic } = data;

    try {
      if (!customerData) throw Error(NO_CUSTOMER_ERROR);

      const customer = customerData.customer_id;

      const subscription = await stripe.subscriptions.retrieve(subscriptionId);

      if (subscription.customer !== customer) throw Error("unauthorized");

      await stripe.subscriptions.update(subscriptionId, {
        cancel_at_period_end: !isAutomatic,
      });
    } catch (error) {
      await handleError(error as Error, uid);
      return;
    }
  }
);

exports.cancelSubscription = functions.https.onCall(async (data, context) => {
  if (!context.auth) return;
  const { uid } = context.auth;

  const snap = await admin
    .firestore()
    .collection("stripe_customers")
    .doc(uid)
    .get();

  const customerData = snap.data();

  const { subscriptionId } = data;

  try {
    if (!customerData) throw Error(NO_CUSTOMER_ERROR);

    const customer = customerData.customer_id;

    const subscription = await stripe.subscriptions.retrieve(subscriptionId, {
      expand: ["latest_invoice"],
    });

    if (subscription.customer !== customer) throw Error("unauthorized");

    if (
      typeof subscription.latest_invoice === "string" ||
      typeof subscription.latest_invoice === null ||
      typeof subscription.latest_invoice?.payment_intent !== "string"
    )
      throw Error("payment intent does not exist");

    await stripe.refunds.create({
      payment_intent: subscription.latest_invoice.payment_intent,
    });
    await stripe.subscriptions.del(subscriptionId);
  } catch (error) {
    await handleError(error as Error, uid);
    return;
  }
});

exports.updatePaymentMethod = functions.https.onCall(async (data, context) => {
  if (!context.auth) return;
  const { uid } = context.auth;

  const snap = await admin
    .firestore()
    .collection("stripe_customers")
    .doc(uid)
    .get();

  const customerData = snap.data();

  const { paymentMethod, subscriptionId } = data;

  try {
    if (!customerData) throw Error(NO_CUSTOMER_ERROR);

    const customer = customerData.customer_id;

    const subscription = await stripe.subscriptions.retrieve(subscriptionId);

    if (subscription.customer !== customer) throw Error("unauthorized");

    const subscriptionDetails = await stripe.subscriptions.update(
      subscriptionId,
      {
        default_payment_method: paymentMethod,
        expand: ["latest_invoice.payment_intent"],
      }
    );

    // If subscription is past due, then update will return the client secret so that the front can confirm the payment.
    if (subscriptionDetails.status === "past_due") {
      if (
        typeof subscriptionDetails.latest_invoice === "string" ||
        !subscriptionDetails.latest_invoice ||
        typeof subscriptionDetails.latest_invoice.payment_intent === "string" ||
        !subscriptionDetails.latest_invoice.payment_intent
      ) {
        throw new Error("No client secret exists.");
      }

      return {
        clientSecret:
          subscriptionDetails.latest_invoice.payment_intent.client_secret,
      };
    }

    return;
  } catch (error) {
    await handleError(error as Error, uid);
    return;
  }
});

type Updates = {
  card?: {
    exp_month: number;
    exp_year: number;
  };
  billing_details?: {
    name?: string;
  };
};

exports.updateCardInfo = functions.https.onCall(async (data, context) => {
  if (!context.auth) return;
  const { uid } = context.auth;

  const snap = await admin
    .firestore()
    .collection("stripe_customers")
    .doc(uid)
    .get();

  const customerData = snap.data();

  const { paymentId, expiration, billingDetails } = data;

  const updates = {} as Updates;
  if (expiration) updates.card = expiration;
  if (billingDetails) updates.billing_details = billingDetails;

  try {
    if (!customerData) throw Error(NO_CUSTOMER_ERROR);

    const customer = customerData.customer_id;

    const paymentInfo = await stripe.paymentMethods.retrieve(paymentId);

    if (paymentInfo.customer !== customer) throw Error("unauthorized");

    await stripe.paymentMethods.update(paymentId, updates);
  } catch (error) {
    await handleError(error as Error, uid);
    return;
  }
});

/**
 * When a user deletes their account, clean up after them
 */
exports.cleanupUser = functions.auth.user().onDelete(async (user) => {
  try {
    const dbRef = admin.firestore().collection("stripe_customers");
    const customer = (await dbRef.doc(user.uid).get()).data();

    if (!customer) {
      throw new Error(NO_CUSTOMER_ERROR);
    }
    await stripe.customers.del(customer.customer_id);
    return await dbRef.doc(user.uid).delete();
  } catch (error) {
    await handleError(error as Error, user.uid);
    return;
  }
});

exports.updateCustomerAddress = functions.https.onCall(
  async (data, context) => {
    if (!context.auth) return;

    const { uid } = context.auth;

    const snap = await admin
      .firestore()
      .collection("stripe_customers")
      .doc(uid)
      .get();

    const customerData = snap.data();

    const { address } = data;

    try {
      if (!customerData) throw Error(NO_CUSTOMER_ERROR);

      const { customer_id: customer } = customerData;

      await stripe.customers.update(customer, { address });
    } catch (error) {
      await handleError(error as Error, uid);
      return;
    }
  }
);

interface ListPaymentMethods {
  customer: string;
  type: "card";
  starting_after?: string;
}

exports.getPaymentMethods = functions.https.onCall(async (data, context) => {
  if (!context.auth) return;

  const { uid } = context.auth;

  const snap = await admin
    .firestore()
    .collection("stripe_customers")
    .doc(uid)
    .get();

  const customerData = snap.data();

  try {
    if (!customerData) throw Error(NO_CUSTOMER_ERROR);

    const { customer_id: customer } = customerData;

    const requestBody = data
      ? {
          customer,
          type: "card",
          starting_after: data.starting_after,
        }
      : { customer, type: "card" };

    const paymentMethods = await stripe.paymentMethods.list(
      requestBody as ListPaymentMethods
    );

    return {
      paymentMethods: paymentMethods.data,
      has_more: paymentMethods.has_more,
    };
  } catch (error) {
    await handleError(error as Error, uid);
    return;
  }
});

exports.createSetupIntent = functions.https.onCall(async (data, context) => {
  if (!context.auth) return;

  const { uid } = context.auth;

  const snap = await admin
    .firestore()
    .collection("stripe_customers")
    .doc(uid)
    .get();

  const customerData = snap.data();

  try {
    if (!customerData) throw Error(NO_CUSTOMER_ERROR);

    const { customer_id: customer } = customerData;

    const setupIntent = await stripe.setupIntents.create({
      payment_method_types: ["card"],
      customer,
    });

    return { clientSecret: setupIntent.client_secret };
  } catch (error) {
    await handleError(error as Error, uid);
    return;
  }
});

interface WebhookObj {
  billing_reason: string;
  subscription: string;
  payment_intent: string;
}

exports.stripeWebhooks = functions.https.onRequest(
  async (request, response) => {
    const endpointSecret = functions.config().stripe.signing;

    // Get the signature from the request header
    const sig = request.headers["stripe-signature"];

    if (sig === undefined) {
      return response.status(400).end();
    }

    try {
      // Verify the request against our endpointSecret
      const event = stripe.webhooks.constructEvent(
        request.rawBody,
        sig,
        endpointSecret
      );

      const dataObject = event.data.object as WebhookObj;

      // eslint-disable-next-line sonarjs/no-small-switch
      switch (event.type) {
        case "invoice.payment_succeeded":
          if (dataObject.billing_reason == "subscription_create") {
            // The subscription automatically activates after successful payment
            // Set the payment method used to pay the first invoice
            // as the default payment method for that subscription
            const subscription_id = dataObject.subscription;
            const payment_intent_id = dataObject.payment_intent;

            // Retrieve the payment intent used to pay the subscription
            const payment_intent = await stripe.paymentIntents.retrieve(
              payment_intent_id
            );

            await stripe.subscriptions.update(subscription_id, {
              default_payment_method: payment_intent.payment_method as string,
            });

            console.log(
              "Default payment method set for subscription:" +
                payment_intent.payment_method
            );
          }

          break;
        case "invoice.payment_failed":
          if (dataObject.billing_reason === "subscription_create") {
            // Failed on subscription creation, so no reason to keep the subscription - delete it.

            const subscription_id = dataObject.subscription;

            await stripe.subscriptions.del(subscription_id);

            console.log("Failed subscription creation was cancelled.");
          }

        default:
      }
    } catch (err) {
      return response.status(400).end();
    }

    response.json({ received: true });
  }
);
