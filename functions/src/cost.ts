import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import { Stripe } from "stripe";

import { handleError } from "./error";
import { NO_CUSTOMER_ERROR } from "./constants";

const stripe = new Stripe(functions.config().stripe.secret, {
  apiVersion: "2020-08-27",
});

enum SubscriptionType {
  Education = "education",
  Enterprise = "enterprise",
}

enum Frequency {
  Monthly = "monthly",
  Annual = "annual",
}

enum Quantity {
  Small = "small",
  Medium = "medium",
  Large = "large",
  Unlimited = "unlimited",
}
interface RequestData {
  subscriptionType: SubscriptionType;
  frequency: Frequency;
  numberOfSubscriptions: Quantity;
}

const priceEnvVars = functions.config().stripe.price;

const stripePriceIds = {
  [SubscriptionType.Education]: {
    [Frequency.Monthly]: {
      [Quantity.Small]: priceEnvVars["edu-monthly-small"],
      [Quantity.Medium]: priceEnvVars["edu-monthly-medium"],
      [Quantity.Large]: priceEnvVars["edu-monthly-large"],
      [Quantity.Unlimited]: priceEnvVars["edu-monthly-unlimited"],
    },
    [Frequency.Annual]: {
      [Quantity.Small]: priceEnvVars["edu-annual-small"],
      [Quantity.Medium]: priceEnvVars["edu-annual-medium"],
      [Quantity.Large]: priceEnvVars["edu-annual-large"],
      [Quantity.Unlimited]: priceEnvVars["edu-annual-unlimited"],
    },
  },
  [SubscriptionType.Enterprise]: {
    [Frequency.Monthly]: {
      [Quantity.Small]: priceEnvVars["ent-monthly-small"],
      [Quantity.Medium]: priceEnvVars["ent-monthly-medium"],
      [Quantity.Large]: priceEnvVars["ent-monthly-large"],
      [Quantity.Unlimited]: priceEnvVars["ent-monthly-unlimited"],
    },
    [Frequency.Annual]: {
      [Quantity.Small]: priceEnvVars["ent-annual-small"],
      [Quantity.Medium]: priceEnvVars["ent-annual-medium"],
      [Quantity.Large]: priceEnvVars["ent-annual-large"],
      [Quantity.Unlimited]: priceEnvVars["ent-annual-unlimited"],
    },
  },
};

const pricing = {
  [SubscriptionType.Education]: {
    [Frequency.Monthly]: {
      [Quantity.Small]: 10000,
      [Quantity.Medium]: 20000,
      [Quantity.Large]: 90000,
      [Quantity.Unlimited]: 230000,
    },
    [Frequency.Annual]: {
      [Quantity.Small]: 100000,
      [Quantity.Medium]: 200000,
      [Quantity.Large]: 900000,
      [Quantity.Unlimited]: 2750000,
    },
  },
  [SubscriptionType.Enterprise]: {
    [Frequency.Monthly]: {
      [Quantity.Small]: 20000,
      [Quantity.Medium]: 40000,
      [Quantity.Large]: 180000,
      [Quantity.Unlimited]: 2300000,
    },
    [Frequency.Annual]: {
      [Quantity.Small]: 200000,
      [Quantity.Medium]: 400000,
      [Quantity.Large]: 1800000,
      [Quantity.Unlimited]: 27500000,
    },
  },
};

export const calculateCost = functions.https.onCall((data) => {
  const { subscriptionType, frequency, numberOfSubscriptions }: RequestData =
    data;

  const price = pricing[subscriptionType][frequency][numberOfSubscriptions];
  const priceId =
    stripePriceIds[subscriptionType][frequency][numberOfSubscriptions];

  return {
    price,
    priceId,
  };
});

export const getSubscriptions = functions.https.onCall(
  async (data, context) => {
    if (!context.auth) return;

    const { uid } = context.auth;

    const snap = await admin
      .firestore()
      .collection("stripe_customers")
      .doc(uid)
      .get();

    const customerData = snap.data();

    try {
      if (!customerData) throw Error(NO_CUSTOMER_ERROR);

      const { customer_id: customer } = customerData;

      const requestBody = data
        ? {
            customer,
            starting_after: data.starting_after,
          }
        : { customer };

      const subscriptions = await stripe.subscriptions.list(requestBody);

      const { data: allData, has_more } = subscriptions;

      const formattedSubscriptions = allData.map((subscription) => {
        const price = subscription.items.data[0].price;

        const { frequency, subscriptionType, quantity } = price.metadata;

        return {
          cancel_at: subscription.cancel_at,
          cancel_at_period_end: subscription.cancel_at_period_end,
          canceled_at: subscription.canceled_at,
          created: subscription.created,
          current_period_end: subscription.current_period_end,
          current_period_start: subscription.current_period_start,
          default_payment_method: subscription.default_payment_method,
          ended_at: subscription.ended_at,
          id: subscription.id,
          start_date: subscription.start_date,
          status: subscription.status,
          frequency,
          subscriptionType,
          quantity,
        };
      });

      return {
        subscriptions: formattedSubscriptions,
        has_more,
      };
    } catch (error) {
      await handleError(error as Error, uid);
      return;
    }
  }
);

export const getSubscriptionInvoice = functions.https.onCall(
  async (data, context) => {
    if (!context.auth) return;

    const { uid } = context.auth;

    const snap = await admin
      .firestore()
      .collection("stripe_customers")
      .doc(uid)
      .get();

    const customerData = snap.data();

    const { subscriptionId } = data;

    try {
      if (!customerData) throw Error(NO_CUSTOMER_ERROR);

      const { customer_id: customer } = customerData;

      const subscription = await stripe.subscriptions.retrieve(subscriptionId, {
        expand: ["latest_invoice.payment_intent", "default_payment_method"],
      });

      if (subscription.customer !== customer) throw Error("unauthorized");
      if (
        !subscription.latest_invoice ||
        typeof subscription.latest_invoice === "string"
      )
        throw Error("invoice does not exist");

      if (typeof subscription.latest_invoice.payment_intent === "string")
        throw Error("payment intent expand error");

      const price = subscription.items.data[0].price;
      const { frequency, subscriptionType, quantity } = price.metadata;

      return {
        subscriptionType,
        frequency,
        quantity,
        paymentMethod: subscription.default_payment_method,
        invoice_pdf: subscription.latest_invoice.invoice_pdf,
        invoice_id: subscription.latest_invoice.id,
        invoice_number: subscription.latest_invoice.number,
        cancel_at_period_end: subscription.cancel_at_period_end,
        current_period_end: subscription.current_period_end,
        cancel_at: subscription.cancel_at,
        subscriptionStatus: subscription.status,
        paymentId: subscription.latest_invoice.payment_intent?.id,
        status: subscription.latest_invoice.payment_intent?.status,
        status_transitions: subscription.latest_invoice.status_transitions,
        amount: subscription.latest_invoice.payment_intent?.amount,
      };
    } catch (error) {
      await handleError(error as Error, uid);
      return;
    }
  }
);

export const getPaymentHistory = functions.https.onCall(
  async (data, context) => {
    if (!context.auth) return;

    const { uid } = context.auth;

    const snap = await admin
      .firestore()
      .collection("stripe_customers")
      .doc(uid)
      .get();

    const customerData = snap.data();

    try {
      if (!customerData) throw Error(NO_CUSTOMER_ERROR);

      const { customer_id: customer } = customerData;

      const requestBody = data
        ? {
            customer,
            starting_after: data.starting_after,
            expand: ["data.subscription", "data.payment_intent.payment_method"],
          }
        : {
            customer,
            expand: ["data.subscription", "data.payment_intent.payment_method"],
          };

      const response = await stripe.invoices.list(requestBody);

      const { data: invoices, has_more } = response;

      const formattedInvoices = invoices.map((invoice) => {
        if (
          !invoice.subscription ||
          !invoice.payment_intent ||
          typeof invoice.subscription === "string" ||
          typeof invoice.payment_intent === "string"
        )
          return null;

        const price = invoice.subscription.items.data[0].price;
        const { frequency, subscriptionType, quantity } = price.metadata;

        return {
          paymentId: invoice.payment_intent.id,
          status: invoice.payment_intent.status,
          status_transitions: invoice.status_transitions,
          amount: invoice.payment_intent.amount,
          subscriptionType,
          frequency,
          quantity,
          paymentMethod: invoice.payment_intent.payment_method,
          invoice_pdf: invoice.invoice_pdf,
          invoice_id: invoice.id,
          invoice_number: invoice.number,
          cancel_at_period_end: invoice.subscription.cancel_at_period_end,
          current_period_end: invoice.subscription.current_period_end,
          cancel_at: invoice.subscription.cancel_at,
          subscriptionStatus: invoice.subscription.status,
        };
      });

      return {
        invoices: formattedInvoices.filter((invoice) => !!invoice),
        has_more,
        next: invoices[invoices.length - 1].id,
      };
    } catch (error) {
      await handleError(error as Error, uid);
      return;
    }
  }
);

// Gets promotion code data to be used before checkout.
// Not all cases are handled. Please consider modifying this function if more complex coupons are needed.
export const checkPromoCode = functions.https.onCall(async (data, context) => {
  if (!context.auth) return;

  const { uid } = context.auth;

  const snap = await admin
    .firestore()
    .collection("stripe_customers")
    .doc(uid)
    .get();

  const customerData = snap.data();

  try {
    if (!customerData) throw Error(NO_CUSTOMER_ERROR);

    const { customer_id: customer } = customerData;

    const { promoCode } = data;

    const response = await stripe.promotionCodes.list({
      active: true,
      code: promoCode,
      expand: ["data.coupon.applies_to"],
    });

    const promotionCodes = response.data;

    if (promotionCodes.length === 0)
      return {
        couponExists: false,
      };

    const promotionCode = promotionCodes[0];

    if (promotionCode.customer && promotionCode.customer !== customer)
      throw Error("Unauthorized promotion code.");

    const { coupon } = promotionCode;
    const { amount_off, percent_off, applies_to } = coupon;

    const subscriptionTypes = applies_to
      ? await Promise.all(
          applies_to.products.map(async (productId) => {
            const product = await stripe.products.retrieve(productId);
            return product.metadata.subscriptionType;
          })
        )
      : [SubscriptionType.Education, SubscriptionType.Enterprise];

    return {
      couponExists: true,
      amount_off,
      percent_off,
      subscriptionTypes,
    };
  } catch (error) {
    await handleError(error as Error, uid);
    return;
  }
});
