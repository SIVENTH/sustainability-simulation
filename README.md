# Sustainability Simulation

This is MVP that allows users to perform simulations that has a set of questions who answers contain specific score. This MVP has started with tea industry and is aimed to target other
industries as well.

## Technologies

Front: React, TypeScript, React-Hook-Form, ChakraUI, Victory, JSPDF
Backend: Firebase Auth and Firestore 

# or with yarn

yarn add

````
## Available commands
Check out all available commands in package.json. You can run all tests as follows:
```bash
npm test
# or with yarn
yarn test
````

## Install all deps

Clone the app and install all deps

```bash
npm i

# or with yarn
yarn install
```

## Start the app

```bash
npm start

# or with yarn
yarn start
```

## Env variables

All keys are stored in .env file which is not commited to the repo. Please contact the owner of
the repo to get all the required keys.

## Project structures

To understand the sequence of actions in this app, follow files in this order:

-components => pages => App.js => index.js

-components: contain low level operations
-pages: higher level operations 
-App.js: declares all the routes for the app
-index.js: contains additional app configurations

To understand interplay between pages and components, start reviewing the pages which contain all essential higher level app logic.
Notable mentions: gateway directory houses all firebase related configuration and APIs. It follows object oriented composition approach. Model directory contains all score calculation related logic.

## Score Calculation

The core logic of the simulation calculation can be found under model directory. The model directory has a series of classes that follow object oriented programming paradigm. For example, TeaScoreCalculation class inherits from ScoreCalculation and delegates mapping of the individual answer scores to the TeaModel so that in the future developers can easily create a AutoScoreCalculation class that works with an Automotive
model for score mapping.

## Simulation Page 
Simulation page is probably most complex component in this app. It paginates 
questions with the help of usePaginate custom hook, renders results and suggestions, stores results and does calculations on the fly and submit selected answers to backend. It also contains Question component which is responsible for rendering questions and options. This Question component contains a factory function that renders Dropdown, Regular Radio and Radio Images.

## Form component 
Form directory in the components contains Form component and related form inputs. It follows a flexible-compound-pattern. You can learn more about this particular pattern in my open source lib [https://github.com/alisherk/react-design-patterns-ts/tree/master/src/patterns]. Basically you can render form inputs that has validation anywhere in the app as long as they are wrapped inside the form. You can see this pattern in action in the Simulation Page. Please note Dropdown, Radio, and RadioImage components within this directory are specificly designed to work inside the Simulation Page => Question component as they are responsible for fetching submitted options from backend. They can not be really used anywhere else. Also, the form components implement react-hook-form API for validation. You can create more generic inputs that will have valiation out of the box as long as you pass specific validation rules. Take a look at Input and Select components and follow the pattern there. They can be used anythere in the app as long as you pass required props. 

